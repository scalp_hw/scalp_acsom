#!/usr/bin/python3

################################################################
#                     _             _
#                    | |_  ___ _ __(_)__ _
#                    | ' \/ -_) '_ \ / _` |
#                    |_||_\___| .__/_\__,_|
#                             |_|
#
################################################################
#
# Company: hepia
# Engineer: Quentin Berthet <quentin.berthet@hes-so.ch>
#
# Project Name: ACSOM
# Version: 0.1
#
# File: run_gen_input.py
# Description: run generation of random input vector for ACSOM
#              simulation
#
# Last update: 2021/11/16 11:42:24
#
#################################################################

from pathlib import Path
import simfloat as sf
import random

def main():
    # Input population config ##########################################################
    param_wE = 5
    param_wF = 4
    param_comp_count = [3]
    param_input_count = 1000

    cfg_rootpath = "../.."

    # Build simfloat context
    context = sf.define_context(param_wE, param_wF, sf.ROUND_DOWN)

    # Generate test population
    for comp_count in param_comp_count:
        path = f"{cfg_rootpath}/data/run_input/"
        Path(path).mkdir(parents=True, exist_ok=True)
        filename = f"{path}/input_{comp_count}.txt"
        with open(filename,"w+") as file:
            print(f"Generating {param_input_count} inputs of {comp_count} components in {param_wE}.{param_wF} format: {filename}")
            for i in range (param_input_count):
                vec = []
                quadrant = random.uniform(0.0, 1.0)
                file.write(f"{i}")
                for comp in range(comp_count):
                    py_rand_value = random.uniform(0.0, 1.0)
                    if quadrant < 0.5:
                        py_rand_value = py_rand_value * 0.5
                    else:
                        py_rand_value = py_rand_value * 0.5 + 0.5
                    # Quantization to representable format
                    fp_rand_value = context(py_rand_value)
                    vec.append(float(fp_rand_value.as_decimal()))
                    file.write(f" {float(fp_rand_value.as_decimal())}")
                file.write(f"\n")

if __name__ == '__main__':
    main()