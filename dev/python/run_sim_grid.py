#!/usr/bin/python3

################################################################
#                     _             _
#                    | |_  ___ _ __(_)__ _
#                    | ' \/ -_) '_ \ / _` |
#                    |_||_\___| .__/_\__,_|
#                             |_|
#
################################################################
#
# Company: hepia
# Engineer: Quentin Berthet <quentin.berthet@hes-so.ch>
#
# Project Name: ACSOM
# Version: 0.1
#
# File: run_sim_grid.py
# Description: Run ACSOM grid simulations
#
# Last update: 2021/11/16 14:17:15
#
#################################################################

import subprocess
from pathlib import Path
import shutil
from os import listdir
from os.path import isfile, join

# Runs parameters #############################################################

default_runs = [
    {
        "fp_repr": [(5, 4)],
        "alpha": [0.25],
        "eta": [0.3],
        "comp_count": [3],
        "grid_size": [(4, 4, 2), (4, 4, 3), (4, 4, 4)],
    },
    {
        "fp_repr": [(6, 8)],
        "alpha": [0.3],
        "eta": [0.1],
        "comp_count": [3],
        "grid_size": [(4, 4, 2), (4, 4, 3), (4, 4, 4)],
    },
    {
        "fp_repr": [(6, 16)],
        "alpha": [0.3],
        "eta": [3.0],
        "comp_count": [3],
        "grid_size": [(4, 4, 2), (4, 4, 3), (4, 4, 4)],
    },
    {
        "fp_repr": [(8, 23)],
        "alpha": [0.3],
        "eta": [3.0],
        "comp_count": [3],
        "grid_size": [(4, 4, 2), (4, 4, 3), (4, 4, 4)],
    },
]

# Parameter used for alpa,eta exploration:
#param_grid_size = [(4, 4, 2)]
#param_comp_count = [3]
#param_fp_repr = [(6, 8), (8, 23)]
#param_alpha = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3]
# param_eta = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.9,
#             1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0,
#             10.0, 15.0, 20.0]

# Run configuration ###########################################################

cfg_do_synth = False
cfg_do_sim = True
cfg_rootpath = "../.."

# Helper functions ############################################################

def setup_flopoco(env):
    print(f"-> Setting up flopoco environnement")
    cwd = f"{env['rootpath']}/dev/fpga/ips/hw"
    command = f"./set_flopoco_{env['wE']}_{env['wF']}.sh"
    p = subprocess.Popen([command], cwd=cwd)
    p.wait()


def setup_grid_size(env):
    print(f"-> Setting up csom_pkg")
    cwd = f"{env['rootpath']}/dev/fpga/packages/hw"
    command = f"./set_csom_pkg_{env['x_size']}x{env['y_size']}x{env['z_size']}_{env['comp_count']}.sh"
    p = subprocess.Popen([command], cwd=cwd)
    p.wait()


def setup_vivado_project(env):
    print(f"-> Cleaning up vivado project")
    cwd = f"{env['rootpath']}/dev/fpga/peripherals/vivado/csom_grid_axi_wrapper/2020.2/lin64/.scripts/"
    command = f"./clean_prj_csom_grid_axi_wrapper.sh"
    p = subprocess.Popen([command], cwd=cwd)
    p.wait()
    print(f"-> setting up vivado project")
    cwd = f"{env['rootpath']}/dev/fpga/peripherals/vivado/csom_grid_axi_wrapper/2020.2/lin64/.scripts/"
    command = f"./create_prj_csom_grid_axi_wrapper.sh"
    p = subprocess.Popen([command], cwd=cwd)
    p.wait()


def run_synth(env):
    print(f"-> Running synthesis")
    cwd = f"{env['rootpath']}/dev/fpga/peripherals/vivado/csom_grid_axi_wrapper/2020.2/lin64/.scripts/"
    command = f"./run_synth_csom_grid_axi_wrapper.sh"
    p = subprocess.Popen([command], cwd=cwd)
    p.wait()
    shutil.copy(f"{env['rootpath']}/dev/fpga/peripherals/vivado/csom_grid_axi_wrapper/2020.2/lin64/csom_grid_axi_wrapper/csom_grid_axi_wrapper.runs/synth_1/csom_grid_axi_wrapper_utilization_synth.rpt",
                f"{env['rootpath']}/{env['output_path']}/"
                )
    shutil.copy(f"{env['rootpath']}/dev/fpga/peripherals/vivado/csom_grid_axi_wrapper/2020.2/lin64/csom_grid_axi_wrapper/csom_grid_axi_wrapper.runs/synth_1/runme.log",
                f"{env['rootpath']}/{env['output_path']}/"
                )


def run_sim(env):
    print(f"-> Running simulation")
    cwd = f"{env['rootpath']}/dev/fpga/peripherals/vivado/csom_grid_axi_wrapper/2020.2/lin64/.scripts/"
    command = f"./run_sim_csom_grid_axi_wrapper.sh"
    initpath = f"{env['rootpath']}/data/run_input/grid_{env['x_size']}x{env['y_size']}x{env['z_size']}_{env['comp_count']}/"
    initfiles = [f for f in listdir(initpath) if isfile(join(initpath, f))]
    for initfile in initfiles:
        idx = initfile.split('_')[0]
        env['dump_file'] = f"./data/run_output/grid_{env['x_size']}x{env['y_size']}x{env['z_size']}_{env['comp_count']}_{env['wE']}.{env['wF']}/{idx}_dump_{env['alpha']}_{env['eta']}.txt"
        env['init_file'] = f"./data/run_input/grid_{env['x_size']}x{env['y_size']}x{env['z_size']}_{env['comp_count']}/{initfile}"
        env['input_file'] = f"./data/run_input/input_{env['comp_count']}.txt"
        p = subprocess.Popen(
            [command, f"{env['alpha']}", f"{env['eta']}", f"{env['dump_file']}", f"{env['init_file']}", f"{env['input_file']}"], cwd=cwd)
        p.wait()

# Main loop ###################################################################

def main(runs):
    env = {}
    env['rootpath'] = cfg_rootpath
    for run in runs:
        param_fp_repr = run["fp_repr"]
        param_alpha = run["alpha"]
        param_eta = run["eta"]
        param_comp_count = run["comp_count"]
        param_grid_size = run["grid_size"]
        for (env['x_size'], env['y_size'], env['z_size']) in param_grid_size:
            for env['comp_count'] in param_comp_count:
                for (env['wE'], env['wF']) in param_fp_repr:
                    print(
                        f"Setting environnement for {env['x_size']}x{env['y_size']}x{env['z_size']} grid with {env['comp_count']} vector components, format {env['wE']}.{env['wF']}")
                    setup_grid_size(env)
                    setup_flopoco(env)
                    setup_vivado_project(env)
                    env["output_path"] = f"./data/run_output/grid_{env['x_size']}x{env['y_size']}x{env['z_size']}_{env['comp_count']}_{env['wE']}.{env['wF']}"
                    Path(f"{env['rootpath']}/{env['output_path']}").mkdir(parents=True,
                                                                        exist_ok=True)
                    if cfg_do_synth:
                        run_synth(env)
                    if cfg_do_sim:
                        for env['eta'] in param_eta:
                            for env['alpha'] in param_alpha:
                                print(
                                    f"Launching simulation with a {env['x_size']}x{env['y_size']}x{env['z_size']} grid, {env['comp_count']} vector components, alpha={env['alpha']}, eta={env['eta']}, format {env['wE']}.{env['wF']}")
                                run_sim(env)

if __name__ == '__main__':
    main(default_runs)