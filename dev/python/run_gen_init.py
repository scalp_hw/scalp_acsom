#!/usr/bin/python3

################################################################
#                     _             _
#                    | |_  ___ _ __(_)__ _
#                    | ' \/ -_) '_ \ / _` |
#                    |_||_\___| .__/_\__,_|
#                             |_|
#
################################################################
#
# Company: hepia
# Engineer: Quentin Berthet <quentin.berthet@hes-so.ch>
#
# Project Name: ACSOM
# Version: 0.1
#
# File: run_gen_init.py
# Description: run generation of initial unit positions for 
#              ACSOM simulation
#
# Last update: 2021/11/16 11:42:01
#
#################################################################

from pathlib import Path
import simfloat as sf
import random

def main():
    # Init values config ##########################################################
    param_wE = 5
    param_wF = 4
    param_grid_size = [(4, 4, 2), (4, 4, 3), (4, 4, 4)]
    param_comp_count = [3]
    param_init_file_count = 10

    cfg_rootpath = "../.."

    # Build simfloat context
    context = sf.define_context(param_wE, param_wF, sf.ROUND_DOWN)

    # Generate units initial state
    for grid_size in param_grid_size:
        (x_size, y_size, z_size) = grid_size
        for comp_count in param_comp_count:
            for filenumber in range(param_init_file_count):
                path = f"{cfg_rootpath}/data/run_input/grid_{x_size}x{y_size}x{z_size}_{comp_count}/"
                Path(path).mkdir(parents=True, exist_ok=True)
                filename = f"{path}/{filenumber+1}_init_{x_size}x{y_size}x{z_size}.{comp_count}_{param_wE}.{param_wF}.txt"
                print(
                    f"Generating initial states n°{filenumber+1} in {param_wE}.{param_wF} format for grid {grid_size} with vector of {comp_count} components: {filename}")
                with open(filename, "w+") as file:
                    for x in range(x_size):
                        for y in range(y_size):
                            for z in range(z_size):
                                vec = []
                                file.write(f"{x} {y} {z}")
                                for comp in range(comp_count):
                                    py_rand_value = random.uniform(0.0, 1.0)
                                    # Quantization to representable format
                                    fp_rand_value = context(py_rand_value)
                                    vec.append(float(fp_rand_value.as_decimal()))
                                    file.write(
                                        f" {float(fp_rand_value.as_decimal())}")
                                file.write(f"\n")

if __name__ == '__main__':
    main()