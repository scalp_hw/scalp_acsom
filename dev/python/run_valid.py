#!/usr/bin/python3

################################################################
#                     _             _
#                    | |_  ___ _ __(_)__ _
#                    | ' \/ -_) '_ \ / _` |
#                    |_||_\___| .__/_\__,_|
#                             |_|
#
################################################################
#
# Company: hepia
# Engineer: Quentin Berthet <quentin.berthet@hes-so.ch>
#
# Project Name: ACSOM
# Version: 0.1
#
# File: run_validation.py
# Description: Run validation of run_output to compute ASE
#
# Last update: 2021/11/16 11:39:09
#
#################################################################

import pprint
import subprocess
from pathlib import Path
import shutil
from os import listdir
from os.path import isfile, join
from postprocess.testdata import TestData
import pandas as pd
import plotly.express as px
# Run parameters ##############################################################

param_comp_count = [3]

# Path configuration ##########################################################

rootpath = "../../"
inputpath = "data/run_input/"
outputpath = "data/run_output/"

# Runs parsing ################################################################

def main():
    runs = []
    for testpath in Path(f"{rootpath}{outputpath}").rglob('grid_*'):
        run = {}
        run['path'] = str(testpath)
        (_, run['grid_size'], run['comp_count'],
        run['fp_repr']) = testpath.name.split('_')
        run['comp_count'] = int(run['comp_count'])
        run['tests'] = {}
        for runpath in Path(testpath).rglob('*dump*.txt'):
            (idx, _, alpha, eta) = runpath.name.split('_')
            eta = eta[:-4]  # remove '.txt' from eta
            param = f"{alpha}_{eta}"
            if param not in run['tests'].keys():
                run['tests'][param] = []
            run['tests'][param].append(idx)
        runs.append(run)

    # Debug
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(runs)

    # Loop over test results
    with open(f"{rootpath}{outputpath}/ase.csv", 'w') as f:
        f.write('grid_size,fp_repr,ase\n')
        for run in runs:
            if run['comp_count'] == param_comp_count[0]:
                for param in run['tests']:
                    # Loop over init conditions
                    ase_acc = 0.0
                    for idx in run['tests'][param]:
                        dumpfile = f"{run['path']}/{idx}_dump_{param}.txt"
                        data = TestData(dumpfile)
                        ase = data.do_validation(f"{rootpath}/{inputpath}/valid_{param_comp_count[0]}.txt")
                        ase_acc = ase_acc + ase
                    run['ase'] = ase_acc / len(run['tests'][param])
                    print(f"{run['grid_size']}, {run['fp_repr']}, alpha_eta={param} average ASE over {len(run['tests'][param])} runs: {run['ase']}")
                    f.write(f"{run['grid_size']}, {str(run['fp_repr']).replace('.','_')}, {run['ase']}\n")

    exit()

    df = pd.read_csv(f"{rootpath}{outputpath}/ase.csv")
    print(df)
    fig = px.bar(df,
                x="grid_size",
                y="ase",
                color='fp_repr',
                barmode='group')
    fig.show()

if __name__ == '__main__':
    main()