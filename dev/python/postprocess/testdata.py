#!/usr/bin/python3

################################################################
#                     _             _
#                    | |_  ___ _ __(_)__ _
#                    | ' \/ -_) '_ \ / _` |
#                    |_||_\___| .__/_\__,_|
#                             |_|
#
################################################################
#
# Company: hepia
# Engineer: Quentin Berthet <quentin.berthet@hes-so.ch>
#
# Project Name: ACSOM
# Version: 0.1
#
# File: testdata.py
# Description: Helper function to process ACSOM test outputs
#
# Last update: 2021/11/16 14:36:30
#
#################################################################

import numpy as np
import sys

class TestData:
    """Parsing and handling of ACSOM test data"""

    def __init__(self, filename = None):
        self.header_fields = {}
        self.inputs = []
        self.validation_inputs = []
        self.grid_states = []
        self.bmu_weights = []
        self.bmu_addr = []
        self.filename = None
        if filename != None:
            self.filename = filename
            self.parse_file(filename)

    def clear(self):
        self.header_fields = {}
        self.inputs = []
        self.grid_states = []
        self.bmu_weights = []
        self.bmu_addr = []

    def parse_file(self, filename):
        with open(filename) as fp:
            last_i = -1
            for line in fp:
                if line.isspace():
                    continue
                if line.lstrip().startswith("#"):
                    (key,value) = line.split(":")
                    key = key.replace("#","").lstrip()
                    self.header_fields[key] = value
                    continue
                (i, t, a, v) = line.split(":")
                i = int(i)
                t = t.replace(' ','')
                if t == "unit":
                    if i > last_i:
                        self.grid_states.append(dict())
                        last_i = i
                    a = eval(a)
                    v = eval(v)
                    self.grid_states[-1][a] = v
                if t == "input":
                    # No grid address for input vector
                    v = eval(v)
                    self.inputs.append(v)
                if t == "bmu":
                    a = eval(a)
                    v = eval(v)
                    self.bmu_addr.append(a)
                    self.bmu_weights.append(v)

    def get_initial_state(self):
        return self.grid_states[0]

    def get_final_state(self):
        return self.grid_states[-1]

    def get_inputs_count(self):
        return len(self.grid_states)

    def get_inputs(self):
        return self.inputs

    def get_squared_errors(self):
        """Return a vector of the squared error
           between inputs and relative BMU weight"""
        QE = []
        for (x,m) in zip(self.inputs,self.bmu_weights):
            x=np.array(x)
            m=np.array(m)
            distance = np.sum(np.square(x - m))
            QE.append(distance)
        return QE

    def get_windowed_squared_error(self, w):
        """Return a rolling (windowed on w) average of the squared error"""
        QE = self.get_squared_errors()
        # Convolution using 'valid', so return only fully overlapped results
        return np.convolve(QE, np.ones(w), 'valid') / w

    def get_average_squared_errors_progress(self):
        QE = self.get_squared_errors()
        aqep=[]
        for i, err in enumerate(QE):
            s = 0
            for j in range (i+1):
                s += QE[j]
            aqep.append(s/(i+1))
        return aqep

    def get_distance(self,v1,v2):
        assert len(v1) == len(v2)
        acc = 0.0
        for c1, c2 in zip(v1, v2):
            acc = acc + (c1-c2)**2
        return np.sqrt(acc)

    def do_validation(self, validation_set_filename):
        self.validation_inputs = []
        with open(validation_set_filename) as fp:
            for line in fp:
                if line.isspace():
                    continue
                if line.lstrip().startswith("#"):
                    continue
                tokens = line.split(" ")
                i = int(tokens[0])
                v = [float(x) for x in tuple(tokens[1:])]
                self.validation_inputs.append(v)
        last_grid_state = self.grid_states[-1]
        ase_acc = 0.0
        count = 0
        for vi in self.validation_inputs:
            # compute BMU
            BMU = None
            dist_min = sys.float_info.max
            for unit in last_grid_state:
                v = last_grid_state[unit]
                new_dist = self.get_distance(vi,v)
                if new_dist < dist_min:
                    BMU = unit
                    dist_min = new_dist
            # accumulate squared error
            ase_acc = ase_acc + dist_min**2
            count = count + 1
        return ase_acc / count