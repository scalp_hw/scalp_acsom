##################################################################################
#                                 _             _
#                                | |_  ___ _ __(_)__ _
#                                | ' \/ -_) '_ \ / _` |
#                                |_||_\___| .__/_\__,_|
#                                         |_|
#
##################################################################################
#
# Company: hepia
# Author: Quentin Berthet <quentin.berthet@hesge.ch>
#
# Project Name: flopoco
# Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
# Tool version: 2020.2
# Description: TCL script creating aliases for Vivado project management scripts
#
# Last update: 2021-04-12 12:06:00
#
##################################################################################

# Create aliases
alias create_project='cd .scripts && ./create_prj_flopoco.sh && cd ..'
alias clean_project='cd .scripts && ./clean_prj_flopoco.sh && cd ..'
alias export_hw='cd .scripts && ./export_hw_flopoco.sh && cd ..'
alias gen_bitstream='cd .scripts && ./gen_bitstream_flopoco.sh && cd ..'
alias load_bitstream='cd .scripts && ./load_bitstream_flopoco.sh && cd ..'
alias open_gui='cd .scripts && ./open_prj_flopoco.sh && cd ..'
