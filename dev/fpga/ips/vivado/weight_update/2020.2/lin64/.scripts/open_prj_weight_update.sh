#!/bin/sh

##################################################################################
#                                 _             _
#                                | |_  ___ _ __(_)__ _
#                                | ' \/ -_) '_ \ / _` |
#                                |_||_\___| .__/_\__,_|
#                                         |_|
#
##################################################################################
#
# Company: hepia
# Author: Quentin Berthet <quentin.berthet@hesge.ch>
#
# Project Name: weight_update
# Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
# Tool version: 2020.2
# Description: Open Vivado project GUI
#
# Last update: 2021-02-02 16:40:23
#
##################################################################################

echo "> Open Vivado GUI..."
vivado -nojournal -nolog -notrace ../weight_update/weight_update.xpr
