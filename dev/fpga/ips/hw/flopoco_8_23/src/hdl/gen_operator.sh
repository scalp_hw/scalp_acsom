#!/bin/sh

# Currently using precision similar to IEEE754
# FIXME: extract this information from CSOM package ?
wE=8 # Exponent bits count
wF=23 # Fractional bits count

today=$(date +"%Y/%m/%d %T")

echo "" > flopoco_pipeline_pkg.vhd
echo "----------------------------------------------------------------------------------" >> flopoco_pipeline_pkg.vhd
echo "--"                                                                                 >> flopoco_pipeline_pkg.vhd
echo "-- Company: hepia"                                                                  >> flopoco_pipeline_pkg.vhd
echo "-- Author: Quentin Berthet <quentin.berthet@hesge.ch>"                              >> flopoco_pipeline_pkg.vhd
echo "--"                                                                                 >> flopoco_pipeline_pkg.vhd
echo "-- Module Name: flopoco_pipeline_pkg"                                              >> flopoco_pipeline_pkg.vhd
echo "-- Description: Constants for flopoco operators pipeline length"                    >> flopoco_pipeline_pkg.vhd
echo "--"                                                                                 >> flopoco_pipeline_pkg.vhd
echo "-- Last generated on: ${today}"                                                     >> flopoco_pipeline_pkg.vhd
echo "--"                                                                                 >> flopoco_pipeline_pkg.vhd
echo "----------------------------------------------------------------------------------" >> flopoco_pipeline_pkg.vhd
echo ""                                 >> flopoco_pipeline_pkg.vhd
echo "library ieee;"                    >> flopoco_pipeline_pkg.vhd
echo "use ieee.std_logic_1164.all;"     >> flopoco_pipeline_pkg.vhd
echo "use ieee.numeric_std.all;"        >> flopoco_pipeline_pkg.vhd
echo ""                                 >> flopoco_pipeline_pkg.vhd
echo "package flopoco_pipeline_pkg is"  >> flopoco_pipeline_pkg.vhd
echo ""                                 >> flopoco_pipeline_pkg.vhd

echo "    constant c_wE : natural := $wE;" >> flopoco_pipeline_pkg.vhd
echo "    constant c_wF : natural := $wF;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=FPAdd
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=175 \
    $opName wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=FPSub
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=175 \
    FPAdd sub=true wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=FPMult
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=200 \
    $opName wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=FPMultNoDSP
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=175 \
    useHardMult=0 \
    FPMult wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=FPDiv
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=260 \
    $opName wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=FPExp
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=210 \
    $opName wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=FPSqrt
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=150 \
    $opName wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=InputIEEE
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=125 \
    $opName wEIn=8 wFIn=23 wEOut=$wE wFOut=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=OutputIEEE
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=125 \
    $opName wEIn=$wE wFIn=$wF wEOut=8 wFOut=23 name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=Signed8_2FP
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=125 \
    Fix2FP signed=true MSB=7 LSB=0 wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=Unsigned8_2FP
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=125 \
    Fix2FP signed=false MSB=7 LSB=0 wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=Signed10_2FP
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=125 \
    Fix2FP signed=true MSB=9 LSB=0 wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

opName=Unsigned10_2FP
docker run --user `id -u`:`id -g` --rm=true -v `pwd`:/flopoco_workspace flopoco \
    plainVHDL=0 \
    pipeline=1 \
    frequency=125 \
    Fix2FP signed=false MSB=9 LSB=0 wE=$wE wF=$wF name=$opName outputFile=${opName}.vhd

Pipeline=$(awk '/-- Pipeline depth: / {this=$4} END{print this}' ${opName}.vhd)
echo "    constant c_${opName}_PIPELINE_LENGTH : natural := $Pipeline;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd

echo "end flopoco_pipeline_pkg;" >> flopoco_pipeline_pkg.vhd
echo "" >> flopoco_pipeline_pkg.vhd