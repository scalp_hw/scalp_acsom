--------------------------------------------------------------------------------
--                     LeftShifter_24_by_max_33_F210_uid4
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_24_by_max_33_F210_uid4 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(23 downto 0);
          S : in  std_logic_vector(5 downto 0);
          R : out  std_logic_vector(56 downto 0)   );
end entity;

architecture arch of LeftShifter_24_by_max_33_F210_uid4 is
signal level0 :  std_logic_vector(23 downto 0);
signal ps, ps_d1 :  std_logic_vector(5 downto 0);
signal level1 :  std_logic_vector(24 downto 0);
signal level2 :  std_logic_vector(26 downto 0);
signal level3 :  std_logic_vector(30 downto 0);
signal level4 :  std_logic_vector(38 downto 0);
signal level5, level5_d1 :  std_logic_vector(54 downto 0);
signal level6 :  std_logic_vector(86 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            ps_d1 <=  ps;
            level5_d1 <=  level5;
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<= level0 & (0 downto 0 => '0') when ps(0)= '1' else     (0 downto 0 => '0') & level0;
   level2<= level1 & (1 downto 0 => '0') when ps(1)= '1' else     (1 downto 0 => '0') & level1;
   level3<= level2 & (3 downto 0 => '0') when ps(2)= '1' else     (3 downto 0 => '0') & level2;
   level4<= level3 & (7 downto 0 => '0') when ps(3)= '1' else     (7 downto 0 => '0') & level3;
   level5<= level4 & (15 downto 0 => '0') when ps(4)= '1' else     (15 downto 0 => '0') & level4;
   ----------------Synchro barrier, entering cycle 1----------------
   level6<= level5_d1 & (31 downto 0 => '0') when ps_d1(5)= '1' else     (31 downto 0 => '0') & level5_d1;
   R <= level6(56 downto 0);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(11 downto 0)   );
end entity;

architecture arch of FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
signal TableOut :  std_logic_vector(11 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "000000001000" when "000000",
   "000000110110" when "000001",
   "000001100100" when "000010",
   "000010010010" when "000011",
   "000011000001" when "000100",
   "000011101111" when "000101",
   "000100011101" when "000110",
   "000101001011" when "000111",
   "000101111001" when "001000",
   "000110100111" when "001001",
   "000111010110" when "001010",
   "001000000100" when "001011",
   "001000110010" when "001100",
   "001001100000" when "001101",
   "001010001110" when "001110",
   "001010111100" when "001111",
   "001011101011" when "010000",
   "001100011001" when "010001",
   "001101000111" when "010010",
   "001101110101" when "010011",
   "001110100011" when "010100",
   "001111010001" when "010101",
   "010000000000" when "010110",
   "010000101110" when "010111",
   "010001011100" when "011000",
   "010010001010" when "011001",
   "010010111000" when "011010",
   "010011100110" when "011011",
   "010100010101" when "011100",
   "010101000011" when "011101",
   "010101110001" when "011110",
   "010110011111" when "011111",
   "010111001101" when "100000",
   "010111111011" when "100001",
   "011000101010" when "100010",
   "011001011000" when "100011",
   "011010000110" when "100100",
   "011010110100" when "100101",
   "011011100010" when "100110",
   "011100010000" when "100111",
   "011100111111" when "101000",
   "011101101101" when "101001",
   "011110011011" when "101010",
   "011111001001" when "101011",
   "011111110111" when "101100",
   "100000100101" when "101101",
   "100001010100" when "101110",
   "100010000010" when "101111",
   "100010110000" when "110000",
   "100011011110" when "110001",
   "100100001100" when "110010",
   "100100111010" when "110011",
   "100101101001" when "110100",
   "100110010111" when "110101",
   "100111000101" when "110110",
   "100111110011" when "110111",
   "101000100001" when "111000",
   "101001001111" when "111001",
   "101001111110" when "111010",
   "101010101100" when "111011",
   "101011011010" when "111100",
   "101100001000" when "111101",
   "101100110110" when "111110",
   "101101100100" when "111111",
   "------------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(3 downto 0);
          Y : out  std_logic_vector(5 downto 0)   );
end entity;

architecture arch of FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
signal TableOut :  std_logic_vector(5 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "000000" when "0000",
   "000011" when "0001",
   "000110" when "0010",
   "001001" when "0011",
   "001100" when "0100",
   "001110" when "0101",
   "010001" when "0110",
   "010100" when "0111",
   "010111" when "1000",
   "011010" when "1001",
   "011101" when "1010",
   "100000" when "1011",
   "100011" when "1100",
   "100110" when "1101",
   "101000" when "1110",
   "101011" when "1111",
   "------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_13_f210_uid24
--                     (IntAdderClassical_13_F210_uid26)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_13_f210_uid24 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(12 downto 0);
          Y : in  std_logic_vector(12 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(12 downto 0)   );
end entity;

architecture arch of IntAdder_13_f210_uid24 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(9 downto 0);
          R : out  std_logic_vector(7 downto 0)   );
end entity;

architecture arch of FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8 is
   component FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(11 downto 0)   );
   end component;

   component FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(3 downto 0);
             Y : out  std_logic_vector(5 downto 0)   );
   end component;

   component IntAdder_13_f210_uid24 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(12 downto 0);
             Y : in  std_logic_vector(12 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(12 downto 0)   );
   end component;

signal FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_A0 :  std_logic_vector(5 downto 0);
signal FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0 :  std_logic_vector(11 downto 0);
signal heap_bh9_w0_0 :  std_logic;
signal heap_bh9_w1_0 :  std_logic;
signal heap_bh9_w2_0 :  std_logic;
signal heap_bh9_w3_0 :  std_logic;
signal heap_bh9_w4_0 :  std_logic;
signal heap_bh9_w5_0 :  std_logic;
signal heap_bh9_w6_0 :  std_logic;
signal heap_bh9_w7_0 :  std_logic;
signal heap_bh9_w8_0 :  std_logic;
signal heap_bh9_w9_0 :  std_logic;
signal heap_bh9_w10_0 :  std_logic;
signal heap_bh9_w11_0 :  std_logic;
signal FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_A1 :  std_logic_vector(3 downto 0);
signal FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1 :  std_logic_vector(5 downto 0);
signal heap_bh9_w0_1 :  std_logic;
signal heap_bh9_w1_1 :  std_logic;
signal heap_bh9_w2_1 :  std_logic;
signal heap_bh9_w3_1 :  std_logic;
signal heap_bh9_w4_1 :  std_logic;
signal heap_bh9_w5_1 :  std_logic;
signal finalAdderIn0_bh9 :  std_logic_vector(12 downto 0);
signal finalAdderIn1_bh9 :  std_logic_vector(12 downto 0);
signal finalAdderCin_bh9 :  std_logic;
signal finalAdderOut_bh9 :  std_logic_vector(12 downto 0);
signal CompressionResult9 :  std_logic_vector(12 downto 0);
signal OutRes :  std_logic_vector(11 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_A0 <= X(9 downto 4); -- input address  m=6  l=1
   FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table0: FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_A0,
                 Y => FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0);
   ---------------- cycle 0----------------
   heap_bh9_w0_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(0); -- cycle= 0 cp= 0
   heap_bh9_w1_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(1); -- cycle= 0 cp= 0
   heap_bh9_w2_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(2); -- cycle= 0 cp= 0
   heap_bh9_w3_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(3); -- cycle= 0 cp= 0
   heap_bh9_w4_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(4); -- cycle= 0 cp= 0
   heap_bh9_w5_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(5); -- cycle= 0 cp= 0
   heap_bh9_w6_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(6); -- cycle= 0 cp= 0
   heap_bh9_w7_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(7); -- cycle= 0 cp= 0
   heap_bh9_w8_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(8); -- cycle= 0 cp= 0
   heap_bh9_w9_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(9); -- cycle= 0 cp= 0
   heap_bh9_w10_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(10); -- cycle= 0 cp= 0
   heap_bh9_w11_0 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T0(11); -- cycle= 0 cp= 0
   FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_A1 <= X(3 downto 0); -- input address  m=0  l=-3
   FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table1: FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_A1,
                 Y => FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1);
   ---------------- cycle 0----------------
   heap_bh9_w0_1 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1(0); -- cycle= 0 cp= 0
   heap_bh9_w1_1 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1(1); -- cycle= 0 cp= 0
   heap_bh9_w2_1 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1(2); -- cycle= 0 cp= 0
   heap_bh9_w3_1 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1(3); -- cycle= 0 cp= 0
   heap_bh9_w4_1 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1(4); -- cycle= 0 cp= 0
   heap_bh9_w5_1 <= FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8_T1(5); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   finalAdderIn0_bh9 <= "0" & heap_bh9_w11_0 & heap_bh9_w10_0 & heap_bh9_w9_0 & heap_bh9_w8_0 & heap_bh9_w7_0 & heap_bh9_w6_0 & heap_bh9_w5_1 & heap_bh9_w4_1 & heap_bh9_w3_1 & heap_bh9_w2_1 & heap_bh9_w1_1 & heap_bh9_w0_1;
   finalAdderIn1_bh9 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh9_w5_0 & heap_bh9_w4_0 & heap_bh9_w3_0 & heap_bh9_w2_0 & heap_bh9_w1_0 & heap_bh9_w0_0;
   finalAdderCin_bh9 <= '0';
   Adder_final9_0: IntAdder_13_f210_uid24  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh9,
                 R => finalAdderOut_bh9   ,
                 X => finalAdderIn0_bh9,
                 Y => finalAdderIn1_bh9);
   -- concatenate all the compressed chunks
   CompressionResult9 <= finalAdderOut_bh9;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult9(11 downto 0);
   R <= OutRes(11 downto 4);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_0 is
signal TableOut :  std_logic_vector(33 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000000000000000000000000000000000" when "000000",
   "0000001011000101110010000101111111" when "000001",
   "0000010110001011100100001011111111" when "000010",
   "0000100001010001010110010001111110" when "000011",
   "0000101100010111001000010111111110" when "000100",
   "0000110111011100111010011101111101" when "000101",
   "0001000010100010101100100011111101" when "000110",
   "0001001101101000011110101001111100" when "000111",
   "0001011000101110010000101111111100" when "001000",
   "0001100011110100000010110101111011" when "001001",
   "0001101110111001110100111011111011" when "001010",
   "0001111001111111100111000001111010" when "001011",
   "0010000101000101011001000111111010" when "001100",
   "0010010000001011001011001101111001" when "001101",
   "0010011011010000111101010011111001" when "001110",
   "0010100110010110101111011001111000" when "001111",
   "0010110001011100100001011111111000" when "010000",
   "0010111100100010010011100101110111" when "010001",
   "0011000111101000000101101011110111" when "010010",
   "0011010010101101110111110001110110" when "010011",
   "0011011101110011101001110111110110" when "010100",
   "0011101000111001011011111101110101" when "010101",
   "0011110011111111001110000011110101" when "010110",
   "0011111111000101000000001001110100" when "010111",
   "0100001010001010110010001111110100" when "011000",
   "0100010101010000100100010101110011" when "011001",
   "0100100000010110010110011011110011" when "011010",
   "0100101011011100001000100001110010" when "011011",
   "0100110110100001111010100111110010" when "011100",
   "0101000001100111101100101101110001" when "011101",
   "0101001100101101011110110011110001" when "011110",
   "0101010111110011010000111001110000" when "011111",
   "0101100010111001000010111111110000" when "100000",
   "0101101101111110110101000101101111" when "100001",
   "0101111001000100100111001011101111" when "100010",
   "0110000100001010011001010001101110" when "100011",
   "0110001111010000001011010111101110" when "100100",
   "0110011010010101111101011101101101" when "100101",
   "0110100101011011101111100011101101" when "100110",
   "0110110000100001100001101001101100" when "100111",
   "0110111011100111010011101111101100" when "101000",
   "0111000110101101000101110101101011" when "101001",
   "0111010001110010110111111011101011" when "101010",
   "0111011100111000101010000001101010" when "101011",
   "0111100111111110011100000111101010" when "101100",
   "0111110011000100001110001101101001" when "101101",
   "0111111110001010000000010011101000" when "101110",
   "1000001001001111110010011001101000" when "101111",
   "1000010100010101100100011111100111" when "110000",
   "1000011111011011010110100101100111" when "110001",
   "1000101010100001001000101011100110" when "110010",
   "1000110101100110111010110001100110" when "110011",
   "1001000000101100101100110111100101" when "110100",
   "1001001011110010011110111101100101" when "110101",
   "1001010110111000010001000011100100" when "110110",
   "1001100001111110000011001001100100" when "110111",
   "1001101101000011110101001111100011" when "111000",
   "1001111000001001100111010101100011" when "111001",
   "1010000011001111011001011011100010" when "111010",
   "1010001110010101001011100001100010" when "111011",
   "1010011001011010111101100111100001" when "111100",
   "1010100100100000101111101101100001" when "111101",
   "1010101111100110100001110011100000" when "111110",
   "1010111010101100010011111001100000" when "111111",
   "----------------------------------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(1 downto 0);
          Y : out  std_logic_vector(27 downto 0)   );
end entity;

architecture arch of FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_1 is
signal TableOut :  std_logic_vector(27 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000000000000000000000000000" when "00",
   "0010110001011100100001100000" when "01",
   "0101100010111001000011000000" when "10",
   "1000010100010101100100100000" when "11",
   "----------------------------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_35_f210_uid50
--                     (IntAdderClassical_35_F210_uid52)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_35_f210_uid50 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(34 downto 0);
          Y : in  std_logic_vector(34 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(34 downto 0)   );
end entity;

architecture arch of IntAdder_35_f210_uid50 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          R : out  std_logic_vector(33 downto 0)   );
end entity;

architecture arch of FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34 is
   component FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(33 downto 0)   );
   end component;

   component FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(1 downto 0);
             Y : out  std_logic_vector(27 downto 0)   );
   end component;

   component IntAdder_35_f210_uid50 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(34 downto 0);
             Y : in  std_logic_vector(34 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(34 downto 0)   );
   end component;

signal FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_A0 :  std_logic_vector(5 downto 0);
signal FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0 :  std_logic_vector(33 downto 0);
signal heap_bh35_w0_0 :  std_logic;
signal heap_bh35_w1_0 :  std_logic;
signal heap_bh35_w2_0 :  std_logic;
signal heap_bh35_w3_0 :  std_logic;
signal heap_bh35_w4_0 :  std_logic;
signal heap_bh35_w5_0 :  std_logic;
signal heap_bh35_w6_0 :  std_logic;
signal heap_bh35_w7_0 :  std_logic;
signal heap_bh35_w8_0 :  std_logic;
signal heap_bh35_w9_0 :  std_logic;
signal heap_bh35_w10_0 :  std_logic;
signal heap_bh35_w11_0 :  std_logic;
signal heap_bh35_w12_0 :  std_logic;
signal heap_bh35_w13_0 :  std_logic;
signal heap_bh35_w14_0 :  std_logic;
signal heap_bh35_w15_0 :  std_logic;
signal heap_bh35_w16_0 :  std_logic;
signal heap_bh35_w17_0 :  std_logic;
signal heap_bh35_w18_0 :  std_logic;
signal heap_bh35_w19_0 :  std_logic;
signal heap_bh35_w20_0 :  std_logic;
signal heap_bh35_w21_0 :  std_logic;
signal heap_bh35_w22_0 :  std_logic;
signal heap_bh35_w23_0 :  std_logic;
signal heap_bh35_w24_0 :  std_logic;
signal heap_bh35_w25_0 :  std_logic;
signal heap_bh35_w26_0 :  std_logic;
signal heap_bh35_w27_0 :  std_logic;
signal heap_bh35_w28_0 :  std_logic;
signal heap_bh35_w29_0 :  std_logic;
signal heap_bh35_w30_0 :  std_logic;
signal heap_bh35_w31_0 :  std_logic;
signal heap_bh35_w32_0 :  std_logic;
signal heap_bh35_w33_0 :  std_logic;
signal FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_A1 :  std_logic_vector(1 downto 0);
signal FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1 :  std_logic_vector(27 downto 0);
signal heap_bh35_w0_1 :  std_logic;
signal heap_bh35_w1_1 :  std_logic;
signal heap_bh35_w2_1 :  std_logic;
signal heap_bh35_w3_1 :  std_logic;
signal heap_bh35_w4_1 :  std_logic;
signal heap_bh35_w5_1 :  std_logic;
signal heap_bh35_w6_1 :  std_logic;
signal heap_bh35_w7_1 :  std_logic;
signal heap_bh35_w8_1 :  std_logic;
signal heap_bh35_w9_1 :  std_logic;
signal heap_bh35_w10_1 :  std_logic;
signal heap_bh35_w11_1 :  std_logic;
signal heap_bh35_w12_1 :  std_logic;
signal heap_bh35_w13_1 :  std_logic;
signal heap_bh35_w14_1 :  std_logic;
signal heap_bh35_w15_1 :  std_logic;
signal heap_bh35_w16_1 :  std_logic;
signal heap_bh35_w17_1 :  std_logic;
signal heap_bh35_w18_1 :  std_logic;
signal heap_bh35_w19_1 :  std_logic;
signal heap_bh35_w20_1 :  std_logic;
signal heap_bh35_w21_1 :  std_logic;
signal heap_bh35_w22_1 :  std_logic;
signal heap_bh35_w23_1 :  std_logic;
signal heap_bh35_w24_1 :  std_logic;
signal heap_bh35_w25_1 :  std_logic;
signal heap_bh35_w26_1 :  std_logic;
signal heap_bh35_w27_1 :  std_logic;
signal finalAdderIn0_bh35 :  std_logic_vector(34 downto 0);
signal finalAdderIn1_bh35 :  std_logic_vector(34 downto 0);
signal finalAdderCin_bh35 :  std_logic;
signal finalAdderOut_bh35 :  std_logic_vector(34 downto 0);
signal CompressionResult35 :  std_logic_vector(34 downto 0);
signal OutRes :  std_logic_vector(33 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_A0 <= X(7 downto 2); -- input address  m=7  l=2
   FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table0: FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_A0,
                 Y => FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0);
   ---------------- cycle 0----------------
   heap_bh35_w0_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(0); -- cycle= 0 cp= 0
   heap_bh35_w1_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(1); -- cycle= 0 cp= 0
   heap_bh35_w2_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(2); -- cycle= 0 cp= 0
   heap_bh35_w3_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(3); -- cycle= 0 cp= 0
   heap_bh35_w4_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(4); -- cycle= 0 cp= 0
   heap_bh35_w5_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(5); -- cycle= 0 cp= 0
   heap_bh35_w6_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(6); -- cycle= 0 cp= 0
   heap_bh35_w7_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(7); -- cycle= 0 cp= 0
   heap_bh35_w8_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(8); -- cycle= 0 cp= 0
   heap_bh35_w9_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(9); -- cycle= 0 cp= 0
   heap_bh35_w10_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(10); -- cycle= 0 cp= 0
   heap_bh35_w11_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(11); -- cycle= 0 cp= 0
   heap_bh35_w12_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(12); -- cycle= 0 cp= 0
   heap_bh35_w13_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(13); -- cycle= 0 cp= 0
   heap_bh35_w14_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(14); -- cycle= 0 cp= 0
   heap_bh35_w15_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(15); -- cycle= 0 cp= 0
   heap_bh35_w16_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(16); -- cycle= 0 cp= 0
   heap_bh35_w17_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(17); -- cycle= 0 cp= 0
   heap_bh35_w18_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(18); -- cycle= 0 cp= 0
   heap_bh35_w19_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(19); -- cycle= 0 cp= 0
   heap_bh35_w20_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(20); -- cycle= 0 cp= 0
   heap_bh35_w21_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(21); -- cycle= 0 cp= 0
   heap_bh35_w22_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(22); -- cycle= 0 cp= 0
   heap_bh35_w23_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(23); -- cycle= 0 cp= 0
   heap_bh35_w24_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(24); -- cycle= 0 cp= 0
   heap_bh35_w25_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(25); -- cycle= 0 cp= 0
   heap_bh35_w26_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(26); -- cycle= 0 cp= 0
   heap_bh35_w27_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(27); -- cycle= 0 cp= 0
   heap_bh35_w28_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(28); -- cycle= 0 cp= 0
   heap_bh35_w29_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(29); -- cycle= 0 cp= 0
   heap_bh35_w30_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(30); -- cycle= 0 cp= 0
   heap_bh35_w31_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(31); -- cycle= 0 cp= 0
   heap_bh35_w32_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(32); -- cycle= 0 cp= 0
   heap_bh35_w33_0 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T0(33); -- cycle= 0 cp= 0
   FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_A1 <= X(1 downto 0); -- input address  m=1  l=0
   FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table1: FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_A1,
                 Y => FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1);
   ---------------- cycle 0----------------
   heap_bh35_w0_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(0); -- cycle= 0 cp= 0
   heap_bh35_w1_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(1); -- cycle= 0 cp= 0
   heap_bh35_w2_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(2); -- cycle= 0 cp= 0
   heap_bh35_w3_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(3); -- cycle= 0 cp= 0
   heap_bh35_w4_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(4); -- cycle= 0 cp= 0
   heap_bh35_w5_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(5); -- cycle= 0 cp= 0
   heap_bh35_w6_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(6); -- cycle= 0 cp= 0
   heap_bh35_w7_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(7); -- cycle= 0 cp= 0
   heap_bh35_w8_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(8); -- cycle= 0 cp= 0
   heap_bh35_w9_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(9); -- cycle= 0 cp= 0
   heap_bh35_w10_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(10); -- cycle= 0 cp= 0
   heap_bh35_w11_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(11); -- cycle= 0 cp= 0
   heap_bh35_w12_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(12); -- cycle= 0 cp= 0
   heap_bh35_w13_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(13); -- cycle= 0 cp= 0
   heap_bh35_w14_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(14); -- cycle= 0 cp= 0
   heap_bh35_w15_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(15); -- cycle= 0 cp= 0
   heap_bh35_w16_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(16); -- cycle= 0 cp= 0
   heap_bh35_w17_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(17); -- cycle= 0 cp= 0
   heap_bh35_w18_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(18); -- cycle= 0 cp= 0
   heap_bh35_w19_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(19); -- cycle= 0 cp= 0
   heap_bh35_w20_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(20); -- cycle= 0 cp= 0
   heap_bh35_w21_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(21); -- cycle= 0 cp= 0
   heap_bh35_w22_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(22); -- cycle= 0 cp= 0
   heap_bh35_w23_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(23); -- cycle= 0 cp= 0
   heap_bh35_w24_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(24); -- cycle= 0 cp= 0
   heap_bh35_w25_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(25); -- cycle= 0 cp= 0
   heap_bh35_w26_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(26); -- cycle= 0 cp= 0
   heap_bh35_w27_1 <= FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34_T1(27); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   finalAdderIn0_bh35 <= "0" & heap_bh35_w33_0 & heap_bh35_w32_0 & heap_bh35_w31_0 & heap_bh35_w30_0 & heap_bh35_w29_0 & heap_bh35_w28_0 & heap_bh35_w27_1 & heap_bh35_w26_1 & heap_bh35_w25_1 & heap_bh35_w24_1 & heap_bh35_w23_1 & heap_bh35_w22_1 & heap_bh35_w21_1 & heap_bh35_w20_1 & heap_bh35_w19_1 & heap_bh35_w18_1 & heap_bh35_w17_1 & heap_bh35_w16_1 & heap_bh35_w15_1 & heap_bh35_w14_1 & heap_bh35_w13_1 & heap_bh35_w12_1 & heap_bh35_w11_1 & heap_bh35_w10_1 & heap_bh35_w9_1 & heap_bh35_w8_1 & heap_bh35_w7_1 & heap_bh35_w6_1 & heap_bh35_w5_1 & heap_bh35_w4_1 & heap_bh35_w3_1 & heap_bh35_w2_1 & heap_bh35_w1_1 & heap_bh35_w0_1;
   finalAdderIn1_bh35 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh35_w27_0 & heap_bh35_w26_0 & heap_bh35_w25_0 & heap_bh35_w24_0 & heap_bh35_w23_0 & heap_bh35_w22_0 & heap_bh35_w21_0 & heap_bh35_w20_0 & heap_bh35_w19_0 & heap_bh35_w18_0 & heap_bh35_w17_0 & heap_bh35_w16_0 & heap_bh35_w15_0 & heap_bh35_w14_0 & heap_bh35_w13_0 & heap_bh35_w12_0 & heap_bh35_w11_0 & heap_bh35_w10_0 & heap_bh35_w9_0 & heap_bh35_w8_0 & heap_bh35_w7_0 & heap_bh35_w6_0 & heap_bh35_w5_0 & heap_bh35_w4_0 & heap_bh35_w3_0 & heap_bh35_w2_0 & heap_bh35_w1_0 & heap_bh35_w0_0;
   finalAdderCin_bh35 <= '0';
   Adder_final35_0: IntAdder_35_f210_uid50  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh35,
                 R => finalAdderOut_bh35   ,
                 X => finalAdderIn0_bh35,
                 Y => finalAdderIn1_bh35);
   -- concatenate all the compressed chunks
   CompressionResult35 <= finalAdderOut_bh35;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult35(33 downto 0);
   R <= OutRes(33 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_26_f231_uid60
--                    (IntAdderAlternative_26_F231_uid64)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_26_f231_uid60 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(25 downto 0);
          Y : in  std_logic_vector(25 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(25 downto 0)   );
end entity;

architecture arch of IntAdder_26_f231_uid60 is
signal s_sum_l0_idx0 :  std_logic_vector(15 downto 0);
signal s_sum_l0_idx1, s_sum_l0_idx1_d1 :  std_logic_vector(11 downto 0);
signal sum_l0_idx0, sum_l0_idx0_d1 :  std_logic_vector(14 downto 0);
signal c_l0_idx0, c_l0_idx0_d1 :  std_logic_vector(0 downto 0);
signal sum_l0_idx1 :  std_logic_vector(10 downto 0);
signal c_l0_idx1 :  std_logic_vector(0 downto 0);
signal s_sum_l1_idx1 :  std_logic_vector(11 downto 0);
signal sum_l1_idx1 :  std_logic_vector(10 downto 0);
signal c_l1_idx1 :  std_logic_vector(0 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            s_sum_l0_idx1_d1 <=  s_sum_l0_idx1;
            sum_l0_idx0_d1 <=  sum_l0_idx0;
            c_l0_idx0_d1 <=  c_l0_idx0;
         end if;
      end process;
   --Alternative
   s_sum_l0_idx0 <= ( "0" & X(14 downto 0)) + ( "0" & Y(14 downto 0)) + Cin;
   s_sum_l0_idx1 <= ( "0" & X(25 downto 15)) + ( "0" & Y(25 downto 15));
   sum_l0_idx0 <= s_sum_l0_idx0(14 downto 0);
   c_l0_idx0 <= s_sum_l0_idx0(15 downto 15);
   sum_l0_idx1 <= s_sum_l0_idx1(10 downto 0);
   c_l0_idx1 <= s_sum_l0_idx1(11 downto 11);
   ----------------Synchro barrier, entering cycle 1----------------
   s_sum_l1_idx1 <=  s_sum_l0_idx1_d1 + c_l0_idx0_d1(0 downto 0);
   sum_l1_idx1 <= s_sum_l1_idx1(10 downto 0);
   c_l1_idx1 <= s_sum_l1_idx1(11 downto 11);
   R <= sum_l1_idx1(10 downto 0) & sum_l0_idx0_d1(14 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                         MagicSPExpTable_F210_uid68
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Radu Tudoran, Florent de Dinechin (2009)
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity MagicSPExpTable_F210_uid68 is
   port ( X1 : in  std_logic_vector(8 downto 0);
          Y1 : out  std_logic_vector(35 downto 0);
          X2 : in  std_logic_vector(8 downto 0);
          Y2 : out  std_logic_vector(35 downto 0)   );
end entity;

architecture arch of MagicSPExpTable_F210_uid68 is
type ROMContent is array (0 to 511) of std_logic_vector(35 downto 0);
constant memVar: ROMContent :=    ( 
      "100000000000000000000000000000000000",       "100000000100000000010000000000000000",       "100000001000000001000000001000000000",       "100000001100000010010000010000000000", 
      "100000010000000100000000101000000000",       "100000010100000110010001010000000000",       "100000011000001001000010010000000000",       "100000011100001100010011101000000000", 
      "100000100000010000000101011000000000",       "100000100100010100010111101000000000",       "100000101000011001001010100000000000",       "100000101100011110011110000000000000", 
      "100000110000100100010010001000000000",       "100000110100101010100111000000000000",       "100000111000110001011100110000000000",       "100000111100111000110011011000000000", 
      "100001000001000000101011000000000000",       "100001000101001001000011101000000000",       "100001001001010001111101010000000000",       "100001001101011011011000001000000000", 
      "100001010001100101010100001000000000",       "100001010101101111110001100000000000",       "100001011001111010110000001000000000",       "100001011110000110010000001000000000", 
      "100001100010010010010001110000000000",       "100001100110011110110100110000000000",       "100001101010101011111001011000000000",       "100001101110111001011111110000000000", 
      "100001110011000111100111111000000000",       "100001110111010110010001110000000000",       "100001111011100101011101011000000000",       "100001111111110101001011001000000000", 
      "100010000100000101011010110000000001",       "100010001000010110001100100000000001",       "100010001100100111100000010000000001",       "100010010000111001010110011000000001", 
      "100010010101001011101110101000000001",       "100010011001011110101001010000000001",       "100010011101110010000110011000000001",       "100010100010000110000101111000000001", 
      "100010100110011010100111111000000001",       "100010101010101111101100100000000001",       "100010101111000101010011111000000001",       "100010110011011011011101111000000001", 
      "100010110111110010001010110000000001",       "100010111100001001011010100000000001",       "100011000000100001001101001000000001",       "100011000100111001100010110000000001", 
      "100011001001010010011011100000000001",       "100011001101101011110111011000000001",       "100011010010000101110110100000000001",       "100011010110100000011000111000000001", 
      "100011011010111011011110101000000001",       "100011011111010111000111110000000001",       "100011100011110011010100011000000001",       "100011101000010000000100101000000001", 
      "100011101100101101011000100000000010",       "100011110001001011010000000000000010",       "100011110101101001101011010000000010",       "100011111010001000101010100000000010", 
      "100011111110101000001101100000000010",       "100100000011001000010100100000000010",       "100100000111101000111111101000000010",       "100100001100001010001110110000000010", 
      "100100010000101100000010001000000010",       "100100010101001110011001111000000010",       "100100011001110001010101110000000010",       "100100011110010100110110001000000010", 
      "100100100010111000111011000000000010",       "100100100111011101100100100000000010",       "100100101100000010110010100000000010",       "100100110000101000100101001000000010", 
      "100100110101001110111100101000000011",       "100100111001110101111000111000000011",       "100100111110011101011010001000000011",       "100101000011000101100000010000000011", 
      "100101000111101110001011100000000011",       "100101001100010111011011111000000011",       "100101010001000001010001011000000011",       "100101010101101011101100010000000011", 
      "100101011010010110101100011000000011",       "100101011111000010010001111000000011",       "100101100011101110011101000000000011",       "100101101000011011001101100000000011", 
      "100101101101001000100011110000000011",       "100101110001110110011111110000000100",       "100101110110100101000001011000000100",       "100101111011010100001001000000000100", 
      "100110000000000011110110100000000100",       "100110000100110100001010000000000100",       "100110001001100101000011100000000100",       "100110001110010110100011010000000100", 
      "100110010011001000101001010000000100",       "100110010111111011010101100000000100",       "100110011100101110101000001000000100",       "100110100001100010100001001000000100", 
      "100110100110010111000000110000000101",       "100110101011001100000110111000000101",       "100110110000000001110011110000000101",       "100110110100111000000111011000000101", 
      "100110111001101111000001111000000101",       "100110111110100110100011001000000101",       "100111000011011110101011100000000101",       "100111001000010111011010111000000101", 
      "100111001101010000110001011000000101",       "100111010010001010101111001000000101",       "100111010111000101010100001000000101",       "100111011100000000100000011000000110", 
      "100111100000111100010100010000000110",       "100111100101111000101111100000000110",       "100111101010110101110010100000000110",       "100111101111110011011101000000000110", 
      "100111110100110001101111011000000110",       "100111111001110000101001100000000110",       "100111111110110000001011100000000110",       "101000000011110000010101100000000110", 
      "101000001000110001000111101000000111",       "101000001101110010100001111000000111",       "101000010010110100100100010000000111",       "101000010111110111001111000000000111", 
      "101000011100111010100010001000000111",       "101000100001111110011101101000000111",       "101000100111000011000001110000000111",       "101000101100001000001110100000000111", 
      "101000110001001110000011111000001000",       "101000110110010100100010000000001000",       "101000111011011011101001000000001000",       "101001000000100011011001000000001000", 
      "101001000101101011110001111000001000",       "101001001010110100110011111000001000",       "101001001111111110011111001000001000",       "101001010101001000110011100000001000", 
      "101001011010010011110001011000001001",       "101001011111011111011000100000001001",       "101001100100101011101001010000001001",       "101001101001111000100011100000001001", 
      "101001101111000110000111011000001001",       "101001110100010100010101000000001001",       "101001111001100011001100100000001001",       "101001111110110010101101111000001001", 
      "101010000100000010111001010000001010",       "101010001001010011101110101000001010",       "101010001110100101001110001000001010",       "101010010011110111010111111000001010", 
      "101010011001001010001100000000001010",       "101010011110011101101010100000001010",       "101010100011110001110011100000001010",       "101010101001000110100111000000001011", 
      "101010101110011100000101001000001011",       "101010110011110010001110000000001011",       "101010111001001001000001110000001011",       "101010111110100000100000011000001011", 
      "101011000011111000101001111000001011",       "101011001001010001011110100000001011",       "101011001110101010111110010000001100",       "101011010100000101001001010000001100", 
      "101011011001011111111111101000001100",       "101011011110111011100001010000001100",       "101011100100010111101110100000001100",       "101011101001110100100111010000001100", 
      "101011101111010010001011110000001101",       "101011110100110000011011111000001101",       "101011111010001111010111111000001101",       "101011111111101110111111110000001101", 
      "101100000101001111010011101000001101",       "101100001010110000010011101000001101",       "101100010000010001111111110000001101",       "101100010101110100011000001000001110", 
      "101100011011010111011100111000001110",       "101100100000111011001110000000001110",       "101100100110011111101011101000001110",       "101100101100000100110110000000001110", 
      "101100110001101010101100111000001110",       "101100110111010001010000101000001111",       "101100111100111000100001010000001111",       "101101000010100000011110110000001111", 
      "101101001000001001001001011000001111",       "101101001101110010100001001000001111",       "101101010011011100100110000000001111",       "101101011001000111011000010000010000", 
      "101101011110110010110111111000010000",       "101101100100011111000101000000010000",       "101101101010001011111111110000010000",       "101101101111111001101000001000010000", 
      "101101110101100111111110001000010001",       "101101111011010111000010001000010001",       "101110000001000110110100000000010001",       "101110000110110111010011111000010001", 
      "101110001100101000100001111000010001",       "101110010010011010011110000000010001",       "101110011000001101001000100000010010",       "101110011110000000100001010000010010", 
      "101110100011110100101000101000010010",       "101110101001101001011110100000010010",       "101110101111011111000011000000010010",       "101110110101010101010110010000010011", 
      "101110111011001100011000011000010011",       "101111000001000100001001011000010011",       "101111000110111100101001100000010011",       "101111001100110101111000101000010011", 
      "101111010010101111110111000000010100",       "101111011000101010100100101000010100",       "101111011110100110000001101000010100",       "101111100100100010001110001000010100", 
      "101111101010011111001010010000010100",       "101111110000011100110110000000010101",       "101111110110011011010001100000010101",       "101111111100011010011100110000010101", 
      "110000000010011010011000001000010101",       "110000001000011011000011011000010101",       "110000001110011100011110111000010110",       "110000010100011110101010101000010110", 
      "110000011010100001100110101000010110",       "110000100000100101010011000000010110",       "110000100110101001110000000000010110",       "110000101100101110111101100000010111", 
      "110000110010110100111011110000010111",       "110000111000111011101010110000010111",       "110000111111000011001010101000010111",       "110001000101001011011011101000010111", 
      "110001001011010100011101100000011000",       "110001010001011110010000110000011000",       "110001010111101000110101001000011000",       "110001011101110100001011000000011000", 
      "110001100100000000010010010000011001",       "110001101010001101001011001000011001",       "110001110000011010110101100000011001",       "110001110110101001010001110000011001", 
      "110001111100111000011111111000011001",       "110010000011001000100000000000011010",       "110010001001011001010010001000011010",       "110010001111101010110110011000011010", 
      "110010010101111101001100111000011010",       "110010011100010000010101101000011011",       "110010100010100100010000111000011011",       "110010101000111000111110110000011011", 
      "110010101111001110011111010000011011",       "110010110101100100110010011000011011",       "110010111011111011111000100000011100",       "110011000010010011110001011000011100", 
      "110011001000101100011101011000011100",       "110011001111000101111100100000011100",       "110011010101100000001110111000011101",       "110011011011111011010100101000011101", 
      "110011100010010111001101110000011101",       "110011101000110011111010100000011101",       "110011101111010001011010110000011110",       "110011110101101111101110111000011110", 
      "110011111100001110110110110000011110",       "110100000010101110110010101000011110",       "110100001001001111100010100000011111",       "110100001111110001000110100000011111", 
      "110100010110010011011110111000011111",       "110100011100110110101011100000011111",       "110100100011011010101100100000100000",       "110100101001111111100010001000100000", 
      "010011011010001011001100000000100000",       "010011011100100110100111000000100000",       "010011011111000010010101101000100001",       "010011100001011110010111101000100001", 
      "010011100011111010101101010000100001",       "010011100110010111010110011000100001",       "010011101000110100010011001000100010",       "010011101011010001100011011000100010", 
      "010011101101101111000111100000100010",       "010011110000001100111111010000100010",       "010011110010101011001010110000100011",       "010011110101001001101010000000100011", 
      "010011110111101000011101001000100011",       "010011111010000111100100001000100011",       "010011111100100110111111000000100100",       "010011111111000110101101111000100100", 
      "010100000001100110110000110000100100",       "010100000100000111000111101000100100",       "010100000110100111110010100000100101",       "010100001001001000110001101000100101", 
      "010100001011101010000100110000100101",       "010100001110001011101100001000100101",       "010100010000101101100111101000100110",       "010100010011001111110111100000100110", 
      "010100010101110010011011101000100110",       "010100011000010101010100001000100111",       "010100011010111000100001000000100111",       "010100011101011100000010010000100111", 
      "010100011111111111111000000000100111",       "010100100010100100000010010000101000",       "010100100101001000100001000000101000",       "010100100111101101010100011000101000", 
      "010100101010010010011100011000101001",       "010100101100110111111001000000101001",       "010100101111011101101010011000101001",       "010100110010000011110000100000101001", 
      "010100110100101010001011011000101010",       "010100110111010000111011000000101010",       "010100111001110111111111101000101010",       "010100111100011111011001000000101011", 
      "010100111111000111000111100000101011",       "010101000001101111001010111000101011",       "010101000100010111100011010000101011",       "010101000111000000010000110000101100", 
      "010101001001101001010011011000101100",       "010101001100010010101011001000101100",       "010101001110111100011000000000101101",       "010101010001100110011010001000101101", 
      "010101010100010000110001101000101101",       "010101010110111011011110011000101101",       "010101011001100110100000100000101110",       "010101011100010001111000000000101110", 
      "010101011110111101100101000000101110",       "010101100001101001100111011000101111",       "010101100100010101111111011000101111",       "010101100111000010101101000000101111", 
      "010101101001101111110000001000110000",       "010101101100011101001000111000110000",       "010101101111001010110111011000110000",       "010101110001111000111011101000110000", 
      "010101110100100111010101101000110001",       "010101110111010110000101100000110001",       "010101111010000101001011001000110001",       "010101111100110100100110110000110010", 
      "010101111111100100011000011000110010",       "010110000010010100011111111000110010",       "010110000101000100111101100000110011",       "010110000111110101110001001000110011", 
      "010110001010100110111011000000110011",       "010110001101011000011010111000110100",       "010110010000001010010001000000110100",       "010110010010111100011101100000110100", 
      "010110010101101111000000001000110101",       "010110011000100001111001010000110101",       "010110011011010101001000101000110101",       "010110011110001000101110100000110110", 
      "010110100000111100101010111000110110",       "010110100011110000111101110000110110",       "010110100110100101100111001000110110",       "010110101001011010100111001000110111", 
      "010110101100001111111101110000110111",       "010110101111000101101011001000110111",       "010110110001111011101111010000111000",       "010110110100110010001010001000111000", 
      "010110110111101000111011110000111000",       "010110111010100000000100011000111001",       "010110111101010111100011111000111001",       "010111000000001111011010010000111001", 
      "010111000011000111100111101000111010",       "010111000110000000001100001000111010",       "010111001000111001000111110000111010",       "010111001011110010011010100000111011", 
      "010111001110101100000100011000111011",       "010111010001100110000101100000111011",       "010111010100100000011101111000111100",       "010111010111011011001101101000111100", 
      "010111011010010110010100110000111101",       "010111011101010001110011010000111101",       "010111100000001101101001001000111101",       "010111100011001001110110101000111110", 
      "010111100110000110011011101000111110",       "010111101001000011011000010000111110",       "010111101100000000101100100000111111",       "010111101110111110011000100000111111", 
      "010111110001111100011100001000111111",       "010111110100111010110111101001000000",       "010111110111111001101010111001000000",       "010111111010111000110110000001000000", 
      "010111111101111000011001001001000001",       "011000000000111000010100001001000001",       "011000000011111000100111001001000001",       "011000000110111001010010010001000010", 
      "011000001001111010010101100001000010",       "011000001100111011110000111001000011",       "011000001111111101100100100001000011",       "011000010010111111110000010001000011", 
      "011000010110000010010100011001000100",       "011000011001000101010000111001000100",       "011000011100001000100101110001000100",       "011000011111001100010011001001000101", 
      "011000100010010000011000111001000101",       "011000100101010100110111001001000101",       "011000101000011001101110001001000110",       "011000101011011110111101101001000110", 
      "011000101110100100100101111001000111",       "011000110001101010100110110001000111",       "011000110100110001000000100001000111",       "011000110111110111110011000001001000", 
      "011000111010111110111110100001001000",       "011000111110000110100010111001001000",       "011001000001001110100000001001001001",       "011001000100010110110110100001001001", 
      "011001000111011111100101111001001010",       "011001001010101000101110011001001010",       "011001001101110010010000000001001010",       "011001010000111100001011000001001011", 
      "011001010100000110011111001001001011",       "011001010111010001001100101001001011",       "011001011010011100010011011001001100",       "011001011101100111110011101001001100", 
      "011001100000110011101101011001001101",       "011001100100000000000000101001001101",       "011001100111001100101101011001001101",       "011001101010011001110011111001001110", 
      "011001101101100111010011111001001110",       "011001110000110101001101101001001111",       "011001110100000011100001010001001111",       "011001110111010010001110101001001111", 
      "011001111010100001010101110001010000",       "011001111101110000110110111001010000",       "011010000001000000110010000001010001",       "011010000100010001000111001001010001", 
      "011010000111100001110110010001010001",       "011010001010110010111111101001010010",       "011010001110000100100011001001010010",       "011010010001010110100000110001010011", 
      "011010010100101000111000110001010011",       "011010010111111011101011000001010011",       "011010011011001110110111101001010100",       "011010011110100010011110110001010100", 
      "011010100001110110100000010001010101",       "011010100101001010111100011001010101",       "011010101000011111110011000001010101",       "011010101011110101000100011001010110", 
      "011010101111001010110000011001010110",       "011010110010100000110111000001010111",       "011010110101110111011000101001010111",       "011010111001001110010100111001010111", 
      "011010111100100101101100001001011000",       "011010111111111101011110011001011000",       "011011000011010101101011100001011001",       "011011000110101110010011110001011001", 
      "011011001010000111010111001001011001",       "011011001101100000110101101001011010",       "011011010000111010101111011001011010",       "011011010100010101000100011001011011", 
      "011011010111101111110100101001011011",       "011011011011001011000000011001011100",       "011011011110100110100111011001011100",       "011011100010000010101010000001011100", 
      "011011100101011111001000001001011101",       "011011101000111100000001110001011101",       "011011101100011001010111001001011110",       "011011101111110111001000001001011110", 
      "011011110011010101010100111001011111",       "011011110110110011111101100001011111",       "011011111010010011000010000001011111",       "011011111101110010100010010001100000", 
      "011100000001010010011110101001100000",       "011100000100110010110110111001100001",       "011100001000010011101011011001100001",       "011100001011110100111011111001100010", 
      "011100001111010110101000101001100010",       "011100010010111000110001100001100010",       "011100010110011011010110110001100011",       "011100011001111110011000011001100011", 
      "011100011101100001110110011001100100",       "011100100001000101110000111001100100",       "011100100100101010000111111001100101",       "011100101000001110111011011001100101", 
      "011100101011110100001011101001100110",       "011100101111011001111000100001100110",       "011100110011000000000010001001100110",       "011100110110100110101000100001100111", 
      "011100111010001101101011110001100111",       "011100111101110101001100000001101000",       "011101000001011101001001001001101000",       "011101000101000101100011010001101001", 
      "011101001000101110011010100001101001",       "011101001100010111101110111001101010",       "011101010000000001100000100001101010",       "011101010011101011101111010001101011", 
      "011101010111010110011011011001101011",       "011101011011000001100100111001101011",       "011101011110101101001011111001101100",       "011101100010011001010000010001101100", 
      "011101100110000101110010001001101101",       "011101101001110010110001101001101101",       "011101101101100000001110111001101110",       "011101110001001110001001110001101110", 
      "011101110100111100100010011001101111",       "011101111000101011011000111001101111",       "011101111100011010101101010001110000",       "011110000000001010011111101001110000", 
      "011110000011111010101111111001110001",       "011110000111101011011110011001110001",       "011110001011011100101010111001110010",       "011110001111001110010101100001110010", 
      "011110010011000000011110011001110010",       "011110010110110011000101101001110011",       "011110011010100110001011010001110011",       "011110011110011001101111010001110100", 
      "011110100010001101110001101001110100",       "011110100110000010010010101001110101",       "011110101001110111010010010001110101",       "011110101101101100110000100001110110", 
      "011110110001100010101101100001110110",       "011110110101011001001001010001110111",       "011110111001010000000011110001110111",       "011110111101000111011101010001111000", 
      "011111000000111111010101101001111000",       "011111000100110111101101001001111001",       "011111001000110000100011101001111001",       "011111001100101001111001010001111010", 
      "011111010000100011101110001001111010",       "011111010100011110000010010001111011",       "011111011000011000110101101001111011",       "011111011100010100001000100001111100", 
      "011111100000001111111010101001111100",       "011111100100001100001100100001111101",       "011111101000001000111101110001111101",       "011111101100000110001110110001111110", 
      "011111110000000011111111011001111110",       "011111110100000010001111110001111111",       "011111111000000000111111111001111111",       "011111111100000000010000000010000000" 
)
;
begin
          Y1 <= memVar(conv_integer(X1)); 
          Y2 <= memVar(conv_integer(X2)); 
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_18_f210_uid72
--                    (IntAdderAlternative_18_F210_uid76)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_18_f210_uid72 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(17 downto 0);
          Y : in  std_logic_vector(17 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(17 downto 0)   );
end entity;

architecture arch of IntAdder_18_f210_uid72 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_18_f210_uid80
--                    (IntAdderAlternative_18_F210_uid84)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_18_f210_uid80 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(17 downto 0);
          Y : in  std_logic_vector(17 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(17 downto 0)   );
end entity;

architecture arch of IntAdder_18_f210_uid80 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_21_f210_uid99
--                     (IntAdderClassical_21_F210_uid101)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_21_f210_uid99 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(20 downto 0);
          Y : in  std_logic_vector(20 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(20 downto 0)   );
end entity;

architecture arch of IntAdder_21_f210_uid99 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--            IntMultiplier_UsingDSP_17_18_19_unsigned_F210_uid88
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_17_18_19_unsigned_F210_uid88 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(16 downto 0);
          Y : in  std_logic_vector(17 downto 0);
          R : out  std_logic_vector(18 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_17_18_19_unsigned_F210_uid88 is
   component IntAdder_21_f210_uid99 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(20 downto 0);
             Y : in  std_logic_vector(20 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(20 downto 0)   );
   end component;

signal XX_m89 :  std_logic_vector(17 downto 0);
signal YY_m89 :  std_logic_vector(16 downto 0);
signal DSP_Res_87 :  std_logic_vector(42 downto 0);
signal heap_bh90_w0_0 :  std_logic;
signal heap_bh90_w1_0 :  std_logic;
signal heap_bh90_w2_0 :  std_logic;
signal heap_bh90_w3_0 :  std_logic;
signal heap_bh90_w4_0, heap_bh90_w4_0_d1 :  std_logic;
signal heap_bh90_w5_0, heap_bh90_w5_0_d1 :  std_logic;
signal heap_bh90_w6_0, heap_bh90_w6_0_d1 :  std_logic;
signal heap_bh90_w7_0, heap_bh90_w7_0_d1 :  std_logic;
signal heap_bh90_w8_0, heap_bh90_w8_0_d1 :  std_logic;
signal heap_bh90_w9_0, heap_bh90_w9_0_d1 :  std_logic;
signal heap_bh90_w10_0, heap_bh90_w10_0_d1 :  std_logic;
signal heap_bh90_w11_0, heap_bh90_w11_0_d1 :  std_logic;
signal heap_bh90_w12_0, heap_bh90_w12_0_d1 :  std_logic;
signal heap_bh90_w13_0, heap_bh90_w13_0_d1 :  std_logic;
signal heap_bh90_w14_0, heap_bh90_w14_0_d1 :  std_logic;
signal heap_bh90_w15_0, heap_bh90_w15_0_d1 :  std_logic;
signal heap_bh90_w16_0, heap_bh90_w16_0_d1 :  std_logic;
signal heap_bh90_w17_0, heap_bh90_w17_0_d1 :  std_logic;
signal heap_bh90_w18_0, heap_bh90_w18_0_d1 :  std_logic;
signal heap_bh90_w19_0, heap_bh90_w19_0_d1 :  std_logic;
signal heap_bh90_w20_0, heap_bh90_w20_0_d1 :  std_logic;
signal heap_bh90_w21_0, heap_bh90_w21_0_d1 :  std_logic;
signal heap_bh90_w22_0, heap_bh90_w22_0_d1 :  std_logic;
signal heap_bh90_w23_0, heap_bh90_w23_0_d1 :  std_logic;
signal heap_bh90_w4_1, heap_bh90_w4_1_d1 :  std_logic;
signal finalAdderIn0_bh90 :  std_logic_vector(20 downto 0);
signal finalAdderIn1_bh90 :  std_logic_vector(20 downto 0);
signal finalAdderCin_bh90 :  std_logic;
signal finalAdderOut_bh90 :  std_logic_vector(20 downto 0);
signal tempR_bh90_0, tempR_bh90_0_d1 :  std_logic_vector(3 downto 0);
signal CompressionResult90 :  std_logic_vector(24 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh90_w4_0_d1 <=  heap_bh90_w4_0;
            heap_bh90_w5_0_d1 <=  heap_bh90_w5_0;
            heap_bh90_w6_0_d1 <=  heap_bh90_w6_0;
            heap_bh90_w7_0_d1 <=  heap_bh90_w7_0;
            heap_bh90_w8_0_d1 <=  heap_bh90_w8_0;
            heap_bh90_w9_0_d1 <=  heap_bh90_w9_0;
            heap_bh90_w10_0_d1 <=  heap_bh90_w10_0;
            heap_bh90_w11_0_d1 <=  heap_bh90_w11_0;
            heap_bh90_w12_0_d1 <=  heap_bh90_w12_0;
            heap_bh90_w13_0_d1 <=  heap_bh90_w13_0;
            heap_bh90_w14_0_d1 <=  heap_bh90_w14_0;
            heap_bh90_w15_0_d1 <=  heap_bh90_w15_0;
            heap_bh90_w16_0_d1 <=  heap_bh90_w16_0;
            heap_bh90_w17_0_d1 <=  heap_bh90_w17_0;
            heap_bh90_w18_0_d1 <=  heap_bh90_w18_0;
            heap_bh90_w19_0_d1 <=  heap_bh90_w19_0;
            heap_bh90_w20_0_d1 <=  heap_bh90_w20_0;
            heap_bh90_w21_0_d1 <=  heap_bh90_w21_0;
            heap_bh90_w22_0_d1 <=  heap_bh90_w22_0;
            heap_bh90_w23_0_d1 <=  heap_bh90_w23_0;
            heap_bh90_w4_1_d1 <=  heap_bh90_w4_1;
            tempR_bh90_0_d1 <=  tempR_bh90_0;
         end if;
      end process;
   XX_m89 <= Y ;
   YY_m89 <= X ;
   DSP_Res_87 <=  std_logic_vector(unsigned'(unsigned("0000000" & XX_m89) * unsigned("0" & YY_m89)));
   heap_bh90_w0_0 <= DSP_Res_87(11); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w1_0 <= DSP_Res_87(12); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w2_0 <= DSP_Res_87(13); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w3_0 <= DSP_Res_87(14); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w4_0 <= DSP_Res_87(15); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w5_0 <= DSP_Res_87(16); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w6_0 <= DSP_Res_87(17); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w7_0 <= DSP_Res_87(18); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w8_0 <= DSP_Res_87(19); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w9_0 <= DSP_Res_87(20); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w10_0 <= DSP_Res_87(21); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w11_0 <= DSP_Res_87(22); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w12_0 <= DSP_Res_87(23); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w13_0 <= DSP_Res_87(24); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w14_0 <= DSP_Res_87(25); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w15_0 <= DSP_Res_87(26); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w16_0 <= DSP_Res_87(27); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w17_0 <= DSP_Res_87(28); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w18_0 <= DSP_Res_87(29); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w19_0 <= DSP_Res_87(30); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w20_0 <= DSP_Res_87(31); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w21_0 <= DSP_Res_87(32); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w22_0 <= DSP_Res_87(33); -- cycle= 0 cp= 2.823e-09
   heap_bh90_w23_0 <= DSP_Res_87(34); -- cycle= 0 cp= 2.823e-09
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh90_w4_1 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   finalAdderIn0_bh90 <= "0" & heap_bh90_w23_0_d1 & heap_bh90_w22_0_d1 & heap_bh90_w21_0_d1 & heap_bh90_w20_0_d1 & heap_bh90_w19_0_d1 & heap_bh90_w18_0_d1 & heap_bh90_w17_0_d1 & heap_bh90_w16_0_d1 & heap_bh90_w15_0_d1 & heap_bh90_w14_0_d1 & heap_bh90_w13_0_d1 & heap_bh90_w12_0_d1 & heap_bh90_w11_0_d1 & heap_bh90_w10_0_d1 & heap_bh90_w9_0_d1 & heap_bh90_w8_0_d1 & heap_bh90_w7_0_d1 & heap_bh90_w6_0_d1 & heap_bh90_w5_0_d1 & heap_bh90_w4_1_d1;
   finalAdderIn1_bh90 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh90_w4_0_d1;
   finalAdderCin_bh90 <= '0';
   Adder_final90_0: IntAdder_21_f210_uid99  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh90,
                 R => finalAdderOut_bh90   ,
                 X => finalAdderIn0_bh90,
                 Y => finalAdderIn1_bh90);
   ----------------Synchro barrier, entering cycle 0----------------
   tempR_bh90_0 <= heap_bh90_w3_0 & heap_bh90_w2_0 & heap_bh90_w1_0 & heap_bh90_w0_0; -- already compressed
   -- concatenate all the compressed chunks
   ----------------Synchro barrier, entering cycle 1----------------
   CompressionResult90 <= finalAdderOut_bh90 & tempR_bh90_0_d1;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult90(23 downto 5);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_27_f210_uid109
--                     (IntAdderClassical_27_F210_uid111)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_27_f210_uid109 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(26 downto 0);
          Y : in  std_logic_vector(26 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(26 downto 0)   );
end entity;

architecture arch of IntAdder_27_f210_uid109 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_33_f210_uid117
--                    (IntAdderAlternative_33_F210_uid121)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_33_f210_uid117 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(32 downto 0);
          Y : in  std_logic_vector(32 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(32 downto 0)   );
end entity;

architecture arch of IntAdder_33_f210_uid117 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                                   FPExp
--                           (FPExp_8_23_F210_uid2)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, Bogdan Pasca (2008-2013)
--------------------------------------------------------------------------------
-- Pipeline depth: 6 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPExp is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8+23+2 downto 0);
          R : out  std_logic_vector(8+23+2 downto 0)   );
end entity;

architecture arch of FPExp is
   component LeftShifter_24_by_max_33_F210_uid4 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(23 downto 0);
             S : in  std_logic_vector(5 downto 0);
             R : out  std_logic_vector(56 downto 0)   );
   end component;

   component FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(9 downto 0);
             R : out  std_logic_vector(7 downto 0)   );
   end component;

   component FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             R : out  std_logic_vector(33 downto 0)   );
   end component;

   component IntAdder_26_f231_uid60 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(25 downto 0);
             Y : in  std_logic_vector(25 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(25 downto 0)   );
   end component;

   component MagicSPExpTable_F210_uid68 is
      port ( X1 : in  std_logic_vector(8 downto 0);
             Y1 : out  std_logic_vector(35 downto 0);
             X2 : in  std_logic_vector(8 downto 0);
             Y2 : out  std_logic_vector(35 downto 0)   );
   end component;

   component IntAdder_18_f210_uid72 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(17 downto 0);
             Y : in  std_logic_vector(17 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(17 downto 0)   );
   end component;

   component IntAdder_18_f210_uid80 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(17 downto 0);
             Y : in  std_logic_vector(17 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(17 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_17_18_19_unsigned_F210_uid88 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(16 downto 0);
             Y : in  std_logic_vector(17 downto 0);
             R : out  std_logic_vector(18 downto 0)   );
   end component;

   component IntAdder_27_f210_uid109 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(26 downto 0);
             Y : in  std_logic_vector(26 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(26 downto 0)   );
   end component;

   component IntAdder_33_f210_uid117 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(32 downto 0);
             Y : in  std_logic_vector(32 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(32 downto 0)   );
   end component;

signal Xexn, Xexn_d1, Xexn_d2, Xexn_d3, Xexn_d4, Xexn_d5, Xexn_d6 :  std_logic_vector(1 downto 0);
signal XSign, XSign_d1, XSign_d2, XSign_d3, XSign_d4, XSign_d5, XSign_d6 :  std_logic;
signal XexpField :  std_logic_vector(7 downto 0);
signal Xfrac :  std_logic_vector(22 downto 0);
signal e0 :  std_logic_vector(9 downto 0);
signal shiftVal :  std_logic_vector(9 downto 0);
signal resultWillBeOne, resultWillBeOne_d1 :  std_logic;
signal mXu :  std_logic_vector(23 downto 0);
signal oufl0, oufl0_d1, oufl0_d2, oufl0_d3, oufl0_d4, oufl0_d5, oufl0_d6 :  std_logic;
signal shiftValIn :  std_logic_vector(5 downto 0);
signal fixX0 :  std_logic_vector(56 downto 0);
signal fixX :  std_logic_vector(33 downto 0);
signal xMulIn :  std_logic_vector(9 downto 0);
signal absK :  std_logic_vector(7 downto 0);
signal minusAbsK :  std_logic_vector(8 downto 0);
signal K, K_d1, K_d2, K_d3, K_d4 :  std_logic_vector(8 downto 0);
signal absKLog2 :  std_logic_vector(33 downto 0);
signal subOp1 :  std_logic_vector(25 downto 0);
signal subOp2 :  std_logic_vector(25 downto 0);
signal Y :  std_logic_vector(25 downto 0);
signal Addr1 :  std_logic_vector(8 downto 0);
signal Z :  std_logic_vector(16 downto 0);
signal Addr2 :  std_logic_vector(8 downto 0);
signal expZ_output :  std_logic_vector(35 downto 0);
signal expA_output :  std_logic_vector(35 downto 0);
signal expA, expA_d1, expA_d2, expA_d3 :  std_logic_vector(26 downto 0);
signal expZmZm1 :  std_logic_vector(8 downto 0);
signal expZminus1X :  std_logic_vector(17 downto 0);
signal expZminus1Y :  std_logic_vector(17 downto 0);
signal expZminus1, expZminus1_d1 :  std_logic_vector(17 downto 0);
signal expArounded0 :  std_logic_vector(17 downto 0);
signal expArounded, expArounded_d1 :  std_logic_vector(16 downto 0);
signal lowerProduct, lowerProduct_d1 :  std_logic_vector(18 downto 0);
signal extendedLowerProduct :  std_logic_vector(26 downto 0);
signal expY :  std_logic_vector(26 downto 0);
signal needNoNorm :  std_logic;
signal preRoundBiasSig :  std_logic_vector(32 downto 0);
signal roundBit :  std_logic;
signal roundNormAddend :  std_logic_vector(32 downto 0);
signal roundedExpSigRes, roundedExpSigRes_d1 :  std_logic_vector(32 downto 0);
signal roundedExpSig :  std_logic_vector(32 downto 0);
signal ofl1 :  std_logic;
signal ofl2 :  std_logic;
signal ofl3 :  std_logic;
signal ofl :  std_logic;
signal ufl1 :  std_logic;
signal ufl2 :  std_logic;
signal ufl3 :  std_logic;
signal ufl :  std_logic;
signal Rexn :  std_logic_vector(1 downto 0);
constant g: positive := 3;
constant wE: positive := 8;
constant wF: positive := 23;
constant wFIn: positive := 23;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            Xexn_d1 <=  Xexn;
            Xexn_d2 <=  Xexn_d1;
            Xexn_d3 <=  Xexn_d2;
            Xexn_d4 <=  Xexn_d3;
            Xexn_d5 <=  Xexn_d4;
            Xexn_d6 <=  Xexn_d5;
            XSign_d1 <=  XSign;
            XSign_d2 <=  XSign_d1;
            XSign_d3 <=  XSign_d2;
            XSign_d4 <=  XSign_d3;
            XSign_d5 <=  XSign_d4;
            XSign_d6 <=  XSign_d5;
            resultWillBeOne_d1 <=  resultWillBeOne;
            oufl0_d1 <=  oufl0;
            oufl0_d2 <=  oufl0_d1;
            oufl0_d3 <=  oufl0_d2;
            oufl0_d4 <=  oufl0_d3;
            oufl0_d5 <=  oufl0_d4;
            oufl0_d6 <=  oufl0_d5;
            K_d1 <=  K;
            K_d2 <=  K_d1;
            K_d3 <=  K_d2;
            K_d4 <=  K_d3;
            expA_d1 <=  expA;
            expA_d2 <=  expA_d1;
            expA_d3 <=  expA_d2;
            expZminus1_d1 <=  expZminus1;
            expArounded_d1 <=  expArounded;
            lowerProduct_d1 <=  lowerProduct;
            roundedExpSigRes_d1 <=  roundedExpSigRes;
         end if;
      end process;
   Xexn <= X(wE+wFIn+2 downto wE+wFIn+1);
   XSign <= X(wE+wFIn);
   XexpField <= X(wE+wFIn-1 downto wFIn);
   Xfrac <= X(wFIn-1 downto 0);
   e0 <= conv_std_logic_vector(101, wE+2);  -- bias - (wF+g)
   shiftVal <= ("00" & XexpField) - e0; -- for a left shift
   -- underflow when input is shifted to zero (shiftval<0), in which case exp = 1
   resultWillBeOne <= shiftVal(wE+1);
   --  mantissa with implicit bit
   mXu <= "1" & Xfrac;
   -- Partial overflow/underflow detection
   oufl0 <= not shiftVal(wE+1) when shiftVal(wE downto 0) >= conv_std_logic_vector(33, wE+1) else '0';
   ---------------- cycle 0----------------
   shiftValIn <= shiftVal(5 downto 0);
   mantissa_shift: LeftShifter_24_by_max_33_F210_uid4  -- pipelineDepth=1 maxInDelay=2.43272e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => fixX0,
                 S => shiftValIn,
                 X => mXu);
   ----------------Synchro barrier, entering cycle 1----------------
   fixX <=  fixX0(56 downto 23)when resultWillBeOne_d1='0' else "0000000000000000000000000000000000";
   xMulIn <=  fixX(32 downto 23); -- truncation, error 2^-3
   mulInvLog2: FixRealKCM_6_M3_0_1_log_2_unsigned_F210_uid8  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absK,
                 X => xMulIn);
   minusAbsK <= (8 downto 0 => '0') - ('0' & absK);
   K <= minusAbsK when  XSign_d1='1'   else ('0' & absK);
   ---------------- cycle 1----------------
   mulLog2: FixRealKCM_7_0_M26_log_2_unsigned_F210_uid34  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absKLog2,
                 X => absK);
   subOp1 <= fixX(25 downto 0) when XSign_d1='0' else not (fixX(25 downto 0));
   subOp2 <= absKLog2(25 downto 0) when XSign_d1='1' else not (absKLog2(25 downto 0));
   theYAdder: IntAdder_26_f231_uid60  -- pipelineDepth=1 maxInDelay=3.31116e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '1',
                 R => Y,
                 X => subOp1,
                 Y => subOp2);

   ----------------Synchro barrier, entering cycle 2----------------
   -- Now compute the exp of this fixed-point value
   Addr1 <= Y(25 downto 17);
   Z <= Y(16 downto 0);
   Addr2 <= Z(16 downto 8);
   table: MagicSPExpTable_F210_uid68
      port map ( X1 => Addr1,
                 X2 => Addr2,
                 Y1 => expA_output,
                 Y2 => expZ_output);
   expA <=  expA_output(35 downto 9);
   expZmZm1 <= expZ_output(8 downto 0);
   -- Computing Z + (exp(Z)-1-Z)
   expZminus1X <= '0' & Z;
   expZminus1Y <= (17 downto 9 => '0') & expZmZm1 ;
   Adder_expZminus1: IntAdder_18_f210_uid72  -- pipelineDepth=0 maxInDelay=2.19472e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => expZminus1,
                 X => expZminus1X,
                 Y => expZminus1Y);
   ---------------- cycle 2----------------
   -- Rounding expA to the same accuracy as expZminus1
   --   (truncation would not be accurate enough and require one more guard bit)
   Adder_expArounded0: IntAdder_18_f210_uid80  -- pipelineDepth=0 maxInDelay=2.186e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1' ,
                 R => expArounded0,
                 X => expA(26 downto 9),
                 Y => "000000000000000000");
   expArounded <= expArounded0(17 downto 1);
   ----------------Synchro barrier, entering cycle 3----------------
   TheLowerProduct: IntMultiplier_UsingDSP_17_18_19_unsigned_F210_uid88  -- pipelineDepth=1 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => lowerProduct,
                 X => expArounded_d1,
                 Y => expZminus1_d1);

   ----------------Synchro barrier, entering cycle 4----------------
   ----------------Synchro barrier, entering cycle 5----------------
   extendedLowerProduct <= ((26 downto 19 => '0') & lowerProduct_d1(18 downto 0));
   -- Final addition -- the product MSB bit weight is -k+2 = -7
   TheFinalAdder: IntAdder_27_f210_uid109  -- pipelineDepth=0 maxInDelay=4.4472e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => expY,
                 X => expA_d3,
                 Y => extendedLowerProduct);

   needNoNorm <= expY(26);
   -- Rounding: all this should consume one row of LUTs
   preRoundBiasSig <= conv_std_logic_vector(127, wE+2)  & expY(25 downto 3) when needNoNorm = '1'
      else conv_std_logic_vector(126, wE+2)  & expY(24 downto 2) ;
   roundBit <= expY(2)  when needNoNorm = '1'    else expY(1) ;
   roundNormAddend <= K_d4(8) & K_d4 & (22 downto 1 => '0') & roundBit;
   roundedExpSigOperandAdder: IntAdder_33_f210_uid117  -- pipelineDepth=0 maxInDelay=2.54348e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => roundedExpSigRes,
                 X => preRoundBiasSig,
                 Y => roundNormAddend);

   -- delay at adder output is 3.97048e-09
   ----------------Synchro barrier, entering cycle 6----------------
   roundedExpSig <= roundedExpSigRes_d1 when Xexn_d6="01" else  "000" & (wE-2 downto 0 => '1') & (wF-1 downto 0 => '0');
   ofl1 <= not XSign_d6 and oufl0_d6 and (not Xexn_d6(1) and Xexn_d6(0)); -- input positive, normal,  very large
   ofl2 <= not XSign_d6 and (roundedExpSig(wE+wF) and not roundedExpSig(wE+wF+1)) and (not Xexn_d6(1) and Xexn_d6(0)); -- input positive, normal, overflowed
   ofl3 <= not XSign_d6 and Xexn_d6(1) and not Xexn_d6(0);  -- input was -infty
   ofl <= ofl1 or ofl2 or ofl3;
   ufl1 <= (roundedExpSig(wE+wF) and roundedExpSig(wE+wF+1))  and (not Xexn_d6(1) and Xexn_d6(0)); -- input normal
   ufl2 <= XSign_d6 and Xexn_d6(1) and not Xexn_d6(0);  -- input was -infty
   ufl3 <= XSign_d6 and oufl0_d6  and (not Xexn_d6(1) and Xexn_d6(0)); -- input negative, normal,  very large
   ufl <= ufl1 or ufl2 or ufl3;
   Rexn <= "11" when Xexn_d6 = "11"
      else "10" when ofl='1'
      else "00" when ufl='1'
      else "01";
   R <= Rexn & '0' & roundedExpSig(30 downto 0);
end architecture;

