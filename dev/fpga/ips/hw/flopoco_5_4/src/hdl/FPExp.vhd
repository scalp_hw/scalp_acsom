--------------------------------------------------------------------------------
--                     LeftShifter_5_by_max_10_F210_uid4
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_5_by_max_10_F210_uid4 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(4 downto 0);
          S : in  std_logic_vector(3 downto 0);
          R : out  std_logic_vector(14 downto 0)   );
end entity;

architecture arch of LeftShifter_5_by_max_10_F210_uid4 is
signal level0 :  std_logic_vector(4 downto 0);
signal ps :  std_logic_vector(3 downto 0);
signal level1 :  std_logic_vector(5 downto 0);
signal level2 :  std_logic_vector(7 downto 0);
signal level3 :  std_logic_vector(11 downto 0);
signal level4 :  std_logic_vector(19 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<= level0 & (0 downto 0 => '0') when ps(0)= '1' else     (0 downto 0 => '0') & level0;
   level2<= level1 & (1 downto 0 => '0') when ps(1)= '1' else     (1 downto 0 => '0') & level1;
   level3<= level2 & (3 downto 0 => '0') when ps(2)= '1' else     (3 downto 0 => '0') & level2;
   level4<= level3 & (7 downto 0 => '0') when ps(3)= '1' else     (7 downto 0 => '0') & level3;
   R <= level4(14 downto 0);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(6 downto 0);
          Y : out  std_logic_vector(7 downto 0)   );
end entity;

architecture arch of FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
signal TableOut :  std_logic_vector(7 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "00000100" when "0000000",
   "00000101" when "0000001",
   "00000111" when "0000010",
   "00001000" when "0000011",
   "00001010" when "0000100",
   "00001011" when "0000101",
   "00001101" when "0000110",
   "00001110" when "0000111",
   "00010000" when "0001000",
   "00010001" when "0001001",
   "00010010" when "0001010",
   "00010100" when "0001011",
   "00010101" when "0001100",
   "00010111" when "0001101",
   "00011000" when "0001110",
   "00011010" when "0001111",
   "00011011" when "0010000",
   "00011101" when "0010001",
   "00011110" when "0010010",
   "00011111" when "0010011",
   "00100001" when "0010100",
   "00100010" when "0010101",
   "00100100" when "0010110",
   "00100101" when "0010111",
   "00100111" when "0011000",
   "00101000" when "0011001",
   "00101010" when "0011010",
   "00101011" when "0011011",
   "00101100" when "0011100",
   "00101110" when "0011101",
   "00101111" when "0011110",
   "00110001" when "0011111",
   "00110010" when "0100000",
   "00110100" when "0100001",
   "00110101" when "0100010",
   "00110110" when "0100011",
   "00111000" when "0100100",
   "00111001" when "0100101",
   "00111011" when "0100110",
   "00111100" when "0100111",
   "00111110" when "0101000",
   "00111111" when "0101001",
   "01000001" when "0101010",
   "01000010" when "0101011",
   "01000011" when "0101100",
   "01000101" when "0101101",
   "01000110" when "0101110",
   "01001000" when "0101111",
   "01001001" when "0110000",
   "01001011" when "0110001",
   "01001100" when "0110010",
   "01001110" when "0110011",
   "01001111" when "0110100",
   "01010000" when "0110101",
   "01010010" when "0110110",
   "01010011" when "0110111",
   "01010101" when "0111000",
   "01010110" when "0111001",
   "01011000" when "0111010",
   "01011001" when "0111011",
   "01011011" when "0111100",
   "01011100" when "0111101",
   "01011101" when "0111110",
   "01011111" when "0111111",
   "01100000" when "1000000",
   "01100010" when "1000001",
   "01100011" when "1000010",
   "01100101" when "1000011",
   "01100110" when "1000100",
   "01101000" when "1000101",
   "01101001" when "1000110",
   "01101010" when "1000111",
   "01101100" when "1001000",
   "01101101" when "1001001",
   "01101111" when "1001010",
   "01110000" when "1001011",
   "01110010" when "1001100",
   "01110011" when "1001101",
   "01110101" when "1001110",
   "01110110" when "1001111",
   "01110111" when "1010000",
   "01111001" when "1010001",
   "01111010" when "1010010",
   "01111100" when "1010011",
   "01111101" when "1010100",
   "01111111" when "1010101",
   "10000000" when "1010110",
   "10000010" when "1010111",
   "10000011" when "1011000",
   "10000100" when "1011001",
   "10000110" when "1011010",
   "10000111" when "1011011",
   "10001001" when "1011100",
   "10001010" when "1011101",
   "10001100" when "1011110",
   "10001101" when "1011111",
   "10001110" when "1100000",
   "10010000" when "1100001",
   "10010001" when "1100010",
   "10010011" when "1100011",
   "10010100" when "1100100",
   "10010110" when "1100101",
   "10010111" when "1100110",
   "10011001" when "1100111",
   "10011010" when "1101000",
   "10011011" when "1101001",
   "10011101" when "1101010",
   "10011110" when "1101011",
   "10100000" when "1101100",
   "10100001" when "1101101",
   "10100011" when "1101110",
   "10100100" when "1101111",
   "10100110" when "1110000",
   "10100111" when "1110001",
   "10101000" when "1110010",
   "10101010" when "1110011",
   "10101011" when "1110100",
   "10101101" when "1110101",
   "10101110" when "1110110",
   "10110000" when "1110111",
   "10110001" when "1111000",
   "10110011" when "1111001",
   "10110100" when "1111010",
   "10110101" when "1111011",
   "10110111" when "1111100",
   "10111000" when "1111101",
   "10111010" when "1111110",
   "10111011" when "1111111",
   "--------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(6 downto 0);
          R : out  std_logic_vector(4 downto 0)   );
end entity;

architecture arch of FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8 is
   component FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(6 downto 0);
             Y : out  std_logic_vector(7 downto 0)   );
   end component;

signal FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_A0 :  std_logic_vector(6 downto 0);
signal FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0 :  std_logic_vector(7 downto 0);
signal heap_bh9_w0_0 :  std_logic;
signal heap_bh9_w1_0 :  std_logic;
signal heap_bh9_w2_0 :  std_logic;
signal heap_bh9_w3_0 :  std_logic;
signal heap_bh9_w4_0 :  std_logic;
signal heap_bh9_w5_0 :  std_logic;
signal heap_bh9_w6_0 :  std_logic;
signal heap_bh9_w7_0 :  std_logic;
signal CompressionResult9 :  std_logic_vector(7 downto 0);
signal OutRes :  std_logic_vector(7 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_A0 <= X(6 downto 0); -- input address  m=3  l=-3
   FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_Table0: FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_A0,
                 Y => FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0);
   ---------------- cycle 0----------------
   heap_bh9_w0_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(0); -- cycle= 0 cp= 0
   heap_bh9_w1_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(1); -- cycle= 0 cp= 0
   heap_bh9_w2_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(2); -- cycle= 0 cp= 0
   heap_bh9_w3_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(3); -- cycle= 0 cp= 0
   heap_bh9_w4_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(4); -- cycle= 0 cp= 0
   heap_bh9_w5_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(5); -- cycle= 0 cp= 0
   heap_bh9_w6_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(6); -- cycle= 0 cp= 0
   heap_bh9_w7_0 <= FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8_T0(7); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   CompressionResult9 <= heap_bh9_w7_0 & heap_bh9_w6_0 & heap_bh9_w5_0 & heap_bh9_w4_0 & heap_bh9_w3_0 & heap_bh9_w2_0 & heap_bh9_w1_0 & heap_bh9_w0_0;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult9(7 downto 0);
   R <= OutRes(7 downto 3);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(4 downto 0);
          Y : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_Table_0 is
signal TableOut :  std_logic_vector(10 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "00000000000" when "00000",
   "00000101100" when "00001",
   "00001011001" when "00010",
   "00010000101" when "00011",
   "00010110001" when "00100",
   "00011011110" when "00101",
   "00100001010" when "00110",
   "00100110111" when "00111",
   "00101100011" when "01000",
   "00110001111" when "01001",
   "00110111100" when "01010",
   "00111101000" when "01011",
   "01000010100" when "01100",
   "01001000001" when "01101",
   "01001101101" when "01110",
   "01010011001" when "01111",
   "01011000110" when "10000",
   "01011110010" when "10001",
   "01100011111" when "10010",
   "01101001011" when "10011",
   "01101110111" when "10100",
   "01110100100" when "10101",
   "01111010000" when "10110",
   "01111111100" when "10111",
   "10000101001" when "11000",
   "10001010101" when "11001",
   "10010000001" when "11010",
   "10010101110" when "11011",
   "10011011010" when "11100",
   "10100000110" when "11101",
   "10100110011" when "11110",
   "10101011111" when "11111",
   "-----------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(4 downto 0);
          R : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16 is
   component FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(4 downto 0);
             Y : out  std_logic_vector(10 downto 0)   );
   end component;

signal FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_A0 :  std_logic_vector(4 downto 0);
signal FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0 :  std_logic_vector(10 downto 0);
signal heap_bh17_w0_0 :  std_logic;
signal heap_bh17_w1_0 :  std_logic;
signal heap_bh17_w2_0 :  std_logic;
signal heap_bh17_w3_0 :  std_logic;
signal heap_bh17_w4_0 :  std_logic;
signal heap_bh17_w5_0 :  std_logic;
signal heap_bh17_w6_0 :  std_logic;
signal heap_bh17_w7_0 :  std_logic;
signal heap_bh17_w8_0 :  std_logic;
signal heap_bh17_w9_0 :  std_logic;
signal heap_bh17_w10_0 :  std_logic;
signal CompressionResult17 :  std_logic_vector(10 downto 0);
signal OutRes :  std_logic_vector(10 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_A0 <= X(4 downto 0); -- input address  m=4  l=0
   FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_Table0: FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_A0,
                 Y => FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0);
   ---------------- cycle 0----------------
   heap_bh17_w0_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(0); -- cycle= 0 cp= 0
   heap_bh17_w1_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(1); -- cycle= 0 cp= 0
   heap_bh17_w2_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(2); -- cycle= 0 cp= 0
   heap_bh17_w3_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(3); -- cycle= 0 cp= 0
   heap_bh17_w4_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(4); -- cycle= 0 cp= 0
   heap_bh17_w5_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(5); -- cycle= 0 cp= 0
   heap_bh17_w6_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(6); -- cycle= 0 cp= 0
   heap_bh17_w7_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(7); -- cycle= 0 cp= 0
   heap_bh17_w8_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(8); -- cycle= 0 cp= 0
   heap_bh17_w9_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(9); -- cycle= 0 cp= 0
   heap_bh17_w10_0 <= FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16_T0(10); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   CompressionResult17 <= heap_bh17_w10_0 & heap_bh17_w9_0 & heap_bh17_w8_0 & heap_bh17_w7_0 & heap_bh17_w6_0 & heap_bh17_w5_0 & heap_bh17_w4_0 & heap_bh17_w3_0 & heap_bh17_w2_0 & heap_bh17_w1_0 & heap_bh17_w0_0;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult17(10 downto 0);
   R <= OutRes(10 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_6_f231_uid24
--                     (IntAdderAlternative_6_F231_uid28)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_6_f231_uid24 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : in  std_logic_vector(5 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(5 downto 0)   );
end entity;

architecture arch of IntAdder_6_f231_uid24 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                          ExpYTable_6_9_F210_uid32
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity ExpYTable_6_9_F210_uid32 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(8 downto 0)   );
end entity;

architecture arch of ExpYTable_6_9_F210_uid32 is
signal TableOut :  std_logic_vector(8 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "100000000" when "000000",
   "100000100" when "000001",
   "100001000" when "000010",
   "100001100" when "000011",
   "100010001" when "000100",
   "100010101" when "000101",
   "100011001" when "000110",
   "100011110" when "000111",
   "100100010" when "001000",
   "100100111" when "001001",
   "100101011" when "001010",
   "100110000" when "001011",
   "100110101" when "001100",
   "100111010" when "001101",
   "100111111" when "001110",
   "101000100" when "001111",
   "101001001" when "010000",
   "101001110" when "010001",
   "101010011" when "010010",
   "101011000" when "010011",
   "101011110" when "010100",
   "101100011" when "010101",
   "101101001" when "010110",
   "101101111" when "010111",
   "101110100" when "011000",
   "101111010" when "011001",
   "110000000" when "011010",
   "110000110" when "011011",
   "110001101" when "011100",
   "110010011" when "011101",
   "110011001" when "011110",
   "110100000" when "011111",
   "010011011" when "100000",
   "010011110" when "100001",
   "010100000" when "100010",
   "010100011" when "100011",
   "010100101" when "100100",
   "010101000" when "100101",
   "010101011" when "100110",
   "010101101" when "100111",
   "010110000" when "101000",
   "010110011" when "101001",
   "010110110" when "101010",
   "010111000" when "101011",
   "010111011" when "101100",
   "010111110" when "101101",
   "011000001" when "101110",
   "011000100" when "101111",
   "011000111" when "110000",
   "011001011" when "110001",
   "011001110" when "110010",
   "011010001" when "110011",
   "011010100" when "110100",
   "011011000" when "110101",
   "011011011" when "110110",
   "011011110" when "110111",
   "011100010" when "111000",
   "011100101" when "111001",
   "011101001" when "111010",
   "011101101" when "111011",
   "011110000" when "111100",
   "011110100" when "111101",
   "011111000" when "111110",
   "011111100" when "111111",
   "---------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_11_f210_uid36
--                    (IntAdderAlternative_11_F210_uid40)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_11_f210_uid36 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(10 downto 0);
          Y : in  std_logic_vector(10 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of IntAdder_11_f210_uid36 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                                   FPExp
--                           (FPExp_5_4_F210_uid2)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, Bogdan Pasca (2008-2013)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPExp is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5+4+2 downto 0);
          R : out  std_logic_vector(5+4+2 downto 0)   );
end entity;

architecture arch of FPExp is
   component LeftShifter_5_by_max_10_F210_uid4 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(4 downto 0);
             S : in  std_logic_vector(3 downto 0);
             R : out  std_logic_vector(14 downto 0)   );
   end component;

   component FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(6 downto 0);
             R : out  std_logic_vector(4 downto 0)   );
   end component;

   component FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(4 downto 0);
             R : out  std_logic_vector(10 downto 0)   );
   end component;

   component IntAdder_6_f231_uid24 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : in  std_logic_vector(5 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(5 downto 0)   );
   end component;

   component ExpYTable_6_9_F210_uid32 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(8 downto 0)   );
   end component;

   component IntAdder_11_f210_uid36 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(10 downto 0);
             Y : in  std_logic_vector(10 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(10 downto 0)   );
   end component;

signal Xexn, Xexn_d1 :  std_logic_vector(1 downto 0);
signal XSign, XSign_d1 :  std_logic;
signal XexpField :  std_logic_vector(4 downto 0);
signal Xfrac :  std_logic_vector(3 downto 0);
signal e0 :  std_logic_vector(6 downto 0);
signal shiftVal :  std_logic_vector(6 downto 0);
signal resultWillBeOne :  std_logic;
signal mXu :  std_logic_vector(4 downto 0);
signal oufl0, oufl0_d1 :  std_logic;
signal shiftValIn :  std_logic_vector(3 downto 0);
signal fixX0 :  std_logic_vector(14 downto 0);
signal fixX :  std_logic_vector(10 downto 0);
signal xMulIn :  std_logic_vector(6 downto 0);
signal absK, absK_d1 :  std_logic_vector(4 downto 0);
signal minusAbsK :  std_logic_vector(5 downto 0);
signal K :  std_logic_vector(5 downto 0);
signal absKLog2 :  std_logic_vector(10 downto 0);
signal subOp1 :  std_logic_vector(5 downto 0);
signal subOp2 :  std_logic_vector(5 downto 0);
signal Y :  std_logic_vector(5 downto 0);
signal expY :  std_logic_vector(8 downto 0);
signal needNoNorm :  std_logic;
signal preRoundBiasSig :  std_logic_vector(10 downto 0);
signal roundBit :  std_logic;
signal roundNormAddend :  std_logic_vector(10 downto 0);
signal roundedExpSigRes :  std_logic_vector(10 downto 0);
signal roundedExpSig, roundedExpSig_d1 :  std_logic_vector(10 downto 0);
signal ofl1 :  std_logic;
signal ofl2 :  std_logic;
signal ofl3 :  std_logic;
signal ofl :  std_logic;
signal ufl1 :  std_logic;
signal ufl2 :  std_logic;
signal ufl3 :  std_logic;
signal ufl :  std_logic;
signal Rexn :  std_logic_vector(1 downto 0);
constant g: positive := 2;
constant wE: positive := 5;
constant wF: positive := 4;
constant wFIn: positive := 4;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            Xexn_d1 <=  Xexn;
            XSign_d1 <=  XSign;
            oufl0_d1 <=  oufl0;
            absK_d1 <=  absK;
            roundedExpSig_d1 <=  roundedExpSig;
         end if;
      end process;
   Xexn <= X(wE+wFIn+2 downto wE+wFIn+1);
   XSign <= X(wE+wFIn);
   XexpField <= X(wE+wFIn-1 downto wFIn);
   Xfrac <= X(wFIn-1 downto 0);
   e0 <= conv_std_logic_vector(9, wE+2);  -- bias - (wF+g)
   shiftVal <= ("00" & XexpField) - e0; -- for a left shift
   -- underflow when input is shifted to zero (shiftval<0), in which case exp = 1
   resultWillBeOne <= shiftVal(wE+1);
   --  mantissa with implicit bit
   mXu <= "1" & Xfrac;
   -- Partial overflow/underflow detection
   oufl0 <= not shiftVal(wE+1) when shiftVal(wE downto 0) >= conv_std_logic_vector(10, wE+1) else '0';
   ---------------- cycle 0----------------
   shiftValIn <= shiftVal(3 downto 0);
   mantissa_shift: LeftShifter_5_by_max_10_F210_uid4  -- pipelineDepth=0 maxInDelay=2.19804e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => fixX0,
                 S => shiftValIn,
                 X => mXu);
   fixX <=  fixX0(14 downto 4)when resultWillBeOne='0' else "00000000000";
   xMulIn <=  fixX(9 downto 3); -- truncation, error 2^-3
   mulInvLog2: FixRealKCM_3_M3_0_1_log_2_unsigned_F210_uid8  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absK,
                 X => xMulIn);
   ----------------Synchro barrier, entering cycle 1----------------
   minusAbsK <= (5 downto 0 => '0') - ('0' & absK_d1);
   K <= minusAbsK when  XSign_d1='1'   else ('0' & absK_d1);
   ---------------- cycle 0----------------
   mulLog2: FixRealKCM_4_0_M6_log_2_unsigned_F210_uid16  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absKLog2,
                 X => absK);
   subOp1 <= fixX(5 downto 0) when XSign='0' else not (fixX(5 downto 0));
   subOp2 <= absKLog2(5 downto 0) when XSign='1' else not (absKLog2(5 downto 0));
   theYAdder: IntAdder_6_f231_uid24  -- pipelineDepth=0 maxInDelay=9.7544e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '1',
                 R => Y,
                 X => subOp1,
                 Y => subOp2);

   -- Now compute the exp of this fixed-point value
   table: ExpYTable_6_9_F210_uid32  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y,
                 Y => expY);
-- signal delay at BRAM output = 1.75e-09
   needNoNorm <= expY(8);
   -- Rounding: all this should consume one row of LUTs
   preRoundBiasSig <= conv_std_logic_vector(15, wE+2)  & expY(7 downto 4) when needNoNorm = '1'
      else conv_std_logic_vector(14, wE+2)  & expY(6 downto 3) ;
   roundBit <= expY(3)  when needNoNorm = '1'    else expY(2) ;
   roundNormAddend <= K(5) & K & (3 downto 1 => '0') & roundBit;
   roundedExpSigOperandAdder: IntAdder_11_f210_uid36  -- pipelineDepth=0 maxInDelay=2.84408e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => roundedExpSigRes,
                 X => preRoundBiasSig,
                 Y => roundNormAddend);

   -- delay at adder output is 3.76508e-09
   roundedExpSig <= roundedExpSigRes when Xexn="01" else  "000" & (wE-2 downto 0 => '1') & (wF-1 downto 0 => '0');
   ----------------Synchro barrier, entering cycle 1----------------
   ofl1 <= not XSign_d1 and oufl0_d1 and (not Xexn_d1(1) and Xexn_d1(0)); -- input positive, normal,  very large
   ofl2 <= not XSign_d1 and (roundedExpSig_d1(wE+wF) and not roundedExpSig_d1(wE+wF+1)) and (not Xexn_d1(1) and Xexn_d1(0)); -- input positive, normal, overflowed
   ofl3 <= not XSign_d1 and Xexn_d1(1) and not Xexn_d1(0);  -- input was -infty
   ofl <= ofl1 or ofl2 or ofl3;
   ufl1 <= (roundedExpSig_d1(wE+wF) and roundedExpSig_d1(wE+wF+1))  and (not Xexn_d1(1) and Xexn_d1(0)); -- input normal
   ufl2 <= XSign_d1 and Xexn_d1(1) and not Xexn_d1(0);  -- input was -infty
   ufl3 <= XSign_d1 and oufl0_d1  and (not Xexn_d1(1) and Xexn_d1(0)); -- input negative, normal,  very large
   ufl <= ufl1 or ufl2 or ufl3;
   Rexn <= "11" when Xexn_d1 = "11"
      else "10" when ofl='1'
      else "00" when ufl='1'
      else "01";
   R <= Rexn & '0' & roundedExpSig_d1(8 downto 0);
end architecture;

