--------------------------------------------------------------------------------
--                                   FPSqrt
--                                (FPSqrt_5_4)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: 
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPSqrt is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5+4+2 downto 0);
          R : out  std_logic_vector(5+4+2 downto 0)   );
end entity;

architecture arch of FPSqrt is
signal fracX :  std_logic_vector(3 downto 0);
signal eRn0 :  std_logic_vector(4 downto 0);
signal xsX, xsX_d1, xsX_d2 :  std_logic_vector(2 downto 0);
signal eRn1, eRn1_d1, eRn1_d2 :  std_logic_vector(4 downto 0);
signal w7 :  std_logic_vector(7 downto 0);
signal d6 :  std_logic;
signal x6 :  std_logic_vector(8 downto 0);
signal ds6 :  std_logic_vector(3 downto 0);
signal xh6 :  std_logic_vector(3 downto 0);
signal wh6 :  std_logic_vector(3 downto 0);
signal w6 :  std_logic_vector(7 downto 0);
signal s6 :  std_logic_vector(0 downto 0);
signal d5 :  std_logic;
signal x5 :  std_logic_vector(8 downto 0);
signal ds5 :  std_logic_vector(4 downto 0);
signal xh5 :  std_logic_vector(4 downto 0);
signal wh5 :  std_logic_vector(4 downto 0);
signal w5 :  std_logic_vector(7 downto 0);
signal s5 :  std_logic_vector(1 downto 0);
signal d4 :  std_logic;
signal x4 :  std_logic_vector(8 downto 0);
signal ds4 :  std_logic_vector(5 downto 0);
signal xh4 :  std_logic_vector(5 downto 0);
signal wh4 :  std_logic_vector(5 downto 0);
signal w4 :  std_logic_vector(7 downto 0);
signal s4 :  std_logic_vector(2 downto 0);
signal d3 :  std_logic;
signal x3 :  std_logic_vector(8 downto 0);
signal ds3 :  std_logic_vector(6 downto 0);
signal xh3 :  std_logic_vector(6 downto 0);
signal wh3 :  std_logic_vector(6 downto 0);
signal w3 :  std_logic_vector(7 downto 0);
signal s3 :  std_logic_vector(3 downto 0);
signal d2 :  std_logic;
signal x2 :  std_logic_vector(8 downto 0);
signal ds2 :  std_logic_vector(7 downto 0);
signal xh2 :  std_logic_vector(7 downto 0);
signal wh2 :  std_logic_vector(7 downto 0);
signal w2, w2_d1 :  std_logic_vector(7 downto 0);
signal s2, s2_d1 :  std_logic_vector(4 downto 0);
signal d1 :  std_logic;
signal x1 :  std_logic_vector(8 downto 0);
signal ds1 :  std_logic_vector(8 downto 0);
signal xh1 :  std_logic_vector(8 downto 0);
signal wh1 :  std_logic_vector(8 downto 0);
signal w1 :  std_logic_vector(7 downto 0);
signal s1 :  std_logic_vector(5 downto 0);
signal d0 :  std_logic;
signal fR :  std_logic_vector(7 downto 0);
signal fRn1, fRn1_d1 :  std_logic_vector(5 downto 0);
signal round, round_d1 :  std_logic;
signal fRn2 :  std_logic_vector(3 downto 0);
signal Rn2 :  std_logic_vector(8 downto 0);
signal xsR :  std_logic_vector(2 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            xsX_d1 <=  xsX;
            xsX_d2 <=  xsX_d1;
            eRn1_d1 <=  eRn1;
            eRn1_d2 <=  eRn1_d1;
            w2_d1 <=  w2;
            s2_d1 <=  s2;
            fRn1_d1 <=  fRn1;
            round_d1 <=  round;
         end if;
      end process;
   fracX <= X(3 downto 0); -- fraction
   eRn0 <= "0" & X(8 downto 5); -- exponent
   xsX <= X(11 downto 9); -- exception and sign
   eRn1 <= eRn0 + ("00" & (2 downto 0 => '1')) + X(4);
   w7 <= "111" & fracX & "0" when X(4) = '0' else
          "1101" & fracX;
   -- Step 6
   d6 <= w7(7);
   x6 <= w7 & "0";
   ds6 <=  "0" &  (not d6) & d6 & "1";
   xh6 <= x6(8 downto 5);
   with d6 select
      wh6 <= xh6 - ds6 when '0',
            xh6 + ds6 when others;
   w6 <= wh6(2 downto 0) & x6(4 downto 0);
   s6 <= "" & (not d6) ;
   -- Step 5
   d5 <= w6(7);
   x5 <= w6 & "0";
   ds5 <=  "0" & s6 &  (not d5) & d5 & "1";
   xh5 <= x5(8 downto 4);
   with d5 select
      wh5 <= xh5 - ds5 when '0',
            xh5 + ds5 when others;
   w5 <= wh5(3 downto 0) & x5(3 downto 0);
   s5 <= s6 & not d5;
   -- Step 4
   d4 <= w5(7);
   x4 <= w5 & "0";
   ds4 <=  "0" & s5 &  (not d4) & d4 & "1";
   xh4 <= x4(8 downto 3);
   with d4 select
      wh4 <= xh4 - ds4 when '0',
            xh4 + ds4 when others;
   w4 <= wh4(4 downto 0) & x4(2 downto 0);
   s4 <= s5 & not d4;
   -- Step 3
   d3 <= w4(7);
   x3 <= w4 & "0";
   ds3 <=  "0" & s4 &  (not d3) & d3 & "1";
   xh3 <= x3(8 downto 2);
   with d3 select
      wh3 <= xh3 - ds3 when '0',
            xh3 + ds3 when others;
   w3 <= wh3(5 downto 0) & x3(1 downto 0);
   s3 <= s4 & not d3;
   -- Step 2
   d2 <= w3(7);
   x2 <= w3 & "0";
   ds2 <=  "0" & s3 &  (not d2) & d2 & "1";
   xh2 <= x2(8 downto 1);
   with d2 select
      wh2 <= xh2 - ds2 when '0',
            xh2 + ds2 when others;
   w2 <= wh2(6 downto 0) & x2(0 downto 0);
   s2 <= s3 & not d2;
   ----------------Synchro barrier, entering cycle 1----------------
   -- Step 1
   d1 <= w2_d1(7);
   x1 <= w2_d1 & "0";
   ds1 <=  "0" & s2_d1 &  (not d1) & d1 & "1";
   xh1 <= x1(8 downto 0);
   with d1 select
      wh1 <= xh1 - ds1 when '0',
            xh1 + ds1 when others;
   w1 <= wh1(7 downto 0);
   s1 <= s2_d1 & not d1;
   d0 <= w1(7) ;
   fR <= s1 & not d0 & '1';
   -- normalisation of the result, removing leading 1
   with fR(7) select
      fRn1 <= fR(6 downto 2) & (fR(1) or fR(0)) when '1',
              fR(5 downto 0)                    when others;
   round <= fRn1(1) and (fRn1(2) or fRn1(0)) ; -- round  and (lsb or sticky) : that's RN, tie to even
   ----------------Synchro barrier, entering cycle 2----------------
   fRn2 <= fRn1_d1(5 downto 2) + ((3 downto 1 => '0') & round_d1); -- rounding sqrt never changes exponents 
   Rn2 <= eRn1_d2 & fRn2;
   -- sign and exception processing
   with xsX_d2 select
      xsR <= "010"  when "010",  -- normal case
             "100"  when "100",  -- +infty
             "000"  when "000",  -- +0
             "001"  when "001",  -- the infamous sqrt(-0)=-0
             "110"  when others; -- return NaN
   R <= xsR & Rn2; 
end architecture;

