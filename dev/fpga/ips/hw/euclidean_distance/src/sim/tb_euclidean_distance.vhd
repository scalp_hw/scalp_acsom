----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_euclidean_distance - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for euclidean_distance
--
-- Last update: 2021/11/16 13:37:02
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all; -- for uniform
use ieee.numeric_std.all;

library std;
use std.env.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_euclidean_distance is
    generic (
        CLK_PERIOD : time := 10 ns -- clock period for clk
    );
end entity;

architecture tb of tb_euclidean_distance is

    ----------------------------------------------------------------------------
    -- Constants
    ----------------------------------------------------------------------------
    -- clock period
    constant PCLK_PERIOD : time := CLK_PERIOD;

    ----------------------------------------------------------------------------
    -- Signals for internal operation
    ----------------------------------------------------------------------------

    --- clock signals ---
    signal clk_sti : std_logic := '0';
    signal rst_sti : std_logic := '0';

    --- DUT input ---

    signal x_sti     : scalar_t;
    signal m_sti     : scalar_t;
    signal valid_sti : std_logic;
    signal last_sti  : std_logic;

    --- DUT output ---
    signal d_obs     : scalar_t;
    signal valid_obs : std_logic;
    signal ready_obs : std_logic;

    ----------------------------------------------------------------------------
    -- DUT component declaration
    ----------------------------------------------------------------------------

    component euclidean_distance is
        port (
            clk_i   : in std_logic;
            rst_i   : in std_logic;
            x_i     : in std_logic_vector(wE + wF + 2 downto 0);  -- new vector component input
            m_i     : in std_logic_vector(wE + wF + 2 downto 0);  -- weight vector component input
            valid_i : in std_logic;                               -- flag valid vector component inputs
            ready_o : out std_logic;                              -- is pipeline ready to accept input
            last_i  : in std_logic;                               -- flag last vector component inputs
            d_o     : out std_logic_vector(wE + wF + 2 downto 0); -- distance output
            valid_o : out std_logic                               -- distance output valid
        );
    end component;

begin

    ----------------------------------------------------------------------------
    -- DUT instantiation
    ----------------------------------------------------------------------------

    DUT_i : euclidean_distance
    port map(
        clk_i   => clk_sti,
        rst_i   => rst_sti,
        x_i     => x_sti,
        m_i     => m_sti,
        valid_i => valid_sti,
        ready_o => ready_obs,
        last_i  => last_sti,
        d_o     => d_obs,
        valid_o => valid_obs
    );

    ----------------------------------------------------------------------------
    -- CLOCK GENERATION
    ----------------------------------------------------------------------------
    clk_proc : process is
    begin
        loop
            clk_sti <= not clk_sti;
            wait for PCLK_PERIOD / 2;
        end loop;
    end process clk_proc;

    ----------------------------------------------------------------------------
    -- TEST BENCH STIMULI
    ----------------------------------------------------------------------------
    tb1 : process is

        procedure drive_input(
            x    : scalar_t;
            m    : scalar_t;
            last : std_logic := '0') is
        begin
            logger.log_note("Driving new input: x=" & to_string(x) & ", m=" & to_string(m) & ", last=" & std_logic'image(last));
            -- Drive input without taking ready_obs into account and use the minimum (hardcoded) pipeline interval
            if true then
                x_sti     <= x;
                m_sti     <= m;
                last_sti  <= last;
                valid_sti <= '1';
                wait until rising_edge(clk_sti);
                valid_sti <= '0';
                last_sti  <= '0';
                wait until rising_edge(clk_sti);
                wait until rising_edge(clk_sti);
            end if;
            -- Drive input after waiting for ready_obs being asserted. Add a cycle of latency because ready_obs is not anticipated
            -- FIXME: on Vivado this create strange bug where even the log_note() above output wrong value. Test on Questa later
            if false then
                loop
                    if ready_obs = '1' then
                        exit;
                    end if;
                    wait until rising_edge(clk_sti);
                end loop;
                x_sti     <= x;
                m_sti     <= m;
                last_sti  <= last;
                valid_sti <= '1';
                wait until rising_edge(clk_sti);
                valid_sti <= '0';
                last_sti  <= '0';
                wait until rising_edge(clk_sti);
            end if;
        end procedure;

        variable test_count : natural := 0;

    begin
        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_report.html",
        "Testbench report: " & "tb_euclidean_distance",
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("Scalar representation: wE: " & integer'image(wE) & ", wF: " & integer'image(wF));

        ------------------------------------------------------------------------
        -- Reset and vectors setup
        --

        x_sti     <= (others => '0');
        m_sti     <= (others => '0');
        valid_sti <= '0';
        last_sti  <= '0';
        rst_sti   <= '0';
        wait until rising_edge(clk_sti);

        --
        rst_sti <= '1';
        wait until rising_edge(clk_sti);
        rst_sti <= '0';
        ------------------------------------------------------------------------
        -- Test vector 1
        -- Two consecutive computations of ((0-2)^2)=4
        if true then
            test_count := test_count + 1;
            logger.log_note("Starting test n°" & integer'image(test_count));

            drive_input(to_scalar_t(0.0), to_scalar_t(2.0), '1');
            drive_input(to_scalar_t(0.0), to_scalar_t(2.0), '1');
            logger.log_note("result: " & to_string(d_obs));
        end if;
        -- End test vector 1
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        -- Test vector 2
        -- Two consecutive computations of ((0-1)^2+(0-1)^2+(0-1)^2+(0-1)^2)=4
        if false then
            test_count := test_count + 1;
            logger.log_note("Starting test n°" & integer'image(test_count));

            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '1');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '1');

            logger.log_note("result: " & to_string(d_obs));
        end if;
        -- End test vector 2
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        -- Test vector 3
        -- Computation of ((2-16)^2+(.8-2.5)^2+(0-1)^2+(2-1)^2+(2-(-8))^2)=
        -- 408,25
        if true then
            test_count := test_count + 1;
            logger.log_note("Starting test n°" & integer'image(test_count));

            drive_input(to_scalar_t(2.0), to_scalar_t(16.0), '0');
            drive_input(to_scalar_t(-8.0), to_scalar_t(2.5), '0');
            drive_input(to_scalar_t(0.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(2.0), to_scalar_t(1.0), '0');
            drive_input(to_scalar_t(2.0), to_scalar_t(-8.0), '1');
        end if;
        -- End test vector 3
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        -- Empty pipeline, do final report and stop simulation
        for I in 1 to 50 loop
            wait until rising_edge(clk_sti);
        end loop;
        logger.final_report;
        stop(0);

    end process tb1;

    tb2 : process is
    begin
        loop
            wait until rising_edge(clk_sti);
            if valid_obs = '1' then
                logger.log_note("New DUT output: d=" & to_string(d_obs));
            end if;
        end loop;
    end process tb2;

    --  End Test Bench

end architecture tb;
