----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: euclidean_distance - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: euclidean_distance
--
-- Last update: 2021/11/16 13:21:25
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.flopoco_pipeline_pkg.all;
use work.csom_pkg.all;

entity euclidean_distance is
    port (
        clk_i   : in std_logic;
        rst_i   : in std_logic;
        x_i     : in std_logic_vector(wE + wF + 2 downto 0);  -- new vector component input
        m_i     : in std_logic_vector(wE + wF + 2 downto 0);  -- weight vector component input
        valid_i : in std_logic;                               -- flag valid vector component inputs
        ready_o : out std_logic;                              -- is pipeline ready to accept input
        last_i  : in std_logic;                               -- flag last vector component inputs
        d_o     : out std_logic_vector(wE + wF + 2 downto 0); -- distance output
        valid_o : out std_logic                               -- distance output valid
    );
end entity;

architecture rtl of euclidean_distance is

    component FPAdd is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component FPSub is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component FPMult is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    constant c_full_pipeline_length :
    natural :=
        1 +                        -- Input registering
        c_FPSub_pipeline_length +  -- Subtraction
        1 +                        -- Subtraction output registering
        c_FPMult_pipeline_length + -- Squaring
        1 +                        -- Squaring output registering
        c_FPAdd_pipeline_length;   -- Sum

    constant c_pipeline_to_sum_reg :
    natural :=
        1 +                        -- Input registering
        c_FPSub_pipeline_length +  -- Subtraction
        1 +                        -- Subtraction output registering
        c_FPMult_pipeline_length + -- Squaring
        1 +                        -- Squaring output registering
        c_FPAdd_pipeline_length;   -- Sum

    signal x_reg : std_logic_vector(wE + wF + 2 downto 0);
    signal m_reg : std_logic_vector(wE + wF + 2 downto 0);
    
    signal xm_diff             : std_logic_vector(wE + wF + 2 downto 0);
    signal xm_diff_reg         : std_logic_vector(wE + wF + 2 downto 0);
    signal xm_diff_squared     : std_logic_vector(wE + wF + 2 downto 0);
    signal xm_diff_squared_reg : std_logic_vector(wE + wF + 2 downto 0);
    signal xm_diff_squared_sum : std_logic_vector(wE + wF + 2 downto 0);
    signal adder_input_s       : std_logic_vector(wE + wF + 2 downto 0);

    signal xm_diff_squared_sum_reg : std_logic_vector(wE + wF + 2 downto 0);

    signal valid_reg : std_logic_vector(c_full_pipeline_length - 1 downto 0);
    signal last_reg  : std_logic_vector(c_full_pipeline_length - 1 downto 0);

    -- Pipeline interval is dependent on the addition pipeline length, due
    -- to the loop of the accumulation
    signal ready_reg : std_logic_vector(c_FPAdd_pipeline_length - 1 downto 0);
    signal ready_s   : std_logic;

    signal valid_s : std_logic;

begin

    FPSub_i : FPSub
    port map(
        clk => clk_i,
        rst => rst_i,
        X   => x_reg,
        Y   => m_reg,
        R   => xm_diff
    );

    FPMult_i : FPMult
    port map(
        clk => clk_i,
        rst => rst_i,
        X   => xm_diff_reg,
        Y   => xm_diff_reg,
        R   => xm_diff_squared
    );

    -- Only feed data to adder on valid cycles to avoid to have to reset it
    adder_input_s <= xm_diff_squared_reg when valid_reg(c_pipeline_to_sum_reg - 1 - c_FPAdd_pipeline_length) = '1' else
        (others => '0');

    FPAdd_i : FPAdd
    port map(
        clk => clk_i,
        rst => rst_i,
        X   => adder_input_s,
        Y   => xm_diff_squared_sum_reg,
        R   => xm_diff_squared_sum
    );

    process (clk_i, rst_i)
    begin
        if rising_edge(clk_i) then
            if rst_i = '1' then
                m_reg                   <= (others => '0');
                x_reg                   <= (others => '0');
                xm_diff_reg             <= (others => '0');
                xm_diff_squared_reg     <= (others => '0');
                xm_diff_squared_sum_reg <= (others => '0');
            else
                m_reg               <= m_i;
                x_reg               <= x_i;
                xm_diff_reg         <= xm_diff;
                xm_diff_squared_reg <= xm_diff_squared;
                -- only accumulate valid results
                if (valid_reg(c_pipeline_to_sum_reg - 1) = '1') then
                    xm_diff_squared_sum_reg <= xm_diff_squared_sum;
                end if;
                -- If it was the last data to be added, reset the register
                if (last_reg(c_pipeline_to_sum_reg - 1) = '1') then
                    xm_diff_squared_sum_reg <= (others => '0');
                end if;

            end if;
        end if;
    end process;

    valid_s <= valid_i and ready_s;

    -- Shift registers for "traveling" valid and last
    process (clk_i, rst_i)
    begin
        if rising_edge(clk_i) then
            if rst_i = '1' then
                valid_reg <= (others => '0');
                last_reg  <= (others => '0');
            else
                valid_reg(valid_reg'left downto 1) <= valid_reg(valid_reg'left - 1 downto 0);
                valid_reg(0)                       <= valid_s;
                last_reg(last_reg'left downto 1)   <= last_reg(last_reg'left - 1 downto 0);
                last_reg(0)                        <= last_i;
            end if;
        end if;
    end process;

    process (clk_i, rst_i)
    begin
        if rising_edge(clk_i) then
            if rst_i = '1' then
                ready_reg <= (others => '1');
            else
                ready_reg(ready_reg'left downto 1) <= ready_reg(ready_reg'left - 1 downto 0);
                ready_reg(0)                       <= not valid_s;
            end if;
        end if;
    end process;

    ready_s <= and ready_reg;
    ready_o <= ready_s;

    d_o     <= xm_diff_squared_sum;
    valid_o <= last_reg(last_reg'left);

end architecture rtl;