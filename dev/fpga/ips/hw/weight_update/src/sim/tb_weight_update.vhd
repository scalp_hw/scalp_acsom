----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_weight_update - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for weight_update
--
-- Last update: 2021/03/31 22:33:16
--
---------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_weight_update is
    generic (
        CLK_PERIOD : time := 10 ns -- clock period for clk
    );
end tb_weight_update;

architecture behavioral of tb_weight_update is

    ----------------------------------------------------------------------------
    -- Constants
    ----------------------------------------------------------------------------
    -- clock period
    constant PCLK_PERIOD : time := CLK_PERIOD;

    ----------------------------------------------------------------------------
    -- Signals for internal operation
    ----------------------------------------------------------------------------

    --- clock signals ---
    signal clk_sti : std_logic := '0';
    signal rst_sti : std_logic := '0';

    --- DUT input ---
    signal x_sti     : scalar_t;
    signal m_sti     : scalar_t;
    signal alpha_sti : scalar_t;
    signal valid_sti : std_logic;

    --- DUT output ---
    signal m_obs     : scalar_t;
    signal valid_obs : std_logic;

    ----------------------------------------------------------------------------
    -- DUT component declaration
    ----------------------------------------------------------------------------

    component weight_update is
        port (
            clk_i   : in std_logic;
            rst_i   : in std_logic;
            valid_i : in std_logic;
            m_i     : in std_logic_vector(wE + wF + 2 downto 0); -- weight vector component input
            x_i     : in std_logic_vector(wE + wF + 2 downto 0); -- new vector component input
            alpha_i : in std_logic_vector(wE + wF + 2 downto 0); -- learning rate
            valid_o : out std_logic;
            m_o     : out std_logic_vector(wE + wF + 2 downto 0) -- weight vector component output
        );
    end component;

begin

    ----------------------------------------------------------------------------
    -- DUT instantiation
    ----------------------------------------------------------------------------

    DUT_i : weight_update
    port map(
        clk_i   => clk_sti,
        rst_i   => rst_sti,
        x_i     => x_sti,
        m_i     => m_sti,
        alpha_i => alpha_sti,
        valid_i => valid_sti,
        m_o     => m_obs,
        valid_o => valid_obs
    );

    ----------------------------------------------------------------------------
    -- CLOCK GENERATION
    ----------------------------------------------------------------------------
    clk_proc : process is
    begin
        loop
            clk_sti <= not clk_sti;
            wait for PCLK_PERIOD / 2;
        end loop;
    end process clk_proc;

    ----------------------------------------------------------------------------
    -- TEST BENCH STIMULI
    ----------------------------------------------------------------------------
    tb1 : process is

        procedure drive_input(
            x     : real;
            m     : real;
            alpha : real) is
        begin
            logger.log_note("Driving new input: x=" & real'image(x) & ", m=" & real'image(m) & ", alpha=" & real'image(alpha));
            logger.log_note("Expected output: " & real'image(m+((x-m)*alpha)));
            x_sti     <= to_scalar_t(x);
            m_sti     <= to_scalar_t(m);
            alpha_sti <= to_scalar_t(alpha);
            valid_sti <= '1';
            wait until rising_edge(clk_sti);
            valid_sti <= '0';
            wait until rising_edge(clk_sti);
        end procedure;

        variable test_count : natural := 0;
    begin
        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_report.html",
        "Testbench report: " & "tb_weight_update",
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("Scalar representation: wE: " & integer'image(wE) & ", wF: " & integer'image(wF));

        ------------------------------------------------------------------------
        -- Reset and vectors setup
        --

        x_sti     <= to_scalar_t(0.0);
        m_sti     <= to_scalar_t(0.0);
        alpha_sti <= to_scalar_t(0.0);
        valid_sti <= '0';
        rst_sti   <= '0';
        wait until rising_edge(clk_sti);

        --
        rst_sti <= '1';
        wait until rising_edge(clk_sti);
        rst_sti <= '0';
        wait until rising_edge(clk_sti);
        ------------------------------------------------------------------------
        -- Test vector 1
        if true then
            test_count := test_count + 1;
            logger.log_note("Starting test n°" & integer'image(test_count));

            drive_input(x => 2.0, m => 1.0, alpha => 0.5);
            drive_input(x => 2.0, m => 1.0, alpha => 0.5);
            drive_input(x => 0.5, m => 1.0, alpha => 0.5);
            drive_input(x => 0.5, m => 2.5, alpha => 0.5);

        end if;
        -- End test vector 1
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        -- Empty pipeline, do final report and stop simulation
        for I in 1 to 50 loop
            wait until rising_edge(clk_sti);
        end loop;
        logger.final_report;
        stop(0);

    end process tb1;

    tb2 : process is
    begin
        loop
            wait until rising_edge(clk_sti);
            if valid_obs = '1' then
                logger.log_note("New DUT output: d=" & to_string(m_obs));
            end if;
        end loop;
    end process tb2;
    --  End Test Bench

end architecture behavioral;
