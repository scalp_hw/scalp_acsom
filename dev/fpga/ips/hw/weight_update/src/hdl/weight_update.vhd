----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: weight_update - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: weight_update
--
-- Last update: 2021/04/14 14:46:51
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.flopoco_pipeline_pkg.all;
use work.csom_pkg.all;

entity weight_update is
    port (
        clk_i   : in std_logic;
        rst_i   : in std_logic;
        valid_i : in std_logic;
        m_i     : in std_logic_vector(wE + wF + 2 downto 0); -- weight vector component input
        x_i     : in std_logic_vector(wE + wF + 2 downto 0); -- new vector component input
        alpha_i : in std_logic_vector(wE + wF + 2 downto 0); -- learning rate
        valid_o : out std_logic;
        m_o     : out std_logic_vector(wE + wF + 2 downto 0) -- weight vector component output
    );
end weight_update;

architecture arch of weight_update is

    component FPSub is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component FPMult is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component FPAdd is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    constant c_full_pipeline_length :
    natural :=
        1 +                        -- Input registering
        c_FPSub_pipeline_length +  -- Subtraction
        1 +                        -- Subtraction output registering
        c_FPMult_pipeline_length + -- Multiplication
        1 +                        -- Multiplication output registering
        c_FPAdd_pipeline_length;   -- Sum output registering

    constant c_pipeline_length_to_add :
    natural :=
        1 +                        -- Input registering
        c_FPSub_pipeline_length +  -- Subtraction
        1 +                        -- Subtraction output registering
        c_FPMult_pipeline_length + -- Multiplication
        1;                         -- Multiplication output registering

    signal x_reg : std_logic_vector(wE + wF + 2 downto 0);
    type m_reg_pipeling_t is array (c_pipeline_length_to_add - 1 downto 0) of std_logic_vector(wE + wF + 2 downto 0);
    signal m_reg : m_reg_pipeling_t;

    signal xm_diff           : std_logic_vector(wE + wF + 2 downto 0);
    signal xm_diff_reg       : std_logic_vector(wE + wF + 2 downto 0);
    signal xm_alpha_prod     : std_logic_vector(wE + wF + 2 downto 0);
    signal xm_alpha_prod_reg : std_logic_vector(wE + wF + 2 downto 0);

    signal valid_reg : std_logic_vector(c_full_pipeline_length - 1 downto 0);
begin

    FPSub_i : FPSub
    port map(
        clk => clk_i,
        rst => rst_i,
        X   => x_reg,
        Y   => m_reg(0),
        R   => xm_diff
    );

    FPMult_i : FPMult
    port map(
        clk => clk_i,
        rst => rst_i,
        X   => alpha_i,
        Y   => xm_diff_reg,
        R   => xm_alpha_prod
    );

    FPAdd_i : FPAdd
    port map(
        clk => clk_i,
        rst => rst_i,
        X   => xm_alpha_prod_reg,
        Y   => m_reg(m_reg'left),
        R   => m_o
    );

    process (clk_i, rst_i)
    begin
        if rising_edge(clk_i) then
            m_reg(m_reg'left downto 1) <= m_reg(m_reg'left - 1 downto 0);
            m_reg(0)                   <= m_i;
            x_reg                      <= x_i;
            xm_diff_reg                <= xm_diff;
            xm_alpha_prod_reg          <= xm_alpha_prod;

        end if;
    end process;

    -- Shift registers for "traveling" valid and last
    process (clk_i, rst_i)
    begin
        if rising_edge(clk_i) then
            if rst_i = '1' then
                valid_reg <= (others => '0');
            else
                valid_reg(valid_reg'left downto 1) <= valid_reg(valid_reg'left - 1 downto 0);
                valid_reg(0)                       <= valid_i;
            end if;
        end if;
    end process;
    valid_o <= valid_reg(valid_reg'left);

end arch;
