--------------------------------------------------------------------------------
--                     LeftShifter_9_by_max_15_F210_uid4
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_9_by_max_15_F210_uid4 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(8 downto 0);
          S : in  std_logic_vector(3 downto 0);
          R : out  std_logic_vector(23 downto 0)   );
end entity;

architecture arch of LeftShifter_9_by_max_15_F210_uid4 is
signal level0 :  std_logic_vector(8 downto 0);
signal ps :  std_logic_vector(3 downto 0);
signal level1 :  std_logic_vector(9 downto 0);
signal level2 :  std_logic_vector(11 downto 0);
signal level3 :  std_logic_vector(15 downto 0);
signal level4 :  std_logic_vector(23 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<= level0 & (0 downto 0 => '0') when ps(0)= '1' else     (0 downto 0 => '0') & level0;
   level2<= level1 & (1 downto 0 => '0') when ps(1)= '1' else     (1 downto 0 => '0') & level1;
   level3<= level2 & (3 downto 0 => '0') when ps(2)= '1' else     (3 downto 0 => '0') & level2;
   level4<= level3 & (7 downto 0 => '0') when ps(3)= '1' else     (7 downto 0 => '0') & level3;
   R <= level4(23 downto 0);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(9 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
signal TableOut :  std_logic_vector(9 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000001000" when "000000",
   "0000010100" when "000001",
   "0000011111" when "000010",
   "0000101011" when "000011",
   "0000110110" when "000100",
   "0001000010" when "000101",
   "0001001101" when "000110",
   "0001011001" when "000111",
   "0001100100" when "001000",
   "0001110000" when "001001",
   "0001111011" when "001010",
   "0010000111" when "001011",
   "0010010010" when "001100",
   "0010011110" when "001101",
   "0010101010" when "001110",
   "0010110101" when "001111",
   "0011000001" when "010000",
   "0011001100" when "010001",
   "0011011000" when "010010",
   "0011100011" when "010011",
   "0011101111" when "010100",
   "0011111010" when "010101",
   "0100000110" when "010110",
   "0100010001" when "010111",
   "0100011101" when "011000",
   "0100101001" when "011001",
   "0100110100" when "011010",
   "0101000000" when "011011",
   "0101001011" when "011100",
   "0101010111" when "011101",
   "0101100010" when "011110",
   "0101101110" when "011111",
   "0101111001" when "100000",
   "0110000101" when "100001",
   "0110010000" when "100010",
   "0110011100" when "100011",
   "0110100111" when "100100",
   "0110110011" when "100101",
   "0110111111" when "100110",
   "0111001010" when "100111",
   "0111010110" when "101000",
   "0111100001" when "101001",
   "0111101101" when "101010",
   "0111111000" when "101011",
   "1000000100" when "101100",
   "1000001111" when "101101",
   "1000011011" when "101110",
   "1000100110" when "101111",
   "1000110010" when "110000",
   "1000111110" when "110001",
   "1001001001" when "110010",
   "1001010101" when "110011",
   "1001100000" when "110100",
   "1001101100" when "110101",
   "1001110111" when "110110",
   "1010000011" when "110111",
   "1010001110" when "111000",
   "1010011010" when "111001",
   "1010100101" when "111010",
   "1010110001" when "111011",
   "1010111100" when "111100",
   "1011001000" when "111101",
   "1011010100" when "111110",
   "1011011111" when "111111",
   "----------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(1 downto 0);
          Y : out  std_logic_vector(3 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
signal TableOut :  std_logic_vector(3 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000" when "00",
   "0011" when "01",
   "0110" when "10",
   "1001" when "11",
   "----" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_11_f210_uid24
--                     (IntAdderClassical_11_F210_uid26)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_11_f210_uid24 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(10 downto 0);
          Y : in  std_logic_vector(10 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of IntAdder_11_f210_uid24 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          R : out  std_logic_vector(5 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8 is
   component FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(9 downto 0)   );
   end component;

   component FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(1 downto 0);
             Y : out  std_logic_vector(3 downto 0)   );
   end component;

   component IntAdder_11_f210_uid24 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(10 downto 0);
             Y : in  std_logic_vector(10 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(10 downto 0)   );
   end component;

signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A0 :  std_logic_vector(5 downto 0);
signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0 :  std_logic_vector(9 downto 0);
signal heap_bh9_w0_0 :  std_logic;
signal heap_bh9_w1_0 :  std_logic;
signal heap_bh9_w2_0 :  std_logic;
signal heap_bh9_w3_0 :  std_logic;
signal heap_bh9_w4_0 :  std_logic;
signal heap_bh9_w5_0 :  std_logic;
signal heap_bh9_w6_0 :  std_logic;
signal heap_bh9_w7_0 :  std_logic;
signal heap_bh9_w8_0 :  std_logic;
signal heap_bh9_w9_0 :  std_logic;
signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A1 :  std_logic_vector(1 downto 0);
signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1 :  std_logic_vector(3 downto 0);
signal heap_bh9_w0_1 :  std_logic;
signal heap_bh9_w1_1 :  std_logic;
signal heap_bh9_w2_1 :  std_logic;
signal heap_bh9_w3_1 :  std_logic;
signal finalAdderIn0_bh9 :  std_logic_vector(10 downto 0);
signal finalAdderIn1_bh9 :  std_logic_vector(10 downto 0);
signal finalAdderCin_bh9 :  std_logic;
signal finalAdderOut_bh9 :  std_logic_vector(10 downto 0);
signal CompressionResult9 :  std_logic_vector(10 downto 0);
signal OutRes :  std_logic_vector(9 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A0 <= X(7 downto 2); -- input address  m=4  l=-1
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table0: FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A0,
                 Y => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0);
   ---------------- cycle 0----------------
   heap_bh9_w0_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(0); -- cycle= 0 cp= 0
   heap_bh9_w1_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(1); -- cycle= 0 cp= 0
   heap_bh9_w2_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(2); -- cycle= 0 cp= 0
   heap_bh9_w3_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(3); -- cycle= 0 cp= 0
   heap_bh9_w4_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(4); -- cycle= 0 cp= 0
   heap_bh9_w5_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(5); -- cycle= 0 cp= 0
   heap_bh9_w6_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(6); -- cycle= 0 cp= 0
   heap_bh9_w7_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(7); -- cycle= 0 cp= 0
   heap_bh9_w8_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(8); -- cycle= 0 cp= 0
   heap_bh9_w9_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(9); -- cycle= 0 cp= 0
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A1 <= X(1 downto 0); -- input address  m=-2  l=-3
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table1: FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A1,
                 Y => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1);
   ---------------- cycle 0----------------
   heap_bh9_w0_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(0); -- cycle= 0 cp= 0
   heap_bh9_w1_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(1); -- cycle= 0 cp= 0
   heap_bh9_w2_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(2); -- cycle= 0 cp= 0
   heap_bh9_w3_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(3); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   finalAdderIn0_bh9 <= "0" & heap_bh9_w9_0 & heap_bh9_w8_0 & heap_bh9_w7_0 & heap_bh9_w6_0 & heap_bh9_w5_0 & heap_bh9_w4_0 & heap_bh9_w3_1 & heap_bh9_w2_1 & heap_bh9_w1_1 & heap_bh9_w0_1;
   finalAdderIn1_bh9 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh9_w3_0 & heap_bh9_w2_0 & heap_bh9_w1_0 & heap_bh9_w0_0;
   finalAdderCin_bh9 <= '0';
   Adder_final9_0: IntAdder_11_f210_uid24  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh9,
                 R => finalAdderOut_bh9   ,
                 X => finalAdderIn0_bh9,
                 Y => finalAdderIn1_bh9);
   -- concatenate all the compressed chunks
   CompressionResult9 <= finalAdderOut_bh9;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult9(9 downto 0);
   R <= OutRes(9 downto 4);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(15 downto 0)   );
end entity;

architecture arch of FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_Table_0 is
signal TableOut :  std_logic_vector(15 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000000000000000" when "000000",
   "0000001011000110" when "000001",
   "0000010110001100" when "000010",
   "0000100001010001" when "000011",
   "0000101100010111" when "000100",
   "0000110111011101" when "000101",
   "0001000010100011" when "000110",
   "0001001101101000" when "000111",
   "0001011000101110" when "001000",
   "0001100011110100" when "001001",
   "0001101110111010" when "001010",
   "0001111010000000" when "001011",
   "0010000101000101" when "001100",
   "0010010000001011" when "001101",
   "0010011011010001" when "001110",
   "0010100110010111" when "001111",
   "0010110001011101" when "010000",
   "0010111100100010" when "010001",
   "0011000111101000" when "010010",
   "0011010010101110" when "010011",
   "0011011101110100" when "010100",
   "0011101000111001" when "010101",
   "0011110011111111" when "010110",
   "0011111111000101" when "010111",
   "0100001010001011" when "011000",
   "0100010101010001" when "011001",
   "0100100000010110" when "011010",
   "0100101011011100" when "011011",
   "0100110110100010" when "011100",
   "0101000001101000" when "011101",
   "0101001100101101" when "011110",
   "0101010111110011" when "011111",
   "0101100010111001" when "100000",
   "0101101101111111" when "100001",
   "0101111001000101" when "100010",
   "0110000100001010" when "100011",
   "0110001111010000" when "100100",
   "0110011010010110" when "100101",
   "0110100101011100" when "100110",
   "0110110000100010" when "100111",
   "0110111011100111" when "101000",
   "0111000110101101" when "101001",
   "0111010001110011" when "101010",
   "0111011100111001" when "101011",
   "0111100111111110" when "101100",
   "0111110011000100" when "101101",
   "0111111110001010" when "101110",
   "1000001001010000" when "101111",
   "1000010100010110" when "110000",
   "1000011111011011" when "110001",
   "1000101010100001" when "110010",
   "1000110101100111" when "110011",
   "1001000000101101" when "110100",
   "1001001011110010" when "110101",
   "1001010110111000" when "110110",
   "1001100001111110" when "110111",
   "1001101101000100" when "111000",
   "1001111000001010" when "111001",
   "1010000011001111" when "111010",
   "1010001110010101" when "111011",
   "1010011001011011" when "111100",
   "1010100100100001" when "111101",
   "1010101111100111" when "111110",
   "1010111010101100" when "111111",
   "----------------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          R : out  std_logic_vector(15 downto 0)   );
end entity;

architecture arch of FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34 is
   component FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(15 downto 0)   );
   end component;

signal FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_A0 :  std_logic_vector(5 downto 0);
signal FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0 :  std_logic_vector(15 downto 0);
signal heap_bh35_w0_0 :  std_logic;
signal heap_bh35_w1_0 :  std_logic;
signal heap_bh35_w2_0 :  std_logic;
signal heap_bh35_w3_0 :  std_logic;
signal heap_bh35_w4_0 :  std_logic;
signal heap_bh35_w5_0 :  std_logic;
signal heap_bh35_w6_0 :  std_logic;
signal heap_bh35_w7_0 :  std_logic;
signal heap_bh35_w8_0 :  std_logic;
signal heap_bh35_w9_0 :  std_logic;
signal heap_bh35_w10_0 :  std_logic;
signal heap_bh35_w11_0 :  std_logic;
signal heap_bh35_w12_0 :  std_logic;
signal heap_bh35_w13_0 :  std_logic;
signal heap_bh35_w14_0 :  std_logic;
signal heap_bh35_w15_0 :  std_logic;
signal CompressionResult35 :  std_logic_vector(15 downto 0);
signal OutRes :  std_logic_vector(15 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_A0 <= X(5 downto 0); -- input address  m=5  l=0
   FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_Table0: FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_A0,
                 Y => FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0);
   ---------------- cycle 0----------------
   heap_bh35_w0_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(0); -- cycle= 0 cp= 0
   heap_bh35_w1_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(1); -- cycle= 0 cp= 0
   heap_bh35_w2_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(2); -- cycle= 0 cp= 0
   heap_bh35_w3_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(3); -- cycle= 0 cp= 0
   heap_bh35_w4_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(4); -- cycle= 0 cp= 0
   heap_bh35_w5_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(5); -- cycle= 0 cp= 0
   heap_bh35_w6_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(6); -- cycle= 0 cp= 0
   heap_bh35_w7_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(7); -- cycle= 0 cp= 0
   heap_bh35_w8_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(8); -- cycle= 0 cp= 0
   heap_bh35_w9_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(9); -- cycle= 0 cp= 0
   heap_bh35_w10_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(10); -- cycle= 0 cp= 0
   heap_bh35_w11_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(11); -- cycle= 0 cp= 0
   heap_bh35_w12_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(12); -- cycle= 0 cp= 0
   heap_bh35_w13_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(13); -- cycle= 0 cp= 0
   heap_bh35_w14_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(14); -- cycle= 0 cp= 0
   heap_bh35_w15_0 <= FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34_T0(15); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   CompressionResult35 <= heap_bh35_w15_0 & heap_bh35_w14_0 & heap_bh35_w13_0 & heap_bh35_w12_0 & heap_bh35_w11_0 & heap_bh35_w10_0 & heap_bh35_w9_0 & heap_bh35_w8_0 & heap_bh35_w7_0 & heap_bh35_w6_0 & heap_bh35_w5_0 & heap_bh35_w4_0 & heap_bh35_w3_0 & heap_bh35_w2_0 & heap_bh35_w1_0 & heap_bh35_w0_0;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult35(15 downto 0);
   R <= OutRes(15 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_10_f231_uid42
--                    (IntAdderAlternative_10_F231_uid46)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_10_f231_uid42 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(9 downto 0);
          Y : in  std_logic_vector(9 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(9 downto 0)   );
end entity;

architecture arch of IntAdder_10_f231_uid42 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                         ExpYTable_10_13_F210_uid50
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity ExpYTable_10_13_F210_uid50 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(9 downto 0);
          Y : out  std_logic_vector(12 downto 0)   );
end entity;

architecture arch of ExpYTable_10_13_F210_uid50 is
signal TableOut :  std_logic_vector(12 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "1000000000000" when "0000000000",
   "1000000000100" when "0000000001",
   "1000000001000" when "0000000010",
   "1000000001100" when "0000000011",
   "1000000010000" when "0000000100",
   "1000000010100" when "0000000101",
   "1000000011000" when "0000000110",
   "1000000011100" when "0000000111",
   "1000000100000" when "0000001000",
   "1000000100100" when "0000001001",
   "1000000101000" when "0000001010",
   "1000000101100" when "0000001011",
   "1000000110000" when "0000001100",
   "1000000110100" when "0000001101",
   "1000000111000" when "0000001110",
   "1000000111100" when "0000001111",
   "1000001000001" when "0000010000",
   "1000001000101" when "0000010001",
   "1000001001001" when "0000010010",
   "1000001001101" when "0000010011",
   "1000001010001" when "0000010100",
   "1000001010101" when "0000010101",
   "1000001011001" when "0000010110",
   "1000001011101" when "0000010111",
   "1000001100001" when "0000011000",
   "1000001100101" when "0000011001",
   "1000001101001" when "0000011010",
   "1000001101101" when "0000011011",
   "1000001110010" when "0000011100",
   "1000001110110" when "0000011101",
   "1000001111010" when "0000011110",
   "1000001111110" when "0000011111",
   "1000010000010" when "0000100000",
   "1000010000110" when "0000100001",
   "1000010001010" when "0000100010",
   "1000010001110" when "0000100011",
   "1000010010011" when "0000100100",
   "1000010010111" when "0000100101",
   "1000010011011" when "0000100110",
   "1000010011111" when "0000100111",
   "1000010100011" when "0000101000",
   "1000010100111" when "0000101001",
   "1000010101011" when "0000101010",
   "1000010110000" when "0000101011",
   "1000010110100" when "0000101100",
   "1000010111000" when "0000101101",
   "1000010111100" when "0000101110",
   "1000011000000" when "0000101111",
   "1000011000101" when "0000110000",
   "1000011001001" when "0000110001",
   "1000011001101" when "0000110010",
   "1000011010001" when "0000110011",
   "1000011010101" when "0000110100",
   "1000011011010" when "0000110101",
   "1000011011110" when "0000110110",
   "1000011100010" when "0000110111",
   "1000011100110" when "0000111000",
   "1000011101010" when "0000111001",
   "1000011101111" when "0000111010",
   "1000011110011" when "0000111011",
   "1000011110111" when "0000111100",
   "1000011111011" when "0000111101",
   "1000100000000" when "0000111110",
   "1000100000100" when "0000111111",
   "1000100001000" when "0001000000",
   "1000100001100" when "0001000001",
   "1000100010001" when "0001000010",
   "1000100010101" when "0001000011",
   "1000100011001" when "0001000100",
   "1000100011110" when "0001000101",
   "1000100100010" when "0001000110",
   "1000100100110" when "0001000111",
   "1000100101010" when "0001001000",
   "1000100101111" when "0001001001",
   "1000100110011" when "0001001010",
   "1000100110111" when "0001001011",
   "1000100111100" when "0001001100",
   "1000101000000" when "0001001101",
   "1000101000100" when "0001001110",
   "1000101001001" when "0001001111",
   "1000101001101" when "0001010000",
   "1000101010001" when "0001010001",
   "1000101010101" when "0001010010",
   "1000101011010" when "0001010011",
   "1000101011110" when "0001010100",
   "1000101100011" when "0001010101",
   "1000101100111" when "0001010110",
   "1000101101011" when "0001010111",
   "1000101110000" when "0001011000",
   "1000101110100" when "0001011001",
   "1000101111000" when "0001011010",
   "1000101111101" when "0001011011",
   "1000110000001" when "0001011100",
   "1000110000101" when "0001011101",
   "1000110001010" when "0001011110",
   "1000110001110" when "0001011111",
   "1000110010011" when "0001100000",
   "1000110010111" when "0001100001",
   "1000110011011" when "0001100010",
   "1000110100000" when "0001100011",
   "1000110100100" when "0001100100",
   "1000110101001" when "0001100101",
   "1000110101101" when "0001100110",
   "1000110110001" when "0001100111",
   "1000110110110" when "0001101000",
   "1000110111010" when "0001101001",
   "1000110111111" when "0001101010",
   "1000111000011" when "0001101011",
   "1000111001000" when "0001101100",
   "1000111001100" when "0001101101",
   "1000111010001" when "0001101110",
   "1000111010101" when "0001101111",
   "1000111011001" when "0001110000",
   "1000111011110" when "0001110001",
   "1000111100010" when "0001110010",
   "1000111100111" when "0001110011",
   "1000111101011" when "0001110100",
   "1000111110000" when "0001110101",
   "1000111110100" when "0001110110",
   "1000111111001" when "0001110111",
   "1000111111101" when "0001111000",
   "1001000000010" when "0001111001",
   "1001000000110" when "0001111010",
   "1001000001011" when "0001111011",
   "1001000001111" when "0001111100",
   "1001000010100" when "0001111101",
   "1001000011000" when "0001111110",
   "1001000011101" when "0001111111",
   "1001000100001" when "0010000000",
   "1001000100110" when "0010000001",
   "1001000101010" when "0010000010",
   "1001000101111" when "0010000011",
   "1001000110100" when "0010000100",
   "1001000111000" when "0010000101",
   "1001000111101" when "0010000110",
   "1001001000001" when "0010000111",
   "1001001000110" when "0010001000",
   "1001001001010" when "0010001001",
   "1001001001111" when "0010001010",
   "1001001010100" when "0010001011",
   "1001001011000" when "0010001100",
   "1001001011101" when "0010001101",
   "1001001100001" when "0010001110",
   "1001001100110" when "0010001111",
   "1001001101010" when "0010010000",
   "1001001101111" when "0010010001",
   "1001001110100" when "0010010010",
   "1001001111000" when "0010010011",
   "1001001111101" when "0010010100",
   "1001010000010" when "0010010101",
   "1001010000110" when "0010010110",
   "1001010001011" when "0010010111",
   "1001010001111" when "0010011000",
   "1001010010100" when "0010011001",
   "1001010011001" when "0010011010",
   "1001010011101" when "0010011011",
   "1001010100010" when "0010011100",
   "1001010100111" when "0010011101",
   "1001010101011" when "0010011110",
   "1001010110000" when "0010011111",
   "1001010110101" when "0010100000",
   "1001010111001" when "0010100001",
   "1001010111110" when "0010100010",
   "1001011000011" when "0010100011",
   "1001011000111" when "0010100100",
   "1001011001100" when "0010100101",
   "1001011010001" when "0010100110",
   "1001011010110" when "0010100111",
   "1001011011010" when "0010101000",
   "1001011011111" when "0010101001",
   "1001011100100" when "0010101010",
   "1001011101000" when "0010101011",
   "1001011101101" when "0010101100",
   "1001011110010" when "0010101101",
   "1001011110111" when "0010101110",
   "1001011111011" when "0010101111",
   "1001100000000" when "0010110000",
   "1001100000101" when "0010110001",
   "1001100001010" when "0010110010",
   "1001100001110" when "0010110011",
   "1001100010011" when "0010110100",
   "1001100011000" when "0010110101",
   "1001100011101" when "0010110110",
   "1001100100001" when "0010110111",
   "1001100100110" when "0010111000",
   "1001100101011" when "0010111001",
   "1001100110000" when "0010111010",
   "1001100110101" when "0010111011",
   "1001100111001" when "0010111100",
   "1001100111110" when "0010111101",
   "1001101000011" when "0010111110",
   "1001101001000" when "0010111111",
   "1001101001101" when "0011000000",
   "1001101010010" when "0011000001",
   "1001101010110" when "0011000010",
   "1001101011011" when "0011000011",
   "1001101100000" when "0011000100",
   "1001101100101" when "0011000101",
   "1001101101010" when "0011000110",
   "1001101101111" when "0011000111",
   "1001101110011" when "0011001000",
   "1001101111000" when "0011001001",
   "1001101111101" when "0011001010",
   "1001110000010" when "0011001011",
   "1001110000111" when "0011001100",
   "1001110001100" when "0011001101",
   "1001110010001" when "0011001110",
   "1001110010110" when "0011001111",
   "1001110011011" when "0011010000",
   "1001110011111" when "0011010001",
   "1001110100100" when "0011010010",
   "1001110101001" when "0011010011",
   "1001110101110" when "0011010100",
   "1001110110011" when "0011010101",
   "1001110111000" when "0011010110",
   "1001110111101" when "0011010111",
   "1001111000010" when "0011011000",
   "1001111000111" when "0011011001",
   "1001111001100" when "0011011010",
   "1001111010001" when "0011011011",
   "1001111010110" when "0011011100",
   "1001111011011" when "0011011101",
   "1001111100000" when "0011011110",
   "1001111100101" when "0011011111",
   "1001111101010" when "0011100000",
   "1001111101111" when "0011100001",
   "1001111110100" when "0011100010",
   "1001111111001" when "0011100011",
   "1001111111110" when "0011100100",
   "1010000000011" when "0011100101",
   "1010000001000" when "0011100110",
   "1010000001101" when "0011100111",
   "1010000010010" when "0011101000",
   "1010000010111" when "0011101001",
   "1010000011100" when "0011101010",
   "1010000100001" when "0011101011",
   "1010000100110" when "0011101100",
   "1010000101011" when "0011101101",
   "1010000110000" when "0011101110",
   "1010000110101" when "0011101111",
   "1010000111010" when "0011110000",
   "1010000111111" when "0011110001",
   "1010001000100" when "0011110010",
   "1010001001001" when "0011110011",
   "1010001001110" when "0011110100",
   "1010001010011" when "0011110101",
   "1010001011000" when "0011110110",
   "1010001011101" when "0011110111",
   "1010001100010" when "0011111000",
   "1010001101000" when "0011111001",
   "1010001101101" when "0011111010",
   "1010001110010" when "0011111011",
   "1010001110111" when "0011111100",
   "1010001111100" when "0011111101",
   "1010010000001" when "0011111110",
   "1010010000110" when "0011111111",
   "1010010001011" when "0100000000",
   "1010010010001" when "0100000001",
   "1010010010110" when "0100000010",
   "1010010011011" when "0100000011",
   "1010010100000" when "0100000100",
   "1010010100101" when "0100000101",
   "1010010101010" when "0100000110",
   "1010010101111" when "0100000111",
   "1010010110101" when "0100001000",
   "1010010111010" when "0100001001",
   "1010010111111" when "0100001010",
   "1010011000100" when "0100001011",
   "1010011001001" when "0100001100",
   "1010011001111" when "0100001101",
   "1010011010100" when "0100001110",
   "1010011011001" when "0100001111",
   "1010011011110" when "0100010000",
   "1010011100011" when "0100010001",
   "1010011101001" when "0100010010",
   "1010011101110" when "0100010011",
   "1010011110011" when "0100010100",
   "1010011111000" when "0100010101",
   "1010011111110" when "0100010110",
   "1010100000011" when "0100010111",
   "1010100001000" when "0100011000",
   "1010100001101" when "0100011001",
   "1010100010011" when "0100011010",
   "1010100011000" when "0100011011",
   "1010100011101" when "0100011100",
   "1010100100010" when "0100011101",
   "1010100101000" when "0100011110",
   "1010100101101" when "0100011111",
   "1010100110010" when "0100100000",
   "1010100111000" when "0100100001",
   "1010100111101" when "0100100010",
   "1010101000010" when "0100100011",
   "1010101001000" when "0100100100",
   "1010101001101" when "0100100101",
   "1010101010010" when "0100100110",
   "1010101011000" when "0100100111",
   "1010101011101" when "0100101000",
   "1010101100010" when "0100101001",
   "1010101101000" when "0100101010",
   "1010101101101" when "0100101011",
   "1010101110010" when "0100101100",
   "1010101111000" when "0100101101",
   "1010101111101" when "0100101110",
   "1010110000010" when "0100101111",
   "1010110001000" when "0100110000",
   "1010110001101" when "0100110001",
   "1010110010011" when "0100110010",
   "1010110011000" when "0100110011",
   "1010110011101" when "0100110100",
   "1010110100011" when "0100110101",
   "1010110101000" when "0100110110",
   "1010110101110" when "0100110111",
   "1010110110011" when "0100111000",
   "1010110111000" when "0100111001",
   "1010110111110" when "0100111010",
   "1010111000011" when "0100111011",
   "1010111001001" when "0100111100",
   "1010111001110" when "0100111101",
   "1010111010100" when "0100111110",
   "1010111011001" when "0100111111",
   "1010111011111" when "0101000000",
   "1010111100100" when "0101000001",
   "1010111101010" when "0101000010",
   "1010111101111" when "0101000011",
   "1010111110100" when "0101000100",
   "1010111111010" when "0101000101",
   "1010111111111" when "0101000110",
   "1011000000101" when "0101000111",
   "1011000001010" when "0101001000",
   "1011000010000" when "0101001001",
   "1011000010110" when "0101001010",
   "1011000011011" when "0101001011",
   "1011000100001" when "0101001100",
   "1011000100110" when "0101001101",
   "1011000101100" when "0101001110",
   "1011000110001" when "0101001111",
   "1011000110111" when "0101010000",
   "1011000111100" when "0101010001",
   "1011001000010" when "0101010010",
   "1011001000111" when "0101010011",
   "1011001001101" when "0101010100",
   "1011001010011" when "0101010101",
   "1011001011000" when "0101010110",
   "1011001011110" when "0101010111",
   "1011001100011" when "0101011000",
   "1011001101001" when "0101011001",
   "1011001101111" when "0101011010",
   "1011001110100" when "0101011011",
   "1011001111010" when "0101011100",
   "1011001111111" when "0101011101",
   "1011010000101" when "0101011110",
   "1011010001011" when "0101011111",
   "1011010010000" when "0101100000",
   "1011010010110" when "0101100001",
   "1011010011100" when "0101100010",
   "1011010100001" when "0101100011",
   "1011010100111" when "0101100100",
   "1011010101101" when "0101100101",
   "1011010110010" when "0101100110",
   "1011010111000" when "0101100111",
   "1011010111110" when "0101101000",
   "1011011000011" when "0101101001",
   "1011011001001" when "0101101010",
   "1011011001111" when "0101101011",
   "1011011010100" when "0101101100",
   "1011011011010" when "0101101101",
   "1011011100000" when "0101101110",
   "1011011100110" when "0101101111",
   "1011011101011" when "0101110000",
   "1011011110001" when "0101110001",
   "1011011110111" when "0101110010",
   "1011011111100" when "0101110011",
   "1011100000010" when "0101110100",
   "1011100001000" when "0101110101",
   "1011100001110" when "0101110110",
   "1011100010011" when "0101110111",
   "1011100011001" when "0101111000",
   "1011100011111" when "0101111001",
   "1011100100101" when "0101111010",
   "1011100101011" when "0101111011",
   "1011100110000" when "0101111100",
   "1011100110110" when "0101111101",
   "1011100111100" when "0101111110",
   "1011101000010" when "0101111111",
   "1011101001000" when "0110000000",
   "1011101001101" when "0110000001",
   "1011101010011" when "0110000010",
   "1011101011001" when "0110000011",
   "1011101011111" when "0110000100",
   "1011101100101" when "0110000101",
   "1011101101011" when "0110000110",
   "1011101110001" when "0110000111",
   "1011101110110" when "0110001000",
   "1011101111100" when "0110001001",
   "1011110000010" when "0110001010",
   "1011110001000" when "0110001011",
   "1011110001110" when "0110001100",
   "1011110010100" when "0110001101",
   "1011110011010" when "0110001110",
   "1011110100000" when "0110001111",
   "1011110100101" when "0110010000",
   "1011110101011" when "0110010001",
   "1011110110001" when "0110010010",
   "1011110110111" when "0110010011",
   "1011110111101" when "0110010100",
   "1011111000011" when "0110010101",
   "1011111001001" when "0110010110",
   "1011111001111" when "0110010111",
   "1011111010101" when "0110011000",
   "1011111011011" when "0110011001",
   "1011111100001" when "0110011010",
   "1011111100111" when "0110011011",
   "1011111101101" when "0110011100",
   "1011111110011" when "0110011101",
   "1011111111001" when "0110011110",
   "1011111111111" when "0110011111",
   "1100000000101" when "0110100000",
   "1100000001011" when "0110100001",
   "1100000010001" when "0110100010",
   "1100000010111" when "0110100011",
   "1100000011101" when "0110100100",
   "1100000100011" when "0110100101",
   "1100000101001" when "0110100110",
   "1100000101111" when "0110100111",
   "1100000110101" when "0110101000",
   "1100000111011" when "0110101001",
   "1100001000001" when "0110101010",
   "1100001000111" when "0110101011",
   "1100001001101" when "0110101100",
   "1100001010011" when "0110101101",
   "1100001011001" when "0110101110",
   "1100001100000" when "0110101111",
   "1100001100110" when "0110110000",
   "1100001101100" when "0110110001",
   "1100001110010" when "0110110010",
   "1100001111000" when "0110110011",
   "1100001111110" when "0110110100",
   "1100010000100" when "0110110101",
   "1100010001010" when "0110110110",
   "1100010010000" when "0110110111",
   "1100010010111" when "0110111000",
   "1100010011101" when "0110111001",
   "1100010100011" when "0110111010",
   "1100010101001" when "0110111011",
   "1100010101111" when "0110111100",
   "1100010110101" when "0110111101",
   "1100010111100" when "0110111110",
   "1100011000010" when "0110111111",
   "1100011001000" when "0111000000",
   "1100011001110" when "0111000001",
   "1100011010100" when "0111000010",
   "1100011011011" when "0111000011",
   "1100011100001" when "0111000100",
   "1100011100111" when "0111000101",
   "1100011101101" when "0111000110",
   "1100011110100" when "0111000111",
   "1100011111010" when "0111001000",
   "1100100000000" when "0111001001",
   "1100100000110" when "0111001010",
   "1100100001101" when "0111001011",
   "1100100010011" when "0111001100",
   "1100100011001" when "0111001101",
   "1100100011111" when "0111001110",
   "1100100100110" when "0111001111",
   "1100100101100" when "0111010000",
   "1100100110010" when "0111010001",
   "1100100111001" when "0111010010",
   "1100100111111" when "0111010011",
   "1100101000101" when "0111010100",
   "1100101001011" when "0111010101",
   "1100101010010" when "0111010110",
   "1100101011000" when "0111010111",
   "1100101011110" when "0111011000",
   "1100101100101" when "0111011001",
   "1100101101011" when "0111011010",
   "1100101110010" when "0111011011",
   "1100101111000" when "0111011100",
   "1100101111110" when "0111011101",
   "1100110000101" when "0111011110",
   "1100110001011" when "0111011111",
   "1100110010001" when "0111100000",
   "1100110011000" when "0111100001",
   "1100110011110" when "0111100010",
   "1100110100101" when "0111100011",
   "1100110101011" when "0111100100",
   "1100110110001" when "0111100101",
   "1100110111000" when "0111100110",
   "1100110111110" when "0111100111",
   "1100111000101" when "0111101000",
   "1100111001011" when "0111101001",
   "1100111010010" when "0111101010",
   "1100111011000" when "0111101011",
   "1100111011111" when "0111101100",
   "1100111100101" when "0111101101",
   "1100111101011" when "0111101110",
   "1100111110010" when "0111101111",
   "1100111111000" when "0111110000",
   "1100111111111" when "0111110001",
   "1101000000101" when "0111110010",
   "1101000001100" when "0111110011",
   "1101000010010" when "0111110100",
   "1101000011001" when "0111110101",
   "1101000100000" when "0111110110",
   "1101000100110" when "0111110111",
   "1101000101101" when "0111111000",
   "1101000110011" when "0111111001",
   "1101000111010" when "0111111010",
   "1101001000000" when "0111111011",
   "1101001000111" when "0111111100",
   "1101001001101" when "0111111101",
   "1101001010100" when "0111111110",
   "1101001011011" when "0111111111",
   "0100110110100" when "1000000000",
   "0100110110111" when "1000000001",
   "0100110111001" when "1000000010",
   "0100110111100" when "1000000011",
   "0100110111110" when "1000000100",
   "0100111000001" when "1000000101",
   "0100111000011" when "1000000110",
   "0100111000101" when "1000000111",
   "0100111001000" when "1000001000",
   "0100111001010" when "1000001001",
   "0100111001101" when "1000001010",
   "0100111001111" when "1000001011",
   "0100111010010" when "1000001100",
   "0100111010100" when "1000001101",
   "0100111010111" when "1000001110",
   "0100111011001" when "1000001111",
   "0100111011011" when "1000010000",
   "0100111011110" when "1000010001",
   "0100111100000" when "1000010010",
   "0100111100011" when "1000010011",
   "0100111100101" when "1000010100",
   "0100111101000" when "1000010101",
   "0100111101010" when "1000010110",
   "0100111101101" when "1000010111",
   "0100111101111" when "1000011000",
   "0100111110010" when "1000011001",
   "0100111110100" when "1000011010",
   "0100111110111" when "1000011011",
   "0100111111001" when "1000011100",
   "0100111111100" when "1000011101",
   "0100111111110" when "1000011110",
   "0101000000001" when "1000011111",
   "0101000000011" when "1000100000",
   "0101000000110" when "1000100001",
   "0101000001000" when "1000100010",
   "0101000001011" when "1000100011",
   "0101000001101" when "1000100100",
   "0101000010000" when "1000100101",
   "0101000010010" when "1000100110",
   "0101000010101" when "1000100111",
   "0101000010111" when "1000101000",
   "0101000011010" when "1000101001",
   "0101000011100" when "1000101010",
   "0101000011111" when "1000101011",
   "0101000100001" when "1000101100",
   "0101000100100" when "1000101101",
   "0101000100110" when "1000101110",
   "0101000101001" when "1000101111",
   "0101000101100" when "1000110000",
   "0101000101110" when "1000110001",
   "0101000110001" when "1000110010",
   "0101000110011" when "1000110011",
   "0101000110110" when "1000110100",
   "0101000111000" when "1000110101",
   "0101000111011" when "1000110110",
   "0101000111101" when "1000110111",
   "0101001000000" when "1000111000",
   "0101001000011" when "1000111001",
   "0101001000101" when "1000111010",
   "0101001001000" when "1000111011",
   "0101001001010" when "1000111100",
   "0101001001101" when "1000111101",
   "0101001001111" when "1000111110",
   "0101001010010" when "1000111111",
   "0101001010101" when "1001000000",
   "0101001010111" when "1001000001",
   "0101001011010" when "1001000010",
   "0101001011100" when "1001000011",
   "0101001011111" when "1001000100",
   "0101001100010" when "1001000101",
   "0101001100100" when "1001000110",
   "0101001100111" when "1001000111",
   "0101001101001" when "1001001000",
   "0101001101100" when "1001001001",
   "0101001101111" when "1001001010",
   "0101001110001" when "1001001011",
   "0101001110100" when "1001001100",
   "0101001110110" when "1001001101",
   "0101001111001" when "1001001110",
   "0101001111100" when "1001001111",
   "0101001111110" when "1001010000",
   "0101010000001" when "1001010001",
   "0101010000011" when "1001010010",
   "0101010000110" when "1001010011",
   "0101010001001" when "1001010100",
   "0101010001011" when "1001010101",
   "0101010001110" when "1001010110",
   "0101010010001" when "1001010111",
   "0101010010011" when "1001011000",
   "0101010010110" when "1001011001",
   "0101010011001" when "1001011010",
   "0101010011011" when "1001011011",
   "0101010011110" when "1001011100",
   "0101010100001" when "1001011101",
   "0101010100011" when "1001011110",
   "0101010100110" when "1001011111",
   "0101010101001" when "1001100000",
   "0101010101011" when "1001100001",
   "0101010101110" when "1001100010",
   "0101010110001" when "1001100011",
   "0101010110011" when "1001100100",
   "0101010110110" when "1001100101",
   "0101010111001" when "1001100110",
   "0101010111011" when "1001100111",
   "0101010111110" when "1001101000",
   "0101011000001" when "1001101001",
   "0101011000011" when "1001101010",
   "0101011000110" when "1001101011",
   "0101011001001" when "1001101100",
   "0101011001011" when "1001101101",
   "0101011001110" when "1001101110",
   "0101011010001" when "1001101111",
   "0101011010011" when "1001110000",
   "0101011010110" when "1001110001",
   "0101011011001" when "1001110010",
   "0101011011100" when "1001110011",
   "0101011011110" when "1001110100",
   "0101011100001" when "1001110101",
   "0101011100100" when "1001110110",
   "0101011100111" when "1001110111",
   "0101011101001" when "1001111000",
   "0101011101100" when "1001111001",
   "0101011101111" when "1001111010",
   "0101011110001" when "1001111011",
   "0101011110100" when "1001111100",
   "0101011110111" when "1001111101",
   "0101011111010" when "1001111110",
   "0101011111100" when "1001111111",
   "0101011111111" when "1010000000",
   "0101100000010" when "1010000001",
   "0101100000101" when "1010000010",
   "0101100000111" when "1010000011",
   "0101100001010" when "1010000100",
   "0101100001101" when "1010000101",
   "0101100010000" when "1010000110",
   "0101100010010" when "1010000111",
   "0101100010101" when "1010001000",
   "0101100011000" when "1010001001",
   "0101100011011" when "1010001010",
   "0101100011110" when "1010001011",
   "0101100100000" when "1010001100",
   "0101100100011" when "1010001101",
   "0101100100110" when "1010001110",
   "0101100101001" when "1010001111",
   "0101100101011" when "1010010000",
   "0101100101110" when "1010010001",
   "0101100110001" when "1010010010",
   "0101100110100" when "1010010011",
   "0101100110111" when "1010010100",
   "0101100111001" when "1010010101",
   "0101100111100" when "1010010110",
   "0101100111111" when "1010010111",
   "0101101000010" when "1010011000",
   "0101101000101" when "1010011001",
   "0101101001000" when "1010011010",
   "0101101001010" when "1010011011",
   "0101101001101" when "1010011100",
   "0101101010000" when "1010011101",
   "0101101010011" when "1010011110",
   "0101101010110" when "1010011111",
   "0101101011000" when "1010100000",
   "0101101011011" when "1010100001",
   "0101101011110" when "1010100010",
   "0101101100001" when "1010100011",
   "0101101100100" when "1010100100",
   "0101101100111" when "1010100101",
   "0101101101010" when "1010100110",
   "0101101101100" when "1010100111",
   "0101101101111" when "1010101000",
   "0101101110010" when "1010101001",
   "0101101110101" when "1010101010",
   "0101101111000" when "1010101011",
   "0101101111011" when "1010101100",
   "0101101111110" when "1010101101",
   "0101110000000" when "1010101110",
   "0101110000011" when "1010101111",
   "0101110000110" when "1010110000",
   "0101110001001" when "1010110001",
   "0101110001100" when "1010110010",
   "0101110001111" when "1010110011",
   "0101110010010" when "1010110100",
   "0101110010101" when "1010110101",
   "0101110011000" when "1010110110",
   "0101110011010" when "1010110111",
   "0101110011101" when "1010111000",
   "0101110100000" when "1010111001",
   "0101110100011" when "1010111010",
   "0101110100110" when "1010111011",
   "0101110101001" when "1010111100",
   "0101110101100" when "1010111101",
   "0101110101111" when "1010111110",
   "0101110110010" when "1010111111",
   "0101110110101" when "1011000000",
   "0101110111000" when "1011000001",
   "0101110111011" when "1011000010",
   "0101110111101" when "1011000011",
   "0101111000000" when "1011000100",
   "0101111000011" when "1011000101",
   "0101111000110" when "1011000110",
   "0101111001001" when "1011000111",
   "0101111001100" when "1011001000",
   "0101111001111" when "1011001001",
   "0101111010010" when "1011001010",
   "0101111010101" when "1011001011",
   "0101111011000" when "1011001100",
   "0101111011011" when "1011001101",
   "0101111011110" when "1011001110",
   "0101111100001" when "1011001111",
   "0101111100100" when "1011010000",
   "0101111100111" when "1011010001",
   "0101111101010" when "1011010010",
   "0101111101101" when "1011010011",
   "0101111110000" when "1011010100",
   "0101111110011" when "1011010101",
   "0101111110110" when "1011010110",
   "0101111111001" when "1011010111",
   "0101111111100" when "1011011000",
   "0101111111111" when "1011011001",
   "0110000000010" when "1011011010",
   "0110000000101" when "1011011011",
   "0110000001000" when "1011011100",
   "0110000001011" when "1011011101",
   "0110000001110" when "1011011110",
   "0110000010001" when "1011011111",
   "0110000010100" when "1011100000",
   "0110000010111" when "1011100001",
   "0110000011010" when "1011100010",
   "0110000011101" when "1011100011",
   "0110000100000" when "1011100100",
   "0110000100011" when "1011100101",
   "0110000100110" when "1011100110",
   "0110000101001" when "1011100111",
   "0110000101100" when "1011101000",
   "0110000101111" when "1011101001",
   "0110000110010" when "1011101010",
   "0110000110101" when "1011101011",
   "0110000111000" when "1011101100",
   "0110000111011" when "1011101101",
   "0110000111110" when "1011101110",
   "0110001000001" when "1011101111",
   "0110001000101" when "1011110000",
   "0110001001000" when "1011110001",
   "0110001001011" when "1011110010",
   "0110001001110" when "1011110011",
   "0110001010001" when "1011110100",
   "0110001010100" when "1011110101",
   "0110001010111" when "1011110110",
   "0110001011010" when "1011110111",
   "0110001011101" when "1011111000",
   "0110001100000" when "1011111001",
   "0110001100011" when "1011111010",
   "0110001100110" when "1011111011",
   "0110001101010" when "1011111100",
   "0110001101101" when "1011111101",
   "0110001110000" when "1011111110",
   "0110001110011" when "1011111111",
   "0110001110110" when "1100000000",
   "0110001111001" when "1100000001",
   "0110001111100" when "1100000010",
   "0110001111111" when "1100000011",
   "0110010000010" when "1100000100",
   "0110010000110" when "1100000101",
   "0110010001001" when "1100000110",
   "0110010001100" when "1100000111",
   "0110010001111" when "1100001000",
   "0110010010010" when "1100001001",
   "0110010010101" when "1100001010",
   "0110010011000" when "1100001011",
   "0110010011100" when "1100001100",
   "0110010011111" when "1100001101",
   "0110010100010" when "1100001110",
   "0110010100101" when "1100001111",
   "0110010101000" when "1100010000",
   "0110010101011" when "1100010001",
   "0110010101111" when "1100010010",
   "0110010110010" when "1100010011",
   "0110010110101" when "1100010100",
   "0110010111000" when "1100010101",
   "0110010111011" when "1100010110",
   "0110010111110" when "1100010111",
   "0110011000010" when "1100011000",
   "0110011000101" when "1100011001",
   "0110011001000" when "1100011010",
   "0110011001011" when "1100011011",
   "0110011001110" when "1100011100",
   "0110011010010" when "1100011101",
   "0110011010101" when "1100011110",
   "0110011011000" when "1100011111",
   "0110011011011" when "1100100000",
   "0110011011110" when "1100100001",
   "0110011100010" when "1100100010",
   "0110011100101" when "1100100011",
   "0110011101000" when "1100100100",
   "0110011101011" when "1100100101",
   "0110011101111" when "1100100110",
   "0110011110010" when "1100100111",
   "0110011110101" when "1100101000",
   "0110011111000" when "1100101001",
   "0110011111100" when "1100101010",
   "0110011111111" when "1100101011",
   "0110100000010" when "1100101100",
   "0110100000101" when "1100101101",
   "0110100001001" when "1100101110",
   "0110100001100" when "1100101111",
   "0110100001111" when "1100110000",
   "0110100010010" when "1100110001",
   "0110100010110" when "1100110010",
   "0110100011001" when "1100110011",
   "0110100011100" when "1100110100",
   "0110100011111" when "1100110101",
   "0110100100011" when "1100110110",
   "0110100100110" when "1100110111",
   "0110100101001" when "1100111000",
   "0110100101101" when "1100111001",
   "0110100110000" when "1100111010",
   "0110100110011" when "1100111011",
   "0110100110110" when "1100111100",
   "0110100111010" when "1100111101",
   "0110100111101" when "1100111110",
   "0110101000000" when "1100111111",
   "0110101000100" when "1101000000",
   "0110101000111" when "1101000001",
   "0110101001010" when "1101000010",
   "0110101001110" when "1101000011",
   "0110101010001" when "1101000100",
   "0110101010100" when "1101000101",
   "0110101011000" when "1101000110",
   "0110101011011" when "1101000111",
   "0110101011110" when "1101001000",
   "0110101100010" when "1101001001",
   "0110101100101" when "1101001010",
   "0110101101000" when "1101001011",
   "0110101101100" when "1101001100",
   "0110101101111" when "1101001101",
   "0110101110010" when "1101001110",
   "0110101110110" when "1101001111",
   "0110101111001" when "1101010000",
   "0110101111101" when "1101010001",
   "0110110000000" when "1101010010",
   "0110110000011" when "1101010011",
   "0110110000111" when "1101010100",
   "0110110001010" when "1101010101",
   "0110110001101" when "1101010110",
   "0110110010001" when "1101010111",
   "0110110010100" when "1101011000",
   "0110110011000" when "1101011001",
   "0110110011011" when "1101011010",
   "0110110011110" when "1101011011",
   "0110110100010" when "1101011100",
   "0110110100101" when "1101011101",
   "0110110101001" when "1101011110",
   "0110110101100" when "1101011111",
   "0110110101111" when "1101100000",
   "0110110110011" when "1101100001",
   "0110110110110" when "1101100010",
   "0110110111010" when "1101100011",
   "0110110111101" when "1101100100",
   "0110111000001" when "1101100101",
   "0110111000100" when "1101100110",
   "0110111001000" when "1101100111",
   "0110111001011" when "1101101000",
   "0110111001110" when "1101101001",
   "0110111010010" when "1101101010",
   "0110111010101" when "1101101011",
   "0110111011001" when "1101101100",
   "0110111011100" when "1101101101",
   "0110111100000" when "1101101110",
   "0110111100011" when "1101101111",
   "0110111100111" when "1101110000",
   "0110111101010" when "1101110001",
   "0110111101110" when "1101110010",
   "0110111110001" when "1101110011",
   "0110111110101" when "1101110100",
   "0110111111000" when "1101110101",
   "0110111111100" when "1101110110",
   "0110111111111" when "1101110111",
   "0111000000011" when "1101111000",
   "0111000000110" when "1101111001",
   "0111000001010" when "1101111010",
   "0111000001101" when "1101111011",
   "0111000010001" when "1101111100",
   "0111000010100" when "1101111101",
   "0111000011000" when "1101111110",
   "0111000011011" when "1101111111",
   "0111000011111" when "1110000000",
   "0111000100010" when "1110000001",
   "0111000100110" when "1110000010",
   "0111000101001" when "1110000011",
   "0111000101101" when "1110000100",
   "0111000110000" when "1110000101",
   "0111000110100" when "1110000110",
   "0111000111000" when "1110000111",
   "0111000111011" when "1110001000",
   "0111000111111" when "1110001001",
   "0111001000010" when "1110001010",
   "0111001000110" when "1110001011",
   "0111001001001" when "1110001100",
   "0111001001101" when "1110001101",
   "0111001010000" when "1110001110",
   "0111001010100" when "1110001111",
   "0111001011000" when "1110010000",
   "0111001011011" when "1110010001",
   "0111001011111" when "1110010010",
   "0111001100010" when "1110010011",
   "0111001100110" when "1110010100",
   "0111001101010" when "1110010101",
   "0111001101101" when "1110010110",
   "0111001110001" when "1110010111",
   "0111001110100" when "1110011000",
   "0111001111000" when "1110011001",
   "0111001111100" when "1110011010",
   "0111001111111" when "1110011011",
   "0111010000011" when "1110011100",
   "0111010000111" when "1110011101",
   "0111010001010" when "1110011110",
   "0111010001110" when "1110011111",
   "0111010010001" when "1110100000",
   "0111010010101" when "1110100001",
   "0111010011001" when "1110100010",
   "0111010011100" when "1110100011",
   "0111010100000" when "1110100100",
   "0111010100100" when "1110100101",
   "0111010100111" when "1110100110",
   "0111010101011" when "1110100111",
   "0111010101111" when "1110101000",
   "0111010110010" when "1110101001",
   "0111010110110" when "1110101010",
   "0111010111010" when "1110101011",
   "0111010111101" when "1110101100",
   "0111011000001" when "1110101101",
   "0111011000101" when "1110101110",
   "0111011001000" when "1110101111",
   "0111011001100" when "1110110000",
   "0111011010000" when "1110110001",
   "0111011010100" when "1110110010",
   "0111011010111" when "1110110011",
   "0111011011011" when "1110110100",
   "0111011011111" when "1110110101",
   "0111011100010" when "1110110110",
   "0111011100110" when "1110110111",
   "0111011101010" when "1110111000",
   "0111011101110" when "1110111001",
   "0111011110001" when "1110111010",
   "0111011110101" when "1110111011",
   "0111011111001" when "1110111100",
   "0111011111101" when "1110111101",
   "0111100000000" when "1110111110",
   "0111100000100" when "1110111111",
   "0111100001000" when "1111000000",
   "0111100001100" when "1111000001",
   "0111100001111" when "1111000010",
   "0111100010011" when "1111000011",
   "0111100010111" when "1111000100",
   "0111100011011" when "1111000101",
   "0111100011110" when "1111000110",
   "0111100100010" when "1111000111",
   "0111100100110" when "1111001000",
   "0111100101010" when "1111001001",
   "0111100101110" when "1111001010",
   "0111100110001" when "1111001011",
   "0111100110101" when "1111001100",
   "0111100111001" when "1111001101",
   "0111100111101" when "1111001110",
   "0111101000001" when "1111001111",
   "0111101000100" when "1111010000",
   "0111101001000" when "1111010001",
   "0111101001100" when "1111010010",
   "0111101010000" when "1111010011",
   "0111101010100" when "1111010100",
   "0111101011000" when "1111010101",
   "0111101011011" when "1111010110",
   "0111101011111" when "1111010111",
   "0111101100011" when "1111011000",
   "0111101100111" when "1111011001",
   "0111101101011" when "1111011010",
   "0111101101111" when "1111011011",
   "0111101110011" when "1111011100",
   "0111101110110" when "1111011101",
   "0111101111010" when "1111011110",
   "0111101111110" when "1111011111",
   "0111110000010" when "1111100000",
   "0111110000110" when "1111100001",
   "0111110001010" when "1111100010",
   "0111110001110" when "1111100011",
   "0111110010010" when "1111100100",
   "0111110010101" when "1111100101",
   "0111110011001" when "1111100110",
   "0111110011101" when "1111100111",
   "0111110100001" when "1111101000",
   "0111110100101" when "1111101001",
   "0111110101001" when "1111101010",
   "0111110101101" when "1111101011",
   "0111110110001" when "1111101100",
   "0111110110101" when "1111101101",
   "0111110111001" when "1111101110",
   "0111110111101" when "1111101111",
   "0111111000000" when "1111110000",
   "0111111000100" when "1111110001",
   "0111111001000" when "1111110010",
   "0111111001100" when "1111110011",
   "0111111010000" when "1111110100",
   "0111111010100" when "1111110101",
   "0111111011000" when "1111110110",
   "0111111011100" when "1111110111",
   "0111111100000" when "1111111000",
   "0111111100100" when "1111111001",
   "0111111101000" when "1111111010",
   "0111111101100" when "1111111011",
   "0111111110000" when "1111111100",
   "0111111110100" when "1111111101",
   "0111111111000" when "1111111110",
   "0111111111100" when "1111111111",
   "-------------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_16_f210_uid54
--                     (IntAdderClassical_16_F210_uid56)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_16_f210_uid54 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(15 downto 0);
          Y : in  std_logic_vector(15 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(15 downto 0)   );
end entity;

architecture arch of IntAdder_16_f210_uid54 is
signal X_d1 :  std_logic_vector(15 downto 0);
signal Y_d1 :  std_logic_vector(15 downto 0);
signal Cin_d1 :  std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   ----------------Synchro barrier, entering cycle 1----------------
   --Classical
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                                   FPExp
--                           (FPExp_6_8_F210_uid2)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, Bogdan Pasca (2008-2013)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPExp is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(6+8+2 downto 0);
          R : out  std_logic_vector(6+8+2 downto 0)   );
end entity;

architecture arch of FPExp is
   component LeftShifter_9_by_max_15_F210_uid4 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(8 downto 0);
             S : in  std_logic_vector(3 downto 0);
             R : out  std_logic_vector(23 downto 0)   );
   end component;

   component FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             R : out  std_logic_vector(5 downto 0)   );
   end component;

   component FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             R : out  std_logic_vector(15 downto 0)   );
   end component;

   component IntAdder_10_f231_uid42 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(9 downto 0);
             Y : in  std_logic_vector(9 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(9 downto 0)   );
   end component;

   component ExpYTable_10_13_F210_uid50 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(9 downto 0);
             Y : out  std_logic_vector(12 downto 0)   );
   end component;

   component IntAdder_16_f210_uid54 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(15 downto 0);
             Y : in  std_logic_vector(15 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(15 downto 0)   );
   end component;

signal Xexn, Xexn_d1 :  std_logic_vector(1 downto 0);
signal XSign, XSign_d1 :  std_logic;
signal XexpField :  std_logic_vector(5 downto 0);
signal Xfrac :  std_logic_vector(7 downto 0);
signal e0 :  std_logic_vector(7 downto 0);
signal shiftVal :  std_logic_vector(7 downto 0);
signal resultWillBeOne :  std_logic;
signal mXu :  std_logic_vector(8 downto 0);
signal oufl0, oufl0_d1 :  std_logic;
signal shiftValIn :  std_logic_vector(3 downto 0);
signal fixX0 :  std_logic_vector(23 downto 0);
signal fixX :  std_logic_vector(15 downto 0);
signal xMulIn :  std_logic_vector(7 downto 0);
signal absK, absK_d1 :  std_logic_vector(5 downto 0);
signal minusAbsK :  std_logic_vector(6 downto 0);
signal K :  std_logic_vector(6 downto 0);
signal absKLog2 :  std_logic_vector(15 downto 0);
signal subOp1 :  std_logic_vector(9 downto 0);
signal subOp2 :  std_logic_vector(9 downto 0);
signal Y :  std_logic_vector(9 downto 0);
signal expY :  std_logic_vector(12 downto 0);
signal needNoNorm :  std_logic;
signal preRoundBiasSig :  std_logic_vector(15 downto 0);
signal roundBit :  std_logic;
signal roundNormAddend :  std_logic_vector(15 downto 0);
signal roundedExpSigRes :  std_logic_vector(15 downto 0);
signal roundedExpSig :  std_logic_vector(15 downto 0);
signal ofl1 :  std_logic;
signal ofl2 :  std_logic;
signal ofl3 :  std_logic;
signal ofl :  std_logic;
signal ufl1 :  std_logic;
signal ufl2 :  std_logic;
signal ufl3 :  std_logic;
signal ufl :  std_logic;
signal Rexn :  std_logic_vector(1 downto 0);
constant g: positive := 2;
constant wE: positive := 6;
constant wF: positive := 8;
constant wFIn: positive := 8;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            Xexn_d1 <=  Xexn;
            XSign_d1 <=  XSign;
            oufl0_d1 <=  oufl0;
            absK_d1 <=  absK;
         end if;
      end process;
   Xexn <= X(wE+wFIn+2 downto wE+wFIn+1);
   XSign <= X(wE+wFIn);
   XexpField <= X(wE+wFIn-1 downto wFIn);
   Xfrac <= X(wFIn-1 downto 0);
   e0 <= conv_std_logic_vector(21, wE+2);  -- bias - (wF+g)
   shiftVal <= ("00" & XexpField) - e0; -- for a left shift
   -- underflow when input is shifted to zero (shiftval<0), in which case exp = 1
   resultWillBeOne <= shiftVal(wE+1);
   --  mantissa with implicit bit
   mXu <= "1" & Xfrac;
   -- Partial overflow/underflow detection
   oufl0 <= not shiftVal(wE+1) when shiftVal(wE downto 0) >= conv_std_logic_vector(15, wE+1) else '0';
   ---------------- cycle 0----------------
   shiftValIn <= shiftVal(3 downto 0);
   mantissa_shift: LeftShifter_9_by_max_15_F210_uid4  -- pipelineDepth=0 maxInDelay=2.25592e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => fixX0,
                 S => shiftValIn,
                 X => mXu);
   fixX <=  fixX0(23 downto 8)when resultWillBeOne='0' else "0000000000000000";
   xMulIn <=  fixX(14 downto 7); -- truncation, error 2^-3
   mulInvLog2: FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absK,
                 X => xMulIn);
   ----------------Synchro barrier, entering cycle 1----------------
   minusAbsK <= (6 downto 0 => '0') - ('0' & absK_d1);
   K <= minusAbsK when  XSign_d1='1'   else ('0' & absK_d1);
   ---------------- cycle 0----------------
   mulLog2: FixRealKCM_5_0_M10_log_2_unsigned_F210_uid34  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absKLog2,
                 X => absK);
   subOp1 <= fixX(9 downto 0) when XSign='0' else not (fixX(9 downto 0));
   subOp2 <= absKLog2(9 downto 0) when XSign='1' else not (absKLog2(9 downto 0));
   theYAdder: IntAdder_10_f231_uid42  -- pipelineDepth=0 maxInDelay=2.75916e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '1',
                 R => Y,
                 X => subOp1,
                 Y => subOp2);

   -- Now compute the exp of this fixed-point value
   table: ExpYTable_10_13_F210_uid50  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y,
                 Y => expY);
-- signal delay at BRAM output = 1.75e-09
   needNoNorm <= expY(12);
   -- Rounding: all this should consume one row of LUTs
   preRoundBiasSig <= conv_std_logic_vector(31, wE+2)  & expY(11 downto 4) when needNoNorm = '1'
      else conv_std_logic_vector(30, wE+2)  & expY(10 downto 3) ;
   roundBit <= expY(3)  when needNoNorm = '1'    else expY(2) ;
   roundNormAddend <= K(6) & K & (7 downto 1 => '0') & roundBit;
   roundedExpSigOperandAdder: IntAdder_16_f210_uid54  -- pipelineDepth=1 maxInDelay=4.7634e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => roundedExpSigRes,
                 X => preRoundBiasSig,
                 Y => roundNormAddend);

   ----------------Synchro barrier, entering cycle 1----------------
   -- delay at adder output is 1.036e-09
   roundedExpSig <= roundedExpSigRes when Xexn_d1="01" else  "000" & (wE-2 downto 0 => '1') & (wF-1 downto 0 => '0');
   ofl1 <= not XSign_d1 and oufl0_d1 and (not Xexn_d1(1) and Xexn_d1(0)); -- input positive, normal,  very large
   ofl2 <= not XSign_d1 and (roundedExpSig(wE+wF) and not roundedExpSig(wE+wF+1)) and (not Xexn_d1(1) and Xexn_d1(0)); -- input positive, normal, overflowed
   ofl3 <= not XSign_d1 and Xexn_d1(1) and not Xexn_d1(0);  -- input was -infty
   ofl <= ofl1 or ofl2 or ofl3;
   ufl1 <= (roundedExpSig(wE+wF) and roundedExpSig(wE+wF+1))  and (not Xexn_d1(1) and Xexn_d1(0)); -- input normal
   ufl2 <= XSign_d1 and Xexn_d1(1) and not Xexn_d1(0);  -- input was -infty
   ufl3 <= XSign_d1 and oufl0_d1  and (not Xexn_d1(1) and Xexn_d1(0)); -- input negative, normal,  very large
   ufl <= ufl1 or ufl2 or ufl3;
   Rexn <= "11" when Xexn_d1 = "11"
      else "10" when ofl='1'
      else "00" when ufl='1'
      else "01";
   R <= Rexn & '0' & roundedExpSig(13 downto 0);
end architecture;

