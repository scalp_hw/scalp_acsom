--------------------------------------------------------------------------------
--                                 InputIEEE
--                          (InputIEEE_8_23_to_6_8)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2008)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity InputIEEE is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(31 downto 0);
          R : out  std_logic_vector(6+8+2 downto 0)   );
end entity;

architecture arch of InputIEEE is
signal expX :  std_logic_vector(7 downto 0);
signal fracX :  std_logic_vector(22 downto 0);
signal sX :  std_logic;
signal expZero :  std_logic;
signal expInfty :  std_logic;
signal fracZero :  std_logic;
signal unSub :  std_logic_vector(8 downto 0);
signal underflow :  std_logic;
signal ovSub :  std_logic_vector(8 downto 0);
signal overflow :  std_logic;
signal expXO :  std_logic_vector(5 downto 0);
signal resultLSB :  std_logic;
signal roundBit :  std_logic;
signal sticky :  std_logic;
signal round :  std_logic;
signal expfracR0 :  std_logic_vector(14 downto 0);
signal roundOverflow :  std_logic;
signal fracR :  std_logic_vector(7 downto 0);
signal expR :  std_logic_vector(5 downto 0);
signal NaN :  std_logic;
signal infinity :  std_logic;
signal zero :  std_logic;
signal exnR :  std_logic_vector(1 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   expX  <= X(30 downto 23);
   fracX  <= X(22 downto 0);
   sX  <= X(31);
   expZero  <= '1' when expX = (7 downto 0 => '0') else '0';
   expInfty  <= '1' when expX = (7 downto 0 => '1') else '0';
   fracZero <= '1' when fracX = (22 downto 0 => '0') else '0';
   -- min exponent value without underflow, biased with input bias: 96
   unSub <= ('0' & expX) - CONV_STD_LOGIC_VECTOR(96,9);
   underflow <= unSub(8);
   -- max exponent value without overflow, biased with input bias: 159
   ovSub <= CONV_STD_LOGIC_VECTOR(159,9)  -  ('0' & expX);
   overflow <= ovSub(8);
   -- copy exponent. Result valid only in the absence of ov/underflow
   expXO <= (not expX(5)) & expX(4 downto 0);
   -- wFO < wFI, need to round fraction
   resultLSB <= fracX(15);
   roundBit <= fracX(14);
   sticky <=  '0' when fracX(13 downto 0) = CONV_STD_LOGIC_VECTOR(0,13)   else '1';
   round <= roundBit and (sticky or resultLSB);
   -- The following addition may overflow
   expfracR0 <= ('0' & expXO & fracX(22 downto 15))  +  (CONV_STD_LOGIC_VECTOR(0,14) & round);
   roundOverflow <= expfracR0(14);
   fracR <= expfracR0(7 downto 0);
   expR <= expfracR0(13 downto 8);
   NaN <= expInfty and not fracZero;
   infinity <= (expInfty and fracZero) or (not NaN and (overflow or roundOverflow));
   zero <= expZero or underflow;
   exnR <= 
           "11" when NaN='1' 
      else "10" when infinity='1' 
      else "00" when zero='1' 
      else "01" ;  -- normal number
   R <= exnR & sX & expR & fracR; 
end architecture;

