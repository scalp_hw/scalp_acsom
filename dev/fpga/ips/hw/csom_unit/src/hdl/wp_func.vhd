----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: csom_unit - wp_func
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: csom_unit weight propagation function
--              currently implement:
--
--              alpha*e^(-(sqrt(d)/eta)*hops(c,i)/(||mi-mj||))
--
-- Last update: 2021/11/16 13:24:54
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.flopoco_pipeline_pkg.all;
use work.util_pkg.all;
use work.csom_pkg.all;

entity wp_func is

    port (
        clk_i       : in std_logic;
        rst_i       : in std_logic;
        start_i     : in std_logic;
        done_o      : out std_logic;
        addr_i      : in t_grid_addr; -- Unit grid address
        alpha_i     : in scalar_t;    -- Current learning rate
        eta_prime_i : in scalar_t;    -- Current eta_prime (-(sqrt(d)/eta))
        bmu_addr_i  : t_grid_addr;    -- BMU grid address
        d_ij_i      : in scalar_t;    -- Weight distance of current and neighbor (||mi-mj||)
        result_o    : out scalar_t    -- Function output
    );

end wp_func;

architecture rtl of wp_func is

    -----------------------------------------------------------------------------
    -- Components declaration

    component Unsigned10_2FP is
        port (
            clk : in std_logic;
            rst : in std_logic;
            I   : in std_logic_vector(9 downto 0);
            O   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component FPMult is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component FPExp is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component FPDiv is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            Y   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    -----------------------------------------------------------------------------
    -- Signals and registers

    -- Number of bits required to store in a signed the maximum index for a
    -- direction in the grid
    constant c_DIM_BITWIDTH : natural := bits_width(c_GRID_MAX_DIM - 1) + 1; -- +1 for signed

    -- Number of bits required to store in a unsigned the maximum hops between two
    -- units in a G_GRID_DIM dimentional grid with G_MAX_DIM in any direction
    constant c_HOPS_BITWIDTH : natural := bits_width(c_GRID_MAX_DIM - 1) + 2;

    signal hops_fp_reg : scalar_t;

    signal alpha_reg : scalar_t;
    signal d_ij_reg  : scalar_t;

    signal mul_in_a_s  : scalar_t;
    signal mul_in_b_s  : scalar_t;
    signal mul_out_s   : scalar_t;
    signal mul_out_reg : scalar_t;

    signal exp_out_s   : scalar_t;
    signal exp_out_reg : scalar_t;

    signal div_out_s   : scalar_t;
    signal div_out_reg : scalar_t;

    --
    signal reg_mul_en : std_logic;

    -----------------------------------------------------------------------------
    -- FSM

    type state_t is (
        s_RST,
        s_IDLE,
        s_FIRST_MUL,
        s_SEC_MUL
    );

    signal current_state : state_t;
    signal next_state    : state_t;

begin

    b_datapath : block
    begin

        b_dist : block

            signal bmu_x_reg : signed(c_DIM_BITWIDTH - 1 downto 0);
            signal bmu_y_reg : signed(c_DIM_BITWIDTH - 1 downto 0);
            signal bmu_z_reg : signed(c_DIM_BITWIDTH - 1 downto 0);

            --constant c_X_POS : signed(c_DIM_BITWIDTH - 1 downto 0) := to_signed(G_UNIT_ADDR.x, c_DIM_BITWIDTH);
            --constant c_Y_POS : signed(c_DIM_BITWIDTH - 1 downto 0) := to_signed(G_UNIT_ADDR.y, c_DIM_BITWIDTH);
            --constant c_Z_POS : signed(c_DIM_BITWIDTH - 1 downto 0) := to_signed(G_UNIT_ADDR.z, c_DIM_BITWIDTH);
            signal x_pos : signed(c_DIM_BITWIDTH - 1 downto 0);
            signal y_pos : signed(c_DIM_BITWIDTH - 1 downto 0);
            signal z_pos : signed(c_DIM_BITWIDTH - 1 downto 0);

            signal dist_x_s : unsigned(c_DIM_BITWIDTH - 1 downto 0);
            signal dist_y_s : unsigned(c_DIM_BITWIDTH - 1 downto 0);
            signal dist_z_s : unsigned(c_DIM_BITWIDTH - 1 downto 0);

            signal hops_s   : std_logic_vector(c_HOPS_BITWIDTH - 1 downto 0);
            signal hops_reg : std_logic_vector(c_HOPS_BITWIDTH - 1 downto 0);

            signal hops_fp_s : scalar_t;

        begin

            p_dist_reg : process (clk_i)
            begin
                if rising_edge(clk_i) then
                    hops_reg    <= hops_s;
                    hops_fp_reg <= hops_fp_s;
                    -- Register input signals on start
                    if start_i = '1' then
                        bmu_x_reg <= to_signed(bmu_addr_i.x, bmu_x_reg'length);
                        bmu_y_reg <= to_signed(bmu_addr_i.y, bmu_y_reg'length);
                        bmu_z_reg <= to_signed(bmu_addr_i.z, bmu_z_reg'length);
                    end if;
                end if;
            end process p_dist_reg;

            x_pos <= to_signed(addr_i.x, c_DIM_BITWIDTH);
            y_pos <= to_signed(addr_i.y, c_DIM_BITWIDTH);
            z_pos <= to_signed(addr_i.z, c_DIM_BITWIDTH);

            dist_x_s <= unsigned(abs(bmu_x_reg - x_pos));
            dist_y_s <= unsigned(abs(bmu_y_reg - y_pos));
            dist_z_s <= unsigned(abs(bmu_z_reg - z_pos));

            hops_s <= '0' & std_logic_vector(dist_x_s + dist_y_s + dist_z_s);

            i_Unsigned10_2FP : Unsigned10_2FP
            port map(
                clk => clk_i,
                rst => rst_i,
                I   => hops_reg,
                O   => hops_fp_s
            );

        end block b_dist;

        p_mul_mux : process (all)
        begin
            case current_state is
                when s_IDLE | s_FIRST_MUL =>
                    mul_in_a_s <= eta_prime_i;
                    mul_in_b_s <= div_out_reg;
                when others =>
                    mul_in_a_s <= alpha_reg;
                    mul_in_b_s <= exp_out_reg;
            end case;
        end process p_mul_mux;

        i_FPMult : FPMult
        port map(
            clk => clk_i,
            rst => rst_i,
            X   => mul_in_a_s,
            Y   => mul_in_b_s,
            R   => mul_out_s
        );

        i_FPExp : FPExp
        port map(
            clk => clk_i,
            rst => rst_i,
            X   => mul_out_reg,
            R   => exp_out_s
        );

        i_FPDiv : FPDiv
        port map(
            clk => clk_i,
            rst => rst_i,
            X   => hops_fp_reg,
            Y   => d_ij_reg,
            R   => div_out_s
        );

        p_datapath_reg : process (clk_i)
        begin
            if rising_edge(clk_i) then
                exp_out_reg <= exp_out_s;
                div_out_reg <= div_out_s;

                -- Register input signals on start
                if start_i = '1' then
                    alpha_reg <= alpha_i;
                    d_ij_reg  <= d_ij_i;
                end if;

                -- Register mulitplier output when asked
                if reg_mul_en = '1' then
                    mul_out_reg <= mul_out_s;
                end if;

            end if;
        end process p_datapath_reg;

        result_o <= mul_out_reg;

    end block b_datapath;

    b_fsm : block

        constant c_CYCLE_DIST : natural :=
        --1 +                                -- Input registering
        --1 +                                -- Combinatorial registering
        c_Unsigned10_2FP_pipeline_length + -- Conversion to FP
        1;                                 -- Output registering

        constant c_CYCLE_FIRST_MUL : natural :=
        c_CYCLE_DIST +             -- Distance computation
        c_FPDiv_pipeline_length +  -- Divistion pipeline
        1 +                        -- Output registering
        c_FPMult_pipeline_length + -- First multiplication pipeline
        1;                         -- Output registering

        constant c_CYCLE_SEC_MUL : natural :=
        c_CYCLE_FIRST_MUL +        -- First multiplication
        c_FPExp_pipeline_length +  -- Exponential pipeline
        1 +                        -- Output registering
        c_FPMult_pipeline_length + -- Second multiplication pipeline
        1;                         -- Output registering

        signal ctr_reg : unsigned(bits_width(c_CYCLE_SEC_MUL) - 1 downto 0);

    begin

        p_ctr_reg : process (clk_i, rst_i)
        begin
            if rising_edge(clk_i) then
                case current_state is
                    when s_RST | s_IDLE =>
                        ctr_reg <= (others  => '0');
                    when others         =>
                        ctr_reg <= ctr_reg + 1;
                end case;
            end if;
        end process p_ctr_reg;

        p_fsm_reg : process (clk_i, rst_i)
        begin
            if rising_edge(clk_i) then
                if rst_i = '1' then
                    current_state <= s_RST;
                else
                    current_state <= next_state;
                end if;
            end if;
        end process p_fsm_reg;

        p_fsm_comb : process (all)
        begin
            next_state <= current_state;
            reg_mul_en <= '0';
            done_o     <= '0';
            case current_state is

                when s_RST =>
                    -------------------------------------------------------------
                    -- Reset
                    -- next state
                    next_state <= s_IDLE;

                when s_IDLE =>
                    -------------------------------------------------------------
                    --
                    done_o <= '1';
                    -- next state
                    if start_i = '1' then
                        next_state <= s_FIRST_MUL;
                    end if;

                when s_FIRST_MUL =>
                    -------------------------------------------------------------
                    --
                    -- next state
                    if ctr_reg = to_unsigned(c_CYCLE_FIRST_MUL, ctr_reg'length) then
                        reg_mul_en <= '1';
                        next_state <= s_SEC_MUL;
                    end if;

                when s_SEC_MUL =>
                    -------------------------------------------------------------
                    --
                    -- next state
                    if ctr_reg = to_unsigned(c_CYCLE_SEC_MUL, ctr_reg'length) then
                        reg_mul_en <= '1';
                        next_state <= s_IDLE;
                    end if;

            end case;

        end process p_fsm_comb;

    end block b_fsm;

end architecture rtl;
