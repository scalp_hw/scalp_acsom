----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: csom_unit - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: csom_unit
--
-- Last update: 2021/11/16 13:24:33
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;

entity csom_unit is

    port (
        clk_i             : in std_logic;
        rst_i             : in std_logic;
        addr_i            : in t_grid_addr; -- Unit grid address
        ready_o           : out std_logic;  -- Unit ready
        vec_i             : in vec_t;       -- Input vector, content depend on one of the valid flags
        x_valid_i         : in std_logic;   -- vec_i contains a new input vector and is valid
        m_valid_i         : in std_logic;   -- vec_i contains a weight and is valid
        nm_valid_i        : in std_logic;   -- vec_i contains a neighbor weight and is valid
        dist_to_x_o       : out scalar_t;   -- Output distance to current input
        dist_to_x_valid_o : out std_logic;  -- Distance to current input valid
        alpha_i           : in scalar_t;    -- Current learning rate
        eta_prime_i       : in scalar_t;    -- Current eta_prime (-(sqrt(d)/eta))
        is_bmu_i          : in std_logic;   -- Notification that this unit is the BMU for current input
        is_bmu_ack_o      : out std_logic;  -- Acknowledge that weight have been updated as BMU
        bmu_addr_i        : in t_grid_addr; -- Grid address of BMU
        init_weight_i     : in std_logic;   -- unused
        vec_o             : out vec_t       -- Output vector, currently a copy of internal weight
    );

end csom_unit;

architecture arch of csom_unit is

    -----------------------------------------------------------------------------
    -- Components declaration

    component euclidean_distance is
        port (
            clk_i   : in std_logic;
            rst_i   : in std_logic;
            x_i     : in std_logic_vector(wE + wF + 2 downto 0);  -- new vector component input
            m_i     : in std_logic_vector(wE + wF + 2 downto 0);  -- weight vector component input
            valid_i : in std_logic;                               -- flag valid vector component inputs
            ready_o : out std_logic;                              -- is pipeline ready to accept input
            last_i  : in std_logic;                               -- flag last vector component inputs
            d_o     : out std_logic_vector(wE + wF + 2 downto 0); -- distance output
            valid_o : out std_logic                               -- distance output valid
        );
    end component;

    signal euclidean_distance_valid_s : std_logic;
    signal euclidean_distance_ready_s : std_logic;
    signal euclidean_distance_last_s  : std_logic;
    signal distance_valid_s           : std_logic;
    signal distance_valid_reg         : std_logic;

    component weight_update is
        port (
            clk_i   : in std_logic;
            rst_i   : in std_logic;
            valid_i : in std_logic;
            m_i     : in std_logic_vector(wE + wF + 2 downto 0); -- weight vector component input
            x_i     : in std_logic_vector(wE + wF + 2 downto 0); -- new vector component input
            alpha_i : in std_logic_vector(wE + wF + 2 downto 0); -- learning rate
            valid_o : out std_logic;
            m_o     : out std_logic_vector(wE + wF + 2 downto 0) -- weight vector component output
        );
    end component;

    signal weight_update_input_valid_s : std_logic;
    signal weight_update_output_valid  : std_logic;

    component wp_func is
        port (
            clk_i       : in std_logic;
            rst_i       : in std_logic;
            start_i     : in std_logic;
            done_o      : out std_logic;
            addr_i      : in t_grid_addr; -- Unit grid address
            alpha_i     : in scalar_t; -- Current learning rate
            eta_prime_i : in scalar_t; -- Current eta_prime (-(sqrt(d)/eta))
            bmu_addr_i  : t_grid_addr; -- BMU grid address
            d_ij_i      : in scalar_t; -- Weight distance of current and neighbor (||mi-mj||)
            result_o    : out scalar_t -- Function output
        );
    end component wp_func;

    signal wp_func_start_s : std_logic;
    signal wp_func_done_s  : std_logic;
    signal d_ij_s          : scalar_t;
    signal wp_s            : scalar_t;

    -----------------------------------------------------------------------------
    -- FSM
    type unit_state_t is (
        s_RST,
        s_IDLE,
        -- Distance computation
        s_DIST_TO_INPUT,
        s_DIST_TO_INPUT_WAIT,
        -- BMU update
        s_UPDATE_AS_BMU,
        s_UPDATE_AS_BMU_WAIT,
        s_PROP_WEIGHT_TO_NEIGHBOR,
        -- Neighbout update
        s_DIST_TO_NEIGHBOR,
        s_DIST_TO_NEIGHBOR_WAIT,
        s_COMPUTE_WP_FUNC,
        s_UPDATE_TOWARD_NEIGHBOR,
        s_UPDATE_TOWARD_NEIGHBOR_WAIT,
        s_CONTINUE_PROP
    );
    signal current_unit_state : unit_state_t;
    signal next_unit_state    : unit_state_t;

    -----------------------------------------------------------------------------
    -- Weight memory read and write index control
    signal rd_idx_rst  : std_logic;
    signal rd_idx_inc  : std_logic;
    signal rd_idx_last : std_logic;
    signal wr_idx_rst  : std_logic;
    signal wr_idx_inc  : std_logic;
    signal wr_idx_last : std_logic;

begin

    b_unit_datapath : block
        -- weight vector
        signal m_reg : vec_t;

        constant IDX_BIT_WIDTH_c : natural := bits_width(C_VEC_COMP_COUNT);
        signal rd_idx            : unsigned(IDX_BIT_WIDTH_c - 1 downto 0);
        signal wr_idx            : unsigned(IDX_BIT_WIDTH_c - 1 downto 0);

        signal dist_to_x_s          : scalar_t;
        signal dist_to_x_reg        : scalar_t;
        signal weight_update_output : scalar_t;

        signal current_x : scalar_t;
        signal current_m : scalar_t;

    begin

        -------------------------------------------------------------------------
        -- Euclidean computation block
        euclidean_distance_i : euclidean_distance
        port map(
            clk_i   => clk_i,
            rst_i   => rst_i,
            x_i     => current_x,
            m_i     => current_m,
            valid_i => euclidean_distance_valid_s,
            ready_o => euclidean_distance_ready_s,
            last_i  => euclidean_distance_last_s,
            d_o     => dist_to_x_s,
            valid_o => distance_valid_s
        );

        -- assign output to notify outer world that distance computation is valid

        p_dist_reg : process (clk_i)
        begin
            if rising_edge(clk_i) then
                distance_valid_reg <= distance_valid_s;
                if distance_valid_s = '1' then
                    dist_to_x_reg <= dist_to_x_s;
                end if;
            end if;
        end process p_dist_reg;

        dist_to_x_valid_o <= distance_valid_reg;
        dist_to_x_o       <= dist_to_x_reg;

        -------------------------------------------------------------------------
        -- wp_func

        d_ij_s <= dist_to_x_reg;

        wp_func_i : wp_func
        port map(
            clk_i       => clk_i,
            rst_i       => rst_i,
            start_i     => wp_func_start_s, -- Start computation
            done_o      => wp_func_done_s,  -- Computation done
            addr_i      => addr_i,          -- Unit grid addr
            alpha_i     => alpha_i,         -- Current learning rate
            eta_prime_i => eta_prime_i,     -- Current eta_prime (-(sqrt(d)/eta))
            bmu_addr_i  => bmu_addr_i,      -- BMU grid addr
            d_ij_i      => d_ij_s,          -- Weight distance of current and neighbor (||mi-mj||)
            result_o    => wp_s             -- Function output
        );

        -------------------------------------------------------------------------
        -- Weight update
        -- Used both for BMU weight update and update toward neighbor:
        --   - As BMU, we need to set with alpha_i = alpha
        --   - In case of update toward a neighbor, alpha_i is the result of wp_func
        --     as alpha_i is aready multiplied in wp_func result

        b_weight_update              : block
            signal weight_update_alpha_s : scalar_t;
        begin

            p_weight_update_mux : process (all)
            begin
                case current_unit_state is
                    when s_UPDATE_TOWARD_NEIGHBOR | s_UPDATE_TOWARD_NEIGHBOR_WAIT =>
                        weight_update_alpha_s <= wp_s;
                    when others =>
                        weight_update_alpha_s <= alpha_i;
                end case;
            end process p_weight_update_mux;

            weight_update_i : weight_update
            port map(
                clk_i   => clk_i,
                rst_i   => rst_i,
                valid_i => weight_update_input_valid_s,
                x_i     => current_x,
                m_i     => current_m,
                alpha_i => weight_update_alpha_s,
                valid_o => weight_update_output_valid,
                m_o     => weight_update_output
            );

        end block b_weight_update;

        -------------------------------------------------------------------------
        -- Weight vector memory and related index management
        rd_idx_last <= '1' when rd_idx = C_VEC_COMP_COUNT - 1 else
            '0';

        wr_idx_last <= '1' when wr_idx = C_VEC_COMP_COUNT - 1 else
            '0';

        -- Weight memory output
        current_m <= m_reg(to_integer(rd_idx mod C_VEC_COMP_COUNT));

        -- Input vector mux, always following the weight memory read index
        current_x <= vec_i(to_integer(rd_idx mod C_VEC_COMP_COUNT));

        p_weight_reg_fsm : process (clk_i, rst_i)
        begin
            if rising_edge(clk_i) then
                m_reg <= m_reg;
                case current_unit_state is
                    when s_IDLE =>
                        if m_valid_i = '1' then
                            -- We have been notified to use input vector
                            -- as new weight.
                            m_reg <= vec_i;
                        end if;

                    when s_UPDATE_AS_BMU | s_UPDATE_AS_BMU_WAIT | s_UPDATE_TOWARD_NEIGHBOR | s_UPDATE_TOWARD_NEIGHBOR_WAIT =>
                        if weight_update_output_valid = '1' then
                            m_reg(to_integer(wr_idx)) <= weight_update_output;
                        end if;

                    when others =>
                        null;
                end case;

                -- Manage read index increment and reset
                rd_idx <= rd_idx;
                if rd_idx_rst = '1' then
                    rd_idx <= (others => '0');
                elsif rd_idx_inc = '1' then
                    rd_idx <= rd_idx + 1;
                end if;

                -- Manage write index increment and reset
                wr_idx <= wr_idx;
                if wr_idx_rst = '1' then
                    wr_idx <= (others => '0');
                elsif wr_idx_inc = '1' then
                    wr_idx <= wr_idx + 1;
                end if;

            end if;
        end process p_weight_reg_fsm;

        -- Assign vector output, always equal to weight for now
        vec_o <= m_reg;

    end block b_unit_datapath;

    b_unit_fsm : block
    begin

        p_unit_fsm_reg : process (clk_i, rst_i)
        begin
            if rising_edge(clk_i) then
                if rst_i = '1' then
                    current_unit_state <= s_RST;
                else
                    current_unit_state <= next_unit_state;
                end if;
            end if;
        end process p_unit_fsm_reg;

        p_unit_fsm_comb : process (all)
        begin
            next_unit_state             <= current_unit_state;
            ready_o                     <= '0';
            rd_idx_rst                  <= '0';
            rd_idx_inc                  <= '0';
            wr_idx_rst                  <= '0';
            wr_idx_inc                  <= '0';
            euclidean_distance_valid_s  <= '0';
            euclidean_distance_last_s   <= '0';
            weight_update_input_valid_s <= '0';
            is_bmu_ack_o                <= '0';
            wp_func_start_s             <= '0';
            case current_unit_state is

                when s_RST =>
                    -------------------------------------------------------------
                    -- Reset
                    rd_idx_rst <= '1';
                    wr_idx_rst <= '1';
                    -- next state
                    next_unit_state <= s_IDLE;

                when s_IDLE =>
                    -------------------------------------------------------------
                    -- IDLE
                    ready_o <= '1';

                    if x_valid_i = '1' then
                        -- A new input vector needs to be handled,
                        -- go to distance computation.
                        next_unit_state <= s_DIST_TO_INPUT;
                    end if;

                    if is_bmu_i = '1' then
                        -- We have been elected as BMU, start the update weight
                        -- and propagation process
                        next_unit_state <= s_UPDATE_AS_BMU;
                    end if;

                    if m_valid_i = '1' then
                        -- m_valid_i = '1' indicate that input vector
                        -- must be loaded as new weight. This action is only allowed
                        -- during s_IDLE state, but is handled directly in
                        -- above weight_reg_fsm and does not implicates a change of
                        -- state in this main FSM. This may be the case in the
                        -- futur if we choose to load new external weight sequentially
                        next_unit_state <= s_IDLE;
                    end if;

                    if nm_valid_i = '1' then
                        -- We are notified that a neighbor weight is presented
                        -- on input vector, start the weight update and propagation
                        -- process
                        -- Try to implement the directional filtering of propagation here:

                        next_unit_state <= s_DIST_TO_NEIGHBOR;
                    end if;

                when s_DIST_TO_INPUT =>
                    -------------------------------------------------------------
                    -- Distance computation to a new input
                    -- For now, we have the full input and weight vector, so
                    -- we are always valid
                    euclidean_distance_valid_s <= '1';
                    -- if distance entity ready, we can pass to next component
                    if euclidean_distance_ready_s = '1' then
                        if rd_idx_last = '1' then
                            -- If last component of the vector, flag input as so
                            euclidean_distance_last_s <= '1';
                            -- anticipate reset of memory index
                            rd_idx_rst      <= '1';
                            next_unit_state <= s_DIST_TO_INPUT_WAIT;
                        else
                            rd_idx_inc <= '1';
                        end if;
                    end if;

                when s_DIST_TO_INPUT_WAIT =>
                    -------------------------------------------------------------
                    -- Wait distance computation
                    -- next state
                    -- If distance valid, we can return to IDLE
                    -- FIXME add handshake to ensure that computed distance
                    -- is output correctly?
                    if distance_valid_reg = '1' then
                        next_unit_state <= s_IDLE;
                    end if;

                    --
                    -- BMU branch
                    --
                when s_UPDATE_AS_BMU =>
                    -------------------------------------------------------------
                    -- Update weight in case we are elected as BMU
                    -- In this state, ensure that we feed all required input into
                    -- weight_update, then move to s_UPDATE_AS_BMU_WAIT
                    -- to finish gather outputs.

                    -- Feed w and x into update weight and manage index update
                    -- We should have a valid value at each cycle
                    weight_update_input_valid_s <= '1';
                    if rd_idx_last /= '1' then
                        rd_idx_inc <= '1';
                    else
                        next_unit_state <= s_UPDATE_AS_BMU_WAIT;
                    end if;

                    -- As ouputs may start to be ready before we have finished to
                    -- feed all the input, already start to manage output here
                    if weight_update_output_valid = '1' then
                        wr_idx_inc <= '1';
                    end if;

                when s_UPDATE_AS_BMU_WAIT =>
                    -------------------------------------------------------------
                    -- Update weight in case we are elected as BMU
                    -- In this state, we only have to gather the output of
                    -- weight_update entity, all input have already beed feed
                    -- during previous s_UPDATE_WEIGHT_BMU state.

                    -- manage write index and next state
                    if weight_update_output_valid = '1' then
                        if wr_idx_last /= '1' then
                            wr_idx_inc <= '1';
                        else
                            wr_idx_rst      <= '1';
                            rd_idx_rst      <= '1';
                            next_unit_state <= s_PROP_WEIGHT_TO_NEIGHBOR;
                        end if;
                    end if;

                when s_PROP_WEIGHT_TO_NEIGHBOR =>
                    -------------------------------------------------------------
                    -- TODO notify neighbors that they must update their weight toward us
                    is_bmu_ack_o    <= '1';
                    next_unit_state <= s_IDLE;

                    --
                    -- Update toward neightbor branch
                    --
                when s_DIST_TO_NEIGHBOR =>
                    -------------------------------------------------------------
                    -- Distance computation with direct neighbor, required as input
                    -- of wp_func constant.
                    -- For now, we have the neighbor weight and weight vector, so
                    -- we are always valid
                    euclidean_distance_valid_s <= '1';
                    -- if distance entity ready, we can pass to next component
                    if euclidean_distance_ready_s = '1' then
                        if rd_idx_last = '1' then
                            -- If last component of the vector, flag input as so
                            euclidean_distance_last_s <= '1';
                            -- anticipate reset of memory index
                            rd_idx_rst      <= '1';
                            next_unit_state <= s_DIST_TO_NEIGHBOR_WAIT;
                        else
                            rd_idx_inc <= '1';
                        end if;
                    end if;

                when s_DIST_TO_NEIGHBOR_WAIT =>
                    -------------------------------------------------------------
                    -- Wait distance computation
                    -- next state
                    if distance_valid_reg = '1' then
                        -- Start wp_func
                        wp_func_start_s <= '1';
                        next_unit_state <= s_COMPUTE_WP_FUNC;
                    end if;

                when s_COMPUTE_WP_FUNC =>
                    -------------------------------------------------------------
                    -- Wait distance computation
                    -- next state
                    if wp_func_done_s = '1' then
                        next_unit_state <= s_UPDATE_TOWARD_NEIGHBOR;
                    end if;

                when s_UPDATE_TOWARD_NEIGHBOR =>
                    -------------------------------------------------------------
                    -- In this state, ensure that we feed all required input into
                    -- weight_update, then move to s_UPDATE_TOWARD_NEIGHBOR_WAIT
                    -- to finish gather outputs.

                    -- Feed w and x into update weight and manage index update
                    -- We should have a valid value at each cycle
                    weight_update_input_valid_s <= '1';
                    if rd_idx_last /= '1' then
                        rd_idx_inc <= '1';
                    else
                        next_unit_state <= s_UPDATE_TOWARD_NEIGHBOR_WAIT;
                    end if;

                    -- As ouputs may start to be ready before we have finished to
                    -- feed all the input, already start to manage output here
                    if weight_update_output_valid = '1' then
                        wr_idx_inc <= '1';
                    end if;

                when s_UPDATE_TOWARD_NEIGHBOR_WAIT =>
                    -------------------------------------------------------------
                    -- In this state, we only have to gather the output of
                    -- weight_update entity, all input have already beed feed
                    -- during previous s_UPDATE_TOWARD_NEIGHBOR state.

                    -- manage write index and next state
                    if weight_update_output_valid = '1' then
                        if wr_idx_last /= '1' then
                            wr_idx_inc <= '1';
                        else
                            wr_idx_rst      <= '1';
                            rd_idx_rst      <= '1';
                            next_unit_state <= s_CONTINUE_PROP;
                        end if;
                    end if;

                when s_CONTINUE_PROP =>
                    -------------------------------------------------------------
                    next_unit_state <= s_IDLE;

            end case;
        end process p_unit_fsm_comb;

    end block b_unit_fsm;

end arch;
