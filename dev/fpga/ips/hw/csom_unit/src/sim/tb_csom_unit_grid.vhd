----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_csom_unit_grid - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for a grid of csom_unit
--
-- Last update: 2021/11/16 13:31:25
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

library std;
use std.env.all;
use std.textio.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_csom_unit_grid is
    generic (
        CLK_PERIOD : time := 8 ns -- clock period for clk
    );
end entity;

architecture behavioral of tb_csom_unit_grid is
    ----------------------------------------------------------------------------
    -- Constants & configuration
    ----------------------------------------------------------------------------

    constant PCLK_PERIOD : time := CLK_PERIOD;

    -- DUT name string
    constant c_DUT_NAME : string := "csom_unit_grid";

    -- Number of input to present to the CSOM network
    constant c_TEST_INPUT_COUNT : natural := 1000;

    -- Print additionnal debug messages of the diffusion algorithm.
    constant c_DEBUG_DIFF : boolean := false;

    -- Dump full network weight after each weight update
    constant c_DUMP_ALL_WEIGHT : boolean := false;

    -- Epsilon used for floating point and FP comparison
    constant c_EPSILON : real := 1.0e-6;

    -- Learning rate alpha
    constant c_ALPHA : real := real(0.5);

    -- Elasticity eta
    constant c_ETA : real := real(0.5);

    -- Project's root relative path
    --constant c_ROOT_PATH: string := "../../../../../../../../../../";
    constant c_ROOT_PATH: string := "";

    -- Data dump path
    --constant c_DUMPS_PATH : string := c_ROOT_PATH & "tb_results/"
    constant c_DUMPS_PATH : string := c_ROOT_PATH & "";

    ----------------------------------------------------------------------------
    -- Signals for internal operation
    ----------------------------------------------------------------------------

    --- clock signals ---
    signal clk_sti : std_logic := '1';
    signal rst_sti : std_logic := '1';

    --- DUT input ---
    signal vec_sti          : vec_t;       -- Global input vector
    signal x_valid_sti      : std_logic;   -- Global vec_i contains a new input vector and is valid
    signal m_valid_xyz_sti  : t_slv_dim3;  -- vec_i contains a weight and is valid
    signal nm_valid_xyz_sti : t_slv_dim3;  -- vec_i contains a neighbor weight and is valid
    signal alpha_sti        : scalar_t;    -- Global current learning rate
    signal eta_prime_sti    : scalar_t;    -- Global eta prime
    signal is_bmu_xyz_sti   : t_slv_dim3;  -- Notification that this unit is the BMU for current input
    signal bmu_addr_sti     : t_grid_addr; -- BMU grid addr
    signal init_weight_sti  : std_logic;   -- Unused

    --- DUT output ---
    signal ready_xyz_obs           : t_slv_dim3;
    signal dist_to_x_xyz_obs       : t_scalar_dim3; -- Output distance to current input
    signal dist_to_x_valid_xyz_obs : t_slv_dim3;    -- Distance to current input valid
    signal is_bmu_ack_xyz_obs      : t_slv_dim3;    --
    signal vec_xyz_obs             : t_vec_dim3;

    ----------------------------------------------------------------------------
    -- DUT component declaration
    ----------------------------------------------------------------------------

    component csom_unit is
        --generic (
        --    G_UNIT_ADDR : t_grid_addr
        --);
        port (
            clk_i             : in std_logic;
            rst_i             : in std_logic;
            addr_i            : in t_grid_addr; -- Unit grid address
            ready_o           : out std_logic;  -- Unit ready
            vec_i             : in vec_t;       -- Input vector, content depend on one of the valid flags
            x_valid_i         : in std_logic;   -- vec_i contains a new input vector and is valid
            m_valid_i         : in std_logic;   -- vec_i contains a weight and is valid
            nm_valid_i        : in std_logic;   -- vec_i contains a neighbor weight and is valid
            dist_to_x_o       : out scalar_t;   -- Output distance to current input
            dist_to_x_valid_o : out std_logic;  -- Distance to current input valid
            alpha_i           : in scalar_t;    -- Current learning rate
            eta_prime_i       : in scalar_t;    -- Current elasticity
            is_bmu_i          : in std_logic;   -- Notification that this unit is the BMU for current input
            is_bmu_ack_o      : out std_logic;  -- Acknowledge that weight have been updated as BMU
            bmu_addr_i        : in t_grid_addr; -- Grid address of BMU
            init_weight_i     : in std_logic;   -- Unused
            vec_o             : out vec_t       -- Output vector, currently a copy of internal weight
        );
    end component;

begin

    ----------------------------------------------------------------------------
    -- DUT instantiation
    ----------------------------------------------------------------------------

    gen_unit_x : for x_idx in 0 to (c_GRID_X_DIM - 1) generate

        gen_unit_y : for y_idx in 0 to (c_GRID_Y_DIM - 1) generate

            gen_unit_z : for z_idx in 0 to (c_GRID_Z_DIM - 1) generate

                logger.log_note("Generating unit " & integer'image(x_idx) & " " & integer'image(y_idx) & " " & integer'image(z_idx));

                i_csom_unit_xyz : csom_unit
                --generic map(
                --    G_UNIT_ADDR => (x => x_idx, y => y_idx, z => z_idx)
                --)
                port map(
                    clk_i             => clk_sti,
                    rst_i             => rst_sti,
                    addr_i            => (x => x_idx, y => y_idx, z => z_idx),
                    ready_o           => ready_xyz_obs(x_idx)(y_idx)(z_idx),           -- Unit is ready
                    vec_i             => vec_sti,                                      -- Input vector
                    x_valid_i         => x_valid_sti,                                  -- vec_i contains a new input vector and is valid
                    m_valid_i         => m_valid_xyz_sti(x_idx)(y_idx)(z_idx),         -- vec_i contains a weight and is valid
                    nm_valid_i        => nm_valid_xyz_sti(x_idx)(y_idx)(z_idx),        -- vec_i contains a neighbor weight and is valid
                    dist_to_x_o       => dist_to_x_xyz_obs(x_idx)(y_idx)(z_idx),       -- Output distance to current input
                    dist_to_x_valid_o => dist_to_x_valid_xyz_obs(x_idx)(y_idx)(z_idx), -- Distance to current input valid
                    alpha_i           => alpha_sti,                                    -- Current learning rate
                    eta_prime_i       => eta_prime_sti,                                -- Elasticity
                    is_bmu_i          => is_bmu_xyz_sti(x_idx)(y_idx)(z_idx),          -- Notification that this unit is the BMU for current input
                    is_bmu_ack_o      => is_bmu_ack_xyz_obs(x_idx)(y_idx)(z_idx),      --
                    bmu_addr_i        => bmu_addr_sti,                                 -- BMU grid address
                    init_weight_i     => init_weight_sti,                              -- Unused
                    vec_o             => vec_xyz_obs(x_idx)(y_idx)(z_idx)              -- Weight vector
                );

            end generate gen_unit_z;

        end generate gen_unit_y;

    end generate gen_unit_x;

    ----------------------------------------------------------------------------
    -- CLOCK GENERATION
    ----------------------------------------------------------------------------
    clk_proc : process is
    begin
        loop
            clk_sti <= not clk_sti;
            wait for PCLK_PERIOD / 2;
        end loop;
    end process clk_proc;

    ----------------------------------------------------------------------------
    tb1 : process is
        file dump_file : text;

        variable seed1              : integer := 1;
        variable seed2              : integer := 1;
        constant c_PREFIX           : string  := " | --> ";
        variable cycle_count        : natural := 0;
        variable test_count         : natural := 0;
        variable bmu                : t_grid_addr;
        variable new_in             : real_vec_t;
        variable diffusion_type_xyz : t_diffusion_type_dim3 := (others => (others => (others => DIFF_XYZ)));
        variable input_count        : integer;

        --* Setup default signal values
        procedure setup_default_signals is
        begin
            rst_sti          <= '0';
            init_weight_sti  <= '0';
            vec_sti          <= (others => to_scalar_t(0.0));
            alpha_sti        <= to_scalar_t(0.0);
            eta_prime_sti    <= to_scalar_t(0.0);
            x_valid_sti      <= '0';
            m_valid_xyz_sti  <= (others => (others => (others => '0')));
            nm_valid_xyz_sti <= (others => (others => (others => '0')));
            is_bmu_xyz_sti   <= (others => (others => (others => '0')));
            bmu_addr_sti     <= (x => 0, y => 0, z => 0);
            wait until rising_edge(clk_sti);
        end;

        --* Pulse clock until the specified unit's ready_obs is high
        procedure wait_unit_ready(
            unit_addr : t_grid_addr
        ) is
        begin
            loop
                wait until rising_edge(clk_sti);
                if ready_xyz_obs(unit_addr.x)(unit_addr.y)(unit_addr.z) = '1' then
                    exit;
                end if;
            end loop;
        end;

        --* Pulse clock until all ready_obs are high
        procedure wait_grid_ready is
        begin
            loop
                wait until rising_edge(clk_sti);
                if and_reduce(ready_xyz_obs) = '1' then
                    exit;
                end if;
            end loop;
        end;

        --* Perform a pulse on rst_sti and wait that all units are ready
        procedure reset_grid is
        begin
            rst_sti <= '1';
            wait until rising_edge(clk_sti);
            rst_sti <= '0';
            wait_grid_ready;
        end;

        --------------------------------------------------------------------

        --* Write dump header to a file
        procedure dump_header(
            file f : text
        ) is
            variable tmp_line : line;
            -- Work around for vivado bug: real'image doesn't produces
            -- correct output when used with constants
            variable alpha : real := c_ALPHA;
            variable eta   : real := c_ETA;
        begin
            write(tmp_line, string'("# grid size: (" & integer'image(c_GRID_X_DIM) & ", "));
            write(tmp_line, string'(integer'image(c_GRID_Y_DIM) & ", "));
            write(tmp_line, string'(integer'image(c_GRID_Z_DIM) & ")"));
            writeline(f, tmp_line);

            write(tmp_line, "# alpha: " & real'image(alpha));
            writeline(f, tmp_line);

            write(tmp_line, "# eta: " & real'image(eta));
            writeline(f, tmp_line);

            write(tmp_line, string'("# input count: " & integer'image(c_TEST_INPUT_COUNT)));
            writeline(f, tmp_line);

            write(tmp_line, string'("# source: tb_" & c_DUT_NAME & ".vhd"));
            writeline(f, tmp_line);

            write(tmp_line, string'("# distribution: vhdl uniform + 2 offsets"));
            writeline(f, tmp_line);

            write(tmp_line, string'("# init state: vhdl uniform"));
            writeline(f, tmp_line);
        end;

        --* Dump grid address and weights of a unit to a provided file
        procedure dump_unit_weights(
            file f     : text;
            unit_addr  : t_grid_addr;
            iter_count : integer
        ) is
            variable tmp_line : line;
        begin
            write(tmp_line, string'(integer'image(iter_count)));
            write(tmp_line, string'(" : unit  : "));
            write(tmp_line, string'(to_string(unit_addr)));
            write(tmp_line, string'(" : "));
            write(tmp_line, string'(to_string(vec_xyz_obs(unit_addr.x)(unit_addr.y)(unit_addr.z))));
            writeline(f, tmp_line);
        end;

        --* Dump the full grid weights to a file
        procedure dump_grid_weights(
            file f     : text;
            iter_count : integer
        ) is
            variable tmp_line : line;
        begin
            for x in 0 to (c_GRID_X_DIM - 1) loop
                for y in 0 to (c_GRID_Y_DIM - 1) loop
                    for z in 0 to (c_GRID_Z_DIM - 1) loop
                        dump_unit_weights(f, (x => x, y => y, z => z), iter_count);
                    end loop;
                end loop;
            end loop;
        end;

        --* Dump an input value to a file
        procedure dump_input(
            file f     : text;
            new_in     : real_vec_t;
            iter_count : integer
        ) is
            variable tmp_line : line;
        begin
            write(tmp_line, string'(integer'image(iter_count)));
            write(tmp_line, string'(" : input : (0, 0, 0) : "));
            write(tmp_line, string'(to_string(new_in)));
            writeline(f, tmp_line);
        end;

        --* Dump a BMU weight to a file
        procedure dump_bmu_weights(
            file f     : text;
            bmu_addr   : t_grid_addr;
            iter_count : integer
        ) is
            variable tmp_line : line;
        begin
            write(tmp_line, string'(integer'image(iter_count)));
            write(tmp_line, string'(" : bmu   : "));
            write(tmp_line, string'(to_string(bmu_addr)));
            write(tmp_line, string'(" : "));
            write(tmp_line, string'(to_string(vec_xyz_obs(bmu_addr.x)(bmu_addr.y)(bmu_addr.z))));
            writeline(f, tmp_line);
        end;

        -- Open a dump file
        procedure open_dump_file(
            file f   : text;
            filename : in string
        ) is
        begin
            file_open(f, filename, WRITE_MODE);
        end;

        -- Close a dump file
        procedure close_dump_file(
            file f : text
        ) is
        begin
            file_close(f);
        end;

        --------------------------------------------------------------------

        --* Randomize weight of all units with uniform distribution 0.0-1.0
        procedure randomize_grid_weights is
            variable random_weight : real_vec_t;
        begin
            for x in 0 to (c_GRID_X_DIM - 1) loop
                for y in 0 to (c_GRID_Y_DIM - 1) loop
                    for z in 0 to (c_GRID_Z_DIM - 1) loop
                        randomize_vect(seed1, seed2, random_weight);
                        vec_sti                  <= to_vec_t(random_weight);
                        m_valid_xyz_sti(x)(y)(z) <= '1';
                        wait until rising_edge(clk_sti);
                        m_valid_xyz_sti(x)(y)(z) <= '0';
                        wait until rising_edge(clk_sti);
                    end loop;
                end loop;
            end loop;
        end;

        -- Drive a new input to the grid and wait that all units compute their
        -- distances
        procedure drive_new_input(
            new_input : in real_vec_t;
            alpha     : in real
        ) is
        begin
            wait_grid_ready;
            logger.log_note(c_PREFIX & "New input: " & to_string(new_input));
            logger.log_note(c_PREFIX & "alpha: " & real'image(alpha));
            vec_sti     <= to_vec_t(new_input);
            alpha_sti   <= to_scalar_t(alpha);
            x_valid_sti <= '1';
            wait until rising_edge(clk_sti);
            x_valid_sti <= '0';
            loop
                wait until rising_edge(clk_sti);
                if and_reduce(dist_to_x_valid_xyz_obs) = '1' then
                    exit;
                end if;
            end loop;
        end;

        -- Find the BMU, the units which expose the smallest distance
        -- to the last input driven
        procedure find_BMU(
            bmu_addr : inout t_grid_addr
        ) is
            variable bmu_candidate      : t_grid_addr;
            variable bmu_candidate_dist : real := real'high;
        begin
            bmu_candidate := (others => 0);
            for x in 0 to (c_GRID_X_DIM - 1) loop
                for y in 0 to (c_GRID_Y_DIM - 1) loop
                    for z in 0 to (c_GRID_Z_DIM - 1) loop
                        if to_real(dist_to_x_xyz_obs(x)(y)(z)) < bmu_candidate_dist then
                            bmu_candidate_dist := to_real(dist_to_x_xyz_obs(x)(y)(z));
                            bmu_candidate      := (x => x, y => y, z => z);
                        end if;
                    end loop;
                end loop;
            end loop;
            logger.log_warning(c_PREFIX & "BMU: " & to_string(bmu_candidate) & " distance: " & real'image(bmu_candidate_dist));
            bmu_addr := bmu_candidate;
        end;

        -- Ask the BMU to update it weight toward the last input
        procedure update_bmu_weight(
            bmu_addr : t_grid_addr
        ) is
        begin
            logger.log_note(c_PREFIX & "BMU weight vector before weight update: " & to_string(vec_xyz_obs(bmu.x)(bmu.y)(bmu.z)));
            wait until rising_edge(clk_sti);
            -- Notify BMU
            is_bmu_xyz_sti                      <= (others => (others => (others => '0')));
            is_bmu_xyz_sti(bmu.x)(bmu.y)(bmu.z) <= '1';
            wait until rising_edge(clk_sti);
            is_bmu_xyz_sti <= (others => (others => (others => '0')));
            wait_unit_ready(bmu_addr);
            logger.log_note(c_PREFIX & "BMU weight vector after weight update: " & to_string(vec_xyz_obs(bmu.x)(bmu.y)(bmu.z)));
        end;

        -- Update unit weight toward a neighbor
        procedure update_unit_weight(
            local_addr : in t_grid_addr;
            last_addr  : in t_grid_addr;
            bmu_addr   : in t_grid_addr;
            alpha      : in real;
            eta        : in real
        ) is
            constant eta_prime : real := - 1.0 * (sqrt(real(c_VEC_COMP_COUNT))/eta);
        begin
            if c_DEBUG_DIFF then
                logger.log_note(c_PREFIX & "Unit weight before update: " & to_string(vec_xyz_obs(local_addr.x)(local_addr.y)(local_addr.z)));
                logger.log_note(c_PREFIX & "Neighbor weight: " & to_string(vec_xyz_obs(last_addr.x)(last_addr.y)(last_addr.z)));
            end if;
            -- Notify unit
            nm_valid_xyz_sti(local_addr.x)(local_addr.y)(local_addr.z) <= '1';
            vec_sti                                                    <= vec_xyz_obs(last_addr.x)(last_addr.y)(last_addr.z);
            alpha_sti                                                  <= to_scalar_t(alpha);
            eta_prime_sti                                              <= to_scalar_t(eta_prime);
            bmu_addr_sti                                               <= bmu_addr;
            wait until rising_edge(clk_sti);
            nm_valid_xyz_sti(local_addr.x)(local_addr.y)(local_addr.z) <= '0';
            wait_unit_ready(local_addr);
            if c_DEBUG_DIFF then
                logger.log_note(c_PREFIX & "Weight after update: " & to_string(vec_xyz_obs(local_addr.x)(local_addr.y)(local_addr.z)));
            end if;
        end;

        -- Recursive implementation of the rotating diffusion algorithm
        procedure diffuse_to(
            dir       : in t_direction;
            diff_type : in t_diffusion_type;
            last_addr : in t_grid_addr;
            bmu_addr  : in t_grid_addr;
            alpha     : in real;
            eta       : in real
        ) is
            variable received_dir : t_direction;
            variable forward_dirs : t_direction;
            variable temp_dir     : t_direction;
            variable local_addr   : t_grid_addr;
        begin
            -- Determine from which direction we have received the update
            received_dir := get_opposite_direction(dir);
            -- Address update and guard
            local_addr := last_addr;
            case received_dir is
                when c_DIR_EAST =>
                    if local_addr.x > 0 then
                        local_addr.x := local_addr.x - 1;
                    else
                        return;
                    end if;
                when c_DIR_WEST =>
                    if local_addr.x < c_GRID_X_DIM - 1 then
                        local_addr.x := local_addr.x + 1;
                    else
                        return;
                    end if;
                when c_DIR_NORTH =>
                    if local_addr.y > 0 then
                        local_addr.y := local_addr.y - 1;
                    else
                        return;
                    end if;
                when c_DIR_SOUTH =>
                    if local_addr.y < c_GRID_Y_DIM - 1 then
                        local_addr.y := local_addr.y + 1;
                    else
                        return;
                    end if;
                when c_DIR_UP =>
                    if local_addr.z > 0 then
                        local_addr.z := local_addr.z - 1;
                    else
                        return;
                    end if;
                when c_DIR_DOWN =>
                    if local_addr.z < c_GRID_Z_DIM - 1 then
                        local_addr.z := local_addr.z + 1;
                    else
                        return;
                    end if;
                when others =>
                    logger.log_error("Invalid direction in diffuse_to()");
                    return;
            end case;
            if c_DEBUG_DIFF then
                logger.log_note("Received from " & dir_to_string(received_dir) & " (direction was " & dir_to_string(dir) & ")");
            end if;
            -- Update local unit
            if c_DEBUG_DIFF then
                logger.log_note("Updating unit " & to_string(local_addr) & " toward " & to_string(last_addr));
            end if;
            update_unit_weight(local_addr, last_addr, bmu_addr, alpha => alpha, eta => eta);
            -- Check on which direction we need to update
            forward_dirs := get_forwarding_directions(diff_type, received_dir);
            -- loop over direction and diffuse if required
            for I in t_direction'range loop
                temp_dir := (I => '1', others => '0');
                if dir_in_list(forward_dirs, temp_dir) then
                    if c_DEBUG_DIFF then
                        logger.log_note("Diffusing to " & dir_to_string(temp_dir));
                    end if;
                    diffuse_to(dir => temp_dir, diff_type => diff_type, last_addr => local_addr, bmu_addr => bmu_addr, alpha => alpha, eta => eta);
                end if;
            end loop;
        end;

        -- Perform a full grid weights update from the BMU, using the rotative diffusion algorithm
        -- be recusively calling diffuse_to() and updating the diffusion mechanism once finished for
        -- the current BMU once finished.
        procedure update_grid_weights(
            bmu_addr : t_grid_addr;
            alpha    : in real;
            eta      : in real
        ) is
            variable diff_type : t_diffusion_type;
            variable temp_dir  : t_direction;
        begin
            -- Get diffusion mechanism for this unit
            diff_type := diffusion_type_xyz(bmu_addr.x)(bmu_addr.y)(bmu_addr.z);
            logger.log_note("Update grid using " & to_string(diff_type) & " mechanism from" & to_string(bmu_addr));
            logger.log_note("alpha:  " & real'image(alpha) & ", eta: " & real'image(eta));
            -- BMU is the only unit to diffuse in all directions
            for I in t_direction'range loop
                temp_dir := (I => '1', others => '0');
                if c_DEBUG_DIFF then
                    logger.log_note("Diffusing to " & dir_to_string(temp_dir));
                end if;
                diffuse_to(dir => t_direction'(temp_dir), diff_type => diff_type, last_addr => bmu_addr, bmu_addr => bmu_addr, alpha => alpha, eta => eta);
            end loop;
            -- Update diffusion mechanism for next propagation from this unit
            diffusion_type_xyz(bmu_addr.x)(bmu_addr.y)(bmu_addr.z) := get_next_diff_mecha(diff_type);
        end;

        -------------------------------------------------------------------------------

        -- Generate next input with a 0.5 probability to be in 0.0-0.5 (on all axis),
        -- and 0.5 probability to be in 0.5-1.0 (on all axis).
        procedure get_new_input(variable seed1, seed2 : inout integer; variable vec : out real_vec_t) is
            variable rand : real;
            variable pop  : real;
        begin
            uniform(seed1, seed2, pop);
            for i in 0 to vec'length - 1 loop
                uniform(seed1, seed2, rand); -- generate random number
                if pop < 0.5 then
                    vec(i) := rand * 0.5;
                else
                    vec(i) := rand * 0.5 + 0.5;
                end if;
            end loop;
        end;

        -------------------------------------------------------------------------------

    begin

        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_" & c_DUT_NAME & "_report.html",
        "Testbench report: " & "tb_" & c_DUT_NAME,
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("Scalar representation: wE: " & integer'image(wE) & ", wF: " & integer'image(wF));
        logger.log_note("Vector component count: C_VEC_COMP_COUNT: " & integer'image(C_VEC_COMP_COUNT));
        logger.log_note("Epsilon: " & to_string(to_scalar_t(c_EPSILON)));
        logger.log_note("CSOM units grid sizes: " &
        " (x: " & integer'image(c_GRID_X_DIM) &
        ", y: " & integer'image(c_GRID_Y_DIM) &
        ", z: " & integer'image(c_GRID_Z_DIM) & ")");

        open_dump_file(dump_file, c_DUMPS_PATH & "csom_data.txt");
        dump_header(dump_file);
        ------------------------------------------------------------------------
        -- Reset and vectors setup
        setup_default_signals;

        logger.log_note("Reset and wait for initial weight vectors initialization");
        reset_grid;

        if (true) then
            logger.log_note("Randomizing weights");
            randomize_grid_weights;
        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        if true then
            test_count := test_count + 1;
            logger.log_note("Test " & integer'image(test_count + 1));
            input_count := 0;

            -- Always dump initial weights for full grid
            dump_grid_weights(dump_file, 0);

            loop
                ----------------------------------------------------------------
                -- Get a new input
                --randomize_vect(seed1, seed2, new_in);
                get_new_input(seed1, seed2, new_in);
                dump_input(dump_file, new_in, input_count);
                -- Drive input to full grid
                drive_new_input(new_input => new_in, alpha => c_ALPHA);
                -- Elect BMU
                find_BMU(bmu_addr => bmu);
                dump_bmu_weights(dump_file, bmu, input_count);
                -- Update BMU weight
                update_bmu_weight(bmu_addr => bmu);
                -- Update grid weight
                update_grid_weights(bmu_addr => bmu, alpha => c_ALPHA, eta => c_ETA);
                --
                input_count := input_count + 1;
                if input_count = c_TEST_INPUT_COUNT then
                    exit;
                end if;
                -- If enabled, dump weight at each iteration
                if c_DUMP_ALL_WEIGHT then
                    dump_grid_weights(dump_file, input_count);
                end if;
                ----------------------------------------------------------------
            end loop;

            -- Always dump final weights for full grid
            dump_grid_weights(dump_file, input_count);

        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        -- Empty pipeline, do final report and stop simulation
        for I in 1 to 500 loop
            wait until rising_edge(clk_sti);
        end loop;
        close_dump_file(dump_file);
        logger.final_report;
        stop(0);

    end process tb1;
    --  End Test Bench

end architecture behavioral;
