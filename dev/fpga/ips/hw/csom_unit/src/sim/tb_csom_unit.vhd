----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_csom_unit - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for csom_unit
--
-- Last update: 2021/11/16 13:52:19
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

library std;
use std.env.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_csom_unit is
    generic (
        CLK_PERIOD : time := 8 ns -- clock period for clk
    );
end entity;

architecture behavioral of tb_csom_unit is

    ----------------------------------------------------------------------------
    -- Constants
    ----------------------------------------------------------------------------
    constant c_DUT_NAME  : string      := "csom_unit";
    constant PCLK_PERIOD : time        := CLK_PERIOD;
    constant c_EPSILON   : real        := 1.0e-5;
    constant c_UNIT_ADDR : t_grid_addr := (x => 0, y => 0, z => 0);

    ----------------------------------------------------------------------------
    -- Signals for internal operation
    ----------------------------------------------------------------------------

    --- clock signals ---
    signal clk_sti : std_logic := '0';
    signal rst_sti : std_logic := '0';

    --- DUT input ---
    signal addr_sti        : t_grid_addr;
    signal vec_sti         : vec_t;
    signal x_valid_sti     : std_logic;
    signal m_valid_sti     : std_logic;
    signal nm_valid_sti    : std_logic;
    signal alpha_sti       : scalar_t;
    signal eta_prime_sti   : scalar_t;
    signal is_bmu_sti      : std_logic;
    signal init_weight_sti : std_logic;
    signal bmu_addr_sti    : t_grid_addr;

    --- DUT output ---
    signal ready_obs           : std_logic;
    signal dist_to_x_obs       : scalar_t;
    signal dist_to_x_valid_obs : std_logic;
    signal is_bmu_ack_obs      : std_logic;

    --- DUT debug output
    signal vec_obs : vec_t;

    ----------------------------------------------------------------------------
    -- DUT component declaration
    ----------------------------------------------------------------------------

    component csom_unit is
        --generic (
        --    G_UNIT_ADDR : t_grid_addr
        --);
        port (
            clk_i             : in std_logic;
            rst_i             : in std_logic;
            addr_i            : in t_grid_addr; -- Unit grid address
            ready_o           : out std_logic;  -- Unit ready
            vec_i             : in vec_t;       -- Input vector, content depend on one of the valid flags
            x_valid_i         : in std_logic;   -- vec_i contains a new input vector and is valid
            m_valid_i         : in std_logic;   -- vec_i contains a weight and is valid
            nm_valid_i        : in std_logic;   -- vec_i contains a neighbor weight and is valid
            dist_to_x_o       : out scalar_t;   -- Output distance to current input
            dist_to_x_valid_o : out std_logic;  -- Distance to current input valid
            alpha_i           : in scalar_t;    -- Current learning rate
            eta_prime_i       : in scalar_t;    -- Current eta_prime (-(sqrt(d)/eta))
            is_bmu_i          : in std_logic;   -- Notification that this unit is the BMU for current input
            is_bmu_ack_o      : out std_logic;  -- Acknowledge that weight have been updated as BMU
            bmu_addr_i        : in t_grid_addr; -- Grid address of BMU
            init_weight_i     : in std_logic;   -- Unused
            vec_o             : out vec_t       -- Output vector, currently a copy of internal weight
        );
    end component;

begin

    ----------------------------------------------------------------------------
    -- DUT instantiation
    ----------------------------------------------------------------------------

    DUT_i : csom_unit
    --generic map(
    --    G_UNIT_ADDR => c_UNIT_ADDR
    --)
    port map(
        clk_i             => clk_sti,
        rst_i             => rst_sti,
        addr_i            => addr_sti,
        ready_o           => ready_obs,
        vec_i             => vec_sti,
        x_valid_i         => x_valid_sti,
        m_valid_i         => m_valid_sti,
        nm_valid_i        => nm_valid_sti,
        dist_to_x_o       => dist_to_x_obs,
        dist_to_x_valid_o => dist_to_x_valid_obs,
        alpha_i           => alpha_sti,
        eta_prime_i       => eta_prime_sti,
        is_bmu_i          => is_bmu_sti,
        is_bmu_ack_o      => is_bmu_ack_obs,
        bmu_addr_i        => bmu_addr_sti,
        init_weight_i     => init_weight_sti,
        vec_o             => vec_obs
    );

    ----------------------------------------------------------------------------
    -- CLOCK GENERATION
    ----------------------------------------------------------------------------
    clk_proc : process is
    begin
        loop
            clk_sti <= not clk_sti;
            wait for PCLK_PERIOD / 2;
        end loop;
    end process clk_proc;

    ----------------------------------------------------------------------------
    -- TEST BENCH STIMULI
    ----------------------------------------------------------------------------
    tb1 : process is

        constant c_PREFIX    : string  := " |--> ";
        variable cycle_count : natural := 0;
        variable test_count  : natural := 0;
        --
        variable seed1 : integer := 1;
        variable seed2 : integer := 1;
        --
        variable weight   : real_vec_t;
        variable new_in   : real_vec_t;
        --variable real_value    : real;

        --* Setup default signal values
        procedure setup_default_signals is
        begin
            rst_sti         <= '0';
            addr_sti        <= c_UNIT_ADDR;
            init_weight_sti <= '0';
            vec_sti         <= (others => to_scalar_t(0.0));
            alpha_sti       <= to_scalar_t(0.0);
            eta_prime_sti   <= to_scalar_t(0.0);
            x_valid_sti     <= '0';
            m_valid_sti     <= '0';
            nm_valid_sti    <= '0';
            is_bmu_sti      <= '0';
            bmu_addr_sti    <= (x => 0, y => 0, z => 0);
            wait until rising_edge(clk_sti);
        end;

        --* Pulse clock until ready_obs is high
        procedure wait_ready is
        begin
            loop
                wait until rising_edge(clk_sti);
                if ready_obs = '1' then
                    exit;
                end if;
            end loop;
        end;

        --* Perform a pulse on rst_sti and wait that unit is ready
        procedure reset_unit is
        begin
            rst_sti <= '1';
            wait until rising_edge(clk_sti);
            rst_sti <= '0';
            wait_ready;
        end;

        --* Load new vector as weight and ensure that it is correctly loaded
        procedure init_weight(
            vec : in real_vec_t
        ) is
        begin
            -- Drive
            vec_sti     <= to_vec_t(vec);
            m_valid_sti <= '1';
            wait until rising_edge(clk_sti);
            m_valid_sti <= '0';
            wait until rising_edge(clk_sti);
            -- Check output
            if vec_obs = vec_sti then
                logger.log_note(c_PREFIX & "Weight vector after initialization: " & to_string(vec_obs));
            else
                logger.log_error(c_PREFIX & "Error: weight vector after initialization: " & to_string(vec_obs));
                logger.log_error(c_PREFIX & "Expected: " & to_string(vec_sti));
            end if;
        end;

        --* Drive a new input and ensure that resulting distance computation is correct
        procedure drive_input(
            new_input   : in real_vec_t;
            unit_weight : in real_vec_t
        ) is
            variable dist : real;
            variable clk_count : integer := 0;
        begin
            -- Drive
            logger.log_note(c_PREFIX & "New input: " & to_string(new_input));
            clk_count := 0;
            vec_sti     <= to_vec_t(new_input);
            x_valid_sti <= '1';
            wait until rising_edge(clk_sti); clk_count := clk_count + 1;
            x_valid_sti <= '0';
            loop
                wait until rising_edge(clk_sti); clk_count := clk_count + 1;
                if ready_obs = '1' then
                    exit;
                end if;
            end loop;
            -- Compute reference
            dist := 0.0;
            for I in vec_sti'range loop
                dist := dist + (unit_weight(I) - new_input(I)) ** 2;
            end loop;
            -- Compare
            if are_relatively_equal(to_real(dist_to_x_obs), dist, c_EPSILON) then
                logger.log_note(c_PREFIX & "Ok: Distance to input: " & to_string(dist_to_x_obs));
            else
                logger.log_error(c_PREFIX & "Error: Distance to input: " & to_string(dist_to_x_obs));
                logger.log_error(c_PREFIX & "Expected: " & to_string(to_scalar_t(dist)));
            end if;
            logger.log_note(c_PREFIX & " distance :" & integer'image(clk_count) & " clk");
        end;

        --* Elect unit as BMU and ensure that weight update is performed correctly
        --* Also update provided unit_weight reference
        procedure elect_as_bmu(
            unit_weight : inout real_vec_t;
            alpha       : in real;
            new_input   : in real_vec_t
        ) is
            variable new_weight_ref : real_vec_t;
            variable clk_count : integer := 0;
        begin
            -- Drive
            logger.log_note(c_PREFIX & "Learning rate alpha: " & real'image(alpha));
            logger.log_note(c_PREFIX & "Input vector: " & to_string(new_input));
            logger.log_note(c_PREFIX & "Weight vector before weight update: " & to_string(vec_obs));
            clk_count := 0;
            is_bmu_sti <= '1';
            vec_sti    <= to_vec_t(new_input);
            alpha_sti  <= to_scalar_t(alpha);
            wait until rising_edge(clk_sti); clk_count := clk_count + 1;
            is_bmu_sti <= '0';
            loop
                wait until rising_edge(clk_sti); clk_count := clk_count + 1;
                if ready_obs = '1' then
                    exit;
                end if;
            end loop;
            -- Compute reference
            for I in vec_sti'range loop
                unit_weight(I) := unit_weight(I) + alpha * (new_input(I) - unit_weight(I));
            end loop;
            -- compare
            if are_relatively_equal(to_real_vec_t(vec_obs), unit_weight, c_EPSILON) then
                logger.log_note(c_PREFIX & "Ok: BMU Weight vector after weight update: " & to_string(vec_obs));
            else
                logger.log_error(c_PREFIX & "Error: BMU Weight vector after weight update: " & to_string(vec_obs));
                logger.log_error(c_PREFIX & "Expected: " & to_string(unit_weight));
            end if;
            logger.log_note(c_PREFIX & " BMU update :" & integer'image(clk_count) & " clk");
        end;

        procedure propagation_update (
            unit_weight     : inout real_vec_t;
            neighbor_weight : in real_vec_t;
            unit_addr       : in t_grid_addr;
            bmu_addr        : in t_grid_addr;
            alpha           : in real;
            eta             : in real
        ) is
            constant eta_prime : real := - 1.0 * (sqrt(real(c_VEC_COMP_COUNT))/eta);
            variable d_ij      : real;
            variable hops      : natural;
            variable exp_res   : real;
            variable temp      : real;
            variable wp_coeff  : real;
            variable clk_count : integer := 0;
        begin
            -- Drive
            logger.log_note(c_PREFIX & "Learning rate alpha: " & real'image(alpha));
            logger.log_note(c_PREFIX & "Elasticity eta: " & real'image(eta));
            logger.log_note(c_PREFIX & "eta_prime: " & real'image(eta_prime));
            logger.log_note(c_PREFIX & "Weight vector before weight update: " & to_string(unit_weight));
            logger.log_note(c_PREFIX & "Neighbor weight: " & to_string(neighbor_weight));
            logger.log_note(c_PREFIX & "BMU grid address: (" & integer'image(bmu_addr.x) & "," & integer'image(bmu_addr.y) & "," & integer'image(bmu_addr.z) & ")");
            clk_count := 0;
            nm_valid_sti  <= '1';
            vec_sti       <= to_vec_t(neighbor_weight);
            alpha_sti     <= to_scalar_t(alpha);
            eta_prime_sti <= to_scalar_t(eta_prime);
            bmu_addr_sti  <= bmu_addr;
            wait until rising_edge(clk_sti); clk_count := clk_count + 1;
            nm_valid_sti <= '0';
            loop
                wait until rising_edge(clk_sti); clk_count := clk_count + 1;
                if ready_obs = '1' then
                    exit;
                end if;
            end loop;
            -- Compute reference
            -- Euclidean distance to neighbor
            d_ij := 0.0;
            for I in vec_sti'range loop
                d_ij := d_ij + (unit_weight(I) - neighbor_weight(I)) ** 2;
            end loop;
            -- wp_func result: wp_coeff
            hops     := abs(bmu_addr.x - unit_addr.x) + abs(bmu_addr.y - unit_addr.y) + abs(bmu_addr.z - unit_addr.z);
            temp     := eta_prime * (real(hops)/d_ij);
            exp_res  := exp(temp);
            wp_coeff := alpha * exp_res;
            -- Weight update
            for I in vec_sti'range loop
                unit_weight(I) := unit_weight(I) + wp_coeff * (neighbor_weight(I) - unit_weight(I));
            end loop;
            -- compare
            if are_relatively_equal(to_real_vec_t(vec_obs), unit_weight, c_EPSILON) then
                logger.log_note(c_PREFIX & "Ok: Weight vector after weight update: " & to_string(vec_obs));
            else
                logger.log_error(c_PREFIX & "Error: Weight vector after weight update: " & to_string(vec_obs));
                logger.log_error(c_PREFIX & "Expected: " & to_string(unit_weight));
            end if;
            logger.log_note(c_PREFIX & " update :" & integer'image(clk_count) & " clk");
        end;

    begin
        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_" & c_DUT_NAME & "_report.html",
        "Testbench report: " & "tb_" & c_DUT_NAME,
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("Scalar representation: wE: " & integer'image(wE) & ", wF: " & integer'image(wF));
        logger.log_note("Vector component count: C_VEC_COMP_COUNT: " & integer'image(C_VEC_COMP_COUNT));
        logger.log_note("Epsilon: " & to_string(to_scalar_t(c_EPSILON)));
        logger.log_note("Unit grid address: (" & integer'image(c_UNIT_ADDR.x) & "," & integer'image(c_UNIT_ADDR.y) & "," & integer'image(c_UNIT_ADDR.z) & ")");

        ------------------------------------------------------------------------
        -- Reset and vectors setup
        setup_default_signals;
        reset_unit;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        if true then
            wait_ready;
            test_count := test_count + 1;
            logger.log_warning("Test " & integer'image(test_count) & ": Weight initialization test");
            --------------------------------------------------------------------
            logger.log_note("Reset and wait for initial weight vector initialization");
            reset_unit;
            logger.log_note(c_PREFIX & "Weight vector after reset: " & to_string(vec_obs));
            --------------------------------------------------------------------
            logger.log_note("Forced weight initialization...");
            for I in vec_sti'range loop
                weight(I) := real(I);
            end loop;
            init_weight(weight);
        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        if true then
            test_count := test_count + 1;
            logger.log_warning("Test " & integer'image(test_count) & ": Distance to input computation test");
            --------------------------------------------------------------------
            logger.log_note("Weight initialization...");
            for I in vec_sti'range loop
                weight(I) := real(I);
            end loop;
            init_weight(weight);
            --------------------------------------------------------------------
            logger.log_note("Driving empty input...");
            new_in := (others => 0.0);
            drive_input(new_input => new_in, unit_weight => weight);
            --------------------------------------------------------------------
            logger.log_note("Driving input identical to weight...");
            new_in := weight;
            drive_input(new_input => new_in, unit_weight => weight);
            --------------------------------------------------------------------
            logger.log_note("Driving random input");
            randomize_vect(seed1, seed2, new_in);
            drive_input(new_input => new_in, unit_weight => weight);
            logger.log_note("Randomizing weight...");
            randomize_vect(seed1, seed2, weight);
            init_weight(weight);
            logger.log_note("Driving random input");
            randomize_vect(seed1, seed2, new_in);
            drive_input(new_input => new_in, unit_weight => weight);
            --------------------------------------------------------------------
        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        if true then
            test_count := test_count + 1;
            logger.log_warning("Test " & integer'image(test_count) & ": BMU weight update test");
            --------------------------------------------------------------------
            logger.log_note("Weight initialization to fixed value...");
            for I in vec_sti'range loop
                weight(I) := real(I);
            end loop;
            init_weight(weight);
            --------------------------------------------------------------------
            logger.log_note("Elect as BMU with a fixed input");
            new_in := (others => 2.0);
            elect_as_bmu(unit_weight => weight, alpha => 0.5, new_input => new_in);
            --------------------------------------------------------------------
            logger.log_note("Weight initialization to random value...");
            randomize_vect(seed1, seed2, weight);
            init_weight(weight);
            --
            logger.log_note("Elect as BMU with a random input");
            randomize_vect(seed1, seed2, new_in);
            elect_as_bmu(unit_weight => weight, alpha => 0.5, new_input => new_in);
            --
            logger.log_note("Elect as BMU with a random input");
            randomize_vect(seed1, seed2, new_in);
            elect_as_bmu(unit_weight => weight, alpha => 0.5, new_input => new_in);
            --
            logger.log_note("Elect as BMU with a random input");
            randomize_vect(seed1, seed2, new_in);
            elect_as_bmu(unit_weight => weight, alpha => 0.5, new_input => new_in);
            --------------------------------------------------------------------
        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        if true then
            test_count := test_count + 1;
            logger.log_warning("Test " & integer'image(test_count) & ": Propagation weight update test");
            --
            logger.log_note("Weight initialization to fixed value...");
            for I in vec_sti'range loop
                weight(I) := real(I);
            end loop;
            init_weight(weight);
            --
            logger.log_note("Notify of a propagation update with a fixed neighbor weight");
            new_in := (others => 3.0);
            propagation_update(
            unit_weight     => weight,
            unit_addr       => c_UNIT_ADDR,
            neighbor_weight => new_in,
            bmu_addr => (x => 1, y => 2, z => 3),
            alpha           => 0.5,
            eta             => 0.5
            );
            --------------------------------------------------------------------
            logger.log_note("Weight initialization to random value...");
            randomize_vect(seed1, seed2, weight);
            init_weight(weight);
            --
            logger.log_note("Notify of a propagation update with a random neighbor weight");
            randomize_vect(seed1, seed2, new_in);
            propagation_update(
            unit_weight     => weight,
            unit_addr       => c_UNIT_ADDR,
            neighbor_weight => new_in,
            bmu_addr => (x => 3, y => 2, z => 1),
            alpha           => 0.25,
            eta             => 0.75
            );
            --
            logger.log_note("Notify of a propagation update with a random neighbor weight");
            randomize_vect(seed1, seed2, new_in);
            propagation_update(
            unit_weight     => weight,
            unit_addr       => c_UNIT_ADDR,
            neighbor_weight => new_in,
            bmu_addr => (x => 3, y => 2, z => 1),
            alpha           => 0.25,
            eta             => 0.75
            );
            --
            logger.log_note("Notify of a propagation update with a random neighbor weight");
            randomize_vect(seed1, seed2, new_in);
            propagation_update(
            unit_weight     => weight,
            unit_addr       => c_UNIT_ADDR,
            neighbor_weight => new_in,
            bmu_addr => (x => 3, y => 2, z => 1),
            alpha           => 0.25,
            eta             => 0.75
            );
            --
            logger.log_note("Notify of a propagation update with a random neighbor weight");
            randomize_vect(seed1, seed2, new_in);
            propagation_update(
            unit_weight     => weight,
            unit_addr       => c_UNIT_ADDR,
            neighbor_weight => new_in,
            bmu_addr => (x => 3, y => 2, z => 1),
            alpha           => 0.25,
            eta             => 0.75
            );
            --------------------------------------------------------------------
        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        logger.log_note("Testbench ending, emptying pipeline...");
        for I in 1 to 500 loop
            wait until rising_edge(clk_sti);
        end loop;
        logger.final_report;
        logger.log_note("Testbench end");
        wait until rising_edge(clk_sti);
        stop(0);

    end process tb1;
    --  End Test Bench

end architecture behavioral;
