----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_wp_func - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for a grid of csom_unit
--
-- Last update: 2021/11/16 13:49:04
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all; -- for uniform
use ieee.numeric_std.all;

library std;
use std.env.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_wp_func is
    generic (
        CLK_PERIOD : time := 10 ns -- clock period for clk
    );
end entity;

architecture behavioral of tb_wp_func is
    ----------------------------------------------------------------------------
    -- Constants
    ----------------------------------------------------------------------------

    constant c_DUT_NAME  : string := "wp_func";
    constant PCLK_PERIOD : time   := CLK_PERIOD;

    constant c_EPSILON  : real    := 1.0e-6;
    constant c_GRID_DIM : natural := 2;
    constant c_MAX_DIM  : natural := 255;
    constant c_X_POS    : natural := 10;
    constant c_Y_POS    : natural := 10;
    constant c_Z_POS    : natural := 1;

    ----------------------------------------------------------------------------
    -- Signals for internal operation
    ----------------------------------------------------------------------------

    --- clock signals ---
    signal clk_sti : std_logic := '1';
    signal rst_sti : std_logic := '1';

    --- DUT input ---
    signal start_sti     : std_logic;
    signal alpha_sti     : scalar_t;    -- Current learning rate
    signal eta_prime_sti : scalar_t;    -- Current eta_prime (-(sqrt(d)/eta))
    signal bmu_addr_sti  : t_grid_addr; -- BMU grid address
    signal d_ij_sti      : scalar_t;    -- Weight distance of current and neighbor (||mi-mj||)

    --- DUT output ---
    signal result_obs : scalar_t; -- Function output
    signal done_obs   : std_logic;

    ----------------------------------------------------------------------------
    -- DUT component declaration
    ----------------------------------------------------------------------------

    component wp_func is
        port (
            clk_i       : in std_logic;
            rst_i       : in std_logic;
            start_i     : in std_logic;
            done_o      : out std_logic;
            alpha_i     : in scalar_t; -- Current learning rate
            eta_prime_i : in scalar_t; -- Current eta_prime (-(sqrt(d)/eta))
            bmu_addr_i  : t_grid_addr; -- BMU grid address
            d_ij_i      : in scalar_t; -- Weight distance of current and neighbor (||mi-mj||)
            result_o    : out scalar_t -- Function output
        );

    end component wp_func;

begin

    ----------------------------------------------------------------------------
    -- DUT instantiation
    ----------------------------------------------------------------------------

    i_DUT : wp_func
    port map(
        clk_i       => clk_sti,
        rst_i       => rst_sti,
        start_i     => start_sti,
        done_o      => done_obs,
        addr_i      => (x => c_X_POS, y => c_Y_POS, z => c_Z_POS),
        alpha_i     => alpha_sti,
        eta_prime_i => eta_prime_sti,
        bmu_addr_i  => bmu_addr_sti,
        d_ij_i      => d_ij_sti,
        result_o    => result_obs
    );

    ----------------------------------------------------------------------------
    -- CLOCK GENERATION
    ----------------------------------------------------------------------------
    clk_proc : process is
    begin
        loop
            clk_sti <= not clk_sti;
            wait for PCLK_PERIOD / 2;
        end loop;
    end process clk_proc;

    ----------------------------------------------------------------------------
    tb1 : process is

        constant c_PREFIX : string := " |--> ";

        function compute_reference(
            alpha : real;
            eta   : real;
            d_ij  : real;
            bmu_x : integer;
            bmu_y : integer;
            bmu_z : integer;
            X_POS : integer;
            Y_POS : integer;
            Z_POS : integer
        ) return real is
            variable eta_prime : real := - 1.0 * (sqrt(real(c_VEC_COMP_COUNT))/eta);
            variable hops      : natural;
            variable exp_res   : real;
            variable temp      : real;
        begin
            hops    := abs(bmu_x - X_POS) + abs(bmu_y - Y_POS) + abs(bmu_z - Z_POS);
            temp    := eta_prime * (real(hops)/d_ij);
            exp_res := exp(temp);
            return alpha * exp_res;
        end function;

        procedure drive_input(
            alpha                : real;
            eta                  : real;
            d_ij                 : real;
            bmu_x                : natural;
            bmu_y                : natural;
            bmu_z                : natural) is
            variable eta_prime   : real    := - 1.0 * (sqrt(real(c_VEC_COMP_COUNT))/eta);
            variable cycle_count : natural := 0;
            variable reference   : real;
        begin
            logger.log_note("Driving new input: <br>" &
            "alpha: " & real'image(alpha) & "<br>" &
            "eta: " & real'image(eta) & "(eta_prime: " & real'image(eta_prime) & ")<br>" &
            "d_ij: " & real'image(d_ij) & "<br>" &
            "bmu_(x,y,z): (" & integer'image(bmu_x) & "," & integer'image(bmu_y) & "," & integer'image(bmu_z) & ")");
            cycle_count := 0;
            --
            alpha_sti     <= to_scalar_t(alpha);
            eta_prime_sti <= to_scalar_t(eta_prime);
            bmu_addr_sti  <= (x => bmu_x, y => bmu_y, z => bmu_z);
            d_ij_sti      <= to_scalar_t(d_ij);
            --
            start_sti <= '1';
            wait until rising_edge(clk_sti);
            start_sti <= '0';
            loop
                wait until rising_edge(clk_sti);
                cycle_count := cycle_count + 1;
                if done_obs = '1' then
                    exit;
                end if;
            end loop;
            logger.log_note(c_PREFIX & " finished in " & integer'image(cycle_count) & " cycles");

            reference := compute_reference(
                alpha => alpha,
                eta   => eta,
                d_ij  => d_ij,
                bmu_x => bmu_x,
                bmu_y => bmu_y,
                bmu_z => bmu_z,
                X_POS => c_X_POS,
                Y_POS => c_Y_POS,
                Z_POS => c_Z_POS
                );
            if are_relatively_equal(to_real(result_obs), reference, c_EPSILON) then
                logger.log_note("Result: " & to_string(result_obs) & " = " & real'image(reference) & ", (epsilon = " & real'image(c_EPSILON) & ")");
            else
                logger.log_error("Result: " & to_string(result_obs) & " /= " & real'image(reference) & ", (epsilon = " & real'image(c_EPSILON) & ")");
            end if;

        end procedure;

        variable seed1      : integer := 1;
        variable seed2      : integer := 1;
        variable test_count : natural := 0;
    begin
        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_" & c_DUT_NAME & "_report.html",
        "Testbench report: " & "tb_" & c_DUT_NAME,
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("Scalar representation: wE: " & integer'image(wE) & ", wF: " & integer'image(wF));
        logger.log_note("Constant c_VEC_COMP_COUNT: " & integer'image(c_VEC_COMP_COUNT));
        logger.log_note("Constant c_GRID_DIM: " & integer'image(c_GRID_DIM));
        logger.log_note("Constant c_MAX_DIM: " & integer'image(c_MAX_DIM));
        logger.log_note("Constant c_X_POS: " & integer'image(c_X_POS));
        logger.log_note("Constant c_Y_POS: " & integer'image(c_Y_POS));
        logger.log_note("Constant c_Z_POS: " & integer'image(c_Z_POS));

        ------------------------------------------------------------------------
        -- Reset and vectors setup
        --
        rst_sti       <= '0';
        start_sti     <= '0';
        alpha_sti     <= to_scalar_t(0.0);
        eta_prime_sti <= to_scalar_t(0.0);
        bmu_addr_sti  <= (x => 0, y => 0, z => 0);
        d_ij_sti      <= to_scalar_t(1.0);

        wait until rising_edge(clk_sti);

        ------------------------------------------------------------------------
        logger.log_note("Reset and wait for initial weight vector initialization");
        rst_sti <= '1';
        wait until rising_edge(clk_sti);
        rst_sti <= '0';
        wait until rising_edge(clk_sti);
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        if true then
            test_count := test_count + 1;
            logger.log_note("Starting test n°" & integer'image(test_count));
            --
            drive_input(alpha => 0.5, eta => 1.0, d_ij => 1.0, bmu_x => 3, bmu_y => 2, bmu_z => 1);
            --
            drive_input(alpha => 1.0, eta => 0.5, d_ij => 1.0, bmu_x => 10, bmu_y => 9, bmu_z => 1);
            drive_input(alpha => 0.1, eta => 0.5, d_ij => 1.0, bmu_x => 10, bmu_y => 9, bmu_z => 1);
            drive_input(alpha => 0.01, eta => 0.5, d_ij => 1.0, bmu_x => 10, bmu_y => 9, bmu_z => 1);
            drive_input(alpha => 0.001, eta => 0.5, d_ij => 1.0, bmu_x => 10, bmu_y => 9, bmu_z => 1);
            drive_input(alpha => 0.0001, eta => 0.5, d_ij => 1.0, bmu_x => 10, bmu_y => 9, bmu_z => 1);
        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        -- Empty pipeline, do final report and stop simulation
        for I in 1 to 1 loop
            wait until rising_edge(clk_sti);
        end loop;
        logger.final_report;
        wait until rising_edge(clk_sti);
        stop(0);

    end process tb1;
    --  End Test Bench

end architecture behavioral;
