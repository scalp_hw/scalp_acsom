--------------------------------------------------------------------------------
--                    SmallMultTableP3x3r6XuYu_F210_uid75
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2012)
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity SmallMultTableP3x3r6XuYu_F210_uid75 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(5 downto 0)   );
end entity;

architecture arch of SmallMultTableP3x3r6XuYu_F210_uid75 is
signal TableOut :  std_logic_vector(5 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "000000" when "000000",
   "000000" when "000001",
   "000000" when "000010",
   "000000" when "000011",
   "000000" when "000100",
   "000000" when "000101",
   "000000" when "000110",
   "000000" when "000111",
   "000000" when "001000",
   "000001" when "001001",
   "000010" when "001010",
   "000011" when "001011",
   "000100" when "001100",
   "000101" when "001101",
   "000110" when "001110",
   "000111" when "001111",
   "000000" when "010000",
   "000010" when "010001",
   "000100" when "010010",
   "000110" when "010011",
   "001000" when "010100",
   "001010" when "010101",
   "001100" when "010110",
   "001110" when "010111",
   "000000" when "011000",
   "000011" when "011001",
   "000110" when "011010",
   "001001" when "011011",
   "001100" when "011100",
   "001111" when "011101",
   "010010" when "011110",
   "010101" when "011111",
   "000000" when "100000",
   "000100" when "100001",
   "001000" when "100010",
   "001100" when "100011",
   "010000" when "100100",
   "010100" when "100101",
   "011000" when "100110",
   "011100" when "100111",
   "000000" when "101000",
   "000101" when "101001",
   "001010" when "101010",
   "001111" when "101011",
   "010100" when "101100",
   "011001" when "101101",
   "011110" when "101110",
   "100011" when "101111",
   "000000" when "110000",
   "000110" when "110001",
   "001100" when "110010",
   "010010" when "110011",
   "011000" when "110100",
   "011110" when "110101",
   "100100" when "110110",
   "101010" when "110111",
   "000000" when "111000",
   "000111" when "111001",
   "001110" when "111010",
   "010101" when "111011",
   "011100" when "111100",
   "100011" when "111101",
   "101010" when "111110",
   "110001" when "111111",
   "------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                               Compressor_6_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_6_3 is
   port ( X0 : in  std_logic_vector(5 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_6_3 is
signal X :  std_logic_vector(5 downto 0);
begin
   X <=X0 ;
   with X select R <= 
      "000" when "000000", 
      "001" when "000001", 
      "001" when "000010", 
      "010" when "000011", 
      "001" when "000100", 
      "010" when "000101", 
      "010" when "000110", 
      "011" when "000111", 
      "001" when "001000", 
      "010" when "001001", 
      "010" when "001010", 
      "011" when "001011", 
      "010" when "001100", 
      "011" when "001101", 
      "011" when "001110", 
      "100" when "001111", 
      "001" when "010000", 
      "010" when "010001", 
      "010" when "010010", 
      "011" when "010011", 
      "010" when "010100", 
      "011" when "010101", 
      "011" when "010110", 
      "100" when "010111", 
      "010" when "011000", 
      "011" when "011001", 
      "011" when "011010", 
      "100" when "011011", 
      "011" when "011100", 
      "100" when "011101", 
      "100" when "011110", 
      "101" when "011111", 
      "001" when "100000", 
      "010" when "100001", 
      "010" when "100010", 
      "011" when "100011", 
      "010" when "100100", 
      "011" when "100101", 
      "011" when "100110", 
      "100" when "100111", 
      "010" when "101000", 
      "011" when "101001", 
      "011" when "101010", 
      "100" when "101011", 
      "011" when "101100", 
      "100" when "101101", 
      "100" when "101110", 
      "101" when "101111", 
      "010" when "110000", 
      "011" when "110001", 
      "011" when "110010", 
      "100" when "110011", 
      "011" when "110100", 
      "100" when "110101", 
      "100" when "110110", 
      "101" when "110111", 
      "011" when "111000", 
      "100" when "111001", 
      "100" when "111010", 
      "101" when "111011", 
      "100" when "111100", 
      "101" when "111101", 
      "101" when "111110", 
      "110" when "111111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                              Compressor_14_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_14_3 is
   port ( X0 : in  std_logic_vector(3 downto 0);
          X1 : in  std_logic_vector(0 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_14_3 is
signal X :  std_logic_vector(4 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "00000", 
      "001" when "00001", 
      "001" when "00010", 
      "010" when "00011", 
      "001" when "00100", 
      "010" when "00101", 
      "010" when "00110", 
      "011" when "00111", 
      "001" when "01000", 
      "010" when "01001", 
      "010" when "01010", 
      "011" when "01011", 
      "010" when "01100", 
      "011" when "01101", 
      "011" when "01110", 
      "100" when "01111", 
      "010" when "10000", 
      "011" when "10001", 
      "011" when "10010", 
      "100" when "10011", 
      "011" when "10100", 
      "100" when "10101", 
      "100" when "10110", 
      "101" when "10111", 
      "011" when "11000", 
      "100" when "11001", 
      "100" when "11010", 
      "101" when "11011", 
      "100" when "11100", 
      "101" when "11101", 
      "101" when "11110", 
      "110" when "11111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                               Compressor_4_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_4_3 is
   port ( X0 : in  std_logic_vector(3 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_4_3 is
signal X :  std_logic_vector(3 downto 0);
begin
   X <=X0 ;
   with X select R <= 
      "000" when "0000", 
      "001" when "0001", 
      "001" when "0010", 
      "010" when "0011", 
      "001" when "0100", 
      "010" when "0101", 
      "010" when "0110", 
      "011" when "0111", 
      "001" when "1000", 
      "010" when "1001", 
      "010" when "1010", 
      "011" when "1011", 
      "010" when "1100", 
      "011" when "1101", 
      "011" when "1110", 
      "100" when "1111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                              Compressor_23_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_23_3 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          X1 : in  std_logic_vector(1 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_23_3 is
signal X :  std_logic_vector(4 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "00000", 
      "001" when "00001", 
      "001" when "00010", 
      "010" when "00011", 
      "001" when "00100", 
      "010" when "00101", 
      "010" when "00110", 
      "011" when "00111", 
      "010" when "01000", 
      "011" when "01001", 
      "011" when "01010", 
      "100" when "01011", 
      "011" when "01100", 
      "100" when "01101", 
      "100" when "01110", 
      "101" when "01111", 
      "010" when "10000", 
      "011" when "10001", 
      "011" when "10010", 
      "100" when "10011", 
      "011" when "10100", 
      "100" when "10101", 
      "100" when "10110", 
      "101" when "10111", 
      "100" when "11000", 
      "101" when "11001", 
      "101" when "11010", 
      "110" when "11011", 
      "101" when "11100", 
      "110" when "11101", 
      "110" when "11110", 
      "111" when "11111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                              Compressor_13_3
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_13_3 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          X1 : in  std_logic_vector(0 downto 0);
          R : out  std_logic_vector(2 downto 0)   );
end entity;

architecture arch of Compressor_13_3 is
signal X :  std_logic_vector(3 downto 0);
begin
   X <=X1 & X0 ;
   with X select R <= 
      "000" when "0000", 
      "001" when "0001", 
      "001" when "0010", 
      "010" when "0011", 
      "001" when "0100", 
      "010" when "0101", 
      "010" when "0110", 
      "011" when "0111", 
      "010" when "1000", 
      "011" when "1001", 
      "011" when "1010", 
      "100" when "1011", 
      "011" when "1100", 
      "100" when "1101", 
      "100" when "1110", 
      "101" when "1111", 
      "---" when others;

end architecture;

--------------------------------------------------------------------------------
--                               Compressor_3_2
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Popa, Illyes Kinga, 2012
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity Compressor_3_2 is
   port ( X0 : in  std_logic_vector(2 downto 0);
          R : out  std_logic_vector(1 downto 0)   );
end entity;

architecture arch of Compressor_3_2 is
signal X :  std_logic_vector(2 downto 0);
begin
   X <=X0 ;
   with X select R <= 
      "00" when "000", 
      "01" when "001", 
      "01" when "010", 
      "10" when "011", 
      "01" when "100", 
      "10" when "101", 
      "10" when "110", 
      "11" when "111", 
      "--" when others;

end architecture;

--------------------------------------------------------------------------------
--                     LeftShifter_17_by_max_24_F210_uid4
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2011)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity LeftShifter_17_by_max_24_F210_uid4 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(16 downto 0);
          S : in  std_logic_vector(4 downto 0);
          R : out  std_logic_vector(40 downto 0)   );
end entity;

architecture arch of LeftShifter_17_by_max_24_F210_uid4 is
signal level0 :  std_logic_vector(16 downto 0);
signal ps :  std_logic_vector(4 downto 0);
signal level1 :  std_logic_vector(17 downto 0);
signal level2 :  std_logic_vector(19 downto 0);
signal level3 :  std_logic_vector(23 downto 0);
signal level4 :  std_logic_vector(31 downto 0);
signal level5 :  std_logic_vector(47 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   level0<= X;
   ps<= S;
   level1<= level0 & (0 downto 0 => '0') when ps(0)= '1' else     (0 downto 0 => '0') & level0;
   level2<= level1 & (1 downto 0 => '0') when ps(1)= '1' else     (1 downto 0 => '0') & level1;
   level3<= level2 & (3 downto 0 => '0') when ps(2)= '1' else     (3 downto 0 => '0') & level2;
   level4<= level3 & (7 downto 0 => '0') when ps(3)= '1' else     (7 downto 0 => '0') & level3;
   level5<= level4 & (15 downto 0 => '0') when ps(4)= '1' else     (15 downto 0 => '0') & level4;
   R <= level5(40 downto 0);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(9 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
signal TableOut :  std_logic_vector(9 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000001000" when "000000",
   "0000010100" when "000001",
   "0000011111" when "000010",
   "0000101011" when "000011",
   "0000110110" when "000100",
   "0001000010" when "000101",
   "0001001101" when "000110",
   "0001011001" when "000111",
   "0001100100" when "001000",
   "0001110000" when "001001",
   "0001111011" when "001010",
   "0010000111" when "001011",
   "0010010010" when "001100",
   "0010011110" when "001101",
   "0010101010" when "001110",
   "0010110101" when "001111",
   "0011000001" when "010000",
   "0011001100" when "010001",
   "0011011000" when "010010",
   "0011100011" when "010011",
   "0011101111" when "010100",
   "0011111010" when "010101",
   "0100000110" when "010110",
   "0100010001" when "010111",
   "0100011101" when "011000",
   "0100101001" when "011001",
   "0100110100" when "011010",
   "0101000000" when "011011",
   "0101001011" when "011100",
   "0101010111" when "011101",
   "0101100010" when "011110",
   "0101101110" when "011111",
   "0101111001" when "100000",
   "0110000101" when "100001",
   "0110010000" when "100010",
   "0110011100" when "100011",
   "0110100111" when "100100",
   "0110110011" when "100101",
   "0110111111" when "100110",
   "0111001010" when "100111",
   "0111010110" when "101000",
   "0111100001" when "101001",
   "0111101101" when "101010",
   "0111111000" when "101011",
   "1000000100" when "101100",
   "1000001111" when "101101",
   "1000011011" when "101110",
   "1000100110" when "101111",
   "1000110010" when "110000",
   "1000111110" when "110001",
   "1001001001" when "110010",
   "1001010101" when "110011",
   "1001100000" when "110100",
   "1001101100" when "110101",
   "1001110111" when "110110",
   "1010000011" when "110111",
   "1010001110" when "111000",
   "1010011010" when "111001",
   "1010100101" when "111010",
   "1010110001" when "111011",
   "1010111100" when "111100",
   "1011001000" when "111101",
   "1011010100" when "111110",
   "1011011111" when "111111",
   "----------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(1 downto 0);
          Y : out  std_logic_vector(3 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
signal TableOut :  std_logic_vector(3 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000" when "00",
   "0011" when "01",
   "0110" when "10",
   "1001" when "11",
   "----" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_11_f210_uid24
--                     (IntAdderClassical_11_F210_uid26)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_11_f210_uid24 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(10 downto 0);
          Y : in  std_logic_vector(10 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of IntAdder_11_f210_uid24 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(7 downto 0);
          R : out  std_logic_vector(5 downto 0)   );
end entity;

architecture arch of FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8 is
   component FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(9 downto 0)   );
   end component;

   component FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(1 downto 0);
             Y : out  std_logic_vector(3 downto 0)   );
   end component;

   component IntAdder_11_f210_uid24 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(10 downto 0);
             Y : in  std_logic_vector(10 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(10 downto 0)   );
   end component;

signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A0 :  std_logic_vector(5 downto 0);
signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0 :  std_logic_vector(9 downto 0);
signal heap_bh9_w0_0 :  std_logic;
signal heap_bh9_w1_0 :  std_logic;
signal heap_bh9_w2_0 :  std_logic;
signal heap_bh9_w3_0 :  std_logic;
signal heap_bh9_w4_0 :  std_logic;
signal heap_bh9_w5_0 :  std_logic;
signal heap_bh9_w6_0 :  std_logic;
signal heap_bh9_w7_0 :  std_logic;
signal heap_bh9_w8_0 :  std_logic;
signal heap_bh9_w9_0 :  std_logic;
signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A1 :  std_logic_vector(1 downto 0);
signal FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1 :  std_logic_vector(3 downto 0);
signal heap_bh9_w0_1 :  std_logic;
signal heap_bh9_w1_1 :  std_logic;
signal heap_bh9_w2_1 :  std_logic;
signal heap_bh9_w3_1 :  std_logic;
signal finalAdderIn0_bh9 :  std_logic_vector(10 downto 0);
signal finalAdderIn1_bh9 :  std_logic_vector(10 downto 0);
signal finalAdderCin_bh9 :  std_logic;
signal finalAdderOut_bh9 :  std_logic_vector(10 downto 0);
signal CompressionResult9 :  std_logic_vector(10 downto 0);
signal OutRes :  std_logic_vector(9 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A0 <= X(7 downto 2); -- input address  m=4  l=-1
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table0: FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A0,
                 Y => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0);
   ---------------- cycle 0----------------
   heap_bh9_w0_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(0); -- cycle= 0 cp= 0
   heap_bh9_w1_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(1); -- cycle= 0 cp= 0
   heap_bh9_w2_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(2); -- cycle= 0 cp= 0
   heap_bh9_w3_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(3); -- cycle= 0 cp= 0
   heap_bh9_w4_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(4); -- cycle= 0 cp= 0
   heap_bh9_w5_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(5); -- cycle= 0 cp= 0
   heap_bh9_w6_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(6); -- cycle= 0 cp= 0
   heap_bh9_w7_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(7); -- cycle= 0 cp= 0
   heap_bh9_w8_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(8); -- cycle= 0 cp= 0
   heap_bh9_w9_0 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T0(9); -- cycle= 0 cp= 0
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A1 <= X(1 downto 0); -- input address  m=-2  l=-3
   FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table1: FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_Table_1  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_A1,
                 Y => FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1);
   ---------------- cycle 0----------------
   heap_bh9_w0_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(0); -- cycle= 0 cp= 0
   heap_bh9_w1_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(1); -- cycle= 0 cp= 0
   heap_bh9_w2_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(2); -- cycle= 0 cp= 0
   heap_bh9_w3_1 <= FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8_T1(3); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 0----------------
   finalAdderIn0_bh9 <= "0" & heap_bh9_w9_0 & heap_bh9_w8_0 & heap_bh9_w7_0 & heap_bh9_w6_0 & heap_bh9_w5_0 & heap_bh9_w4_0 & heap_bh9_w3_1 & heap_bh9_w2_1 & heap_bh9_w1_1 & heap_bh9_w0_1;
   finalAdderIn1_bh9 <= "0" & '0' & '0' & '0' & '0' & '0' & '0' & heap_bh9_w3_0 & heap_bh9_w2_0 & heap_bh9_w1_0 & heap_bh9_w0_0;
   finalAdderCin_bh9 <= '0';
   Adder_final9_0: IntAdder_11_f210_uid24  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh9,
                 R => finalAdderOut_bh9   ,
                 X => finalAdderIn0_bh9,
                 Y => finalAdderIn1_bh9);
   -- concatenate all the compressed chunks
   CompressionResult9 <= finalAdderOut_bh9;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult9(9 downto 0);
   R <= OutRes(9 downto 4);
end architecture;

--------------------------------------------------------------------------------
--            FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_Table_0
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2011-?), 3IF Dev Team
--------------------------------------------------------------------------------
library ieee; 
use ieee.std_logic_1164.all;
library work;
entity FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_Table_0 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          Y : out  std_logic_vector(24 downto 0)   );
end entity;

architecture arch of FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_Table_0 is
signal TableOut :  std_logic_vector(24 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
  with X select TableOut <= 
   "0000000000000000000000000" when "000000",
   "0000001011000101110010001" when "000001",
   "0000010110001011100100001" when "000010",
   "0000100001010001010110010" when "000011",
   "0000101100010111001000011" when "000100",
   "0000110111011100111010100" when "000101",
   "0001000010100010101100100" when "000110",
   "0001001101101000011110101" when "000111",
   "0001011000101110010000110" when "001000",
   "0001100011110100000010111" when "001001",
   "0001101110111001110100111" when "001010",
   "0001111001111111100111000" when "001011",
   "0010000101000101011001001" when "001100",
   "0010010000001011001011010" when "001101",
   "0010011011010000111101010" when "001110",
   "0010100110010110101111011" when "001111",
   "0010110001011100100001100" when "010000",
   "0010111100100010010011101" when "010001",
   "0011000111101000000101101" when "010010",
   "0011010010101101110111110" when "010011",
   "0011011101110011101001111" when "010100",
   "0011101000111001011100000" when "010101",
   "0011110011111111001110000" when "010110",
   "0011111111000101000000001" when "010111",
   "0100001010001010110010010" when "011000",
   "0100010101010000100100011" when "011001",
   "0100100000010110010110011" when "011010",
   "0100101011011100001000100" when "011011",
   "0100110110100001111010101" when "011100",
   "0101000001100111101100110" when "011101",
   "0101001100101101011110110" when "011110",
   "0101010111110011010000111" when "011111",
   "0101100010111001000011000" when "100000",
   "0101101101111110110101001" when "100001",
   "0101111001000100100111001" when "100010",
   "0110000100001010011001010" when "100011",
   "0110001111010000001011011" when "100100",
   "0110011010010101111101100" when "100101",
   "0110100101011011101111100" when "100110",
   "0110110000100001100001101" when "100111",
   "0110111011100111010011110" when "101000",
   "0111000110101101000101111" when "101001",
   "0111010001110010110111111" when "101010",
   "0111011100111000101010000" when "101011",
   "0111100111111110011100001" when "101100",
   "0111110011000100001110010" when "101101",
   "0111111110001010000000010" when "101110",
   "1000001001001111110010011" when "101111",
   "1000010100010101100100100" when "110000",
   "1000011111011011010110101" when "110001",
   "1000101010100001001000101" when "110010",
   "1000110101100110111010110" when "110011",
   "1001000000101100101100111" when "110100",
   "1001001011110010011111000" when "110101",
   "1001010110111000010001000" when "110110",
   "1001100001111110000011001" when "110111",
   "1001101101000011110101010" when "111000",
   "1001111000001001100111011" when "111001",
   "1010000011001111011001011" when "111010",
   "1010001110010101001011100" when "111011",
   "1010011001011010111101101" when "111100",
   "1010100100100000101111110" when "111101",
   "1010101111100110100001110" when "111110",
   "1010111010101100010011111" when "111111",
   "-------------------------" when others;
    Y <= TableOut;
end architecture;

--------------------------------------------------------------------------------
--                FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007-2016)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(5 downto 0);
          R : out  std_logic_vector(24 downto 0)   );
end entity;

architecture arch of FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34 is
   component FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_Table_0 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(24 downto 0)   );
   end component;

signal FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_A0 :  std_logic_vector(5 downto 0);
signal FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0 :  std_logic_vector(24 downto 0);
signal heap_bh35_w0_0 :  std_logic;
signal heap_bh35_w1_0 :  std_logic;
signal heap_bh35_w2_0 :  std_logic;
signal heap_bh35_w3_0 :  std_logic;
signal heap_bh35_w4_0 :  std_logic;
signal heap_bh35_w5_0 :  std_logic;
signal heap_bh35_w6_0 :  std_logic;
signal heap_bh35_w7_0 :  std_logic;
signal heap_bh35_w8_0 :  std_logic;
signal heap_bh35_w9_0 :  std_logic;
signal heap_bh35_w10_0 :  std_logic;
signal heap_bh35_w11_0 :  std_logic;
signal heap_bh35_w12_0 :  std_logic;
signal heap_bh35_w13_0 :  std_logic;
signal heap_bh35_w14_0 :  std_logic;
signal heap_bh35_w15_0 :  std_logic;
signal heap_bh35_w16_0 :  std_logic;
signal heap_bh35_w17_0 :  std_logic;
signal heap_bh35_w18_0 :  std_logic;
signal heap_bh35_w19_0 :  std_logic;
signal heap_bh35_w20_0 :  std_logic;
signal heap_bh35_w21_0 :  std_logic;
signal heap_bh35_w22_0 :  std_logic;
signal heap_bh35_w23_0 :  std_logic;
signal heap_bh35_w24_0 :  std_logic;
signal CompressionResult35 :  std_logic_vector(24 downto 0);
signal OutRes :  std_logic_vector(24 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_A0 <= X(5 downto 0); -- input address  m=5  l=0
   FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_Table0: FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_Table_0  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_A0,
                 Y => FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0);
   ---------------- cycle 0----------------
   heap_bh35_w0_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(0); -- cycle= 0 cp= 0
   heap_bh35_w1_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(1); -- cycle= 0 cp= 0
   heap_bh35_w2_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(2); -- cycle= 0 cp= 0
   heap_bh35_w3_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(3); -- cycle= 0 cp= 0
   heap_bh35_w4_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(4); -- cycle= 0 cp= 0
   heap_bh35_w5_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(5); -- cycle= 0 cp= 0
   heap_bh35_w6_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(6); -- cycle= 0 cp= 0
   heap_bh35_w7_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(7); -- cycle= 0 cp= 0
   heap_bh35_w8_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(8); -- cycle= 0 cp= 0
   heap_bh35_w9_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(9); -- cycle= 0 cp= 0
   heap_bh35_w10_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(10); -- cycle= 0 cp= 0
   heap_bh35_w11_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(11); -- cycle= 0 cp= 0
   heap_bh35_w12_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(12); -- cycle= 0 cp= 0
   heap_bh35_w13_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(13); -- cycle= 0 cp= 0
   heap_bh35_w14_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(14); -- cycle= 0 cp= 0
   heap_bh35_w15_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(15); -- cycle= 0 cp= 0
   heap_bh35_w16_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(16); -- cycle= 0 cp= 0
   heap_bh35_w17_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(17); -- cycle= 0 cp= 0
   heap_bh35_w18_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(18); -- cycle= 0 cp= 0
   heap_bh35_w19_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(19); -- cycle= 0 cp= 0
   heap_bh35_w20_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(20); -- cycle= 0 cp= 0
   heap_bh35_w21_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(21); -- cycle= 0 cp= 0
   heap_bh35_w22_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(22); -- cycle= 0 cp= 0
   heap_bh35_w23_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(23); -- cycle= 0 cp= 0
   heap_bh35_w24_0 <= FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34_T0(24); -- cycle= 0 cp= 0
   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
      -- All the constant bits are zero, nothing to add

   ----------------Synchro barrier, entering cycle 0----------------
   CompressionResult35 <= heap_bh35_w24_0 & heap_bh35_w23_0 & heap_bh35_w22_0 & heap_bh35_w21_0 & heap_bh35_w20_0 & heap_bh35_w19_0 & heap_bh35_w18_0 & heap_bh35_w17_0 & heap_bh35_w16_0 & heap_bh35_w15_0 & heap_bh35_w14_0 & heap_bh35_w13_0 & heap_bh35_w12_0 & heap_bh35_w11_0 & heap_bh35_w10_0 & heap_bh35_w9_0 & heap_bh35_w8_0 & heap_bh35_w7_0 & heap_bh35_w6_0 & heap_bh35_w5_0 & heap_bh35_w4_0 & heap_bh35_w3_0 & heap_bh35_w2_0 & heap_bh35_w1_0 & heap_bh35_w0_0;
   -- End of code generated by BitHeap::generateCompressorVHDL
OutRes <= CompressionResult35(24 downto 0);
   R <= OutRes(24 downto 0);
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_19_f231_uid42
--                    (IntAdderAlternative_19_F231_uid46)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_19_f231_uid42 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(18 downto 0);
          Y : in  std_logic_vector(18 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(18 downto 0)   );
end entity;

architecture arch of IntAdder_19_f231_uid42 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                         MagicSPExpTable_F210_uid50
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Radu Tudoran, Florent de Dinechin (2009)
--------------------------------------------------------------------------------
-- combinatorial

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity MagicSPExpTable_F210_uid50 is
   port ( X1 : in  std_logic_vector(8 downto 0);
          Y1 : out  std_logic_vector(21 downto 0);
          X2 : in  std_logic_vector(8 downto 0);
          Y2 : out  std_logic_vector(21 downto 0)   );
end entity;

architecture arch of MagicSPExpTable_F210_uid50 is
type ROMContent is array (0 to 511) of std_logic_vector(21 downto 0);
constant memVar: ROMContent :=    ( 
      "1000000000000000000000",       "1000000001000000000100",       "1000000010000000010000",       "1000000011000000100100", 
      "1000000100000001000000",       "1000000101000001100100",       "1000000110000010010000",       "1000000111000011000100", 
      "1000001000000100000000",       "1000001001000101000100",       "1000001010000110010100",       "1000001011000111101000", 
      "1000001100001001000100",       "1000001101001010101000",       "1000001110001100011000",       "1000001111001110001100", 
      "1000010000010000001100",       "1000010001010010010000",       "1000010010010100100000",       "1000010011010110111000", 
      "1000010100011001010100",       "1000010101011011111100",       "1000010110011110101100",       "1000010111100001100100", 
      "1000011000100100100100",       "1000011001100111101100",       "1000011010101011000000",       "1000011011101110011000", 
      "1000011100110001111000",       "1000011101110101100100",       "1000011110111001011000",       "1000011111111101010100", 
      "1000100001000001011000",       "1000100010000101100100",       "1000100011001001111000",       "1000100100001110010100", 
      "1000100101010010111100",       "1000100110010111101100",       "1000100111011100100000",       "1000101000100001100000", 
      "1000101001100110101000",       "1000101010101011111100",       "1000101011110001010100",       "1000101100110110111000", 
      "1000101101111100100100",       "1000101111000010011000",       "1000110000001000010100",       "1000110001001110011000", 
      "1000110010010100101000",       "1000110011011010111100",       "1000110100100001011100",       "1000110101101000001000", 
      "1000110110101110111000",       "1000110111110101110000",       "1000111000111100110100",       "1000111010000100000000", 
      "1000111011001011011000",       "1000111100010010110100",       "1000111101011010011100",       "1000111110100010001100", 
      "1000111111101010000100",       "1001000000110010000100",       "1001000001111010010000",       "1001000011000010100100", 
      "1001000100001011000000",       "1001000101010011101000",       "1001000110011100010100",       "1001000111100101001100", 
      "1001001000101110010000",       "1001001001110111011000",       "1001001011000000101100",       "1001001100001010001000", 
      "1001001101010011110000",       "1001001110011101100000",       "1001001111100111011000",       "1001010000110001011000", 
      "1001010001111011100100",       "1001010011000101111000",       "1001010100010000010100",       "1001010101011010111100", 
      "1001010110100101101100",       "1001010111110000100100",       "1001011000111011101000",       "1001011010000110110100", 
      "1001011011010010001000",       "1001011100011101101000",       "1001011101101001010000",       "1001011110110101000100", 
      "1001100000000000111100",       "1001100001001101000100",       "1001100010011001010000",       "1001100011100101101000", 
      "1001100100110010001100",       "1001100101111110110100",       "1001100111001011101100",       "1001101000011000101000", 
      "1001101001100101110000",       "1001101010110011000000",       "1001101100000000011100",       "1001101101001110000000", 
      "1001101110011011110000",       "1001101111101001101000",       "1001110000110111101100",       "1001110010000101111000", 
      "1001110011010100001100",       "1001110100100010101100",       "1001110101110001010100",       "1001110111000000001000", 
      "1001111000001111000100",       "1001111001011110001100",       "1001111010101101011100",       "1001111011111100111000", 
      "1001111101001100011100",       "1001111110011100001100",       "1001111111101100000100",       "1010000000111100000100", 
      "1010000010001100010000",       "1010000011011100101000",       "1010000100101101001000",       "1010000101111101110100", 
      "1010000111001110101000",       "1010001000011111101000",       "1010001001110000110000",       "1010001011000010000100", 
      "1010001100010011100000",       "1010001101100101001000",       "1010001110110110111100",       "1010010000001000111000", 
      "1010010001011010111100",       "1010010010101101001100",       "1010010011111111101000",       "1010010101010010001100", 
      "1010010110100100111100",       "1010010111110111111000",       "1010011001001010111100",       "1010011010011110001000", 
      "1010011011110001100000",       "1010011101000101000100",       "1010011110011000110100",       "1010011111101100101100", 
      "1010100001000000110000",       "1010100010010100111100",       "1010100011101001010100",       "1010100100111101110100", 
      "1010100110010010100100",       "1010100111100111011100",       "1010101000111100011100",       "1010101010010001101000", 
      "1010101011100111000000",       "1010101100111100100100",       "1010101110010010010000",       "1010101111101000001000", 
      "1010110000111110001100",       "1010110010010100011000",       "1010110011101010110000",       "1010110101000001010100", 
      "1010110110011000000000",       "1010110111101110111000",       "1010111001000101111100",       "1010111010011101001000", 
      "1010111011110100100100",       "1010111101001100001000",       "1010111110100011110100",       "1010111111111011110000", 
      "1011000001010011110100",       "1011000010101100000100",       "1011000100000100100000",       "1011000101011101001000", 
      "1011000110110101111000",       "1011001000001110110100",       "1011001001100111111100",       "1011001011000001001100", 
      "1011001100011010101100",       "1011001101110100010100",       "1011001111001110001000",       "1011010000101000001000", 
      "1011010010000010010100",       "1011010011011100101000",       "1011010100110111001000",       "1011010110010001111000", 
      "1011010111101100101100",       "1011011001000111110000",       "1011011010100011000000",       "1011011011111110011100", 
      "1011011101011010000000",       "1011011110110101110000",       "1011100000010001101100",       "1011100001101101110100", 
      "1011100011001010001000",       "1011100100100110101000",       "1011100110000011010100",       "1011100111100000001000", 
      "1011101000111101001100",       "1011101010011010011000",       "1011101011110111110000",       "1011101101010101010100", 
      "1011101110110011001000",       "1011110000010001000100",       "1011110001101111001100",       "1011110011001101100000", 
      "1011110100101011111100",       "1011110110001010101000",       "1011110111101001100000",       "1011111001001000100100", 
      "1011111010100111110100",       "1011111100000111001100",       "1011111101100110110100",       "1011111111000110101000", 
      "1100000000100110101000",       "1100000010000110110000",       "1100000011100111001000",       "1100000101000111101100", 
      "1100000110101000011000",       "1100001000001001010100",       "1100001001101010011100",       "1100001011001011110000", 
      "1100001100101101010000",       "1100001110001110111100",       "1100001111110000110100",       "1100010001010010111000", 
      "1100010010110101001000",       "1100010100010111100100",       "1100010101111010001100",       "1100010111011101000100", 
      "1100011001000000000100",       "1100011010100011010100",       "1100011100000110101100",       "1100011101101010010100", 
      "1100011111001110001000",       "1100100000110010001000",       "1100100010010110010100",       "1100100011111010101100", 
      "1100100101011111010100",       "1100100111000100000100",       "1100101000101001000100",       "1100101010001110010000", 
      "1100101011110011101000",       "1100101101011001001100",       "1100101110111111000000",       "1100110000100100111100", 
      "1100110010001011001000",       "1100110011110001100000",       "1100110101011000000100",       "1100110110111110110100", 
      "1100111000100101110100",       "1100111010001101000000",       "1100111011110100011000",       "1100111101011011111100", 
      "1100111111000011101100",       "1101000000101011101100",       "1101000010010011111000",       "1101000011111100010000", 
      "1101000101100100111000",       "1101000111001101101100",       "1101001000110110101100",       "1101001010011111111000", 
      "0100110110100010110100",       "0100110111001001101000",       "0100110111110000100100",       "0100111000010111100100", 
      "0100111000111110101100",       "0100111001100101110100",       "0100111010001101000100",       "0100111010110100011000", 
      "0100111011011011110000",       "0100111100000011010000",       "0100111100101010110100",       "0100111101010010011100", 
      "0100111101111010001000",       "0100111110100001111000",       "0100111111001001110000",       "0100111111110001101100", 
      "0101000000011001101100",       "0101000001000001110000",       "0101000001101001111100",       "0101000010010010001100", 
      "0101000010111010100000",       "0101000011100010111100",       "0101000100001011011000",       "0101000100110011111100", 
      "0101000101011100101000",       "0101000110000101010100",       "0101000110101110001000",       "0101000111010111000000", 
      "0101001000000000000000",       "0101001000101001000000",       "0101001001010010001000",       "0101001001111011010100", 
      "0101001010100100101000",       "0101001011001110000000",       "0101001011110111011100",       "0101001100100000111100", 
      "0101001101001010100100",       "0101001101110100010000",       "0101001110011110000000",       "0101001111000111111000", 
      "0101001111110001110000",       "0101010000011011110100",       "0101010001000101111000",       "0101010001110000000100", 
      "0101010010011010010100",       "0101010011000100101100",       "0101010011101111001000",       "0101010100011001101000", 
      "0101010101000100001100",       "0101010101101110111000",       "0101010110011001101000",       "0101010111000100100000", 
      "0101010111101111011000",       "0101011000011010011000",       "0101011001000101100000",       "0101011001110000101100", 
      "0101011010011011111100",       "0101011011000111010100",       "0101011011110010101100",       "0101011100011110010000", 
      "0101011101001001110100",       "0101011101110101100000",       "0101011110100001010100",       "0101011111001101001000", 
      "0101011111111001001000",       "0101100000100101001000",       "0101100001010001010000",       "0101100001111101011100", 
      "0101100010101001110000",       "0101100011010110001000",       "0101100100000010100100",       "0101100100101111001000", 
      "0101100101011011110000",       "0101100110001000100000",       "0101100110110101010100",       "0101100111100010001100", 
      "0101101000001111001100",       "0101101000111100010000",       "0101101001101001011000",       "0101101010010110101000", 
      "0101101011000100000000",       "0101101011110001011100",       "0101101100011110111100",       "0101101101001100100100", 
      "0101101101111010010000",       "0101101110101000000000",       "0101101111010101111000",       "0101110000000011111000", 
      "0101110000110001111000",       "0101110001100000000100",       "0101110010001110010000",       "0101110010111100101000", 
      "0101110011101011000000",       "0101110100011001100000",       "0101110101001000001000",       "0101110101110110110100", 
      "0101110110100101100100",       "0101110111010100011100",       "0101111000000011011100",       "0101111000110010011100", 
      "0101111001100001101000",       "0101111010010000111000",       "0101111011000000001100",       "0101111011101111101000", 
      "0101111100011111001000",       "0101111101001110101100",       "0101111101111110011101",       "0101111110101110001101", 
      "0101111111011110001001",       "0110000000001110000101",       "0110000000111110001001",       "0110000001101110010101", 
      "0110000010011110100101",       "0110000011001110111101",       "0110000011111111011001",       "0110000100101111111101", 
      "0110000101100000100101",       "0110000110010001010101",       "0110000111000010001001",       "0110000111110011000101", 
      "0110001000100100001001",       "0110001001010101001101",       "0110001010000110011101",       "0110001010110111110001", 
      "0110001011101001001001",       "0110001100011010101001",       "0110001101001100010001",       "0110001101111101111101", 
      "0110001110101111110001",       "0110001111100001101001",       "0110010000010011101001",       "0110010001000101101101", 
      "0110010001110111111001",       "0110010010101010001101",       "0110010011011100100101",       "0110010100001111000101", 
      "0110010101000001101001",       "0110010101110100010101",       "0110010110100111000101",       "0110010111011001111101", 
      "0110011000001100111101",       "0110011001000000000001",       "0110011001110011001101",       "0110011010100110011101", 
      "0110011011011001110101",       "0110011100001101010101",       "0110011101000000111001",       "0110011101110100100101", 
      "0110011110101000010101",       "0110011111011100001101",       "0110100000010000001101",       "0110100001000100010001", 
      "0110100001111000011101",       "0110100010101100110001",       "0110100011100001001001",       "0110100100010101101001", 
      "0110100101001010010001",       "0110100101111110111101",       "0110100110110011101101",       "0110100111101000101001", 
      "0110101000011101101001",       "0110101001010010110001",       "0110101010000111111101",       "0110101010111101010001", 
      "0110101011110010101101",       "0110101100101000001101",       "0110101101011101111001",       "0110101110010011100101", 
      "0110101111001001011101",       "0110101111111111011001",       "0110110000110101011101",       "0110110001101011100101", 
      "0110110010100001110101",       "0110110011011000001101",       "0110110100001110101101",       "0110110101000101010001", 
      "0110110101111011111101",       "0110110110110010110001",       "0110110111101001101001",       "0110111000100000101101", 
      "0110111001010111110101",       "0110111010001111000001",       "0110111011000110010101",       "0110111011111101110101", 
      "0110111100110101010101",       "0110111101101101000001",       "0110111110100100110001",       "0110111111011100101001", 
      "0111000000010100101001",       "0111000001001100101101",       "0111000010000100111101",       "0111000010111101010001", 
      "0111000011110101101101",       "0111000100101110001101",       "0111000101100110110101",       "0111000110011111101001", 
      "0111000111011000011101",       "0111001000010001011101",       "0111001001001010100001",       "0111001010000011110001", 
      "0111001010111101000101",       "0111001011110110100001",       "0111001100110000000001",       "0111001101101001101101", 
      "0111001110100011011101",       "0111001111011101010101",       "0111010000010111010101",       "0111010001010001011001", 
      "0111010010001011101001",       "0111010011000101111101",       "0111010100000000011001",       "0111010100111010111101", 
      "0111010101110101101001",       "0111010110110000011001",       "0111010111101011010101",       "0111011000100110010101", 
      "0111011001100001011101",       "0111011010011100101101",       "0111011011011000000101",       "0111011100010011100101", 
      "0111011101001111001001",       "0111011110001010111001",       "0111011111000110101101",       "0111100000000010101001", 
      "0111100000111110101101",       "0111100001111010111001",       "0111100010110111001101",       "0111100011110011100101", 
      "0111100100110000001001",       "0111100101101100110001",       "0111100110101001100101",       "0111100111100110011101", 
      "0111101000100011011101",       "0111101001100000100101",       "0111101010011101110101",       "0111101011011011001101", 
      "0111101100011000101101",       "0111101101010110010101",       "0111101110010100000001",       "0111101111010001111001", 
      "0111110000001111110101",       "0111110001001101111101",       "0111110010001100001001",       "0111110011001010100001", 
      "0111110100001000111101",       "0111110101000111100001",       "0111110110000110001101",       "0111110111000101000101", 
      "0111111000000100000001",       "0111111001000011000101",       "0111111010000010010001",       "0111111011000001100101", 
      "0111111100000001000001",       "0111111101000000100101",       "0111111110000000010001",       "0111111111000000000101" 
)
;
begin
          Y1 <= memVar(conv_integer(X1)); 
          Y2 <= memVar(conv_integer(X2)); 
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_11_f210_uid54
--                     (IntAdderClassical_11_F210_uid56)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_11_f210_uid54 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(10 downto 0);
          Y : in  std_logic_vector(10 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of IntAdder_11_f210_uid54 is
signal X_d1 :  std_logic_vector(10 downto 0);
signal Y_d1 :  std_logic_vector(10 downto 0);
signal Cin_d1 :  std_logic;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            X_d1 <=  X;
            Y_d1 <=  Y;
            Cin_d1 <=  Cin;
         end if;
      end process;
   --Classical
   ----------------Synchro barrier, entering cycle 1----------------
    R <= X_d1 + Y_d1 + Cin_d1;
end architecture;

--------------------------------------------------------------------------------
--                           IntAdder_11_f210_uid62
--                    (IntAdderAlternative_11_F210_uid66)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_11_f210_uid62 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(10 downto 0);
          Y : in  std_logic_vector(10 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(10 downto 0)   );
end entity;

architecture arch of IntAdder_11_f210_uid62 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_17_f210_uid166
--                     (IntAdderClassical_17_F210_uid168)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_17_f210_uid166 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(16 downto 0);
          Y : in  std_logic_vector(16 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(16 downto 0)   );
end entity;

architecture arch of IntAdder_17_f210_uid166 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--            IntMultiplier_UsingDSP_10_11_12_unsigned_F210_uid70
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin, Kinga Illyes, Bogdan Popa, Bogdan Pasca, 2012
--------------------------------------------------------------------------------
-- Pipeline depth: 1 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_UsingDSP_10_11_12_unsigned_F210_uid70 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(9 downto 0);
          Y : in  std_logic_vector(10 downto 0);
          R : out  std_logic_vector(11 downto 0)   );
end entity;

architecture arch of IntMultiplier_UsingDSP_10_11_12_unsigned_F210_uid70 is
   component IntAdder_17_f210_uid166 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(16 downto 0);
             Y : in  std_logic_vector(16 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(16 downto 0)   );
   end component;

   component SmallMultTableP3x3r6XuYu_F210_uid75 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             Y : out  std_logic_vector(5 downto 0)   );
   end component;

   component Compressor_6_3 is
      port ( X0 : in  std_logic_vector(5 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_14_3 is
      port ( X0 : in  std_logic_vector(3 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_4_3 is
      port ( X0 : in  std_logic_vector(3 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_23_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(1 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_13_3 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             X1 : in  std_logic_vector(0 downto 0);
             R : out  std_logic_vector(2 downto 0)   );
   end component;

   component Compressor_3_2 is
      port ( X0 : in  std_logic_vector(2 downto 0);
             R : out  std_logic_vector(1 downto 0)   );
   end component;

signal XX_m71 :  std_logic_vector(10 downto 0);
signal YY_m71 :  std_logic_vector(9 downto 0);
signal Xp_m71b73 :  std_logic_vector(11 downto 0);
signal Yp_m71b73 :  std_logic_vector(11 downto 0);
signal x_m71b73_0 :  std_logic_vector(2 downto 0);
signal x_m71b73_1 :  std_logic_vector(2 downto 0);
signal x_m71b73_2 :  std_logic_vector(2 downto 0);
signal x_m71b73_3 :  std_logic_vector(2 downto 0);
signal y_m71b73_0 :  std_logic_vector(2 downto 0);
signal y_m71b73_1 :  std_logic_vector(2 downto 0);
signal y_m71b73_2 :  std_logic_vector(2 downto 0);
signal y_m71b73_3 :  std_logic_vector(2 downto 0);
signal Y0X1_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X1Y0_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w0_0, heap_bh72_w0_0_d1 :  std_logic;
signal Y0X2_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X2Y0_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w0_1 :  std_logic;
signal heap_bh72_w1_0 :  std_logic;
signal heap_bh72_w2_0 :  std_logic;
signal heap_bh72_w3_0 :  std_logic;
signal Y0X3_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X3Y0_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w3_1 :  std_logic;
signal heap_bh72_w4_0 :  std_logic;
signal heap_bh72_w5_0 :  std_logic;
signal heap_bh72_w6_0 :  std_logic;
signal Y1X0_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X0Y1_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w0_2 :  std_logic;
signal Y1X1_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X1Y1_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w0_3 :  std_logic;
signal heap_bh72_w1_1 :  std_logic;
signal heap_bh72_w2_1 :  std_logic;
signal heap_bh72_w3_2 :  std_logic;
signal Y1X2_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X2Y1_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w1_2 :  std_logic;
signal heap_bh72_w2_2 :  std_logic;
signal heap_bh72_w3_3 :  std_logic;
signal heap_bh72_w4_1 :  std_logic;
signal heap_bh72_w5_1 :  std_logic;
signal heap_bh72_w6_1 :  std_logic;
signal Y1X3_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X3Y1_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w4_2 :  std_logic;
signal heap_bh72_w5_2 :  std_logic;
signal heap_bh72_w6_2 :  std_logic;
signal heap_bh72_w7_0 :  std_logic;
signal heap_bh72_w8_0 :  std_logic;
signal heap_bh72_w9_0 :  std_logic;
signal Y2X0_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X0Y2_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w0_4 :  std_logic;
signal heap_bh72_w1_3 :  std_logic;
signal heap_bh72_w2_3 :  std_logic;
signal heap_bh72_w3_4 :  std_logic;
signal Y2X1_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X1Y2_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w1_4 :  std_logic;
signal heap_bh72_w2_4 :  std_logic;
signal heap_bh72_w3_5 :  std_logic;
signal heap_bh72_w4_3 :  std_logic;
signal heap_bh72_w5_3 :  std_logic;
signal heap_bh72_w6_3 :  std_logic;
signal Y2X2_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X2Y2_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w4_4 :  std_logic;
signal heap_bh72_w5_4 :  std_logic;
signal heap_bh72_w6_4 :  std_logic;
signal heap_bh72_w7_1 :  std_logic;
signal heap_bh72_w8_1 :  std_logic;
signal heap_bh72_w9_1 :  std_logic;
signal Y2X3_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X3Y2_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w7_2 :  std_logic;
signal heap_bh72_w8_2 :  std_logic;
signal heap_bh72_w9_2 :  std_logic;
signal heap_bh72_w10_0 :  std_logic;
signal heap_bh72_w11_0 :  std_logic;
signal heap_bh72_w12_0 :  std_logic;
signal Y3X0_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X0Y3_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w2_5 :  std_logic;
signal heap_bh72_w3_6 :  std_logic;
signal heap_bh72_w4_5 :  std_logic;
signal heap_bh72_w5_5 :  std_logic;
signal heap_bh72_w6_5 :  std_logic;
signal Y3X1_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X1Y3_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w4_6 :  std_logic;
signal heap_bh72_w5_6 :  std_logic;
signal heap_bh72_w6_6 :  std_logic;
signal heap_bh72_w7_3 :  std_logic;
signal heap_bh72_w8_3 :  std_logic;
signal heap_bh72_w9_3 :  std_logic;
signal Y3X2_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X2Y3_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w7_4 :  std_logic;
signal heap_bh72_w8_4 :  std_logic;
signal heap_bh72_w9_4 :  std_logic;
signal heap_bh72_w10_1 :  std_logic;
signal heap_bh72_w11_1 :  std_logic;
signal heap_bh72_w12_1 :  std_logic;
signal Y3X3_73_m71 :  std_logic_vector(5 downto 0);
signal PP73X3Y3_m71 :  std_logic_vector(5 downto 0);
signal heap_bh72_w10_2 :  std_logic;
signal heap_bh72_w11_2 :  std_logic;
signal heap_bh72_w12_2 :  std_logic;
signal heap_bh72_w13_0 :  std_logic;
signal heap_bh72_w14_0, heap_bh72_w14_0_d1 :  std_logic;
signal heap_bh72_w15_0, heap_bh72_w15_0_d1 :  std_logic;
signal heap_bh72_w3_7 :  std_logic;
signal CompressorIn_bh72_0_0 :  std_logic_vector(5 downto 0);
signal CompressorOut_bh72_0_0 :  std_logic_vector(2 downto 0);
signal heap_bh72_w2_6 :  std_logic;
signal heap_bh72_w3_8 :  std_logic;
signal heap_bh72_w4_7 :  std_logic;
signal CompressorIn_bh72_1_1 :  std_logic_vector(5 downto 0);
signal CompressorOut_bh72_1_1 :  std_logic_vector(2 downto 0);
signal heap_bh72_w3_9 :  std_logic;
signal heap_bh72_w4_8 :  std_logic;
signal heap_bh72_w5_7 :  std_logic;
signal CompressorIn_bh72_2_2 :  std_logic_vector(5 downto 0);
signal CompressorOut_bh72_2_2 :  std_logic_vector(2 downto 0);
signal heap_bh72_w4_9 :  std_logic;
signal heap_bh72_w5_8 :  std_logic;
signal heap_bh72_w6_7 :  std_logic;
signal CompressorIn_bh72_3_3 :  std_logic_vector(5 downto 0);
signal CompressorOut_bh72_3_3 :  std_logic_vector(2 downto 0);
signal heap_bh72_w5_9 :  std_logic;
signal heap_bh72_w6_8 :  std_logic;
signal heap_bh72_w7_5 :  std_logic;
signal CompressorIn_bh72_4_4 :  std_logic_vector(5 downto 0);
signal CompressorOut_bh72_4_4 :  std_logic_vector(2 downto 0);
signal heap_bh72_w6_9 :  std_logic;
signal heap_bh72_w7_6 :  std_logic;
signal heap_bh72_w8_5 :  std_logic;
signal CompressorIn_bh72_5_5 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_5_6 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_5_5 :  std_logic_vector(2 downto 0);
signal heap_bh72_w0_5, heap_bh72_w0_5_d1 :  std_logic;
signal heap_bh72_w1_5, heap_bh72_w1_5_d1 :  std_logic;
signal heap_bh72_w2_7 :  std_logic;
signal CompressorIn_bh72_6_7 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_6_8 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_6_6 :  std_logic_vector(2 downto 0);
signal heap_bh72_w7_7 :  std_logic;
signal heap_bh72_w8_6 :  std_logic;
signal heap_bh72_w9_5 :  std_logic;
signal CompressorIn_bh72_7_9 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_7_10 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_7_7 :  std_logic_vector(2 downto 0);
signal heap_bh72_w8_7 :  std_logic;
signal heap_bh72_w9_6 :  std_logic;
signal heap_bh72_w10_3 :  std_logic;
signal CompressorIn_bh72_8_11 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_8_12 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_8_8 :  std_logic_vector(2 downto 0);
signal heap_bh72_w9_7 :  std_logic;
signal heap_bh72_w10_4 :  std_logic;
signal heap_bh72_w11_3 :  std_logic;
signal CompressorIn_bh72_9_13 :  std_logic_vector(3 downto 0);
signal CompressorOut_bh72_9_9 :  std_logic_vector(2 downto 0);
signal heap_bh72_w1_6, heap_bh72_w1_6_d1 :  std_logic;
signal heap_bh72_w2_8 :  std_logic;
signal heap_bh72_w3_10 :  std_logic;
signal CompressorIn_bh72_10_14 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_10_15 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh72_10_10 :  std_logic_vector(2 downto 0);
signal heap_bh72_w11_4 :  std_logic;
signal heap_bh72_w12_3 :  std_logic;
signal heap_bh72_w13_1 :  std_logic;
signal CompressorIn_bh72_11_16 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_11_17 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_11_11 :  std_logic_vector(2 downto 0);
signal heap_bh72_w3_11, heap_bh72_w3_11_d1 :  std_logic;
signal heap_bh72_w4_10 :  std_logic;
signal heap_bh72_w5_10, heap_bh72_w5_10_d1 :  std_logic;
signal CompressorIn_bh72_12_18 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_12_19 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_12_12 :  std_logic_vector(2 downto 0);
signal heap_bh72_w5_11 :  std_logic;
signal heap_bh72_w6_10 :  std_logic;
signal heap_bh72_w7_8 :  std_logic;
signal CompressorIn_bh72_13_20 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_13_21 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_13_13 :  std_logic_vector(2 downto 0);
signal heap_bh72_w7_9 :  std_logic;
signal heap_bh72_w8_8 :  std_logic;
signal heap_bh72_w9_8 :  std_logic;
signal CompressorIn_bh72_14_22 :  std_logic_vector(3 downto 0);
signal CompressorIn_bh72_14_23 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_14_14 :  std_logic_vector(2 downto 0);
signal heap_bh72_w10_5 :  std_logic;
signal heap_bh72_w11_5 :  std_logic;
signal heap_bh72_w12_4 :  std_logic;
signal CompressorIn_bh72_15_24 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_15_25 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_15_15 :  std_logic_vector(2 downto 0);
signal heap_bh72_w2_9, heap_bh72_w2_9_d1 :  std_logic;
signal heap_bh72_w3_12, heap_bh72_w3_12_d1 :  std_logic;
signal heap_bh72_w4_11 :  std_logic;
signal CompressorIn_bh72_16_26 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh72_16_16 :  std_logic_vector(1 downto 0);
signal heap_bh72_w4_12 :  std_logic;
signal heap_bh72_w5_12 :  std_logic;
signal CompressorIn_bh72_17_27 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh72_17_17 :  std_logic_vector(1 downto 0);
signal heap_bh72_w6_11 :  std_logic;
signal heap_bh72_w7_10 :  std_logic;
signal CompressorIn_bh72_18_28 :  std_logic_vector(2 downto 0);
signal CompressorOut_bh72_18_18 :  std_logic_vector(1 downto 0);
signal heap_bh72_w9_9 :  std_logic;
signal heap_bh72_w10_6 :  std_logic;
signal CompressorIn_bh72_19_29 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_19_30 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh72_19_19 :  std_logic_vector(2 downto 0);
signal heap_bh72_w4_13, heap_bh72_w4_13_d1 :  std_logic;
signal heap_bh72_w5_13, heap_bh72_w5_13_d1 :  std_logic;
signal heap_bh72_w6_12 :  std_logic;
signal CompressorIn_bh72_20_31 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_20_32 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh72_20_20 :  std_logic_vector(2 downto 0);
signal heap_bh72_w7_11 :  std_logic;
signal heap_bh72_w8_9 :  std_logic;
signal heap_bh72_w9_10 :  std_logic;
signal CompressorIn_bh72_21_33 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_21_34 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh72_21_21 :  std_logic_vector(2 downto 0);
signal heap_bh72_w12_5 :  std_logic;
signal heap_bh72_w13_2, heap_bh72_w13_2_d1 :  std_logic;
signal heap_bh72_w14_1, heap_bh72_w14_1_d1 :  std_logic;
signal CompressorIn_bh72_22_35 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_22_36 :  std_logic_vector(1 downto 0);
signal CompressorOut_bh72_22_22 :  std_logic_vector(2 downto 0);
signal heap_bh72_w9_11 :  std_logic;
signal heap_bh72_w10_7, heap_bh72_w10_7_d1 :  std_logic;
signal heap_bh72_w11_6 :  std_logic;
signal CompressorIn_bh72_23_37 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_23_38 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_23_23 :  std_logic_vector(2 downto 0);
signal heap_bh72_w6_13, heap_bh72_w6_13_d1 :  std_logic;
signal heap_bh72_w7_12, heap_bh72_w7_12_d1 :  std_logic;
signal heap_bh72_w8_10 :  std_logic;
signal CompressorIn_bh72_24_39 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_24_40 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_24_24 :  std_logic_vector(2 downto 0);
signal heap_bh72_w8_11, heap_bh72_w8_11_d1 :  std_logic;
signal heap_bh72_w9_12, heap_bh72_w9_12_d1 :  std_logic;
signal heap_bh72_w10_8, heap_bh72_w10_8_d1 :  std_logic;
signal CompressorIn_bh72_25_41 :  std_logic_vector(2 downto 0);
signal CompressorIn_bh72_25_42 :  std_logic_vector(0 downto 0);
signal CompressorOut_bh72_25_25 :  std_logic_vector(2 downto 0);
signal heap_bh72_w11_7, heap_bh72_w11_7_d1 :  std_logic;
signal heap_bh72_w12_6, heap_bh72_w12_6_d1 :  std_logic;
signal heap_bh72_w13_3, heap_bh72_w13_3_d1 :  std_logic;
signal finalAdderIn0_bh72 :  std_logic_vector(16 downto 0);
signal finalAdderIn1_bh72 :  std_logic_vector(16 downto 0);
signal finalAdderCin_bh72 :  std_logic;
signal finalAdderOut_bh72 :  std_logic_vector(16 downto 0);
signal CompressionResult72 :  std_logic_vector(16 downto 0);
attribute rom_extract: string;
attribute rom_style: string;
attribute rom_extract of SmallMultTableP3x3r6XuYu_F210_uid75: component is "yes";
attribute rom_style of SmallMultTableP3x3r6XuYu_F210_uid75: component is "distributed";
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            heap_bh72_w0_0_d1 <=  heap_bh72_w0_0;
            heap_bh72_w14_0_d1 <=  heap_bh72_w14_0;
            heap_bh72_w15_0_d1 <=  heap_bh72_w15_0;
            heap_bh72_w0_5_d1 <=  heap_bh72_w0_5;
            heap_bh72_w1_5_d1 <=  heap_bh72_w1_5;
            heap_bh72_w1_6_d1 <=  heap_bh72_w1_6;
            heap_bh72_w3_11_d1 <=  heap_bh72_w3_11;
            heap_bh72_w5_10_d1 <=  heap_bh72_w5_10;
            heap_bh72_w2_9_d1 <=  heap_bh72_w2_9;
            heap_bh72_w3_12_d1 <=  heap_bh72_w3_12;
            heap_bh72_w4_13_d1 <=  heap_bh72_w4_13;
            heap_bh72_w5_13_d1 <=  heap_bh72_w5_13;
            heap_bh72_w13_2_d1 <=  heap_bh72_w13_2;
            heap_bh72_w14_1_d1 <=  heap_bh72_w14_1;
            heap_bh72_w10_7_d1 <=  heap_bh72_w10_7;
            heap_bh72_w6_13_d1 <=  heap_bh72_w6_13;
            heap_bh72_w7_12_d1 <=  heap_bh72_w7_12;
            heap_bh72_w8_11_d1 <=  heap_bh72_w8_11;
            heap_bh72_w9_12_d1 <=  heap_bh72_w9_12;
            heap_bh72_w10_8_d1 <=  heap_bh72_w10_8;
            heap_bh72_w11_7_d1 <=  heap_bh72_w11_7;
            heap_bh72_w12_6_d1 <=  heap_bh72_w12_6;
            heap_bh72_w13_3_d1 <=  heap_bh72_w13_3;
         end if;
      end process;
   XX_m71 <= Y ;
   YY_m71 <= X ;
   -- code generated by IntMultiplier::buildHeapLogicOnly()
   -- buildheaplogiconly called for lsbX=0 lsbY=0 msbX=11 msbY=10
   Xp_m71b73 <= XX_m71(10 downto 0) & "0";
   Yp_m71b73 <= YY_m71(9 downto 0) & "00";
   x_m71b73_0 <= Xp_m71b73(2 downto 0);
   x_m71b73_1 <= Xp_m71b73(5 downto 3);
   x_m71b73_2 <= Xp_m71b73(8 downto 6);
   x_m71b73_3 <= Xp_m71b73(11 downto 9);
   y_m71b73_0 <= Yp_m71b73(2 downto 0);
   y_m71b73_1 <= Yp_m71b73(5 downto 3);
   y_m71b73_2 <= Yp_m71b73(8 downto 6);
   y_m71b73_3 <= Yp_m71b73(11 downto 9);
   ----------------Synchro barrier, entering cycle 0----------------
   -- Partial product row number 0
   Y0X1_73_m71 <= y_m71b73_0 & x_m71b73_1;
   PP_m71_73X1Y0_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y0X1_73_m71,
                 Y => PP73X1Y0_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w0_0 <= PP73X1Y0_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y0X2_73_m71 <= y_m71b73_0 & x_m71b73_2;
   PP_m71_73X2Y0_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y0X2_73_m71,
                 Y => PP73X2Y0_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w0_1 <= PP73X2Y0_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w1_0 <= PP73X2Y0_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w2_0 <= PP73X2Y0_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w3_0 <= PP73X2Y0_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y0X3_73_m71 <= y_m71b73_0 & x_m71b73_3;
   PP_m71_73X3Y0_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y0X3_73_m71,
                 Y => PP73X3Y0_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w3_1 <= PP73X3Y0_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w4_0 <= PP73X3Y0_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w5_0 <= PP73X3Y0_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w6_0 <= PP73X3Y0_m71(5); -- cycle= 0 cp= 5.5688e-10

   -- Partial product row number 1
   Y1X0_73_m71 <= y_m71b73_1 & x_m71b73_0;
   PP_m71_73X0Y1_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y1X0_73_m71,
                 Y => PP73X0Y1_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w0_2 <= PP73X0Y1_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y1X1_73_m71 <= y_m71b73_1 & x_m71b73_1;
   PP_m71_73X1Y1_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y1X1_73_m71,
                 Y => PP73X1Y1_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w0_3 <= PP73X1Y1_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w1_1 <= PP73X1Y1_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w2_1 <= PP73X1Y1_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w3_2 <= PP73X1Y1_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y1X2_73_m71 <= y_m71b73_1 & x_m71b73_2;
   PP_m71_73X2Y1_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y1X2_73_m71,
                 Y => PP73X2Y1_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w1_2 <= PP73X2Y1_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w2_2 <= PP73X2Y1_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w3_3 <= PP73X2Y1_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w4_1 <= PP73X2Y1_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w5_1 <= PP73X2Y1_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w6_1 <= PP73X2Y1_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y1X3_73_m71 <= y_m71b73_1 & x_m71b73_3;
   PP_m71_73X3Y1_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y1X3_73_m71,
                 Y => PP73X3Y1_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w4_2 <= PP73X3Y1_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w5_2 <= PP73X3Y1_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w6_2 <= PP73X3Y1_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w7_0 <= PP73X3Y1_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w8_0 <= PP73X3Y1_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w9_0 <= PP73X3Y1_m71(5); -- cycle= 0 cp= 5.5688e-10

   -- Partial product row number 2
   Y2X0_73_m71 <= y_m71b73_2 & x_m71b73_0;
   PP_m71_73X0Y2_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y2X0_73_m71,
                 Y => PP73X0Y2_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w0_4 <= PP73X0Y2_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w1_3 <= PP73X0Y2_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w2_3 <= PP73X0Y2_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w3_4 <= PP73X0Y2_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y2X1_73_m71 <= y_m71b73_2 & x_m71b73_1;
   PP_m71_73X1Y2_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y2X1_73_m71,
                 Y => PP73X1Y2_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w1_4 <= PP73X1Y2_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w2_4 <= PP73X1Y2_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w3_5 <= PP73X1Y2_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w4_3 <= PP73X1Y2_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w5_3 <= PP73X1Y2_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w6_3 <= PP73X1Y2_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y2X2_73_m71 <= y_m71b73_2 & x_m71b73_2;
   PP_m71_73X2Y2_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y2X2_73_m71,
                 Y => PP73X2Y2_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w4_4 <= PP73X2Y2_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w5_4 <= PP73X2Y2_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w6_4 <= PP73X2Y2_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w7_1 <= PP73X2Y2_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w8_1 <= PP73X2Y2_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w9_1 <= PP73X2Y2_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y2X3_73_m71 <= y_m71b73_2 & x_m71b73_3;
   PP_m71_73X3Y2_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y2X3_73_m71,
                 Y => PP73X3Y2_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w7_2 <= PP73X3Y2_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w8_2 <= PP73X3Y2_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w9_2 <= PP73X3Y2_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w10_0 <= PP73X3Y2_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w11_0 <= PP73X3Y2_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w12_0 <= PP73X3Y2_m71(5); -- cycle= 0 cp= 5.5688e-10

   -- Partial product row number 3
   Y3X0_73_m71 <= y_m71b73_3 & x_m71b73_0;
   PP_m71_73X0Y3_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y3X0_73_m71,
                 Y => PP73X0Y3_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w2_5 <= PP73X0Y3_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w3_6 <= PP73X0Y3_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w4_5 <= PP73X0Y3_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w5_5 <= PP73X0Y3_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w6_5 <= PP73X0Y3_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y3X1_73_m71 <= y_m71b73_3 & x_m71b73_1;
   PP_m71_73X1Y3_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y3X1_73_m71,
                 Y => PP73X1Y3_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w4_6 <= PP73X1Y3_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w5_6 <= PP73X1Y3_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w6_6 <= PP73X1Y3_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w7_3 <= PP73X1Y3_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w8_3 <= PP73X1Y3_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w9_3 <= PP73X1Y3_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y3X2_73_m71 <= y_m71b73_3 & x_m71b73_2;
   PP_m71_73X2Y3_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y3X2_73_m71,
                 Y => PP73X2Y3_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w7_4 <= PP73X2Y3_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w8_4 <= PP73X2Y3_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w9_4 <= PP73X2Y3_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w10_1 <= PP73X2Y3_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w11_1 <= PP73X2Y3_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w12_1 <= PP73X2Y3_m71(5); -- cycle= 0 cp= 5.5688e-10

   Y3X3_73_m71 <= y_m71b73_3 & x_m71b73_3;
   PP_m71_73X3Y3_Tbl: SmallMultTableP3x3r6XuYu_F210_uid75  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 X => Y3X3_73_m71,
                 Y => PP73X3Y3_m71);
   -- Adding the relevant bits to the heap of bits
   heap_bh72_w10_2 <= PP73X3Y3_m71(0); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w11_2 <= PP73X3Y3_m71(1); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w12_2 <= PP73X3Y3_m71(2); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w13_0 <= PP73X3Y3_m71(3); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w14_0 <= PP73X3Y3_m71(4); -- cycle= 0 cp= 5.5688e-10
   heap_bh72_w15_0 <= PP73X3Y3_m71(5); -- cycle= 0 cp= 5.5688e-10

   
   -- Beginning of code generated by BitHeap::generateCompressorVHDL
   -- code generated by BitHeap::generateSupertileVHDL()
   ----------------Synchro barrier, entering cycle 0----------------

   -- Adding the constant bits
   heap_bh72_w3_7 <= '1'; -- cycle= 0 cp= 0

   ----------------Synchro barrier, entering cycle 0----------------

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_0_0 <= heap_bh72_w2_5 & heap_bh72_w2_4 & heap_bh72_w2_3 & heap_bh72_w2_2 & heap_bh72_w2_1 & heap_bh72_w2_0;
   Compressor_bh72_0: Compressor_6_3
      port map ( R => CompressorOut_bh72_0_0   ,
                 X0 => CompressorIn_bh72_0_0);
   heap_bh72_w2_6 <= CompressorOut_bh72_0_0(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w3_8 <= CompressorOut_bh72_0_0(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w4_7 <= CompressorOut_bh72_0_0(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_1_1 <= heap_bh72_w3_7 & heap_bh72_w3_6 & heap_bh72_w3_5 & heap_bh72_w3_4 & heap_bh72_w3_3 & heap_bh72_w3_2;
   Compressor_bh72_1: Compressor_6_3
      port map ( R => CompressorOut_bh72_1_1   ,
                 X0 => CompressorIn_bh72_1_1);
   heap_bh72_w3_9 <= CompressorOut_bh72_1_1(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w4_8 <= CompressorOut_bh72_1_1(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w5_7 <= CompressorOut_bh72_1_1(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_2_2 <= heap_bh72_w4_6 & heap_bh72_w4_5 & heap_bh72_w4_4 & heap_bh72_w4_3 & heap_bh72_w4_2 & heap_bh72_w4_1;
   Compressor_bh72_2: Compressor_6_3
      port map ( R => CompressorOut_bh72_2_2   ,
                 X0 => CompressorIn_bh72_2_2);
   heap_bh72_w4_9 <= CompressorOut_bh72_2_2(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w5_8 <= CompressorOut_bh72_2_2(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w6_7 <= CompressorOut_bh72_2_2(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_3_3 <= heap_bh72_w5_6 & heap_bh72_w5_5 & heap_bh72_w5_4 & heap_bh72_w5_3 & heap_bh72_w5_2 & heap_bh72_w5_1;
   Compressor_bh72_3: Compressor_6_3
      port map ( R => CompressorOut_bh72_3_3   ,
                 X0 => CompressorIn_bh72_3_3);
   heap_bh72_w5_9 <= CompressorOut_bh72_3_3(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w6_8 <= CompressorOut_bh72_3_3(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w7_5 <= CompressorOut_bh72_3_3(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_4_4 <= heap_bh72_w6_6 & heap_bh72_w6_5 & heap_bh72_w6_4 & heap_bh72_w6_3 & heap_bh72_w6_2 & heap_bh72_w6_1;
   Compressor_bh72_4: Compressor_6_3
      port map ( R => CompressorOut_bh72_4_4   ,
                 X0 => CompressorIn_bh72_4_4);
   heap_bh72_w6_9 <= CompressorOut_bh72_4_4(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w7_6 <= CompressorOut_bh72_4_4(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w8_5 <= CompressorOut_bh72_4_4(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_5_5 <= heap_bh72_w0_4 & heap_bh72_w0_3 & heap_bh72_w0_2 & heap_bh72_w0_1;
   CompressorIn_bh72_5_6(0) <= heap_bh72_w1_4;
   Compressor_bh72_5: Compressor_14_3
      port map ( R => CompressorOut_bh72_5_5   ,
                 X0 => CompressorIn_bh72_5_5,
                 X1 => CompressorIn_bh72_5_6);
   heap_bh72_w0_5 <= CompressorOut_bh72_5_5(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w1_5 <= CompressorOut_bh72_5_5(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w2_7 <= CompressorOut_bh72_5_5(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_6_7 <= heap_bh72_w7_4 & heap_bh72_w7_3 & heap_bh72_w7_2 & heap_bh72_w7_1;
   CompressorIn_bh72_6_8(0) <= heap_bh72_w8_4;
   Compressor_bh72_6: Compressor_14_3
      port map ( R => CompressorOut_bh72_6_6   ,
                 X0 => CompressorIn_bh72_6_7,
                 X1 => CompressorIn_bh72_6_8);
   heap_bh72_w7_7 <= CompressorOut_bh72_6_6(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w8_6 <= CompressorOut_bh72_6_6(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w9_5 <= CompressorOut_bh72_6_6(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_7_9 <= heap_bh72_w8_3 & heap_bh72_w8_2 & heap_bh72_w8_1 & heap_bh72_w8_0;
   CompressorIn_bh72_7_10(0) <= heap_bh72_w9_4;
   Compressor_bh72_7: Compressor_14_3
      port map ( R => CompressorOut_bh72_7_7   ,
                 X0 => CompressorIn_bh72_7_9,
                 X1 => CompressorIn_bh72_7_10);
   heap_bh72_w8_7 <= CompressorOut_bh72_7_7(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w9_6 <= CompressorOut_bh72_7_7(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w10_3 <= CompressorOut_bh72_7_7(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_8_11 <= heap_bh72_w9_3 & heap_bh72_w9_2 & heap_bh72_w9_1 & heap_bh72_w9_0;
   CompressorIn_bh72_8_12(0) <= heap_bh72_w10_2;
   Compressor_bh72_8: Compressor_14_3
      port map ( R => CompressorOut_bh72_8_8   ,
                 X0 => CompressorIn_bh72_8_11,
                 X1 => CompressorIn_bh72_8_12);
   heap_bh72_w9_7 <= CompressorOut_bh72_8_8(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w10_4 <= CompressorOut_bh72_8_8(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w11_3 <= CompressorOut_bh72_8_8(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_9_13 <= heap_bh72_w1_3 & heap_bh72_w1_2 & heap_bh72_w1_1 & heap_bh72_w1_0;
   Compressor_bh72_9: Compressor_4_3
      port map ( R => CompressorOut_bh72_9_9   ,
                 X0 => CompressorIn_bh72_9_13);
   heap_bh72_w1_6 <= CompressorOut_bh72_9_9(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w2_8 <= CompressorOut_bh72_9_9(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w3_10 <= CompressorOut_bh72_9_9(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_10_14 <= heap_bh72_w11_2 & heap_bh72_w11_1 & heap_bh72_w11_0;
   CompressorIn_bh72_10_15 <= heap_bh72_w12_2 & heap_bh72_w12_1;
   Compressor_bh72_10: Compressor_23_3
      port map ( R => CompressorOut_bh72_10_10   ,
                 X0 => CompressorIn_bh72_10_14,
                 X1 => CompressorIn_bh72_10_15);
   heap_bh72_w11_4 <= CompressorOut_bh72_10_10(0); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w12_3 <= CompressorOut_bh72_10_10(1); -- cycle= 0 cp= 1.0876e-09
   heap_bh72_w13_1 <= CompressorOut_bh72_10_10(2); -- cycle= 0 cp= 1.0876e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_11_16 <= heap_bh72_w3_1 & heap_bh72_w3_0 & heap_bh72_w3_10 & heap_bh72_w3_9;
   CompressorIn_bh72_11_17(0) <= heap_bh72_w4_0;
   Compressor_bh72_11: Compressor_14_3
      port map ( R => CompressorOut_bh72_11_11   ,
                 X0 => CompressorIn_bh72_11_16,
                 X1 => CompressorIn_bh72_11_17);
   heap_bh72_w3_11 <= CompressorOut_bh72_11_11(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w4_10 <= CompressorOut_bh72_11_11(1); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w5_10 <= CompressorOut_bh72_11_11(2); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_12_18 <= heap_bh72_w5_0 & heap_bh72_w5_9 & heap_bh72_w5_8 & heap_bh72_w5_7;
   CompressorIn_bh72_12_19(0) <= heap_bh72_w6_0;
   Compressor_bh72_12: Compressor_14_3
      port map ( R => CompressorOut_bh72_12_12   ,
                 X0 => CompressorIn_bh72_12_18,
                 X1 => CompressorIn_bh72_12_19);
   heap_bh72_w5_11 <= CompressorOut_bh72_12_12(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w6_10 <= CompressorOut_bh72_12_12(1); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w7_8 <= CompressorOut_bh72_12_12(2); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_13_20 <= heap_bh72_w7_0 & heap_bh72_w7_7 & heap_bh72_w7_6 & heap_bh72_w7_5;
   CompressorIn_bh72_13_21(0) <= heap_bh72_w8_7;
   Compressor_bh72_13: Compressor_14_3
      port map ( R => CompressorOut_bh72_13_13   ,
                 X0 => CompressorIn_bh72_13_20,
                 X1 => CompressorIn_bh72_13_21);
   heap_bh72_w7_9 <= CompressorOut_bh72_13_13(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w8_8 <= CompressorOut_bh72_13_13(1); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w9_8 <= CompressorOut_bh72_13_13(2); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_14_22 <= heap_bh72_w10_1 & heap_bh72_w10_0 & heap_bh72_w10_4 & heap_bh72_w10_3;
   CompressorIn_bh72_14_23(0) <= heap_bh72_w11_4;
   Compressor_bh72_14: Compressor_14_3
      port map ( R => CompressorOut_bh72_14_14   ,
                 X0 => CompressorIn_bh72_14_22,
                 X1 => CompressorIn_bh72_14_23);
   heap_bh72_w10_5 <= CompressorOut_bh72_14_14(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w11_5 <= CompressorOut_bh72_14_14(1); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w12_4 <= CompressorOut_bh72_14_14(2); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_15_24 <= heap_bh72_w2_8 & heap_bh72_w2_7 & heap_bh72_w2_6;
   CompressorIn_bh72_15_25(0) <= heap_bh72_w3_8;
   Compressor_bh72_15: Compressor_13_3
      port map ( R => CompressorOut_bh72_15_15   ,
                 X0 => CompressorIn_bh72_15_24,
                 X1 => CompressorIn_bh72_15_25);
   heap_bh72_w2_9 <= CompressorOut_bh72_15_15(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w3_12 <= CompressorOut_bh72_15_15(1); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w4_11 <= CompressorOut_bh72_15_15(2); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_16_26 <= heap_bh72_w4_9 & heap_bh72_w4_8 & heap_bh72_w4_7;
   Compressor_bh72_16: Compressor_3_2
      port map ( R => CompressorOut_bh72_16_16   ,
                 X0 => CompressorIn_bh72_16_26);
   heap_bh72_w4_12 <= CompressorOut_bh72_16_16(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w5_12 <= CompressorOut_bh72_16_16(1); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_17_27 <= heap_bh72_w6_9 & heap_bh72_w6_8 & heap_bh72_w6_7;
   Compressor_bh72_17: Compressor_3_2
      port map ( R => CompressorOut_bh72_17_17   ,
                 X0 => CompressorIn_bh72_17_27);
   heap_bh72_w6_11 <= CompressorOut_bh72_17_17(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w7_10 <= CompressorOut_bh72_17_17(1); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_18_28 <= heap_bh72_w9_7 & heap_bh72_w9_6 & heap_bh72_w9_5;
   Compressor_bh72_18: Compressor_3_2
      port map ( R => CompressorOut_bh72_18_18   ,
                 X0 => CompressorIn_bh72_18_28);
   heap_bh72_w9_9 <= CompressorOut_bh72_18_18(0); -- cycle= 0 cp= 1.61832e-09
   heap_bh72_w10_6 <= CompressorOut_bh72_18_18(1); -- cycle= 0 cp= 1.61832e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_19_29 <= heap_bh72_w4_12 & heap_bh72_w4_11 & heap_bh72_w4_10;
   CompressorIn_bh72_19_30 <= heap_bh72_w5_12 & heap_bh72_w5_11;
   Compressor_bh72_19: Compressor_23_3
      port map ( R => CompressorOut_bh72_19_19   ,
                 X0 => CompressorIn_bh72_19_29,
                 X1 => CompressorIn_bh72_19_30);
   heap_bh72_w4_13 <= CompressorOut_bh72_19_19(0); -- cycle= 0 cp= 2.14904e-09
   heap_bh72_w5_13 <= CompressorOut_bh72_19_19(1); -- cycle= 0 cp= 2.14904e-09
   heap_bh72_w6_12 <= CompressorOut_bh72_19_19(2); -- cycle= 0 cp= 2.14904e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_20_31 <= heap_bh72_w7_10 & heap_bh72_w7_9 & heap_bh72_w7_8;
   CompressorIn_bh72_20_32 <= heap_bh72_w8_6 & heap_bh72_w8_5;
   Compressor_bh72_20: Compressor_23_3
      port map ( R => CompressorOut_bh72_20_20   ,
                 X0 => CompressorIn_bh72_20_31,
                 X1 => CompressorIn_bh72_20_32);
   heap_bh72_w7_11 <= CompressorOut_bh72_20_20(0); -- cycle= 0 cp= 2.14904e-09
   heap_bh72_w8_9 <= CompressorOut_bh72_20_20(1); -- cycle= 0 cp= 2.14904e-09
   heap_bh72_w9_10 <= CompressorOut_bh72_20_20(2); -- cycle= 0 cp= 2.14904e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_21_33 <= heap_bh72_w12_0 & heap_bh72_w12_3 & heap_bh72_w12_4;
   CompressorIn_bh72_21_34 <= heap_bh72_w13_0 & heap_bh72_w13_1;
   Compressor_bh72_21: Compressor_23_3
      port map ( R => CompressorOut_bh72_21_21   ,
                 X0 => CompressorIn_bh72_21_33,
                 X1 => CompressorIn_bh72_21_34);
   heap_bh72_w12_5 <= CompressorOut_bh72_21_21(0); -- cycle= 0 cp= 2.14904e-09
   heap_bh72_w13_2 <= CompressorOut_bh72_21_21(1); -- cycle= 0 cp= 2.14904e-09
   heap_bh72_w14_1 <= CompressorOut_bh72_21_21(2); -- cycle= 0 cp= 2.14904e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_22_35 <= heap_bh72_w9_9 & heap_bh72_w9_8 & heap_bh72_w9_10;
   CompressorIn_bh72_22_36 <= heap_bh72_w10_6 & heap_bh72_w10_5;
   Compressor_bh72_22: Compressor_23_3
      port map ( R => CompressorOut_bh72_22_22   ,
                 X0 => CompressorIn_bh72_22_35,
                 X1 => CompressorIn_bh72_22_36);
   heap_bh72_w9_11 <= CompressorOut_bh72_22_22(0); -- cycle= 0 cp= 2.67976e-09
   heap_bh72_w10_7 <= CompressorOut_bh72_22_22(1); -- cycle= 0 cp= 2.67976e-09
   heap_bh72_w11_6 <= CompressorOut_bh72_22_22(2); -- cycle= 0 cp= 2.67976e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_23_37 <= heap_bh72_w6_11 & heap_bh72_w6_10 & heap_bh72_w6_12;
   CompressorIn_bh72_23_38(0) <= heap_bh72_w7_11;
   Compressor_bh72_23: Compressor_13_3
      port map ( R => CompressorOut_bh72_23_23   ,
                 X0 => CompressorIn_bh72_23_37,
                 X1 => CompressorIn_bh72_23_38);
   heap_bh72_w6_13 <= CompressorOut_bh72_23_23(0); -- cycle= 0 cp= 2.67976e-09
   heap_bh72_w7_12 <= CompressorOut_bh72_23_23(1); -- cycle= 0 cp= 2.67976e-09
   heap_bh72_w8_10 <= CompressorOut_bh72_23_23(2); -- cycle= 0 cp= 2.67976e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_24_39 <= heap_bh72_w8_8 & heap_bh72_w8_9 & heap_bh72_w8_10;
   CompressorIn_bh72_24_40(0) <= heap_bh72_w9_11;
   Compressor_bh72_24: Compressor_13_3
      port map ( R => CompressorOut_bh72_24_24   ,
                 X0 => CompressorIn_bh72_24_39,
                 X1 => CompressorIn_bh72_24_40);
   heap_bh72_w8_11 <= CompressorOut_bh72_24_24(0); -- cycle= 0 cp= 3.21048e-09
   heap_bh72_w9_12 <= CompressorOut_bh72_24_24(1); -- cycle= 0 cp= 3.21048e-09
   heap_bh72_w10_8 <= CompressorOut_bh72_24_24(2); -- cycle= 0 cp= 3.21048e-09

   ----------------Synchro barrier, entering cycle 0----------------
   CompressorIn_bh72_25_41 <= heap_bh72_w11_3 & heap_bh72_w11_5 & heap_bh72_w11_6;
   CompressorIn_bh72_25_42(0) <= heap_bh72_w12_5;
   Compressor_bh72_25: Compressor_13_3
      port map ( R => CompressorOut_bh72_25_25   ,
                 X0 => CompressorIn_bh72_25_41,
                 X1 => CompressorIn_bh72_25_42);
   heap_bh72_w11_7 <= CompressorOut_bh72_25_25(0); -- cycle= 0 cp= 3.21048e-09
   heap_bh72_w12_6 <= CompressorOut_bh72_25_25(1); -- cycle= 0 cp= 3.21048e-09
   heap_bh72_w13_3 <= CompressorOut_bh72_25_25(2); -- cycle= 0 cp= 3.21048e-09
   ----------------Synchro barrier, entering cycle 0----------------
   ----------------Synchro barrier, entering cycle 1----------------
   finalAdderIn0_bh72 <= "0" & heap_bh72_w15_0_d1 & heap_bh72_w14_0_d1 & heap_bh72_w13_2_d1 & heap_bh72_w12_6_d1 & heap_bh72_w11_7_d1 & heap_bh72_w10_7_d1 & heap_bh72_w9_12_d1 & heap_bh72_w8_11_d1 & heap_bh72_w7_12_d1 & heap_bh72_w6_13_d1 & heap_bh72_w5_10_d1 & heap_bh72_w4_13_d1 & heap_bh72_w3_12_d1 & heap_bh72_w2_9_d1 & heap_bh72_w1_6_d1 & heap_bh72_w0_0_d1;
   finalAdderIn1_bh72 <= "0" & '0' & heap_bh72_w14_1_d1 & heap_bh72_w13_3_d1 & '0' & '0' & heap_bh72_w10_8_d1 & '0' & '0' & '0' & '0' & heap_bh72_w5_13_d1 & '0' & heap_bh72_w3_11_d1 & '0' & heap_bh72_w1_5_d1 & heap_bh72_w0_5_d1;
   finalAdderCin_bh72 <= '0';
   Adder_final72_0: IntAdder_17_f210_uid166  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => finalAdderCin_bh72,
                 R => finalAdderOut_bh72   ,
                 X => finalAdderIn0_bh72,
                 Y => finalAdderIn1_bh72);
   -- concatenate all the compressed chunks
   CompressionResult72 <= finalAdderOut_bh72;
   -- End of code generated by BitHeap::generateCompressorVHDL
   R <= CompressionResult72(15 downto 4);
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_20_f210_uid176
--                     (IntAdderClassical_20_F210_uid178)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_20_f210_uid176 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(19 downto 0);
          Y : in  std_logic_vector(19 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(19 downto 0)   );
end entity;

architecture arch of IntAdder_20_f210_uid176 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                          IntAdder_24_f210_uid184
--                    (IntAdderAlternative_24_F210_uid188)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_24_f210_uid184 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(23 downto 0);
          Y : in  std_logic_vector(23 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(23 downto 0)   );
end entity;

architecture arch of IntAdder_24_f210_uid184 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Alternative
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                                   FPExp
--                           (FPExp_6_16_F210_uid2)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: F. de Dinechin, Bogdan Pasca (2008-2013)
--------------------------------------------------------------------------------
-- Pipeline depth: 6 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity FPExp is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(6+16+2 downto 0);
          R : out  std_logic_vector(6+16+2 downto 0)   );
end entity;

architecture arch of FPExp is
   component LeftShifter_17_by_max_24_F210_uid4 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(16 downto 0);
             S : in  std_logic_vector(4 downto 0);
             R : out  std_logic_vector(40 downto 0)   );
   end component;

   component FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(7 downto 0);
             R : out  std_logic_vector(5 downto 0)   );
   end component;

   component FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(5 downto 0);
             R : out  std_logic_vector(24 downto 0)   );
   end component;

   component IntAdder_19_f231_uid42 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(18 downto 0);
             Y : in  std_logic_vector(18 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(18 downto 0)   );
   end component;

   component MagicSPExpTable_F210_uid50 is
      port ( X1 : in  std_logic_vector(8 downto 0);
             Y1 : out  std_logic_vector(21 downto 0);
             X2 : in  std_logic_vector(8 downto 0);
             Y2 : out  std_logic_vector(21 downto 0)   );
   end component;

   component IntAdder_11_f210_uid54 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(10 downto 0);
             Y : in  std_logic_vector(10 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(10 downto 0)   );
   end component;

   component IntAdder_11_f210_uid62 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(10 downto 0);
             Y : in  std_logic_vector(10 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(10 downto 0)   );
   end component;

   component IntMultiplier_UsingDSP_10_11_12_unsigned_F210_uid70 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(9 downto 0);
             Y : in  std_logic_vector(10 downto 0);
             R : out  std_logic_vector(11 downto 0)   );
   end component;

   component IntAdder_20_f210_uid176 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(19 downto 0);
             Y : in  std_logic_vector(19 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(19 downto 0)   );
   end component;

   component IntAdder_24_f210_uid184 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(23 downto 0);
             Y : in  std_logic_vector(23 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(23 downto 0)   );
   end component;

signal Xexn, Xexn_d1, Xexn_d2, Xexn_d3, Xexn_d4, Xexn_d5, Xexn_d6 :  std_logic_vector(1 downto 0);
signal XSign, XSign_d1, XSign_d2, XSign_d3, XSign_d4, XSign_d5, XSign_d6 :  std_logic;
signal XexpField :  std_logic_vector(5 downto 0);
signal Xfrac :  std_logic_vector(15 downto 0);
signal e0 :  std_logic_vector(7 downto 0);
signal shiftVal :  std_logic_vector(7 downto 0);
signal resultWillBeOne, resultWillBeOne_d1 :  std_logic;
signal mXu :  std_logic_vector(16 downto 0);
signal oufl0, oufl0_d1, oufl0_d2, oufl0_d3, oufl0_d4, oufl0_d5, oufl0_d6 :  std_logic;
signal shiftValIn :  std_logic_vector(4 downto 0);
signal fixX0, fixX0_d1 :  std_logic_vector(40 downto 0);
signal fixX :  std_logic_vector(24 downto 0);
signal xMulIn :  std_logic_vector(7 downto 0);
signal absK :  std_logic_vector(5 downto 0);
signal minusAbsK :  std_logic_vector(6 downto 0);
signal K, K_d1, K_d2, K_d3, K_d4 :  std_logic_vector(6 downto 0);
signal absKLog2 :  std_logic_vector(24 downto 0);
signal subOp1 :  std_logic_vector(18 downto 0);
signal subOp2 :  std_logic_vector(18 downto 0);
signal Y :  std_logic_vector(18 downto 0);
signal Addr1 :  std_logic_vector(8 downto 0);
signal Z :  std_logic_vector(9 downto 0);
signal Addr2 :  std_logic_vector(8 downto 0);
signal expZ_output :  std_logic_vector(21 downto 0);
signal expA_output :  std_logic_vector(21 downto 0);
signal expA, expA_d1, expA_d2, expA_d3, expA_d4 :  std_logic_vector(19 downto 0);
signal expZmZm1 :  std_logic_vector(1 downto 0);
signal expZminus1X :  std_logic_vector(10 downto 0);
signal expZminus1Y :  std_logic_vector(10 downto 0);
signal expZminus1, expZminus1_d1 :  std_logic_vector(10 downto 0);
signal expArounded0 :  std_logic_vector(10 downto 0);
signal expArounded, expArounded_d1, expArounded_d2 :  std_logic_vector(9 downto 0);
signal lowerProduct, lowerProduct_d1 :  std_logic_vector(11 downto 0);
signal extendedLowerProduct :  std_logic_vector(19 downto 0);
signal expY :  std_logic_vector(19 downto 0);
signal needNoNorm :  std_logic;
signal preRoundBiasSig :  std_logic_vector(23 downto 0);
signal roundBit :  std_logic;
signal roundNormAddend :  std_logic_vector(23 downto 0);
signal roundedExpSigRes :  std_logic_vector(23 downto 0);
signal roundedExpSig, roundedExpSig_d1 :  std_logic_vector(23 downto 0);
signal ofl1 :  std_logic;
signal ofl2 :  std_logic;
signal ofl3 :  std_logic;
signal ofl :  std_logic;
signal ufl1 :  std_logic;
signal ufl2 :  std_logic;
signal ufl3 :  std_logic;
signal ufl :  std_logic;
signal Rexn :  std_logic_vector(1 downto 0);
constant g: positive := 3;
constant wE: positive := 6;
constant wF: positive := 16;
constant wFIn: positive := 16;
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            Xexn_d1 <=  Xexn;
            Xexn_d2 <=  Xexn_d1;
            Xexn_d3 <=  Xexn_d2;
            Xexn_d4 <=  Xexn_d3;
            Xexn_d5 <=  Xexn_d4;
            Xexn_d6 <=  Xexn_d5;
            XSign_d1 <=  XSign;
            XSign_d2 <=  XSign_d1;
            XSign_d3 <=  XSign_d2;
            XSign_d4 <=  XSign_d3;
            XSign_d5 <=  XSign_d4;
            XSign_d6 <=  XSign_d5;
            resultWillBeOne_d1 <=  resultWillBeOne;
            oufl0_d1 <=  oufl0;
            oufl0_d2 <=  oufl0_d1;
            oufl0_d3 <=  oufl0_d2;
            oufl0_d4 <=  oufl0_d3;
            oufl0_d5 <=  oufl0_d4;
            oufl0_d6 <=  oufl0_d5;
            fixX0_d1 <=  fixX0;
            K_d1 <=  K;
            K_d2 <=  K_d1;
            K_d3 <=  K_d2;
            K_d4 <=  K_d3;
            expA_d1 <=  expA;
            expA_d2 <=  expA_d1;
            expA_d3 <=  expA_d2;
            expA_d4 <=  expA_d3;
            expZminus1_d1 <=  expZminus1;
            expArounded_d1 <=  expArounded;
            expArounded_d2 <=  expArounded_d1;
            lowerProduct_d1 <=  lowerProduct;
            roundedExpSig_d1 <=  roundedExpSig;
         end if;
      end process;
   Xexn <= X(wE+wFIn+2 downto wE+wFIn+1);
   XSign <= X(wE+wFIn);
   XexpField <= X(wE+wFIn-1 downto wFIn);
   Xfrac <= X(wFIn-1 downto 0);
   e0 <= conv_std_logic_vector(12, wE+2);  -- bias - (wF+g)
   shiftVal <= ("00" & XexpField) - e0; -- for a left shift
   -- underflow when input is shifted to zero (shiftval<0), in which case exp = 1
   resultWillBeOne <= shiftVal(wE+1);
   --  mantissa with implicit bit
   mXu <= "1" & Xfrac;
   -- Partial overflow/underflow detection
   oufl0 <= not shiftVal(wE+1) when shiftVal(wE downto 0) >= conv_std_logic_vector(24, wE+1) else '0';
   ---------------- cycle 0----------------
   shiftValIn <= shiftVal(4 downto 0);
   mantissa_shift: LeftShifter_17_by_max_24_F210_uid4  -- pipelineDepth=0 maxInDelay=2.32568e-09
      port map ( clk  => clk,
                 rst  => rst,
                 R => fixX0,
                 S => shiftValIn,
                 X => mXu);
   ----------------Synchro barrier, entering cycle 1----------------
   fixX <=  fixX0_d1(40 downto 16)when resultWillBeOne_d1='0' else "0000000000000000000000000";
   xMulIn <=  fixX(23 downto 16); -- truncation, error 2^-3
   mulInvLog2: FixRealKCM_4_M3_0_1_log_2_unsigned_F210_uid8  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absK,
                 X => xMulIn);
   minusAbsK <= (6 downto 0 => '0') - ('0' & absK);
   K <= minusAbsK when  XSign_d1='1'   else ('0' & absK);
   ---------------- cycle 1----------------
   mulLog2: FixRealKCM_5_0_M19_log_2_unsigned_F210_uid34  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 R => absKLog2,
                 X => absK);
   subOp1 <= fixX(18 downto 0) when XSign_d1='0' else not (fixX(18 downto 0));
   subOp2 <= absKLog2(18 downto 0) when XSign_d1='1' else not (absKLog2(18 downto 0));
   theYAdder: IntAdder_19_f231_uid42  -- pipelineDepth=0 maxInDelay=2.75916e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '1',
                 R => Y,
                 X => subOp1,
                 Y => subOp2);

   -- Now compute the exp of this fixed-point value
   Addr1 <= Y(18 downto 10);
   Z <= Y(9 downto 0);
   Addr2 <= Z(9 downto 1);
   table: MagicSPExpTable_F210_uid50
      port map ( X1 => Addr1,
                 X2 => Addr2,
                 Y1 => expA_output,
                 Y2 => expZ_output);
   expA <=  expA_output(21 downto 2);
   expZmZm1 <= expZ_output(1 downto 0);
   -- Computing Z + (exp(Z)-1-Z)
   expZminus1X <= '0' & Z;
   expZminus1Y <= (10 downto 2 => '0') & expZmZm1 ;
   Adder_expZminus1: IntAdder_11_f210_uid54  -- pipelineDepth=1 maxInDelay=4.30888e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '0' ,
                 R => expZminus1,
                 X => expZminus1X,
                 Y => expZminus1Y);
   ----------------Synchro barrier, entering cycle 2----------------
   ---------------- cycle 1----------------
   -- Rounding expA to the same accuracy as expZminus1
   --   (truncation would not be accurate enough and require one more guard bit)
   Adder_expArounded0: IntAdder_11_f210_uid62  -- pipelineDepth=0 maxInDelay=2.186e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin =>  '1' ,
                 R => expArounded0,
                 X => expA(19 downto 9),
                 Y => "00000000000");
   expArounded <= expArounded0(10 downto 1);
   ----------------Synchro barrier, entering cycle 2----------------
   ----------------Synchro barrier, entering cycle 3----------------
   TheLowerProduct: IntMultiplier_UsingDSP_10_11_12_unsigned_F210_uid70  -- pipelineDepth=1 maxInDelay=4.36e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => lowerProduct,
                 X => expArounded_d2,
                 Y => expZminus1_d1);

   ----------------Synchro barrier, entering cycle 4----------------
   ----------------Synchro barrier, entering cycle 5----------------
   extendedLowerProduct <= ((19 downto 12 => '0') & lowerProduct_d1(11 downto 0));
   -- Final addition -- the product MSB bit weight is -k+2 = -7
   TheFinalAdder: IntAdder_20_f210_uid176  -- pipelineDepth=0 maxInDelay=4.4472e-10
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => expY,
                 X => expA_d4,
                 Y => extendedLowerProduct);

   needNoNorm <= expY(19);
   -- Rounding: all this should consume one row of LUTs
   preRoundBiasSig <= conv_std_logic_vector(31, wE+2)  & expY(18 downto 3) when needNoNorm = '1'
      else conv_std_logic_vector(30, wE+2)  & expY(17 downto 2) ;
   roundBit <= expY(2)  when needNoNorm = '1'    else expY(1) ;
   roundNormAddend <= K_d4(6) & K_d4 & (15 downto 1 => '0') & roundBit;
   roundedExpSigOperandAdder: IntAdder_24_f210_uid184  -- pipelineDepth=0 maxInDelay=2.304e-09
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => '0',
                 R => roundedExpSigRes,
                 X => preRoundBiasSig,
                 Y => roundNormAddend);

   -- delay at adder output is 3.524e-09
   roundedExpSig <= roundedExpSigRes when Xexn_d5="01" else  "000" & (wE-2 downto 0 => '1') & (wF-1 downto 0 => '0');
   ----------------Synchro barrier, entering cycle 6----------------
   ofl1 <= not XSign_d6 and oufl0_d6 and (not Xexn_d6(1) and Xexn_d6(0)); -- input positive, normal,  very large
   ofl2 <= not XSign_d6 and (roundedExpSig_d1(wE+wF) and not roundedExpSig_d1(wE+wF+1)) and (not Xexn_d6(1) and Xexn_d6(0)); -- input positive, normal, overflowed
   ofl3 <= not XSign_d6 and Xexn_d6(1) and not Xexn_d6(0);  -- input was -infty
   ofl <= ofl1 or ofl2 or ofl3;
   ufl1 <= (roundedExpSig_d1(wE+wF) and roundedExpSig_d1(wE+wF+1))  and (not Xexn_d6(1) and Xexn_d6(0)); -- input normal
   ufl2 <= XSign_d6 and Xexn_d6(1) and not Xexn_d6(0);  -- input was -infty
   ufl3 <= XSign_d6 and oufl0_d6  and (not Xexn_d6(1) and Xexn_d6(0)); -- input negative, normal,  very large
   ufl <= ufl1 or ufl2 or ufl3;
   Rexn <= "11" when Xexn_d6 = "11"
      else "10" when ofl='1'
      else "00" when ufl='1'
      else "01";
   R <= Rexn & '0' & roundedExpSig_d1(21 downto 0);
end architecture;

