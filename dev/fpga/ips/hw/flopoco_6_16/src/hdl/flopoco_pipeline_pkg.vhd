
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: flopoco_pipeline_pkg
-- Description: Constants for flopoco operators pipeline length
--
-- Last generated on: 2021/06/01 19:04:31
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package flopoco_pipeline_pkg is

    constant c_wE : natural := 6;
    constant c_wF : natural := 16;

    constant c_FPAdd_PIPELINE_LENGTH : natural := 2;

    constant c_FPSub_PIPELINE_LENGTH : natural := 2;

    constant c_FPMult_PIPELINE_LENGTH : natural := 1;

    constant c_FPMultNoDSP_PIPELINE_LENGTH : natural := 1;

    constant c_FPDiv_PIPELINE_LENGTH : natural := 6;

    constant c_FPExp_PIPELINE_LENGTH : natural := 6;

    constant c_FPSqrt_PIPELINE_LENGTH : natural := 5;

    constant c_InputIEEE_PIPELINE_LENGTH : natural := 0;

    constant c_OutputIEEE_PIPELINE_LENGTH : natural := 0;

    constant c_Signed8_2FP_PIPELINE_LENGTH : natural := 0;

    constant c_Unsigned8_2FP_PIPELINE_LENGTH : natural := 0;

    constant c_Signed10_2FP_PIPELINE_LENGTH : natural := 0;

    constant c_Unsigned10_2FP_PIPELINE_LENGTH : natural := 0;

end flopoco_pipeline_pkg;

