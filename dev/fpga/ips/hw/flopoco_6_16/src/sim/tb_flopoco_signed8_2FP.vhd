----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_flopoco_signed8_2fp - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for Flopoco Signed8_2fp
--
-- Last update: 2021/04/12 13:57:51
--
---------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_flopoco_signed8_2fp is
    generic (
        CLK_PERIOD : time := 8 ns -- clock period for clk
    );
end entity;

architecture behavioral of tb_flopoco_signed8_2fp is

    function slv_to_string (a : std_logic_vector) return string is
        variable b                : string (1 to a'length) := (others => NUL);
        variable stri             : integer                := 1;
    begin
        for i in a'range loop
            b(stri) := std_logic'image(a((i)))(2);
            stri    := stri + 1;
        end loop;
        return b;
    end function;

    ----------------------------------------------------------------------------
    -- Constants
    ----------------------------------------------------------------------------
    -- clock period
    constant PCLK_PERIOD : time := CLK_PERIOD; -- parallel high-speed clock

    ----------------------------------------------------------------------------
    -- Signals for internal operation
    ----------------------------------------------------------------------------

    --- clock signals ---
    signal clk_sti : std_logic := '1';
    signal rst_sti : std_logic := '1';

    --- DUT input ---
    signal I_sti : signed(7 downto 0);

    --- DUT output ---
    signal O_obs : scalar_t;

    ----------------------------------------------------------------------------
    -- DUT component declaration
    ----------------------------------------------------------------------------

    component Signed8_2FP is
        port (
            clk : in std_logic;
            rst : in std_logic;
            I   : in std_logic_vector(7 downto 0);
            O   : out std_logic_vector(8 + 23 + 2 downto 0)
        );
    end component;

begin

    ----------------------------------------------------------------------------
    -- DUT instantiation
    ----------------------------------------------------------------------------

    DUT_i : Signed8_2FP
    port map(
        clk => clk_sti,
        rst => rst_sti,
        I   => std_logic_vector(I_sti),
        O   => O_obs
    );

    ----------------------------------------------------------------------------
    -- CLOCK GENERATION
    ----------------------------------------------------------------------------
    clk_proc : process is
    begin
        loop
            clk_sti <= not clk_sti;
            wait for PCLK_PERIOD / 2;
        end loop;
    end process clk_proc;

    ----------------------------------------------------------------------------
    -- TEST BENCH STIMULI
    ----------------------------------------------------------------------------
    tb1 : process is
        constant c_PREFIX : string := " | --> ";

        procedure drive_input(I : signed) is
        begin
            I_sti <= I;
            wait until rising_edge(clk_sti);
            wait until rising_edge(clk_sti);
            logger.log_note(c_PREFIX & "Input: " & integer'image(to_integer(I_sti)) & " (" & slv_to_string(std_logic_vector(I_sti)) & ")" & ", output: " & to_string(O_obs));
        end procedure;
        variable cycle_count : natural := 0;
        variable test_count  : natural := 0;

        variable I : signed(I_sti'range);
    begin
        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_report.html",
        "Testbench report: " & "Signed8_2FP",
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("wE: " & integer'image(wE));
        logger.log_note("wF: " & integer'image(wF));

        ------------------------------------------------------------------------
        -- Reset and vectors setup
        --

        rst_sti <= '0';
        I_sti   <= (others => '0');
        wait until rising_edge(clk_sti);

        ------------------------------------------------------------------------
        logger.log_note("Reset");
        rst_sti <= '1';
        wait until rising_edge(clk_sti);
        rst_sti <= '0';
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        if true then
            test_count := test_count + 1;
            logger.log_note("Limit cases:");
            I := (I_sti'left => '1', others => '0');
            drive_input(I);
            I := (others => '1');
            drive_input(I);
            I := (others => '0');
            drive_input(I);
            I := (I_sti'left => '0', others => '1');
            drive_input(I);
            logger.log_note("Hand-picked cases:");
            I := to_signed(1, I_sti'length);
            drive_input(I);
            I := to_signed(2, I_sti'length);
            drive_input(I);
            I := to_signed(-1, I_sti'length);
            drive_input(I);
            I := to_signed(-2, I_sti'length);
            drive_input(I);
            I := to_signed(42, I_sti'length);
            drive_input(I);
            I := to_signed(-42, I_sti'length);
            drive_input(I);
            I := to_signed(127, I_sti'length);
            drive_input(I);
            I := to_signed(-127, I_sti'length);
            drive_input(I);
        end if;
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        for I in 1 to 10 loop
            wait until rising_edge(clk_sti);
        end loop;
        logger.final_report;
        stop(0);

    end process tb1;
    --  End Test Bench

end behavioral;
