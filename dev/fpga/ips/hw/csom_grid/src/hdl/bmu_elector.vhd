----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: csom_grid - bmu_elector
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: csom_grid
--
-- Last update: 2021/11/16 13:27:38
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;

entity bmu_elector is
    port (
        clk_i               : in std_logic;
        rst_i               : in std_logic;
        x_valid_i           : in std_logic;
        grid_ready_i        : in std_logic;
        bmu_elector_ready_o : out std_logic;
        dist_to_x_i         : in t_scalar_dim3; -- Matrice of distances of each unit to the current input
        is_bmu_ack_i        : in t_slv_dim3;    -- Acknowledge that weight have been updated as BMU
        bmu_dist_o          : out scalar_t;     -- Distance of the elected BMU to the current input
        is_bmu_o            : out t_slv_dim3;   -- Notification that the corresponding BMU for current input
        bmu_addr_o          : out t_grid_addr;
        bmu_addr_valid_o    : out std_logic
    );
end bmu_elector;

architecture arch of bmu_elector is

    signal is_bmu_s     : t_slv_dim3;
    signal bmu_addr_s   : t_grid_addr;
    signal bmu_addr_reg : t_grid_addr;

    -----------------------------------------------------------------------------
    -- FSM
    type bmu_elector_state_t is (
        s_IDLE,
        s_WAIT_DISTANCE_COMP,
        s_PULSE_VALID
    );
    signal current_bmu_elector_state : bmu_elector_state_t;
    signal next_bmu_elector_state    : bmu_elector_state_t;
begin

    p_matrix_min : process (all)
        variable dist_to_x : t_scalar_dim3;
        variable is_bmu    : t_slv_dim3;
        variable dist_min  : scalar_t;
        variable l, r      : scalar_t;
        variable x         : t_grid_addr_comp;
        variable y         : t_grid_addr_comp;
        variable z         : t_grid_addr_comp;
        variable x_smaller : t_grid_addr_comp;
        variable y_smaller : t_grid_addr_comp;
        variable z_smaller : t_grid_addr_comp;
    begin
        dist_to_x := dist_to_x_i;
        for idx in 1 to ((c_GRID_X_DIM * c_GRID_Y_DIM * c_GRID_Z_DIM) - 1) loop
            if idx = 1 then
                linear_to_3d(idx - 1, x, y, z);
                --assert false report "idx " & integer'image(idx - 1) & " : (" & integer'image(x) & "," & integer'image(y) & ", " & integer'image(z) & ")"severity note;
                --l := dist_to_x(z)(y)(x);
                l := dist_to_x(x)(y)(z);
                linear_to_3d(idx, x, y, z);
                --assert false report "idx " & integer'image(idx) & " : (" & integer'image(x) & "," & integer'image(y) & ", " & integer'image(z) & ")"severity note;
                r := dist_to_x(x)(y)(z);
                if scalar_lt(l, r) then
                    dist_min  := l;
                    x_smaller := 0;
                    y_smaller := 0;
                    z_smaller := 0;
                else
                    dist_min  := r;
                    x_smaller := x;
                    y_smaller := y;
                    z_smaller := z;
                end if;
            else
                l := dist_min;
                linear_to_3d(idx, x, y, z);
                --assert false report "idx " & integer'image(idx) & " : (" & integer'image(x) & "," & integer'image(y) & ", " & integer'image(z) & ")"severity note;
                r := dist_to_x(x)(y)(z);
                if scalar_lt(l, r) then
                    dist_min := l;
                else
                    dist_min  := r;
                    x_smaller := x;
                    y_smaller := y;
                    z_smaller := z;
                end if;
            end if;
            --assert false report " = " & real'image(to_real(dist_min)) severity note;
        end loop;
        is_bmu                                  := (others => (others => (others => '0')));
        is_bmu(x_smaller)(y_smaller)(z_smaller) := '1';
        is_bmu_s   <= is_bmu;
        bmu_dist_o <= dist_min; -- not registered
        bmu_addr_s <= (x => x_smaller, y => y_smaller, z => z_smaller);
    end process p_matrix_min;

    b_bmu_elector_fsm          : block
        signal bmu_addr_reg_update : std_logic;
    begin

        p_bmu_elector_fsm_reg : process (clk_i, rst_i)
        begin
            if rising_edge(clk_i) then
                if rst_i = '1' then
                    current_bmu_elector_state <= s_IDLE;
                else
                    current_bmu_elector_state <= next_bmu_elector_state;
                end if;
            end if;
        end process p_bmu_elector_fsm_reg;

        p_bmu_addr_reg : process (clk_i, rst_i)
        begin
            if rising_edge(clk_i) then
                if rst_i = '1' then
                    bmu_addr_reg <= (others => 0);
                else
                    bmu_addr_reg <= bmu_addr_reg;
                    if bmu_addr_reg_update = '1' then
                        bmu_addr_reg <= bmu_addr_s;
                    end if;
                end if;
            end if;
        end process p_bmu_addr_reg;

        p_bmu_elector_fsm_comb : process (all)
        begin
            next_bmu_elector_state <= current_bmu_elector_state;
            is_bmu_o               <= (others => (others => (others => '0')));
            bmu_elector_ready_o    <= '0';
            bmu_addr_valid_o       <= '0';
            bmu_addr_reg_update    <= '0';
            case current_bmu_elector_state is

                when s_IDLE =>
                    -------------------------------------------------------------
                    -- IDLE
                    bmu_elector_ready_o <= '1';
                    if x_valid_i = '1' then
                        next_bmu_elector_state <= s_WAIT_DISTANCE_COMP;
                    end if;

                when s_WAIT_DISTANCE_COMP =>
                    -------------------------------------------------------------
                    -- Distance computation
                    if grid_ready_i = '1' then
                        is_bmu_o               <= is_bmu_s;
                        bmu_addr_reg_update    <= '1';
                        next_bmu_elector_state <= s_PULSE_VALID;
                    end if;

                when s_PULSE_VALID =>
                    bmu_addr_valid_o       <= '1';
                    next_bmu_elector_state <= s_IDLE;

            end case;
        end process p_bmu_elector_fsm_comb;

        bmu_addr_o <= bmu_addr_reg;

    end block b_bmu_elector_fsm;

end arch;