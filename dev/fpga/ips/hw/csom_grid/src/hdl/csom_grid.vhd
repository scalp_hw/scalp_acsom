----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: csom_grid - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: csom_grid
--
-- Last update: 2021/11/16 13:31:07
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;

entity csom_grid is
    port (
        clk_i            : in std_logic;
        rst_i            : in std_logic;
        addr_select_i    : in t_grid_addr;
        alpha_i          : in scalar_t;
        eta_prime_i      : in scalar_t;
        vec_i            : in vec_t;
        vec_o            : out vec_t;
        bmu_addr_o       : out t_grid_addr;
        bmu_addr_valid_o : out std_logic;
        x_valid_i        : in std_logic;
        m_valid_i        : in std_logic;
        init_weight_i    : in std_logic;
        grid_ready_o     : out std_logic

    );
end csom_grid;

architecture arch of csom_grid is

    component csom_unit is
        port (
            clk_i             : in std_logic;
            rst_i             : in std_logic;
            addr_i            : in t_grid_addr; -- Unit grid address
            ready_o           : out std_logic;  -- Unit ready
            vec_i             : in vec_t;       -- Input vector, content depend on one of the valid flags
            x_valid_i         : in std_logic;   -- vec_i contains a new input vector and is valid
            m_valid_i         : in std_logic;   -- vec_i contains a weight and is valid
            nm_valid_i        : in std_logic;   -- vec_i contains a neighbor weight and is valid
            dist_to_x_o       : out scalar_t;   -- Output distance to current input
            dist_to_x_valid_o : out std_logic;  -- Distance to current input valid
            alpha_i           : in scalar_t;    -- Current learning rate
            eta_prime_i       : in scalar_t;    -- Current elasticity
            is_bmu_i          : in std_logic;   -- Notification that this unit is the BMU for current input
            is_bmu_ack_o      : out std_logic;  -- Acknowledge that weight have been updated as BMU
            bmu_addr_i        : in t_grid_addr; -- Grid address of BMU
            init_weight_i     : in std_logic;   -- Unused
            vec_o             : out vec_t       -- Output vector, currently a copy of internal weight
        );
    end component;

    component bmu_elector is
        port (
            clk_i               : in std_logic;
            rst_i               : in std_logic;
            x_valid_i           : in std_logic;
            grid_ready_i        : in std_logic;
            bmu_elector_ready_o : out std_logic;
            dist_to_x_i         : in t_scalar_dim3; -- Matrice of distances of each unit to the current input
            is_bmu_ack_i        : in t_slv_dim3;    -- Acknowledge that weight have been updated as BMU
            bmu_dist_o          : out scalar_t;     -- Distance of the elected BMU to the current input
            is_bmu_o            : out t_slv_dim3;   -- Notification that the corresponding BMU for current input
            bmu_addr_o          : out t_grid_addr;
            bmu_addr_valid_o    : out std_logic
        );
    end component bmu_elector;

    signal vec_in_s                  : t_vec_dim3;
    signal vec_out_s                 : t_vec_dim3;
    signal ready_xyz_s               : t_slv_dim3;
    signal dist_to_x_xyz_s           : t_scalar_dim3; -- Output distance to current input
    signal m_valid_xyz_s             : t_slv_dim3;    -- Distance to current input valid
    signal is_bmu_xyz_s              : t_slv_dim3;    -- Notification that this unit is the BMU for current input
    signal is_bmu_ack_xyz_s          : t_slv_dim3;    --
    signal bmu_addr_s                : t_grid_addr;
    signal grid_ready_s              : std_logic;
    signal update_signal_to_neighbor : t_dir_dim3;
    signal bmu_elector_ready_s       : std_logic;
    signal bmu_addr_valid_s          : std_logic;

begin

    ----------------------------------------------------------------------------
    -- Units instantiation
    ----------------------------------------------------------------------------

    gen_unit_x : for x_idx in 0 to (c_GRID_X_DIM - 1) generate

        gen_unit_y : for y_idx in 0 to (c_GRID_Y_DIM - 1) generate

            gen_unit_z : for z_idx in 0 to (c_GRID_Z_DIM - 1) generate

                b_gen_unit                          : block
                    signal local_vec_s                  : vec_t;            -- local unit vector input
                    signal nm_valid                     : std_logic;        --
                    signal unit_addr                    : t_grid_addr;      -- locad unit address
                    signal diff_type_reg                : t_diffusion_type; -- register the next diffusion algorithm to use
                    signal update_diff_type             : std_logic;        -- trig the update of diffusion algorithm
                    signal update_signal_from_neighbor  : t_direction;      -- update signal from neighbor, built from update_signal_to_neighbor
                    signal update_requested_by_neighbor : std_logic;        -- or reduction of update_signal_from_neighbor
                    signal update_dir_reg               : t_direction;      -- register the incoming update request direction
                    signal local_unit_ready_s           : std_logic;        -- local unit ready
                    -----------------------------------------------------------------------------
                    -- FSM
                    type update_router_state_t is (
                        s_IDLE,
                        s_WAIT_BMU_UPDATE,
                        s_PROPAGATE_AS_BMU,
                        s_UDPATE_FROM_NEIGHBOR,
                        s_WAIT_UDPATE_FROM_NEIGHBOR,
                        s_PROPAGATE,
                        s_UPDATE_ROUTING_ALG
                    );
                    signal current_update_router_state : update_router_state_t;
                    signal next_update_router_state    : update_router_state_t;
                begin

                    ----------------------------------------------------------------------------
                    -- Unit instance XYZ
                    ----------------------------------------------------------------------------

                    unit_addr <= (x => x_idx, y => y_idx, z => z_idx);

                    i_csom_unit_xyz : csom_unit
                    port map(
                        clk_i             => clk_i,
                        rst_i             => rst_i,
                        addr_i            => unit_addr,
                        ready_o           => local_unit_ready_s,                    -- Unit is ready
                        vec_i             => local_vec_s,                           -- Input vector
                        x_valid_i         => x_valid_i,                             -- vec_i contains a new input vector and is valid
                        m_valid_i         => m_valid_xyz_s(x_idx)(y_idx)(z_idx),    -- vec_i contains a weight and is valid
                        nm_valid_i        => nm_valid,                              -- vec_i contains a neighbor weight and is valid
                        dist_to_x_o       => dist_to_x_xyz_s(x_idx)(y_idx)(z_idx),  -- Output distance to current input
                        dist_to_x_valid_o => open,                                  -- Distance to current input valid
                        alpha_i           => alpha_i,                               -- Current learning rate
                        eta_prime_i       => eta_prime_i,                           -- Elasticity
                        is_bmu_i          => is_bmu_xyz_s(x_idx)(y_idx)(z_idx),     -- Notification that this unit is the BMU for current input
                        is_bmu_ack_o      => is_bmu_ack_xyz_s(x_idx)(y_idx)(z_idx), --
                        bmu_addr_i        => bmu_addr_s,                            -- BMU grid address
                        init_weight_i     => init_weight_i,                         -- Unused
                        vec_o             => vec_out_s(x_idx)(y_idx)(z_idx)
                    );

                    ----------------------------------------------------------------------------
                    -- Unit XYZ input vector muxing
                    ----------------------------------------------------------------------------

                    p_update_signal_routing : process (all)
                    begin

                        -- Our EAST connected to WEST of unit x + 1 (if any)
                        if x_idx < (c_GRID_X_DIM - 1) then
                            update_signal_from_neighbor(0) <= update_signal_to_neighbor(x_idx + 1)(y_idx)(z_idx)(1);
                        else
                            update_signal_from_neighbor(0) <= '0';
                        end if;

                        -- Our WEST connected to EAST of unit x - 1 (if any)
                        if x_idx > 0 then
                            update_signal_from_neighbor(1) <= update_signal_to_neighbor(x_idx - 1)(y_idx)(z_idx)(0);
                        else
                            update_signal_from_neighbor(1) <= '0';
                        end if;

                        -- Our NORTH connected to SOUTH of unit y + 1 (if any)
                        if y_idx < (c_GRID_Y_DIM - 1) then
                            update_signal_from_neighbor(2) <= update_signal_to_neighbor(x_idx)(y_idx + 1)(z_idx)(3);
                        else
                            update_signal_from_neighbor(2) <= '0';
                        end if;

                        -- Our SOUTH connected to NORTH of unit y - 1 (if any)
                        if y_idx > 0 then
                            update_signal_from_neighbor(3) <= update_signal_to_neighbor(x_idx)(y_idx - 1)(z_idx)(2);
                        else
                            update_signal_from_neighbor(3) <= '0';
                        end if;

                        -- Our UP connected to DOWN of unit z + 1 (if any)
                        if z_idx < (c_GRID_Z_DIM - 1) then
                            update_signal_from_neighbor(4) <= update_signal_to_neighbor(x_idx)(y_idx)(z_idx + 1)(5);
                        else
                            update_signal_from_neighbor(4) <= '0';
                        end if;

                        -- Our DWON connected to UP of unit z - 1 (if any)
                        if z_idx > 0 then
                            update_signal_from_neighbor(5) <= update_signal_to_neighbor(x_idx)(y_idx)(z_idx - 1)(4);
                        else
                            update_signal_from_neighbor(5) <= '0';
                        end if;

                        update_requested_by_neighbor <=
                            update_signal_from_neighbor(0) or
                            update_signal_from_neighbor(1) or
                            update_signal_from_neighbor(2) or
                            update_signal_from_neighbor(3) or
                            update_signal_from_neighbor(4) or
                            update_signal_from_neighbor(5);

                    end process p_update_signal_routing;

                    ----------------------------------------------------------------------------
                    -- Update direction register
                    ----------------------------------------------------------------------------

                    p_update_dir_reg : process (clk_i, rst_i)
                    begin
                        if rising_edge(clk_i) then
                            if rst_i = '1' then
                                update_dir_reg <= (others => '0');
                            else
                                update_dir_reg <= update_dir_reg;
                                if update_requested_by_neighbor = '1' then
                                    update_dir_reg <= update_signal_from_neighbor;
                                end if;
                            end if;
                        end if;
                    end process p_update_dir_reg;

                    ----------------------------------------------------------------------------
                    -- Unit XYZ input vector muxing
                    ----------------------------------------------------------------------------

                    p_local_vec_mux : process (current_update_router_state, vec_in_s, update_dir_reg, vec_out_s)
                    begin
                        case current_update_router_state is
                            when s_UDPATE_FROM_NEIGHBOR | s_WAIT_UDPATE_FROM_NEIGHBOR =>
                                -- EAST - WEST
                                if (update_dir_reg = c_DIR_EAST) and (x_idx < (c_GRID_X_DIM - 1)) then
                                    local_vec_s <= vec_out_s(x_idx + 1)(y_idx)(z_idx);
                                elsif (update_dir_reg = c_DIR_WEST) and (x_idx > 0) then
                                    local_vec_s <= vec_out_s(x_idx - 1)(y_idx)(z_idx);
                                    -- NORTH - SOUTH
                                elsif (update_dir_reg = c_DIR_NORTH) and (y_idx < (c_GRID_Y_DIM - 1)) then
                                    local_vec_s <= vec_out_s(x_idx)(y_idx + 1)(z_idx);
                                elsif (update_dir_reg = c_DIR_SOUTH) and (y_idx > 0) then
                                    local_vec_s <= vec_out_s(x_idx)(y_idx - 1)(z_idx);
                                    -- UP - DOWN
                                elsif (update_dir_reg = c_DIR_UP) and z_idx < (c_GRID_Z_DIM - 1) then
                                    local_vec_s <= vec_out_s(x_idx)(y_idx)(z_idx + 1);
                                elsif (update_dir_reg = c_DIR_DOWN) and (z_idx > 0) then
                                    local_vec_s <= vec_out_s(x_idx)(y_idx)(z_idx - 1);

                                else
                                    local_vec_s <= vec_in_s(x_idx)(y_idx)(z_idx);
                                end if;
                            when others =>
                                local_vec_s <= vec_in_s(x_idx)(y_idx)(z_idx);
                        end case;
                    end process p_local_vec_mux;

                    ----------------------------------------------------------------------------
                    -- Unit XYZ update routing FSM
                    ----------------------------------------------------------------------------

                    p_update_router_fsm_reg : process (clk_i, rst_i)
                    begin
                        if rising_edge(clk_i) then
                            if rst_i = '1' then
                                current_update_router_state <= s_IDLE;
                            else
                                current_update_router_state <= next_update_router_state;
                            end if;
                        end if;
                    end process p_update_router_fsm_reg;

                    p_update_router_fsm_comb : process (all)
                    begin
                        next_update_router_state                       <= current_update_router_state;
                        update_signal_to_neighbor(x_idx)(y_idx)(z_idx) <= (others => '0');
                        ready_xyz_s(x_idx)(y_idx)(z_idx)               <= '0';
                        update_diff_type                               <= '0';
                        nm_valid                                       <= '0';
                        case current_update_router_state is

                            when s_IDLE =>
                                -------------------------------------------------------------
                                -- IDLE

                                ready_xyz_s(x_idx)(y_idx)(z_idx) <= local_unit_ready_s;

                                -- If local unit is elected as BMU, wait update
                                -- and initiate propagation as BMU.
                                if is_bmu_xyz_s(x_idx)(y_idx)(z_idx) = '1' then
                                    next_update_router_state <= s_WAIT_BMU_UPDATE;
                                end if;

                                -- If one of our neighboor signal an update request,
                                -- initiate update and perform update diffusion.
                                if update_requested_by_neighbor = '1' then
                                    next_update_router_state <= s_UDPATE_FROM_NEIGHBOR;
                                end if;

                            when s_WAIT_BMU_UPDATE =>
                                -------------------------------------------------------------
                                -- Wait local unit update after BMU notification.
                                if local_unit_ready_s = '1' then
                                    next_update_router_state <= s_PROPAGATE_AS_BMU;
                                end if;

                            when s_PROPAGATE_AS_BMU =>
                                -------------------------------------------------------------
                                -- Diffuse update request to all neighbors.
                                -- send update signal to all neighbor
                                update_signal_to_neighbor(x_idx)(y_idx)(z_idx) <= c_DIR_ALL;
                                next_update_router_state                       <= s_UPDATE_ROUTING_ALG;

                            when s_UDPATE_FROM_NEIGHBOR =>
                                -------------------------------------------------------------
                                -- Trig local unit update.
                                -- TODO, this state may be useless if we can perform nm_valid
                                -- pulse from IDLE, but first check that the correct routing
                                -- is already established.
                                nm_valid                 <= '1';
                                next_update_router_state <= s_WAIT_UDPATE_FROM_NEIGHBOR;

                            when s_WAIT_UDPATE_FROM_NEIGHBOR =>
                                -------------------------------------------------------------
                                -- Wait local unit update after update notification.
                                if local_unit_ready_s = '1' then
                                    next_update_router_state <= s_PROPAGATE;
                                end if;

                            when s_PROPAGATE =>
                                -------------------------------------------------------------
                                -- Continue update diffusion as a standard unit.
                                update_signal_to_neighbor(x_idx)(y_idx)(z_idx) <= get_forwarding_directions(diff_type_reg, update_dir_reg);
                                next_update_router_state                       <= s_UPDATE_ROUTING_ALG;

                            when s_UPDATE_ROUTING_ALG =>
                                -------------------------------------------------------------
                                -- Trig update of diffusion type
                                update_diff_type         <= '1';
                                next_update_router_state <= s_IDLE;

                        end case;
                    end process p_update_router_fsm_comb;

                    ----------------------------------------------------------------------------
                    -- Unit XYZ diffusion type register
                    ----------------------------------------------------------------------------

                    p_diff_type_reg : process (all)
                    begin
                        if rising_edge(clk_i) then
                            if rst_i = '1' then
                                diff_type_reg <= DIFF_XYZ;
                            else
                                diff_type_reg <= diff_type_reg;
                                if update_diff_type = '1' then
                                    diff_type_reg <= get_next_diff_mecha(diff_type_reg);
                                end if;
                            end if;
                        end if;
                    end process p_diff_type_reg;

                end block b_gen_unit;

            end generate gen_unit_z;

        end generate gen_unit_y;

    end generate gen_unit_x;

    ----------------------------------------------------------------------------
    -- BMU elector instantiation
    ----------------------------------------------------------------------------

    i_bmu_elector : bmu_elector
    port map(
        clk_i               => clk_i,
        rst_i               => rst_i,
        x_valid_i           => x_valid_i,
        grid_ready_i        => grid_ready_s,
        bmu_elector_ready_o => bmu_elector_ready_s,
        dist_to_x_i         => dist_to_x_xyz_s,
        is_bmu_o            => is_bmu_xyz_s,
        is_bmu_ack_i        => is_bmu_ack_xyz_s,
        bmu_dist_o          => open,
        bmu_addr_o          => bmu_addr_s,
        bmu_addr_valid_o    => bmu_addr_valid_s
    );

    bmu_addr_o       <= bmu_addr_s;
    bmu_addr_valid_o <= bmu_addr_valid_s;

    ----------------------------------------------------------------------------
    -- And reduce all the ready signal
    ----------------------------------------------------------------------------

    grid_ready_s <= and_reduce(ready_xyz_s);
    grid_ready_o <= grid_ready_s and bmu_elector_ready_s;

    ----------------------------------------------------------------------------
    -- Muxing of m_valid
    ----------------------------------------------------------------------------

    p_m_valid_mux : process (m_valid_i, addr_select_i)
    begin
        for x_idx in 0 to (c_GRID_X_DIM - 1) loop
            for y_idx in 0 to (c_GRID_Y_DIM - 1) loop
                for z_idx in 0 to (c_GRID_Z_DIM - 1) loop
                    if (x_idx = addr_select_i.x) and (y_idx = addr_select_i.y) and (z_idx = addr_select_i.z) then
                        m_valid_xyz_s(x_idx)(y_idx)(z_idx) <= m_valid_i;
                    else
                        m_valid_xyz_s(x_idx)(y_idx)(z_idx) <= '0';
                    end if;
                end loop;
            end loop;
        end loop;
    end process p_m_valid_mux;

    ----------------------------------------------------------------------------
    -- Muxing of input and output vector
    ----------------------------------------------------------------------------

    p_in_vector_mux : process (vec_i, addr_select_i)
    begin
        -- TODO: this will need to be updated to address the grid weight update
        for x_idx in 0 to (c_GRID_X_DIM - 1) loop
            for y_idx in 0 to (c_GRID_Y_DIM - 1) loop
                for z_idx in 0 to (c_GRID_Z_DIM - 1) loop
                    vec_in_s(x_idx)(y_idx)(z_idx) <= vec_i;
                end loop;
            end loop;
        end loop;
    end process p_in_vector_mux;

    p_out_vector_mux : process (vec_out_s, addr_select_i)
    begin
        vec_o <= vec_out_s(addr_select_i.x)(addr_select_i.y)(addr_select_i.z);
    end process p_out_vector_mux;

end arch;