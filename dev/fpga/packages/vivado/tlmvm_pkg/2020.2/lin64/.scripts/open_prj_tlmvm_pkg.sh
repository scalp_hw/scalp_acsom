#!/bin/sh

##################################################################################
#                                 _             _
#                                | |_  ___ _ __(_)__ _
#                                | ' \/ -_) '_ \ / _` |
#                                |_||_\___| .__/_\__,_|
#                                         |_|
#
##################################################################################
#
# Company: hepia
# Author: Quentin Berthet <quentin.berthet@hesge.ch>
#
# Project Name: tlmvm_pkg
# Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
# Tool version: 2020.2
# Description: Open Vivado project GUI
#
# Last update: 2021-03-31 22:55:42
#
##################################################################################

echo "> Open Vivado GUI..."
vivado -nojournal -nolog -notrace ../tlmvm_pkg/tlmvm_pkg.xpr
