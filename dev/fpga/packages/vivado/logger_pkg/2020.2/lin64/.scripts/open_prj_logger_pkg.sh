#!/bin/sh

##################################################################################
#                                 _             _
#                                | |_  ___ _ __(_)__ _
#                                | ' \/ -_) '_ \ / _` |
#                                |_||_\___| .__/_\__,_|
#                                         |_|
#
##################################################################################
#
# Company: hepia
# Author: Quentin Berthet <quentin.berthet@hesge.ch>
#
# Project Name: logger_pkg
# Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
# Tool version: 2020.2
# Description: Open Vivado project GUI
#
# Last update: 2021-03-15 14:34:14
#
##################################################################################

echo "> Open Vivado GUI..."
vivado -nojournal -nolog -notrace ../logger_pkg/logger_pkg.xpr
