----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_tlmvm_pkg - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for package tlmvm_pkg
--
-- Last update: 2021-03-31 22:55:42
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_tlmvm_pkg is
end tb_tlmvm_pkg;


architecture behavioral of tb_tlmvm_pkg is

begin

end behavioral;
