----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: util_pkg - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Package util_pkg
--
-- Last update: 2021/03/09 14:26:07
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

package util_pkg is

    ----------------------------------------------------------------------------
    -- Commodity helpers

    --* Return the number of bits required to encode a natural value
    function bits_width(max_value : in natural) return natural;

    --* return a vector with bits reversed (MSB<->LSB)
    --* (works only with X downto 0 vectors)
    function bit_reverse(vec : std_logic_vector) return std_logic_vector;

    --* return a vector with each bytes reversed using bit_reverse()
    --* vector must be a byte multiple
    function byte_reverse(vec : std_logic_vector) return std_logic_vector;

    function round_up(i : natural; multiple : natural) return natural;

    function max(a : in integer; b : in integer) return integer;
    function min(a : in integer; b : in integer) return integer;

    --* Compare equality between two real values with a relative epsilon
    function are_relatively_equal(a, b, epsilon : real := 1.0e-7) return boolean;

    --
    ----------------------------------------------------------------------------

end util_pkg;

package body util_pkg is

    function bits_width(max_value : in natural) return natural is
    begin
        if max_value < 1 then
            return 0;
        elsif max_value = 1 then
            return 1;
        else
            return natural(ceil(log2(real(max_value))));
        end if;
    end;

    function bit_reverse(vec : std_logic_vector) return std_logic_vector is
        variable res             : std_logic_vector(vec'high downto vec'low);
    begin
        for i in vec'high downto vec'low loop
            res(i) := vec(vec'high - i);
        end loop;
        return res;
    end bit_reverse;

    function byte_reverse(vec : std_logic_vector) return std_logic_vector is
        variable res              : std_logic_vector(vec'high downto vec'low);
        variable temp             : std_logic_vector(7 downto 0);
        variable idx              : integer;
    begin
        if (vec'length mod 8 /= 0) then
            assert false
            report "byte_reverse: expecting a byte-multiple sized vector" severity error;
        end if;

        idx := vec'high;
        loop
            temp                    := vec(idx downto idx - 7);
            res(idx downto idx - 7) := bit_reverse(temp);
            if (idx = 7) then
                exit;
            end if;
            idx := idx - 8;
        end loop;
        return res;
    end byte_reverse;

    function round_up(i : natural; multiple : natural) return natural is
    begin
        return ((i + multiple - 1) / multiple) * multiple;
    end;

    function max(a : in integer; b : in integer) return integer is
    begin
        if a > b then
            return a;
        else
            return b;
        end if;
    end max;

    function min(a : in integer; b : in integer) return integer is
    begin
        if a < b then
            return a;
        else
            return b;
        end if;
    end min;

    -- Adapted from: http://floating-point-gui.de/errors/comparison/
    function are_relatively_equal(a, b, epsilon : real := 1.0e-7) return boolean is
    begin
        if a = b then -- Take care of infinities
            return true;
        elsif a * b = 0.0 then -- Either a or b is zero
            return abs(a - b) < epsilon ** 2;
        else -- Relative error
            return abs(a - b) / (abs(a) + abs(b)) < epsilon;
        end if;
    end function;

end package body util_pkg;
