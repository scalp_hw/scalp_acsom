----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_csom_pkg - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for package csom_pkg
--
-- Last update: 2021/06/09 09:38:29
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

library std;
use std.env.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_csom_pkg is
end tb_csom_pkg;

architecture behavioral of tb_csom_pkg is
begin

    ----------------------------------------------------------------------------
    -- TEST BENCH STIMULI
    ----------------------------------------------------------------------------
    tb1 : process is
        variable seed1      : integer := 1;
        variable seed2      : integer := 1;
        variable real_value : real;
        variable scalar_value : scalar_t;

        procedure test_real_loop(arg : real) is
            constant epsilon             : real := 1.0e-7;
            variable fp_value            : scalar_t;
            variable real_value          : real;
        begin
            -- Do loop from real -> FP -> real and compare
            fp_value   := to_scalar_t(arg);
            real_value := to_real(fp_value);
            if are_relatively_equal(arg, real_value, epsilon) then
                logger.log_note("Real input: (" & real'image(arg) & ") = (" & real'image(real_value) & "), FP binary (" & to_bin_string(fp_value) & ")");
                else
                logger.log_error("Real input: (" & real'image(arg) & ") /= (" & real'image(real_value) & "), delta = (" & real'image(abs(arg - real_value)) & "), epsilon = (" & real'image(epsilon) & "), FP binary  (" & to_bin_string(fp_value) & ")");
            end if;
        end procedure;

        procedure test_fp_loop(arg : scalar_t) is
            variable fp_value          : scalar_t;
            variable real_value        : real;
        begin
            -- Do loop from FP -> real -> FP and compare
            real_value := to_real(arg);
            fp_value   := to_scalar_t(real_value);
            if arg = fp_value then
                logger.log_note("FP input: (" & to_bin_string(arg) & ") = (" & to_bin_string(fp_value) & "), real (" & real'image(real_value) & ")");
                else
                logger.log_error("FP input: (" & to_bin_string(arg) & ") /= (" & to_bin_string(fp_value) & "), real (" & real'image(real_value) & ")");
            end if;
        end procedure;

        --procedure get_random_value(variable seed1, seed2 : inout integer; variable output : out real) is
        --    constant exponent_bias : real := (2.0 ** real(wE))/2.0+1.0;
        --    variable sign          : real;
        --    variable mantissa      : real;
        --    variable exponent      : real;
        --begin
        --    -- Sign
        --    uniform(seed1, seed2, sign);
        --    if sign > 0.5 then
        --        sign := 1.0;
        --        else
        --        sign := - 1.0;
        --    end if;
        --    -- Mantissa
        --    uniform(seed1, seed2, mantissa);
        --    mantissa := floor(((2.0 ** real(wF - 1)) - 1.0) * mantissa);
        --    -- Exponent
        --    uniform(seed1, seed2, exponent);
        --    exponent := floor(((2.0 ** real(wE)) - 1.0) * exponent);
        --    -- Exponent bias
        --    --logger.log_note("Exponent bias " & real'image(exponent_bias));
        --    --logger.log_note("Exponent before bias " & real'image(exponent));
        --    --exponent := exponent - exponent_bias;
        --    exponent := 19.0;
        --    logger.log_note("Exponent after bias " & real'image(exponent));
        --    -- Real result
        --    output := sign * mantissa * (2.0 ** exponent);
        --end;

        procedure get_random_value(variable seed1, seed2 : inout integer; variable output : out real) is
            constant exponent_bias : integer := ((2 ** (wE))/2) - 1;
            variable rand_real     : real;
            variable sign          : integer;
            variable mantissa      : integer;
            variable mantissa_real : real;
            variable exponent      : integer;
        begin
            -- Sign
            uniform(seed1, seed2, rand_real);
            if rand_real > 0.5 then
                sign := 1;
                else
                sign := - 1;
            end if;
            sign := 1;
            -- Mantissa
            --uniform(seed1, seed2, rand_real);
            --mantissa := integer(round(real((2 ** wF) - 1) * rand_real));
            --mantissa := (2 ** wF) - mantissa;
            --mantissa_real := 2.0 - (1.0/real(mantissa));
            mantissa      := integer(round(real((2 ** wF) - 1) * rand_real));
            mantissa_real := 2.0 ** (-1.0 * real(mantissa));
            --result        := 0.0;
            --for i in arg_int'range loop
            --    if (arg_int(i) = '1') then
            --        result := result + (2.0 ** i);
            --    end if;
            --end loop;

            logger.log_note("Mantissa " & integer'image(mantissa));
            logger.log_note("Mantissa real " & real'image(mantissa_real));
            -- Exponent
            --uniform(seed1, seed2, rand_real);
            exponent := ((2 ** wE) - 1);
            --exponent := 31;
            -- Exponent bias
            logger.log_note("Exponent bias " & integer'image(exponent_bias));
            logger.log_note("Exponent before bias " & integer'image(exponent));
            exponent := exponent - exponent_bias;
            logger.log_note("Exponent after bias " & integer'image(exponent));
            -- Real result
            output := real(sign) * mantissa_real * (2.0 ** real(exponent));
        end;

        procedure get_random_fp_value(variable seed1, seed2 : inout integer; variable output : out scalar_t) is
            variable rand_real : real;
            variable ex        : std_logic_vector(1 downto 0) := "01";
            variable sign      : std_logic;
            variable exponent  : std_logic_vector(wE - 1 downto 0);
            variable mantissa  : std_logic_vector(wF - 1 downto 0);
        begin
            -- Sign
            uniform(seed1, seed2, rand_real);
            if rand_real > 0.5 then
                sign := '1';
                else
                sign := '0';
            end if;
            -- Exponent
            uniform(seed1, seed2, rand_real);
            exponent := std_logic_vector(to_unsigned(integer(floor(real((2 ** wE) - 1) * rand_real)), wE));
            -- Mantissa
            uniform(seed1, seed2, rand_real);
            mantissa := std_logic_vector(to_unsigned(integer(floor(real((2 ** wF) - 1) * rand_real)), wF));
            output   := scalar_t(std_logic_vector'(ex & sign & exponent & mantissa));
        end;

        procedure test_scalar_comp(l, r : in scalar_t) is
            variable smaller, greater                : boolean;
        begin
            smaller := scalar_lt(l,r);
            greater := scalar_gt(l,r);
            if to_real(l) < to_real(r) then
                if not smaller then
                    logger.log_error(real'image(to_real(l)) & " (" & to_bin_string(std_logic_vector(l)) & ") is smaller than " & real'image(to_real(r)) & " (" & to_bin_string(std_logic_vector(r)) & ") but was not reported as smaller by scalar_lt");
                elsif greater then
                    logger.log_error(real'image(to_real(l)) & " (" & to_bin_string(std_logic_vector(l))& ") is smaller than " & real'image(to_real(r)) & " (" & to_bin_string(std_logic_vector(r)) & ") but was reported as greater by scalar_gt");
                else
                    logger.log_note(real'image(to_real(l)) & " correctly reported smaller than " & real'image(to_real(r)));
                end if;
            elsif to_real(l) > to_real(r) then
                if smaller then
                    logger.log_error(real'image(to_real(l)) & " (" & to_bin_string(std_logic_vector(l))& ") is greater than " & real'image(to_real(r)) & " (" & to_bin_string(std_logic_vector(r)) & ") but was reported as smaller by scalar_lt");
                elsif not greater then
                    logger.log_error(real'image(to_real(l)) & " (" & to_bin_string(std_logic_vector(l))& ") is greater than " & real'image(to_real(r)) & " (" & to_bin_string(std_logic_vector(r)) & ") but was not reported as greater by scalar_gt");
                else
                    logger.log_note(real'image(to_real(l)) & " correctly reported greater than " & real'image(to_real(r)));
                end if;
            else
                if smaller then
                    logger.log_error(real'image(to_real(l)) & " (" & to_bin_string(std_logic_vector(l))& ") is equal to " & real'image(to_real(r)) & " (" & to_bin_string(std_logic_vector(r)) & ") but was reported as smaller by scalar_lt");
                elsif greater then
                    logger.log_error(real'image(to_real(l)) & " (" & to_bin_string(std_logic_vector(l))& ") is equal to " & real'image(to_real(r)) & " (" & to_bin_string(std_logic_vector(r)) & ") but was reported as greater by scalar_gt");
                else
                    logger.log_note(real'image(to_real(l)) & " correctly reported neither smaller or greater than " & real'image(to_real(r)));
                end if;
            end if;
        end;

        procedure test_scalar_comp(l, r : in real) is
        begin
            test_scalar_comp(to_scalar_t(l), to_scalar_t(r));
        end;

        variable value_sti : scalar_t;
        variable value_obs : scalar_t;
        variable real_sti  : real;
        variable real_obs  : real;
    begin
        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_report.html",
        "Testbench report: ",
        "csom_pkg");

        logger.set_log_file_time(true);

        --logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("wE: " & integer'image(wE));
        logger.log_note("wF: " & integer'image(wF));

        if false then
            -- This test is functional only with 8.23 FP format
            logger.log_note("Test to_string and to_bin_string function:");

            value_sti := "0000000000000000000000000000000000"; -- 0.0
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "1000000000000000000000000000000000"; -- + infinity
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "1010000000000000000000000000000000"; -- - infinity
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "1100000000000000000000000000000000"; -- Not a number
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "0100111111000000000000000000000000"; -- 0.5
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "0101000000000000000000000000000000"; -- 2
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "0100111111100000000000000000000000"; -- 1
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "0101000001100000000000000000000000"; -- 16
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "0101000000001000000000000000000000"; -- 2.5
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

            value_sti := "0111000001000000000000000000000000"; -- -8
            logger.log_note(to_bin_string(value_sti) & " : " & to_string(value_sti));

        end if;

        if false then
            logger.log_note("Test to_scalar_t: Simple edge case: 0.0 and 1.0");
            test_real_loop(0.0);
            test_real_loop(1.0);
        end if;

        if false then
            logger.log_note("Test to_scalar_t: Random value matching wE and wF");
            for I in 0 to 100 loop
                get_random_value(seed1, seed2, real_value);
                test_real_loop(real_value);
            end loop;
        end if;
        if false then
            logger.log_note("Test to_scalar_t: Random value matching wE and wF");
            for I in 0 to 1000 loop
                get_random_fp_value(seed1, seed2, value_sti);
                test_fp_loop(value_sti);
            end loop;
        end if;

        if false then
            logger.log_note("Test to_scalar_t: simfloat 4.4 -1.0 to 1.0");
            -- format: IEEE754 5.4
            test_real_loop(-0.84375);
            test_real_loop(-0.9375);
            test_real_loop(0.05859375);
            test_real_loop(-0.71875);
            test_real_loop(0.46875);
            test_real_loop(0.01953125);
            test_real_loop(-0.75);
            test_real_loop(-0.5625);
            test_real_loop(0.90625);
            test_real_loop(0.5);
            test_real_loop(-0.3125);
            test_real_loop(-0.421875);
            test_real_loop(-0.40625);
            test_real_loop(0.90625);
            test_real_loop(-0.84375);
            test_real_loop(0.375);
            test_real_loop(-0.96875);
            test_real_loop(-0.90625);
            test_real_loop(0.34375);
            test_real_loop(-0.5);
            test_real_loop(0.34375);
            test_real_loop(-0.2265625);
            test_real_loop(0.96875);
            test_real_loop(-0.234375);
            test_real_loop(0.033203125);
            test_real_loop(0.3125);
            test_real_loop(-0.9375);
            test_real_loop(-0.375);
            test_real_loop(-0.46875);
            test_real_loop(-0.65625);
            test_real_loop(-0.65625);
            test_real_loop(-0.4375);
            test_real_loop(-0.125);
            test_real_loop(-0.5625);
            test_real_loop(0.53125);
            test_real_loop(0.625);
            test_real_loop(0.09765625);
            test_real_loop(-0.625);
            test_real_loop(0.90625);
            test_real_loop(-0.625);
            test_real_loop(-0.53125);
            test_real_loop(-0.46875);
            test_real_loop(0.12109375);
            test_real_loop(-0.5);
            test_real_loop(0.5);
            test_real_loop(0.359375);
            test_real_loop(0.59375);
            test_real_loop(-0.2265625);
            test_real_loop(-0.25);
            test_real_loop(0.9375);
            test_real_loop(0.84375);
            test_real_loop(0.15625);
            test_real_loop(0.90625);
            test_real_loop(0.71875);
            test_real_loop(-0.4375);
            test_real_loop(0.59375);
            test_real_loop(-0.59375);
            test_real_loop(-0.11328125);
            test_real_loop(-0.078125);
            test_real_loop(0.84375);
            test_real_loop(0.060546875);
            test_real_loop(0.71875);
            test_real_loop(0.84375);
            test_real_loop(0.2109375);
            test_real_loop(-0.90625);
            test_real_loop(-0.75);
            test_real_loop(-0.6875);
            test_real_loop(0.4375);
            test_real_loop(0.9375);
            test_real_loop(-0.6875);
            test_real_loop(0.40625);
            test_real_loop(-0.84375);
            test_real_loop(-0.65625);
            test_real_loop(-0.59375);
            test_real_loop(0.1953125);
            test_real_loop(-0.328125);
            test_real_loop(-0.53125);
            test_real_loop(-0.1640625);
        end if;

        if true then
            logger.log_note("Test scalar_lt and scalar_gt:");
            test_scalar_comp(1.5,2.5);
            test_scalar_comp(2.5,1.5);

            test_scalar_comp(110.5,-110.5);
            test_scalar_comp(-110.5,110.5);

            test_scalar_comp(110.5,1.5);
            test_scalar_comp(1.5,110.5);

            test_scalar_comp(1.55,1.5);
            test_scalar_comp(1.5,1.55);

            test_scalar_comp(-1.5,-1.5);
            test_scalar_comp(0.0,0.0);
            test_scalar_comp(1.5,1.5);

            test_scalar_comp(-1.5,0.0);
            test_scalar_comp(1.5,0.0);
            test_scalar_comp(0.0,-1.5);
            test_scalar_comp(0.0,1.5);

            test_scalar_comp(fp_infinity(true),to_scalar_t(-1.5));
            test_scalar_comp(fp_infinity(true),to_scalar_t(0.0));
            test_scalar_comp(fp_infinity(true),to_scalar_t(1.5));
            test_scalar_comp(fp_infinity(true),fp_infinity(true));

            test_scalar_comp(fp_infinity(false),fp_infinity(false));
            test_scalar_comp(fp_infinity(false),to_scalar_t(-1.5));
            test_scalar_comp(fp_infinity(false),to_scalar_t(0.0));
            test_scalar_comp(fp_infinity(false),to_scalar_t(1.5));

            test_scalar_comp(fp_infinity(true),fp_infinity(false));
            test_scalar_comp(fp_infinity(false),fp_infinity(true));

        end if;

        logger.final_report;
        stop(0);
    end process tb1;
    --  End Test Bench

end behavioral;
