
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;

entity test_comp is
    port (
        l : in scalar_t;
        r : in scalar_t;
        gt : out std_logic;
        lt : out std_logic
    );
end test_comp;

architecture arch of test_comp is
begin

    gt <= '1' when scalar_gt(l,r) else '0';
    lt <= '1' when scalar_lt(l,r) else '0';

end arch;