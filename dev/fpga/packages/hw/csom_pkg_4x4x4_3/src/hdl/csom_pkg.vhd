----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: csom_pkg - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Package csom_pkg
--
-- Last update: 2021/07/12 16:39:49
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all; -- for uniform
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.float_pkg.all;
use work.flopoco_pipeline_pkg.all;

package csom_pkg is

    ----------------------------------------------------------------------------
    -- FloPoCo configuration

    --* Exponent size in bits
    --* Must match value used during FloPoCO operators generation
    constant wE : natural := c_wE;

    --* Fractional size in bits
    --* Must match value used during FloPoCO operators generation
    constant wF : natural := c_wF;

    ----------------------------------------------------------------------------
    --* Vector and scalar types

    --* Scalar type
    subtype scalar_t is std_logic_vector(wE + wF + 2 downto 0);

    --* Convert to and from IEEE754 32bits floating point representation
    function to_scalar_t(Value : float32) return scalar_t;
    function to_float32(Value  : scalar_t) return float32;

    --* Helper function to convert scalar_t to other representation
    function to_scalar_t(Value : real) return scalar_t;
    function to_real(Value     : scalar_t) return real;

    function to_string(Value     : scalar_t) return string;
    function to_bin_string(Value : std_logic_vector) return string;

    --* Comparison operator for scalar_t
    function scalar_lt (l, r : scalar_t) return boolean;
    function scalar_gt (l, r : scalar_t) return boolean;

    --* Return (positive or negative) infinity in scalar_t
    function fp_infinity(pos : boolean := true) return scalar_t;

    --* Vector component count
    constant C_VEC_COMP_COUNT      : natural := 3;
    constant C_VEC_COMP_COUNT_BITS : natural := bits_width(C_VEC_COMP_COUNT);

    --* Vector type
    type vec_t is array (0 to C_VEC_COMP_COUNT - 1) of scalar_t;

    --* Vector of real type, simulation only
    type real_vec_t is array (0 to C_VEC_COMP_COUNT - 1) of real;

    function to_vec_t(vec      : real_vec_t) return vec_t;
    function to_real_vec_t(vec : vec_t) return real_vec_t;

    --* Compare equality between two vector of real values with a relative epsilon
    function are_relatively_equal(
        a, b    : real_vec_t;
        epsilon : real := 1.0e-7
    ) return boolean;

    --* Helper function to convert vec_t to other representation
    function to_string(Vec : vec_t) return string;

    --* Helper function to convert vec_t to other representation
    function to_string(Vec : real_vec_t) return string;

    --* Randomize real_vect_t
    procedure randomize_vect(variable seed1, seed2 : inout integer; variable vec : out vec_t);

    procedure randomize_vect(variable seed1, seed2 : inout integer; variable vec : out real_vec_t);

    ----------------------------------------------------------------------------
    --* Network/Grid addressing

    constant c_GRID_MAX_X_DIM : natural := 256;
    constant c_GRID_MAX_Y_DIM : natural := 256;
    constant c_GRID_MAX_Z_DIM : natural := 256;

    --* Size of each grid dimension
    constant c_GRID_X_DIM : natural := 4;
    constant c_GRID_Y_DIM : natural := 4;
    constant c_GRID_Z_DIM : natural := 4;

    constant c_GRID_MAX_DIM : natural := MAX(c_GRID_MAX_X_DIM, MAX(c_GRID_MAX_Y_DIM, c_GRID_MAX_Z_DIM));

    subtype t_grid_addr_comp is natural range 0 to c_GRID_MAX_X_DIM - 1;

    type t_grid_addr is record
        x : t_grid_addr_comp;
        y : t_grid_addr_comp;
        z : t_grid_addr_comp;
    end record t_grid_addr;

    --* Helper function to convert t_grid_addr to string
    function to_string(addr : t_grid_addr) return string;
    ----------------------------------------------------------------------------
    --* Matrices types
    -- From https://stackoverflow.com/questions/30651269/synthesizable-multidimensional-arrays-in-vhdl

    -- std_logic 3d matrices
    subtype t_slv_dim1 is std_logic_vector(c_GRID_Z_DIM - 1 downto 0);
    type t_slv_dim1_vector is array(natural range <>) of t_slv_dim1;
    subtype t_slv_dim2 is t_slv_dim1_vector(0 to c_GRID_Y_DIM - 1);
    type t_slv_dim3_vector is array(natural range <>) of t_slv_dim2;
    subtype t_slv_dim3 is t_slv_dim3_vector(0 to c_GRID_X_DIM - 1);

    -- Scalar 3d matrices
    type t_scalar_vector is array(natural range <>) of scalar_t;
    subtype t_scalar_dim1 is t_scalar_vector(0 to c_GRID_Z_DIM - 1);

    type t_scalar_dim1_vector is array(natural range <>) of t_scalar_dim1;
    subtype t_scalar_dim2 is t_scalar_dim1_vector(0 to c_GRID_Y_DIM - 1);

    type t_scalar_dim3_vector is array(natural range <>) of t_scalar_dim2;
    subtype t_scalar_dim3 is t_scalar_dim3_vector(0 to c_GRID_X_DIM - 1);

    -- Vector 3d matrices
    type t_vec_vector is array(natural range <>) of vec_t;
    subtype t_vec_dim1 is t_vec_vector(0 to c_GRID_Z_DIM - 1);

    type t_vec_dim1_vector is array(natural range <>) of t_vec_dim1;
    subtype t_vec_dim2 is t_vec_dim1_vector(0 to c_GRID_Y_DIM - 1);

    type t_vec_dim3_vector is array(natural range <>) of t_vec_dim2;
    subtype t_vec_dim3 is t_vec_dim3_vector(0 to c_GRID_X_DIM - 1);

    function and_reduce(m : t_slv_dim3) return std_logic;

    function or_reduce(m : t_slv_dim3) return std_logic;

    --* Convert linear addressing to 3d adressing
    procedure linear_to_3d(
        idx   : in integer;
        x     : out integer;
        y     : out integer;
        z     : out integer;
        x_dim : in integer := c_GRID_X_DIM;
        y_dim : in integer := c_GRID_Y_DIM;
        z_dim : in integer := c_GRID_Z_DIM
    );
    ----------------------------------------------------------------------------
    --* Diffusion algorithm mechanisms

    subtype t_direction is std_logic_vector(5 downto 0);

    constant c_DIR_NONE  : t_direction := "000000";
    constant c_DIR_ALL   : t_direction := "111111";
    constant c_DIR_EAST  : t_direction := "000001";
    constant c_DIR_WEST  : t_direction := "000010";
    constant c_DIR_NORTH : t_direction := "000100";
    constant c_DIR_SOUTH : t_direction := "001000";
    constant c_DIR_UP    : t_direction := "010000";
    constant c_DIR_DOWN  : t_direction := "100000";

    function dir_to_string(dir : t_direction) return string;

    type t_diffusion_type is (
        DIFF_XYZ,
        DIFF_YXZ,
        DIFF_ZXY,
        DIFF_ZYX,
        DIFF_YZX,
        DIFF_XZY
    );

    function get_forwarding_directions(
        diff_type : t_diffusion_type;
        input_dir : t_direction
    ) return t_direction;

    function get_opposite_direction(input_dir : t_direction) return t_direction;

    function dir_in_list(
        dirs_list : t_direction;
        dir       : t_direction
    ) return boolean;

    --* Helper function to convert t_diffusion_type to string
    function to_string(diff_type : t_diffusion_type) return string;

    function get_next_diff_mecha(diff_type : t_diffusion_type) return t_diffusion_type;

    -- Diffusion type 3d matrices
    type t_diffusion_type_vector is array(natural range <>) of t_diffusion_type;
    subtype t_diffusion_type_dim1 is t_diffusion_type_vector(0 to c_GRID_Z_DIM - 1);

    type t_diffusion_type_dim1_vector is array(natural range <>) of t_diffusion_type_dim1;
    subtype t_diffusion_type_dim2 is t_diffusion_type_dim1_vector(0 to c_GRID_Y_DIM - 1);

    type t_diffusion_type_dim3_vector is array(natural range <>) of t_diffusion_type_dim2;
    subtype t_diffusion_type_dim3 is t_diffusion_type_dim3_vector(0 to c_GRID_X_DIM - 1);

    -- t_direction 3d matrices
    type t_dir_vector is array(natural range <>) of t_direction;
    subtype t_dir_dim1 is t_dir_vector(0 to c_GRID_Z_DIM - 1);

    type t_dir_dim1_vector is array(natural range <>) of t_dir_dim1;
    subtype t_dir_dim2 is t_dir_dim1_vector(0 to c_GRID_Y_DIM - 1);

    type t_dir_dim3_vector is array(natural range <>) of t_dir_dim2;
    subtype t_dir_dim3 is t_dir_dim3_vector(0 to c_GRID_X_DIM - 1);

    ----------------------------------------------------------------------------
    --* Time management

    --* Time
    subtype time_t is std_logic_vector(32 - 1 downto 0);

end csom_pkg;

package body csom_pkg is

    --* Convert from scalar_t to decimal string
    function to_string(Value : scalar_t) return string is
    begin
        case Value(Value'left downto Value'left - 1) is
            when "00" | "01" =>
                -- Zero and other valid numbers
                return real'image(to_real(Value));
            when "10" =>
                -- Infinities
                if Value(Value'left - 2) = '0' then
                    return "+Inf";
                else
                    return "-Inf";
                end if;
            when others =>
                return "NaN";
        end case;
    end function;

    --* Convert from scalar_t to binary string
    --* Stolen from https://gist.github.com/mathieucaroff/3132c36ff21b63a72c8c0574998859ee
    function to_bin_string (Value : std_logic_vector) return string is
        variable text                 : string(Value'length - 1 downto 0) := (others => '9');
    begin
        for k in Value'range loop
            case Value(k) is
                when '0'    => text(k)    := '0';
                when '1'    => text(k)    := '1';
                when 'U'    => text(k)    := 'U';
                when 'X'    => text(k)    := 'X';
                when 'Z'    => text(k)    := 'Z';
                when '-'    => text(k)    := '-';
                when others => text(k) := '?';
            end case;
        end loop;
        return text;
    end function;

    --* Convert IEEE754 float32 to Flopoco FP format
    function to_scalar_t(Value : float32) return scalar_t is
        -- these should be constant, but simulation tool seems to
        -- be a bit overzealous with optimisation if set so
        variable wEI                : natural := 8;  -- As per IEEE754 float32
        variable wFI                : natural := 23; -- As per IEEE754 float32
        variable wEO                : natural := wE;
        variable wFO                : natural := wF;
        constant biasI              : integer := (2 ** (wEI - 1)) - 1;
        constant eMaxO              : integer := (2 ** (wEO - 1)); -- that's our maximal exponent, one more than IEEE's
        constant expAddend          : integer := - biasI + eMaxO - 1;
        constant eMinO              : integer := - ((2 ** (wEO - 1)) - 1); -- that's our minimal exponent, equal to -bias
        constant underflowThreshold : integer := eMinO + biasI;            -- positive since wEI>wEO.
        constant overflowThreshold  : integer := eMaxO + biasI;
        variable X                  : std_logic_vector(wEI + wFI downto 0);
        variable expX               : std_logic_vector(wEI - 1 downto 0);
        variable fracX              : std_logic_vector(wFI - 1 downto 0);
        variable sX                 : std_logic;
        variable expZero            : std_logic;
        variable expInfty           : std_logic;
        variable fracZero           : std_logic;
        variable reprSubNormal      : std_logic;
        variable sfracX             : std_logic_vector(wFI - 1 downto 0);
        variable fracR              : std_logic_vector(wFO - 1 downto 0);
        variable expR               : std_logic_vector(wEO - 1 downto 0);
        variable resultLSB          : std_logic;
        variable roundBit           : std_logic;
        variable sticky             : std_logic;
        variable round              : std_logic;
        variable expfracR0          : std_logic_vector(wEO + wFO - 1 downto 0);
        variable expfracR0p         : std_logic_vector(wEO + wFO + 1 - 1 downto 0); -- one more bit for wFI > wFO and wEI>wEO
        variable infinity           : std_logic;
        variable zero               : std_logic;
        variable NaN                : std_logic;
        variable exnR               : std_logic_vector(2 - 1 downto 0);
        variable overflow           : std_logic;
        variable underflow          : std_logic;
        variable roundOverflow      : std_logic;
        variable unSub              : std_logic_vector(wEI + 1 - 1 downto 0);
        variable ovSub              : std_logic_vector (wEI + 1 - 1 downto 0);
        variable expXO              : std_logic_vector(wEO - 1 downto 0);
        variable R                  : std_logic_vector(wEO + wFO + 2 downto 0);
    begin
        X     := std_logic_vector(Value);
        expX  := X(wEI + wFI - 1 downto wFI);
        fracX := X(wFI - 1 downto 0);
        sX    := X(wEI + wFI);
        -- There are three exponent cases to consider:
        -- wEI==wEO is the easiest case, with one subtelty:
        --    we have two more exponent values than IEEE (field 0...0, value -(1<<wEI)+1, and field 11..11, value 1<<wEI),
        --    we may thus convert into normal numbers subnormal input values whose mantissa field begins with a 1
        --    Other subnormals are flushed to zero
        -- wEI > wEO (range downgrading) is probably the most useful, as we want to minimize the precision of the FPGA computation
        --    with respect to a software implementation
        --    Anyway it is fairly easy to implement, too: subnormals are all flushed to zero, and some normal numbers may overflow or underflow.
        -- wEI < wEO (range widening) may be useful to some, but I don't see whom yet :) mail me if you want it implemented.
        --    It will  be costly, since all input subnormals will need to be converted into normal numbers by means of a barrel shifter.

        -- In each exponent case, the mantissa may be copied (if wFI<=wFO -- taking care of the above subtlety,
        -- or it must be rounded if wFI>wFO (again, probably the most useful case)
        -- If wEI=WEO, the rounding may not lead to an overflow since we have this one more large exponent value,
        -- If wEI<WEO, the rounding may lead to an overflow
        -- This is reasonably cheap.
        -- analyze the input exponent
        expZero := '1' when expX = (wEI - 1 downto 0 => '0') else
        '0';
        expInfty := '1' when expX = (wEI - 1 downto 0 => '1') else
        '0';
        fracZero := '1' when fracX = (wFI - 1 downto 0 => '0') else
        '0';

        if wEI = wEO then
            reprSubNormal := fracX(wFI - 1);
            -- since we have one more exponent value than IEEE (field 0...0, value emin-1),
            -- we can represent subnormal numbers whose mantissa field begins with a 1
            sfracX := fracX(wFI - 2 downto 0) & '0' when (expZero = '1' and reprSubNormal = '1') else
            fracX;
            if wFO >= wFI then
                if wFO > wFI then
                    -- need to pad with 0s
                    fracR := sfracX & std_logic_vector(to_unsigned(0, wFO - wFI));
                    else
                    fracR := sfracX;
                end if;
                -- copy exponent. This will be OK even for subnormals, zero and infty since in such cases the exn bits will prevail" << endl;
                expR := expX;
                else -- wFI > wFO, wEI==wEO
                -- wFO < wFI, need to round fraction
                resultLSB := sfracX(wFI - wFO);
                roundBit  := sfracX(wFI - wFO - 1);
                -- need to define a sticky bit
                if wFI - wFO > 1 then
                    sticky := '0' when sfracX(wFI - wFO - 2 downto 0) = std_logic_vector(to_unsigned(0, wFI - wFO - 2)) else
                    '1';
                    else
                    sticky := '0';
                end if; -- end of sticky computation
                round := roundBit and (sticky or resultLSB);
                -- The following addition will not overflow since FloPoCo format has one more exponent value
                expfracR0 := std_logic_vector(unsigned(std_logic_vector'(expX & sfracX(wFI - 1 downto wFI - wFO))) + unsigned((std_logic_vector(to_unsigned(0, wEO + wFO - 1)) & round)));
                fracR     := expfracR0(wFO - 1 downto 0);
                expR      := expfracR0(wFO + wEO - 1 downto wFO);
            end if;
            infinity := expInfty and fracZero;
            zero     := expZero and not reprSubNormal;
            NaN      := expInfty and not fracZero;
            exnR     := "00" when zero = '1'
            else
            "10" when infinity = '1'
            else
            "11" when NaN = '1'
            else
            "01"; -- normal number
            elsif wEI < wEO then
            assert False report "Warning: subnormal inputs would be representable in the destination format, but will be flushed to zero anyway (TODO). Please complain to the FloPoCo team if you need correct rounding" severity warning;
            overflow  := '0'; -- overflow never happens for these (wE_in, wE_out)
            underflow := '0'; -- underflow never happens for these (wE_in, wE_out)
            -- We have to compute ER = E_X - bias(wE_in) + bias(wE_R)
            -- Let us pack all the constants together
            expR := std_logic_vector(unsigned(std_logic_vector'((wEO - 1 downto wEI => '0') & expX)) + to_unsigned(expAddend, wEO)); -- to check
            if wFO >= wFI then                                                                                                       -- no rounding needed
                if wFO > wFI then                                                                                                        -- need to pad with 0s
                    fracR := fracX & std_logic_vector(to_unsigned(0, wFO - wFI));
                    else
                    fracR := fracX;
                end if;
                roundOverflow := '0';
                else
                assert False report "InputIEEE not yet implemented for wEI<wEO and wFI>wFO, send us a mail if you need it" severity failure;
            end if;
            NaN      := expInfty and not fracZero;
            infinity := (expInfty and fracZero) or (not NaN and (overflow or roundOverflow));
            zero     := expZero or underflow;
            exnR     := "11" when NaN = '1'
            else
            "10" when infinity = '1'
            else
            "00" when zero = '1'
            else
            "01"; -- normal number
            else  -- wEI>wEO: some exponents will lead to over/underflow. All subnormals are flushed to zero
            -- min exponent value without underflow, biased with input bias:
            unSub     := std_logic_vector(unsigned(std_logic_vector'('0' & expX)) - to_unsigned(underflowThreshold, wEI + 1));
            underflow := unSub(wEI);
            -- max exponent value without overflow, biased with input bias: overflowThreshold
            ovSub    := std_logic_vector(to_unsigned(overflowThreshold, wEI + 1) - unsigned('0' & expX));
            overflow := ovSub(wEI);
            -- copy exponent. Result valid only in the absence of ov/underflow
            -- wEI>wEO therefore biasI>biasO
            -- have to compute expR = ((expX-biasI) + biasO) (wEO-1 downto 0)
            -- but the wEO-1 LSB bits of both biases are identical, therefore simply copy
            -- and the remaining MSB are 1 for input and 0 for output, therefore subtract the leading 1 by inverting it
            expXO := (not expX(wEO - 1)) & expX(wEO - 2 downto 0);
            if wFO >= wFI then -- no rounding needed
                if wFO > wFI then  -- need to pad with 0s
                    fracR := fracX & std_logic_vector(to_unsigned(0, wFO - wFI));
                    else
                    fracR := fracX;
                end if;
                expR          := expXO;
                roundOverflow := '0';
                else -- wFI > wFO, wEI>wEO
                -- wFO < wFI, need to round fraction
                resultLSB := fracX(wFI - wFO);
                roundBit  := fracX(wFI - wFO - 1);
                -- need to define a sticky bit
                if wFI - wFO > 1 then
                    sticky := '0' when fracX(wFI - wFO - 2 downto 0) = std_logic_vector(to_unsigned(0, wFI - wFO - 2)) else
                    '1';
                    else
                    sticky := '0';
                end if;
                round := roundBit and (sticky or resultLSB);
                -- The following addition may overflow
                expfracR0p    := std_logic_vector(unsigned(std_logic_vector'('0' & expXO & fracX(wFI - 1 downto wFI - wFO))) + unsigned(std_logic_vector(to_unsigned(0, wEO + wFO)) & round));
                roundOverflow := expfracR0p(wEO + wFO);
                fracR         := expfracR0p(wFO - 1 downto 0);
                expR          := expfracR0p(wFO + wEO - 1 downto wFO);
            end if;
            NaN      := expInfty and not fracZero;
            infinity := (expInfty and fracZero) or (not NaN and (overflow or roundOverflow));
            zero     := expZero or underflow;
            exnR     := "11" when NaN = '1'
            else
            "10" when infinity = '1'
            else
            "00" when zero = '1'
            else
            "01"; -- normal number"
        end if;
        R := exnR & sX & expR & fracR;
        return R;
    end function;

    --* Convert Flopoco FP format to IEEE754 float32
    function to_float32(Value : scalar_t) return float32 is
        -- these should be constant, but simulation tool seems to
        -- be a bit overzealous with optimisation if set so
        variable wEI       : natural := wE;
        variable wFI       : natural := wF;
        variable wEO       : natural := 8;
        variable wFO       : natural := 23;
        constant biasO     : integer := (2 ** (wEO - 1)) - 1;
        constant biasI     : integer := (2 ** (wEI - 1)) - 1;
        constant biasCorr  : integer := biasO - biasI;
        variable X         : std_logic_vector(wE + wF + 2 downto 0);
        variable fracX     : std_logic_vector(wFI - 1 downto 0);
        variable exnX      : std_logic_vector(2 - 1 downto 0);
        variable expX      : std_logic_vector(max(wEI, wEO) - 1 downto 0);
        variable sX        : std_logic;
        variable expZero   : std_logic;
        variable fracR     : std_logic_vector(wFO - 1 downto 0);
        variable expR      : std_logic_vector(wEO - 1 downto 0);
        variable sfracX    : std_logic_vector(wFI - 1 downto 0);
        variable resultLSB : std_logic;
        variable roundBit  : std_logic;
        variable sticky    : std_logic;
        variable round     : std_logic;
        variable expfracR0 : std_logic_vector(wEO + wFO - 1 downto 0);
        variable R         : std_logic_vector (wEO + wFO downto 0);
    begin
        X     := Value;
        fracX := X(wFI - 1 downto 0);
        exnX  := X(wEI + wFI + 2 downto wEI + wFI + 1);
        if wEI = wEO then
            expX := X(wEI + wFI - 1 downto wFI);
            elsif wEI < wEO then
            -- Now, we have to adjust the exponent to a wider word size and a new bias.
            -- For that, we have to compute Enew = Eold + biasCorr with biasCorr = biasO - biasI where biasI and bias= are the input and output biases (biasI = (1<<(wEI-1))-1, biasO = (1<<(wEO-1))-1)
            -- This could be done by this line:
            --   vhdl << tab << declare(getTarget()->adderDelay(wEO),"expX", wEO) << "  <= (" << zg(wEO-wEI) << " & X" << range(wEI+wFI-1, wFI) << ") + conv_std_logic_vector(" << biasCorr << "," << wEO << ");" << endl;
            -- Instead, as the wEI-1 LSBs are always zero and all the upper bits are one in biasCorr, we can utilize this to perform this constant addition by simple negations:
            --expX := X(wEI + wFI - 1 downto 0)
            --//for(int i=0; i < wEO-wEI; i++)
            --//{
            --   & not X(wEI+wFI-1)
            --//}
            -- & X(wEI+wFI-2 downto wFI);
            -- Note Quentin: use first implementation suggested above
            expX := std_logic_vector(unsigned(std_logic_vector'(std_logic_vector(to_unsigned(0, wEO - wEI)) & X(wEI + wFI - 1 downto wFI))) + to_unsigned(biasCorr, wEO));
        end if;
        -- onlyPositiveZeroes
        sX := X(wEI + wFI) when (exnX = "01" or exnX = "10") else
        '0';
        expZero := '1' when expX = (wEI - 1 downto 0 => '0') else
        '0';
        if wEI = wEO or wEI < wEO then
            -- since we have one more exponent value than IEEE (field 0...0, value emin-1),
            -- we can represent subnormal numbers whose mantissa field begins with a 1
            if wFO > wFI then
                fracR := std_logic_vector(to_unsigned(0, wFO)) when (exnX = "00") else                                                    -- zero
                '1' & fracX(wFI - 1 downto 0) & std_logic_vector(to_unsigned(0, wFO - wFI - 1)) when (expZero = '1' and exnX = "01") else -- subnormal, no rounding is performed here when wFO==wFI
                fracX & std_logic_vector(to_unsigned(0, wFO - wFI)) when (exnX = "01") else                                               -- normal number
                std_logic_vector(to_unsigned(0, wFO - 1)) & exnX(0);                                                                      -- +/- infty or NaN
                expR := (wEO - 1 downto 0 => '0') when (exnX = "00") else
                expX when (exnX = "01") else
                (wEO - 1 downto 0 => '1');
                elsif wFO = wFI then
                fracR := std_logic_vector(to_unsigned(0, wFO)) when (exnX = "00") else                                                    -- zero
                '1' & fracX(wFI - 1 downto 1) & std_logic_vector(to_unsigned(0, wFO - wFI - 0)) when (expZero = '1' and exnX = "01") else --subnormal, no rounding is performed here when wFO==wFI
                fracX & std_logic_vector(to_unsigned(0, wFO - wFI)) when (exnX = "01") else                                               -- normal number
                std_logic_vector(to_unsigned(0, wFO - 1)) & exnX(0);                                                                      --+/- infty or NaN
                expR := (wEO - 1 downto 0 => '0') when (exnX = "00") else
                expX when (exnX = "01") else
                (wEO - 1 downto 0 => '1');
                else -- wFO < wFI
                sfracX := '1' & fracX(wFI - 1 downto 1) when (expZero = '1' and exnX = "01") else
                fracX;
                -- wFO < wFI, need to round fraction
                resultLSB := sfracX(wFI - wFO);
                roundBit  := sfracX(wFI - wFO - 1);
                if wFI - wFO > 1 then
                    sticky := '0' when sfracX(wFI - wFO - 2 downto 0) = std_logic_vector(to_unsigned(0, wFI - wFO - 2)) else
                    '1';
                    else
                    sticky := '0';
                end if;
                round := roundBit and (sticky or resultLSB);
                -- The following addition will not overflow since FloPoCo format has one more exponent value
                expfracR0 := std_logic_vector(unsigned(std_logic_vector'(expX & sfracX(wFI - 1 downto wFI - wFO))) + unsigned(std_logic_vector(to_unsigned(0, wEO + wFO - 1)) & round));
                -- Note Quentin : By using "std_logic_unsigned", the previous line can be written as :
                --expfracR0 := (expX & sfracX(wFI-1 downto wFI-wFO))  +  (std_logic_vector(to_unsigned(0,wEO+wFO-1)) & round);
                fracR := (wFO - 1 downto 0 => '0') when (exnX = "00") else
                expfracR0(wFO - 1 downto 0) when (exnX = "01") else
                (wFO - 1 downto 1         => '0') & exnX(0);
                expR := (wEO - 1 downto 0 => '0') when (exnX = "00") else
                expfracR0(wFO + wEO - 1 downto wFO) when (exnX = "01") else
                (wEO - 1 downto 0 => '1');
            end if;
            else
            assert False report "OutputIEEE not yet implemented for wEI>wEO, send us a mail if you need it" severity failure;
        end if;
        R := sX & expR & fracR;
        return float32(R);
    end function;

    --* Convert to VHDL real type by first converting to float32
    function to_real(Value : scalar_t) return real is
    begin
        return to_real(to_float32(Value));
    end function;

    --* Convert from VHDL real type by first converting to float32
    function to_scalar_t(Value : real) return scalar_t is
        variable float_value       : float32 := to_float(Value);
    begin
        return to_scalar_t(float_value);
    end to_scalar_t;

    function to_string(Vec : vec_t) return string is
        variable str_i         : natural := 1;
        variable str           : string(1 to ((C_VEC_COMP_COUNT * (13 + 2) + 2)));
        type str_ptr is access string;
        variable str_tmp : str_ptr;
    begin
        str(str_i) := '(';
        str_i      := str_i + 1;
        for i in Vec'range loop
            str_tmp                                      := new string(to_string(Vec(i))'range);
            str_tmp.all                                  := to_string(Vec(i));
            str(str_i to str_i + str_tmp.all'length - 1) := str_tmp.all;
            str_i                                        := str_i + str_tmp'length;
            deallocate(str_tmp);
            if i < C_VEC_COMP_COUNT - 1 then
                str(str_i to str_i + 1) := ", ";
                str_i                   := str_i + 2;
            end if;
        end loop;
        str(str_i) := ')';
        str_i      := str_i + 1;
        return str;
    end function;

    function to_string(Vec : real_vec_t) return string is
        variable str_i         : natural := 1;
        variable str           : string(1 to ((C_VEC_COMP_COUNT * (13 + 2) + 2)));
        type str_ptr is access string;
        variable str_tmp : str_ptr;
    begin
        str(str_i) := '(';
        str_i      := str_i + 1;
        for i in Vec'range loop
            str_tmp                                      := new string(real'image(Vec(i))'range);
            str_tmp.all                                  := real'image(Vec(i));
            str(str_i to str_i + str_tmp.all'length - 1) := str_tmp.all;
            str_i                                        := str_i + str_tmp'length;
            deallocate(str_tmp);
            if i < C_VEC_COMP_COUNT - 1 then
                str(str_i to str_i + 1) := ", ";
                str_i                   := str_i + 2;
            end if;
        end loop;
        str(str_i) := ')';
        str_i      := str_i + 1;
        return str;
    end function;

    --* Helper function to convert t_grid_addr to string
    function to_string(addr : t_grid_addr) return string is
    begin
        return string'("(" & integer'image(addr.x) & ", " & integer'image(addr.y) & ", " & integer'image(addr.z) & ")");
    end;

    function and_reduce(m : t_slv_dim3) return std_logic is
        variable temp         : std_logic;
    begin
        temp := '1';
        for x in 0 to (c_GRID_X_DIM - 1) loop
            for y in 0 to (c_GRID_Y_DIM - 1) loop
                for z in 0 to (c_GRID_Z_DIM - 1) loop
                    temp := temp and m(x)(y)(z);
                end loop;
            end loop;
        end loop;
        return temp;
    end;

    function or_reduce(m : t_slv_dim3) return std_logic is
        variable temp        : std_logic;
    begin
        temp := '0';
        for x in 0 to (c_GRID_X_DIM - 1) loop
            for y in 0 to (c_GRID_Y_DIM - 1) loop
                for z in 0 to (c_GRID_Z_DIM - 1) loop
                    temp := temp or m(x)(y)(z);
                end loop;
            end loop;
        end loop;
        return temp;
    end;

    --* Helper function to convert t_diffusion_type to string
    function to_string(diff_type : t_diffusion_type) return string is
    begin
        case diff_type is
            when DIFF_XYZ => return "DIFF_XYZ";
            when DIFF_YXZ => return "DIFF_YXZ";
            when DIFF_ZXY => return "DIFF_ZXY";
            when DIFF_ZYX => return "DIFF_ZYX";
            when DIFF_YZX => return "DIFF_YZX";
            when DIFF_XZY => return "DIFF_XZY";
        end case;
    end;

    function get_next_diff_mecha(
        diff_type : t_diffusion_type
    ) return t_diffusion_type is
    begin
        case diff_type is
            when DIFF_XYZ => return DIFF_YXZ;
            when DIFF_YXZ => return DIFF_ZXY;
            when DIFF_ZXY => return DIFF_ZYX;
            when DIFF_ZYX => return DIFF_YZX;
            when DIFF_YZX => return DIFF_XZY;
            when DIFF_XZY => return DIFF_XYZ;
        end case;
    end;

    function get_forwarding_directions(
        diff_type : t_diffusion_type;
        input_dir : t_direction
    ) return t_direction is
    begin
        case diff_type is
            when DIFF_XYZ =>
                case input_dir is
                    when c_DIR_EAST =>
                        return c_DIR_WEST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_WEST =>
                        return c_DIR_EAST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_NORTH =>
                        return c_DIR_SOUTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_SOUTH =>
                        return c_DIR_NORTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_UP =>
                        return c_DIR_DOWN;
                    when c_DIR_DOWN =>
                        return c_DIR_UP;
                    when others =>
                        return c_DIR_NONE;
                end case;
            when DIFF_YXZ =>
                case input_dir is
                    when c_DIR_EAST =>
                        return c_DIR_WEST or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_WEST =>
                        return c_DIR_EAST or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_NORTH =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_SOUTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_SOUTH =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_NORTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_UP =>
                        return c_DIR_DOWN;
                    when c_DIR_DOWN =>
                        return c_DIR_UP;
                    when others =>
                        return c_DIR_NONE;
                end case;
            when DIFF_ZXY =>
                case input_dir is
                    when c_DIR_EAST =>
                        return c_DIR_WEST or c_DIR_NORTH or c_DIR_SOUTH;
                    when c_DIR_WEST =>
                        return c_DIR_EAST or c_DIR_NORTH or c_DIR_SOUTH;
                    when c_DIR_NORTH =>
                        return c_DIR_SOUTH;
                    when c_DIR_SOUTH =>
                        return c_DIR_NORTH;
                    when c_DIR_UP =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_DOWN;
                    when c_DIR_DOWN =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_UP;
                    when others =>
                        return c_DIR_NONE;
                end case;
            when DIFF_ZYX =>
                case input_dir is
                    when c_DIR_EAST =>
                        return c_DIR_WEST;
                    when c_DIR_WEST =>
                        return c_DIR_EAST;
                    when c_DIR_NORTH =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_SOUTH;
                    when c_DIR_SOUTH =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_NORTH;
                    when c_DIR_UP =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_DOWN;
                    when c_DIR_DOWN =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_UP;
                    when others =>
                        return c_DIR_NONE;
                end case;
            when DIFF_YZX =>
                case input_dir is
                    when c_DIR_EAST =>
                        return c_DIR_WEST;
                    when c_DIR_WEST =>
                        return c_DIR_EAST;
                    when c_DIR_NORTH =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_SOUTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_SOUTH =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_NORTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_UP =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_DOWN;
                    when c_DIR_DOWN =>
                        return c_DIR_EAST or c_DIR_WEST or c_DIR_UP;
                    when others =>
                        return c_DIR_NONE;
                end case;
            when DIFF_XZY =>
                case input_dir is
                    when c_DIR_EAST =>
                        return c_DIR_WEST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_WEST =>
                        return c_DIR_EAST or c_DIR_NORTH or c_DIR_SOUTH or c_DIR_UP or c_DIR_DOWN;
                    when c_DIR_NORTH =>
                        return c_DIR_SOUTH;
                    when c_DIR_SOUTH =>
                        return c_DIR_NORTH;
                    when c_DIR_UP =>
                        return c_DIR_NORTH or c_DIR_SOUTH or c_DIR_DOWN;
                    when c_DIR_DOWN =>
                        return c_DIR_NORTH or c_DIR_SOUTH or c_DIR_UP;
                    when others =>
                        return c_DIR_NONE;
                end case;
        end case;
    end;

    function get_opposite_direction(input_dir : t_direction) return t_direction is
    begin
        case input_dir is
            when c_DIR_EAST =>
                return c_DIR_WEST;
            when c_DIR_WEST =>
                return c_DIR_EAST;
            when c_DIR_NORTH =>
                return c_DIR_SOUTH;
            when c_DIR_SOUTH =>
                return c_DIR_NORTH;
            when c_DIR_UP =>
                return c_DIR_DOWN;
            when c_DIR_DOWN =>
                return c_DIR_UP;
            when others =>
                return c_DIR_NONE;
        end case;
    end;

    function dir_in_list(
        dirs_list : t_direction;
        dir       : t_direction
    ) return boolean is
        variable temp : t_direction;
    begin
        for I in t_direction'range loop
            temp(I) := dir(I) and dirs_list(I);
        end loop;
        if temp = dir then
            return true;
            else
            return false;
        end if;
    end;

    function dir_to_string(
        dir : t_direction
    ) return string is
        variable temp_str : string (t_direction'range);
    begin
        case dir is
            when c_DIR_EAST =>
                return "EST";
            when c_DIR_WEST =>
                return "WEST";
            when c_DIR_NORTH =>
                return "NORTH";
            when c_DIR_SOUTH =>
                return "SOUTH";
            when c_DIR_UP =>
                return "UP";
            when c_DIR_DOWN =>
                return "DOWN";
            when c_DIR_ALL =>
                return "ALL";
            when c_DIR_NONE =>
                return "NONE";
            when others =>
                for I in t_direction'range loop
                    case dir(I) is
                        when '0'    => temp_str(I)    := '0';
                        when '1'    => temp_str(I)    := '1';
                        when 'U'    => temp_str(I)    := 'U';
                        when 'X'    => temp_str(I)    := 'X';
                        when 'Z'    => temp_str(I)    := 'Z';
                        when '-'    => temp_str(I)    := '-';
                        when others => temp_str(I) := '?';
                    end case;
                end loop;
                return "INVALID: " & temp_str;
        end case;
    end;

    function to_vec_t(vec : real_vec_t) return vec_t is
        variable vec_out      : vec_t;
    begin
        for I in vec'range loop
            vec_out(I) := to_scalar_t(vec(I));
        end loop;
        return vec_out;
    end;

    function to_real_vec_t(vec : vec_t) return real_vec_t is
        variable vec_out           : real_vec_t;
    begin
        for I in vec'range loop
            vec_out(I) := to_real(vec(I));
        end loop;
        return vec_out;
    end;

    procedure randomize_vect(variable seed1, seed2 : inout integer; variable vec : out vec_t) is
        variable rand : real;
    begin
        for i in 0 to vec'length - 1 loop
            uniform(seed1, seed2, rand); -- generate random number
            vec(i) := to_scalar_t(rand);
        end loop;
    end;

    procedure randomize_vect(variable seed1, seed2 : inout integer; variable vec : out real_vec_t) is
        variable rand : real;
    begin
        for i in 0 to vec'length - 1 loop
            uniform(seed1, seed2, rand); -- generate random number
            vec(i) := rand;
        end loop;
    end;

    function are_relatively_equal(
        a, b    : real_vec_t;
        epsilon : real := 1.0e-7
    ) return boolean is
        variable temp_a, temp_b : real;
    begin
        for I in a'range loop
            temp_a := a(I);
            temp_b := b(I);
            if not are_relatively_equal(temp_a, temp_b, epsilon) then
                return false;
            end if;
        end loop;
        return true;
    end function;

    -- Synthisable comparison operators

    function scalar_lt (
        l, r : scalar_t -- scalar input
    )
        return boolean is
        constant c_zero               : std_logic_vector(1 downto 0) := "00";
        constant c_normal             : std_logic_vector(1 downto 0) := "01";
        constant c_inf                : std_logic_vector(1 downto 0) := "10";
        constant c_nan                : std_logic_vector(1 downto 0) := "11";
        variable lex, rex             : std_logic_vector(1 downto 0);
        variable lsign, rsign         : std_logic;
        variable lexponent, rexponent : unsigned(wE - 1 downto 0);
        variable lmantissa, rmantissa : unsigned(wF - 1 downto 0);
    begin
        lex       := l(l'left downto l'left - 1);
        rex       := r(r'left downto r'left - 1);
        lsign     := l(l'left - 2);
        rsign     := r(r'left - 2);
        lexponent := unsigned(l(wE + wF - 1 downto wF));
        rexponent := unsigned(r(wE + wF - 1 downto wF));
        lmantissa := unsigned(l(wF - 1 downto 0));
        rmantissa := unsigned(r(wF - 1 downto 0));
        -- No check for NaN
        -- Check for inifities
        if lex = c_inf then
            if lsign = '1' then
                -- l is negative infinite
                if rex = c_inf and rsign = '1' then
                    -- r is also negative infinite
                    return false;
                end if;
                return true;
                else
                -- l is positive infinite
                return false;
            end if;
        end if;
        -- Check zero cases
        if lex = c_zero then
            if rex = c_zero then
                -- Both zero
                return false;
                elsif rsign = '0' then
                -- l zero, r positive
                return true;
                else
                -- l zero, r negative
                return false;
            end if;
        end if;
        if rex = c_zero then
            if lsign = '1' then
                -- r zero and l negative
                return true;
                else
                -- r zero and l positive
                return false;
            end if;
        end if;
        -- Check sign
        if lsign = '1' and rsign = '0' then
            -- l is negative or zero and r is positive or zero
            -- (both zero guared above)
            return true;
            elsif lsign = rsign then
            -- Compare exponent
            if lexponent < rexponent then
                return true;
                elsif lexponent = rexponent then
                -- Compare mantissa
                if lmantissa < rmantissa then
                    return true;
                end if;
            end if;
        end if;
        return false;
    end function scalar_lt;

    function scalar_gt (
        l, r : scalar_t -- scalar input
    )
        return boolean is
        constant c_zero               : std_logic_vector(1 downto 0) := "00";
        constant c_normal             : std_logic_vector(1 downto 0) := "01";
        constant c_inf                : std_logic_vector(1 downto 0) := "10";
        constant c_nan                : std_logic_vector(1 downto 0) := "11";
        variable lex, rex             : std_logic_vector(1 downto 0);
        variable lsign, rsign         : std_logic;
        variable lexponent, rexponent : unsigned(wE - 1 downto 0);
        variable lmantissa, rmantissa : unsigned(wF - 1 downto 0);
    begin
        lex       := l(l'left downto l'left - 1);
        rex       := r(r'left downto r'left - 1);
        lsign     := l(l'left - 2);
        rsign     := r(r'left - 2);
        lexponent := unsigned(l(wE + wF - 1 downto wF));
        rexponent := unsigned(r(wE + wF - 1 downto wF));
        lmantissa := unsigned(l(wF - 1 downto 0));
        rmantissa := unsigned(r(wF - 1 downto 0));
        -- No check for NaN
        -- Check for inifities
        if lex = c_inf then
            if lsign = '0' then
                -- l is positive infinite
                if rex = c_inf and rsign = '0' then
                    -- r is also positive infinite
                    return false;
                end if;
                return true;
                else
                -- l is negative infinite
                return false;
            end if;
        end if;
        -- Check zero cases
        if lex = c_zero then
            if rex = c_zero then
                -- Both zero
                return false;
                elsif rsign = '0' then
                -- l zero, r positive
                return false;
                else
                -- l zero, r negative
                return true;
            end if;
        end if;
        if rex = c_zero then
            if lsign = '1' then
                -- r zero and l negative
                return false;
                else
                -- r zero and l positive
                return true;
            end if;
        end if;
        -- Check sign
        if lsign = '0' and rsign = '1' then
            -- l is postive or zero and r is negative or zero
            -- (both zero guarded above)
            return true;
            elsif lsign = rsign then
            -- Compare exponent
            if lexponent > rexponent then
                return true;
                elsif lexponent = rexponent then
                -- Compare mantissa
                if lmantissa > rmantissa then
                    return true;
                end if;
            end if;
        end if;
        return false;
    end function scalar_gt;

    function fp_infinity(pos : boolean                           := true) return scalar_t is
        constant ex              : std_logic_vector(1 downto 0)      := "10";
        constant exponent        : std_logic_vector(wE - 1 downto 0) := (others => '0');
        constant mantissa        : std_logic_vector(wF - 1 downto 0) := (others => '0');
        variable sign            : std_logic;
        variable res             : std_logic_vector(wE + wF + 2 downto 0);
    begin
        if pos then
            sign := '0';
            else
            sign := '1';
        end if;
        res := ex & sign & exponent & mantissa;
        return scalar_t(res);
    end;

    procedure linear_to_3d(
        idx   : in integer;
        x     : out integer;
        y     : out integer;
        z     : out integer;
        x_dim : in integer := c_GRID_X_DIM;
        y_dim : in integer := c_GRID_Y_DIM;
        z_dim : in integer := c_GRID_Z_DIM
    ) is
        variable i : integer;
    begin
        i := idx;
        z := i / (x_dim * y_dim);
        i := i - (z * x_dim * y_dim);
        y := i / x_dim;
        x := i mod x_dim;
        --assert false report "idx: " & integer'image(idx) & " -> (" & integer'image(x) & "," & integer'image(y)& "," & integer'image(z) & ")" severity note;
    end;

end package body csom_pkg;