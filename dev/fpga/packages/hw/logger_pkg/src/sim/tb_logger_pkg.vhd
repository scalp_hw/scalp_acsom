----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_logger_pkg - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for package logger_pkg
--
-- Last update: 2021-03-15 14:34:14
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_logger_pkg is
end tb_logger_pkg;


architecture behavioral of tb_logger_pkg is

begin

end behavioral;
