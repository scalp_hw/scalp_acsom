#!/bin/sh

##################################################################################
#                                 _             _
#                                | |_  ___ _ __(_)__ _
#                                | ' \/ -_) '_ \ / _` |
#                                |_||_\___| .__/_\__,_|
#                                         |_|
#
##################################################################################
#
# Company: hepia
# Author: Quentin Berthet <quentin.berthet@hesge.ch>
#
# Project Name: csom_grid_axi_wrapper
# Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
# Tool version: 2020.2
# Description: Run simulation
#
# Last update: 2021/07/12 16:24:05
#
##################################################################################

PRJ_DIR=..

if [ "$#" -ne 5 ]; then
    echo "Illegal number of parameters"
    echo "Expecting 5 argument: <alpha> <eta> <dumpfilename> <initfilename> <inputfilename>"
    exit
fi

echo "> Running simulation..."
vivado -nojournal -nolog -mode tcl -source ./run_sim_csom_grid_axi_wrapper.tcl -notrace -tclargs "$1" "$2" "$3" "$4" "$5"

echo "> Done"
