#!/bin/sh

##################################################################################
#                                 _             _
#                                | |_  ___ _ __(_)__ _
#                                | ' \/ -_) '_ \ / _` |
#                                |_||_\___| .__/_\__,_|
#                                         |_|
#
##################################################################################
#
# Company: hepia
# Author: Quentin Berthet <quentin.berthet@hesge.ch>
#
# Project Name: csom_grid_axi_wrapper
# Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
# Tool version: 2020.2
# Description: Run synthesis
#
# Last update: 2021/07/08 15:27:42
#
##################################################################################

PRJ_DIR=..

echo "> Running synthesis..."
vivado -nojournal -nolog -mode tcl -source ./run_synth_csom_grid_axi_wrapper.tcl -notrace

echo "> Done"
