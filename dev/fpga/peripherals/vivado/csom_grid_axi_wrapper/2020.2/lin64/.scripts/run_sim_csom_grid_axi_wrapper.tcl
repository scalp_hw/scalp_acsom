#!/usr/bin/tcl

##################################################################################
#                                 _             _
#                                | |_  ___ _ __(_)__ _
#                                | ' \/ -_) '_ \ / _` |
#                                |_||_\___| .__/_\__,_|
#                                         |_|
#
##################################################################################
#
# Company: hepia
# Author: Quentin Berthet <quentin.berthet@hesge.ch>
#
# Project Name: csom_grid_axi_wrapper
# Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
# Tool version: 2020.2
# Description: TCL script used to run simulation
#
# Last update: 2021/07/12 16:24:01
#
##################################################################################

source utils.tcl

set PRJ_DIR ".."
set prj_name "csom_grid_axi_wrapper"

# Create a variable to store the start time
set start_time [clock format [clock seconds] -format {%b. %d, %Y %I:%M:%S %p}]

# Open the project
open_project -verbose ${PRJ_DIR}/$prj_name/$prj_name.xpr
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1
print_status "Open project $prj_name" "OK"

print_status "Run simulation" "IN_PROGRESS"

# launch_simulation always return success when run with -quiet
set_property top tb_csom_grid_axi_wrapper [get_filesets sim_1]
set_property top_lib xil_defaultlib [get_filesets sim_1]

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

reorder_files -fileset sim_1 -auto -disable_unused -verbose

# Set simulation time
set_property -name {xsim.simulate.runtime} -value {-all} -objects [get_filesets sim_1]

## Check that we got the correct argument count
if { [llength $argv] < 5} {
    puts "Error: not enough argument"
    puts "Expecting 5 argument: <alpha> <eta> <dumpfilename>"
    exit 2
}

set alpha [lindex $argv 0]
set eta [lindex $argv 1]
set dump_file_name [lindex $argv 2]
set init_file_name [lindex $argv 3]
set input_file_name [lindex $argv 4]

set generic_str "g_ALPHA=${alpha} g_ETA=${eta} g_DUMP_FILE=${dump_file_name} g_INIT_FILE=${init_file_name} g_INPUT_FILE=${input_file_name}"

set_property generic $generic_str [get_filesets sim_1]
if { [catch {launch_simulation -step all -simset sim_1 -mode behavioral} fid] } {
    puts stderr "Testbench failed: $fid"
    close_project
    exit 1
}

# Set the completion time
set end_time [clock format [clock seconds] -format {%b. %d, %Y %I:%M:%S %p}]

# Display the start and end time on the screen
puts $start_time
puts $end_time

exit
