/*********************************************************************************
 *                                 _             _
 *                                | |_  ___ _ __(_)__ _
 *                                | ' \/ -_) '_ \ / _` |
 *                                |_||_\___| .__/_\__,_|
 *                                         |_|
 *
 *********************************************************************************
 *
 * Company: hepia
 * Author: Quentin Berthet <quentin.berthet@hesge.ch>
 *
 * Project Name: csom_unit_axi_wrapper
 * Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
 * Tool version: 2020.2
 * Description: csom_unit_axi_wrapper software driver source file
 *
 * Last update: 2021-05-05 17:58:47
 *
 ********************************************************************************/

#include "csom_unit_axi_wrapper.h"

