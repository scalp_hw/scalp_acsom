----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: csom_unit_axi_wrapper - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: AXI4-Lite registers for csom_unit_axi_wrapper
--
-- Last update: 2021/11/16 13:31:31
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;

entity csom_unit_axi_wrapper is
    generic (
        -- Users to add parameters here

        -- User parameters ends

        -- Parameters of Axi Slave Bus Interface S00_AXI
        C_S00_AXI_DATA_WIDTH : integer := 32;
        C_S00_AXI_ADDR_WIDTH : integer := 12
    );
    port (
        -- Users to add ports here

        -- User ports ends

        -- Ports of Axi Slave Bus Interface S00_AXI
        s00_axi_aclk    : in std_logic;
        s00_axi_aresetn : in std_logic;
        s00_axi_awaddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
        s00_axi_awprot  : in std_logic_vector(2 downto 0);
        s00_axi_awvalid : in std_logic;
        s00_axi_awready : out std_logic;
        s00_axi_wdata   : in std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
        s00_axi_wstrb   : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8) - 1 downto 0);
        s00_axi_wvalid  : in std_logic;
        s00_axi_wready  : out std_logic;
        s00_axi_bresp   : out std_logic_vector(1 downto 0);
        s00_axi_bvalid  : out std_logic;
        s00_axi_bready  : in std_logic;
        s00_axi_araddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
        s00_axi_arprot  : in std_logic_vector(2 downto 0);
        s00_axi_arvalid : in std_logic;
        s00_axi_arready : out std_logic;
        s00_axi_rdata   : out std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
        s00_axi_rresp   : out std_logic_vector(1 downto 0);
        s00_axi_rvalid  : out std_logic;
        s00_axi_rready  : in std_logic
    );
end csom_unit_axi_wrapper;

architecture arch of csom_unit_axi_wrapper is

    -- component declaration
    component csom_unit_axi_wrapper_S00_AXI is
        generic (
            C_S_AXI_DATA_WIDTH : integer := 32;
            C_S_AXI_ADDR_WIDTH : integer := 12
        );
        port (
            -- Users to add ports here

            ready_i           : in std_logic;    -- Register Status
            dist_to_x_valid_i : in std_logic;    -- Register Status
            is_bmu_ack_i      : in std_logic;    -- Register Status
            x_valid_o         : out std_logic;   -- Register Control
            m_valid_o         : out std_logic;   -- Register Control
            nm_valid_o        : out std_logic;   -- Register Control
            init_weight_o     : out std_logic;   -- Register Control
            is_bmu_s          : out std_logic;   -- Register Control
            addr_o            : out t_grid_addr; -- Register Unit Grid Address
            bmu_addr_o        : out t_grid_addr; -- Register BMU Grid Address
            alpha_o           : out scalar_t;    -- Register Alpha
            eta_prime_o       : out scalar_t;    -- Register Eta prime
            dist_to_x_i       : in scalar_t;     -- Register Distance
            vec_i             : in vec_t;        -- Registers Vector
            vec_o             : out vec_t;       -- Registers Vector

            -- User ports ends

            -- Ports of Axi Slave Bus Interface S00_AXI
            S_AXI_ACLK    : in std_logic;
            S_AXI_ARESETN : in std_logic;
            S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
            S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
            S_AXI_AWVALID : in std_logic;
            S_AXI_AWREADY : out std_logic;
            S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
            S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8) - 1 downto 0);
            S_AXI_WVALID  : in std_logic;
            S_AXI_WREADY  : out std_logic;
            S_AXI_BRESP   : out std_logic_vector(1 downto 0);
            S_AXI_BVALID  : out std_logic;
            S_AXI_BREADY  : in std_logic;
            S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
            S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
            S_AXI_ARVALID : in std_logic;
            S_AXI_ARREADY : out std_logic;
            S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
            S_AXI_RRESP   : out std_logic_vector(1 downto 0);
            S_AXI_RVALID  : out std_logic;
            S_AXI_RREADY  : in std_logic
        );
    end component csom_unit_axi_wrapper_S00_AXI;

    component csom_unit is
        port (
            clk_i             : in std_logic;
            rst_i             : in std_logic;
            addr_i            : in t_grid_addr; -- Unit grid address
            ready_o           : out std_logic;  -- Unit ready
            vec_i             : in vec_t;       -- Input vector, content depend on one of the valid flags
            x_valid_i         : in std_logic;   -- vec_i contains a new input vector and is valid
            m_valid_i         : in std_logic;   -- vec_i contains a weight and is valid
            nm_valid_i        : in std_logic;   -- vec_i contains a neighbor weight and is valid
            dist_to_x_o       : out scalar_t;   -- Output distance to current input
            dist_to_x_valid_o : out std_logic;  -- Distance to current input valid
            alpha_i           : in scalar_t;    -- Current learning rate
            eta_prime_i       : in scalar_t;    -- Current eta_prime (-(sqrt(d)/eta))
            is_bmu_i          : in std_logic;   -- Notification that this unit is the BMU for current input
            is_bmu_ack_o      : out std_logic;  -- Acknowledge that weight have been updated as BMU
            bmu_addr_i        : in t_grid_addr; -- Grid address of BMU
            init_weight_i     : in std_logic;   -- Unused
            vec_o             : out vec_t       -- Output vector, currently a copy of internal weight
        );
    end component;

    --
    --- unit input ---
    signal vec_in_s      : vec_t;
    signal addr_s        : t_grid_addr;
    signal x_valid_s     : std_logic;
    signal m_valid_s     : std_logic;
    signal nm_valid_s    : std_logic;
    signal alpha_s       : scalar_t;
    signal eta_prime_s   : scalar_t;
    signal is_bmu_s      : std_logic;
    signal init_weight_s : std_logic;
    signal bmu_addr_s    : t_grid_addr;

    --- unit output ---
    signal ready_s           : std_logic;
    signal dist_to_x_s       : scalar_t;
    signal dist_to_x_valid_s : std_logic;
    signal is_bmu_ack_s      : std_logic;
    signal vec_out_s         : vec_t;

begin

    -- Instantiation of Axi Bus Interface S00_AXI
    csom_unit_axi_wrapper_S00_AXI_inst : csom_unit_axi_wrapper_S00_AXI
    generic map(
        C_S_AXI_DATA_WIDTH => C_S00_AXI_DATA_WIDTH,
        C_S_AXI_ADDR_WIDTH => C_S00_AXI_ADDR_WIDTH
    )
    port map(
        -- Users to add ports here

        ready_i           => ready_s,
        dist_to_x_valid_i => dist_to_x_valid_s,
        is_bmu_ack_i      => is_bmu_ack_s,
        x_valid_o         => x_valid_s,
        m_valid_o         => m_valid_s,
        nm_valid_o        => nm_valid_s,
        init_weight_o     => init_weight_s,
        is_bmu_s          => is_bmu_s,
        addr_o            => addr_s,
        bmu_addr_o        => bmu_addr_s,
        alpha_o           => alpha_s,
        eta_prime_o       => eta_prime_s,
        dist_to_x_i       => dist_to_x_s,
        vec_i             => vec_out_s,
        vec_o             => vec_in_s,

        -- User ports ends

        -- Ports of Axi Slave Bus Interface S00_AXI
        S_AXI_ACLK    => s00_axi_aclk,
        S_AXI_ARESETN => s00_axi_aresetn,
        S_AXI_AWADDR  => s00_axi_awaddr,
        S_AXI_AWPROT  => s00_axi_awprot,
        S_AXI_AWVALID => s00_axi_awvalid,
        S_AXI_AWREADY => s00_axi_awready,
        S_AXI_WDATA   => s00_axi_wdata,
        S_AXI_WSTRB   => s00_axi_wstrb,
        S_AXI_WVALID  => s00_axi_wvalid,
        S_AXI_WREADY  => s00_axi_wready,
        S_AXI_BRESP   => s00_axi_bresp,
        S_AXI_BVALID  => s00_axi_bvalid,
        S_AXI_BREADY  => s00_axi_bready,
        S_AXI_ARADDR  => s00_axi_araddr,
        S_AXI_ARPROT  => s00_axi_arprot,
        S_AXI_ARVALID => s00_axi_arvalid,
        S_AXI_ARREADY => s00_axi_arready,
        S_AXI_RDATA   => s00_axi_rdata,
        S_AXI_RRESP   => s00_axi_rresp,
        S_AXI_RVALID  => s00_axi_rvalid,
        S_AXI_RREADY  => s00_axi_rready
    );

    -- Add user logic here

    b_csom_unit : block

        signal unit_clk : std_logic;
        signal unit_rst : std_logic;

    begin

        unit_clk <= s00_axi_aclk;
        unit_rst <= not s00_axi_aresetn;

        i_csom_unit : csom_unit
        port map(
            clk_i             => unit_clk,
            rst_i             => unit_rst,
            addr_i            => addr_s,            -- Unit grid addr
            ready_o           => ready_s,           -- Unit is ready
            vec_i             => vec_in_s,          -- Input vector
            x_valid_i         => x_valid_s,         -- vec_i contains a new input vector and is valid
            m_valid_i         => m_valid_s,         -- vec_i contains a weight and is valid
            nm_valid_i        => nm_valid_s,        -- vec_i contains a neighbor weight and is valid
            dist_to_x_o       => dist_to_x_s,       -- Output distance to current input
            dist_to_x_valid_o => dist_to_x_valid_s, -- Distance to current input valid
            alpha_i           => alpha_s,           -- Current learning rate
            eta_prime_i       => eta_prime_s,       -- Elasticity
            is_bmu_i          => is_bmu_s,          -- Notification that this unit is the BMU for current input
            is_bmu_ack_o      => is_bmu_ack_s,      --
            bmu_addr_i        => bmu_addr_s,        -- BMU grid address
            init_weight_i     => init_weight_s,     -- Unused
            vec_o             => vec_out_s          -- Weight vector
        );

    end block b_csom_unit;

    -- User logic ends

end arch;
