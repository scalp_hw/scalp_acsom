----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_csom_unit_axi_wrapper - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for csom_unit_axi_wrapper
--
-- Last update: 2021/11/16 13:30:51
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

library std;
use std.env.all;

library work;
use work.util_pkg.all;
use work.float_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

entity tb_csom_unit_axi_wrapper is
    generic (
        CLK_PERIOD : time := 8 ns -- clock period for clk
    );
end tb_csom_unit_axi_wrapper;

architecture behavioral of tb_csom_unit_axi_wrapper is

    ----------------------------------------------------------------------------
    -- Constants
    ----------------------------------------------------------------------------
    constant c_DUT_NAME  : string := "csom_unit_axi_wrapper";
    constant PCLK_PERIOD : time   := CLK_PERIOD;
    constant c_EPSILON   : real   := 1.0e-6;

    ----------------------------------------------------------------------------
    -- AXI Constants
    ----------------------------------------------------------------------------
    constant C_S00_AXI_DATA_WIDTH : integer := 32;
    constant C_S00_AXI_ADDR_WIDTH : integer := 12;

    component csom_unit_axi_wrapper is
        generic (
            C_S00_AXI_DATA_WIDTH : integer;
            C_S00_AXI_ADDR_WIDTH : integer
        );
        port (
            s00_axi_aclk    : in std_logic;
            s00_axi_aresetn : in std_logic;
            s00_axi_awaddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
            s00_axi_awprot  : in std_logic_vector(2 downto 0);
            s00_axi_awvalid : in std_logic;
            s00_axi_awready : out std_logic;
            s00_axi_wdata   : in std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
            s00_axi_wstrb   : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8) - 1 downto 0);
            s00_axi_wvalid  : in std_logic;
            s00_axi_wready  : out std_logic;
            s00_axi_bresp   : out std_logic_vector(1 downto 0);
            s00_axi_bvalid  : out std_logic;
            s00_axi_bready  : in std_logic;
            s00_axi_araddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
            s00_axi_arprot  : in std_logic_vector(2 downto 0);
            s00_axi_arvalid : in std_logic;
            s00_axi_arready : out std_logic;
            s00_axi_rdata   : out std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
            s00_axi_rresp   : out std_logic_vector(1 downto 0);
            s00_axi_rvalid  : out std_logic;
            s00_axi_rready  : in std_logic
        );
    end component csom_unit_axi_wrapper;

    signal s00_axi_aclk    : std_logic := '1';
    signal s00_axi_aresetn : std_logic := '0';

    signal s00_axi_awaddr  : std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0) := (others => '0');
    signal s00_axi_awprot  : std_logic_vector(2 downto 0)                        := (others => '0');
    signal s00_axi_awvalid : std_logic                                           := '0';
    signal s00_axi_awready : std_logic;
    signal s00_axi_wdata   : std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0)     := (others => '0');
    signal s00_axi_wstrb   : std_logic_vector((C_S00_AXI_DATA_WIDTH/8) - 1 downto 0) := (others => '0');
    signal s00_axi_wvalid  : std_logic                                               := '0';
    signal s00_axi_wready  : std_logic;
    signal s00_axi_bresp   : std_logic_vector(1 downto 0);
    signal s00_axi_bvalid  : std_logic;
    signal s00_axi_bready  : std_logic                                           := '0';
    signal s00_axi_araddr  : std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0) := (others => '0');
    signal s00_axi_arprot  : std_logic_vector(2 downto 0)                        := (others => '0');
    signal s00_axi_arvalid : std_logic                                           := '0';
    signal s00_axi_arready : std_logic;
    signal s00_axi_rdata   : std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
    signal s00_axi_rresp   : std_logic_vector(1 downto 0);
    signal s00_axi_rvalid  : std_logic;
    signal s00_axi_rready  : std_logic := '0';

    constant s00_axi_aclk_period : time := 10 ns;

    -- Registers offset
    constant INFO_REG_OFFSET      : integer := 0;    -- register 0
    constant STATUS_REG_OFFSET    : integer := 4;    -- register 1
    constant CTRL_INFO_REG_OFFSET : integer := 8;    -- register 2
    constant ALPHA_REG_OFFSET     : integer := 12;   -- register 3
    constant ETA_PRIME_REG_OFFSET : integer := 16;   -- register 4
    constant UNIT_ADDR_REG_OFFSET : integer := 20;   -- register 5
    constant BMU_ADDR_REG_OFFSET  : integer := 24;   -- register 6
    constant DISTANCE_REG_OFFSET  : integer := 28;   -- register 7
    constant VECTOR_REG_BASE      : integer := 2048; -- vector registers base

    -- Data read from registers
    signal rd_data : std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0) := (others => '0');

begin

    csom_unit_axi_wrapper_i : entity work.csom_unit_axi_wrapper
        generic map(
            C_S00_AXI_DATA_WIDTH => C_S00_AXI_DATA_WIDTH,
            C_S00_AXI_ADDR_WIDTH => C_S00_AXI_ADDR_WIDTH
        )
        port map(
            s00_axi_aclk    => s00_axi_aclk,
            s00_axi_aresetn => s00_axi_aresetn,
            s00_axi_awaddr  => s00_axi_awaddr,
            s00_axi_awprot  => s00_axi_awprot,
            s00_axi_awvalid => s00_axi_awvalid,
            s00_axi_awready => s00_axi_awready,
            s00_axi_wdata   => s00_axi_wdata,
            s00_axi_wstrb   => s00_axi_wstrb,
            s00_axi_wvalid  => s00_axi_wvalid,
            s00_axi_wready  => s00_axi_wready,
            s00_axi_bresp   => s00_axi_bresp,
            s00_axi_bvalid  => s00_axi_bvalid,
            s00_axi_bready  => s00_axi_bready,
            s00_axi_araddr  => s00_axi_araddr,
            s00_axi_arprot  => s00_axi_arprot,
            s00_axi_arvalid => s00_axi_arvalid,
            s00_axi_arready => s00_axi_arready,
            s00_axi_rdata   => s00_axi_rdata,
            s00_axi_rresp   => s00_axi_rresp,
            s00_axi_rvalid  => s00_axi_rvalid,
            s00_axi_rready  => s00_axi_rready
        );

    s00_axi_aclk <= not s00_axi_aclk after s00_axi_aclk_period / 2;

    waveform_proc : process
        variable COMP_COUNT    : natural;
        variable real_value    : real;
        variable float32_value : float32;

        ---------------------------------------------------------------------------------
        -- Write a value in an AXI register
        ---------------------------------------------------------------------------------
        procedure write_reg(
            reg_offset : integer;
            wr_data    : std_logic_vector((C_S00_AXI_DATA_WIDTH - 1) downto 0)
        ) is
        begin

            s00_axi_awaddr  <= std_logic_vector(to_unsigned(reg_offset, C_S00_AXI_ADDR_WIDTH));
            s00_axi_awvalid <= '1';
            s00_axi_wdata   <= wr_data;
            s00_axi_wvalid  <= '1';
            s00_axi_wstrb   <= (others => '1');
            if s00_axi_awready = '0' then
                wait until s00_axi_awready = '1';
            end if;
            if s00_axi_wready = '0' then
                wait until s00_axi_wready = '1';
            end if;
            wait for s00_axi_aclk_period;
            s00_axi_wvalid  <= '0';
            s00_axi_awvalid <= '0';

            if s00_axi_bvalid = '0' then
                wait until s00_axi_bvalid = '1';
            end if;
            wait for s00_axi_aclk_period;
            s00_axi_bready <= '1';
            wait for s00_axi_aclk_period;
            s00_axi_bready <= '0';

        end procedure write_reg;

        ---------------------------------------------------------------------------------
        -- Read a value from an AXI register
        ---------------------------------------------------------------------------------
        procedure read_reg(
            reg_offset     : in integer;
            signal rd_data : out std_logic_vector((C_S00_AXI_DATA_WIDTH - 1) downto 0)
        ) is
        begin

            s00_axi_araddr  <= std_logic_vector(to_unsigned(reg_offset, C_S00_AXI_ADDR_WIDTH));
            s00_axi_arvalid <= '1';
            if s00_axi_arready = '0' then
                wait until s00_axi_arready = '1';
            end if;
            if s00_axi_rvalid = '0' then
                wait until s00_axi_rvalid = '1';
            end if;
            wait for s00_axi_aclk_period;
            s00_axi_arvalid <= '0';
            s00_axi_rready  <= '1';
            rd_data         <= s00_axi_rdata;
            wait for s00_axi_aclk_period;
            s00_axi_rready <= '0';

        end procedure read_reg;

    begin
        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_" & c_DUT_NAME & "_report.html",
        "Testbench report: " & "tb_" & c_DUT_NAME,
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        --logger.log_note("Scalar representation: wE: " & integer'image(wE) & ", wF: " & integer'image(wF));
        --logger.log_note("Vector component count: C_VEC_COMP_COUNT: " & integer'image(C_VEC_COMP_COUNT));
        --logger.log_note("Epsilon: " & to_string(to_scalar_t(c_EPSILON)));

        ------------------------------------------------------------------------

        -- hold reset
        s00_axi_aresetn <= '0';
        wait for s00_axi_aclk_period * 10;
        s00_axi_aresetn <= '1';
        wait for s00_axi_aclk_period * 20;

        ------------------------------------------------------------------------

        read_reg(INFO_REG_OFFSET, rd_data);
        logger.log_note("Read info register: " & to_hex_string(rd_data));
        COMP_COUNT := to_integer(unsigned(rd_data(15 downto 0)));
        wait for s00_axi_aclk_period * 40;

        read_reg(STATUS_REG_OFFSET, rd_data);
        logger.log_note("Read status register: " & to_hex_string(rd_data));
        wait for s00_axi_aclk_period * 40;

        read_reg(CTRL_INFO_REG_OFFSET, rd_data);
        logger.log_note("Read control/info register: " & to_hex_string(rd_data));
        wait for s00_axi_aclk_period * 40;

        read_reg(DISTANCE_REG_OFFSET, rd_data);
        logger.log_note("Read distance register: " & to_hex_string(rd_data));
        wait for s00_axi_aclk_period * 40;

        ------------------------------------------------------------------------

        real_value    := 2.7;
        float32_value := to_float(real_value);
        logger.log_note("Write alpha register: " & real'image(real_value));
        write_reg(ALPHA_REG_OFFSET, std_logic_vector(float32_value));
        wait for s00_axi_aclk_period * 40;

        real_value    := - 454.6548;
        float32_value := to_float(real_value);
        logger.log_note("Write eta prime register: " & real'image(real_value));
        write_reg(ETA_PRIME_REG_OFFSET, std_logic_vector(float32_value));
        wait for s00_axi_aclk_period * 40;

        logger.log_note("Write unit addr register");
        write_reg(UNIT_ADDR_REG_OFFSET, X"00030201");
        wait for s00_axi_aclk_period * 40;

        logger.log_note("Write bmu addr register");
        write_reg(BMU_ADDR_REG_OFFSET, X"00010203");
        wait for s00_axi_aclk_period * 40;

        ------------------------------------------------------------------------

        read_reg(ALPHA_REG_OFFSET, rd_data);
        float32_value := float32(rd_data);
        real_value    := to_real(float32_value);
        logger.log_note("Read alpha register: " & real'image(real_value));
        wait for s00_axi_aclk_period * 40;

        read_reg(ETA_PRIME_REG_OFFSET, rd_data);
        float32_value := float32(rd_data);
        real_value    := to_real(float32_value);
        logger.log_note("Read eta register: " & real'image(real_value));
        wait for s00_axi_aclk_period * 40;

        ------------------------------------------------------------------------
        --  - bit  [0] : unused
        --  - bit  [1] : is BMU
        --  - bit  [2] : distance to X
        --  - bit  [3] : nm valid
        --  - bit  [4] : m valid
        --  - bit  [6] : x valid

        -- Initialization of weights
        if true then

            logger.log_note("Write control register: init weights");
            write_reg(CTRL_INFO_REG_OFFSET, X"00000001");
            wait for s00_axi_aclk_period * 100;

            for I in 0 to COMP_COUNT - 1 loop
                read_reg(VECTOR_REG_BASE + (I * 4), rd_data);
                float32_value := float32(rd_data);
                real_value    := to_real(float32_value);
                logger.log_note("Read component register " & integer'image(I) & ": " & real'image(real_value));
                wait for s00_axi_aclk_period * 100;
            end loop;

            for I in 0 to COMP_COUNT - 1 loop
                real_value    := real(I)/10;
                float32_value := to_float(real_value);
                logger.log_note("Write component register " & integer'image(I) & ": " & real'image(real_value));
                write_reg(VECTOR_REG_BASE + (I * 4), std_logic_vector(float32_value));
                wait for s00_axi_aclk_period * 100;
            end loop;

            logger.log_note("Write control register: m valid");
            write_reg(CTRL_INFO_REG_OFFSET, X"00000008");
            wait for s00_axi_aclk_period * 100;

            for I in 0 to COMP_COUNT - 1 loop
                read_reg(VECTOR_REG_BASE + (I * 4), rd_data);
                float32_value := float32(rd_data);
                real_value    := to_real(float32_value);
                logger.log_note("Read component register " & integer'image(I) & ": " & real'image(real_value));
                wait for s00_axi_aclk_period * 100;
            end loop;
        end if;

        ------------------------------------------------------------------------
        logger.log_note("Testbench ending, emptying pipeline...");
        for I in 1 to 500 loop
            wait until rising_edge(s00_axi_aclk);
        end loop;
        logger.final_report;
        logger.log_note("Testbench end");
        wait until rising_edge(s00_axi_aclk);
        stop(0);
        wait;

    end process waveform_proc;

end behavioral;