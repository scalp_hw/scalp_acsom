----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: tb_csom_grid_axi_wrapper - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: Testbench for csom_grid_axi_wrapper
--
-- Last update: 2021/07/07 13:50:47
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.float_pkg.all;
use work.csom_pkg.all;
use work.html_report_pkg.all;
use work.logger_pkg.all;
use work.project_logger_pkg.all;

library std;
use std.env.all;
use std.textio.all;

entity csom_grid_axi_wrapper_diff_test is
    generic (
        CLK_PERIOD : time := 8 ns -- clock period for clk
    );
end csom_grid_axi_wrapper_diff_test;

architecture behavioral of csom_grid_axi_wrapper_diff_test is

    ----------------------------------------------------------------------------
    -- Constants & configuration
    ----------------------------------------------------------------------------

    constant PCLK_PERIOD : time := CLK_PERIOD;

    -- DUT name string
    constant c_DUT_NAME : string := "csom_grid_axi_wrapper_diff_test";

    -- Number of input to present to the CSOM network
    constant c_TEST_INPUT_COUNT : natural := 10;

    -- Dump full network weight after each weight update
    constant c_DUMP_ALL_WEIGHT : boolean := true;

    -- Epsilon used for floating point and FP comparison
    constant c_EPSILON : real := 1.0e-6;

    -- Learning rate alpha
    constant c_ALPHA : real := real(0.9);

    -- Elasticity eta
    constant c_ETA : real := real(100.0);

    -- Project's root relative path
    --constant c_ROOT_PATH: string := "../../../../../../../../../../";
    constant c_ROOT_PATH : string := "";

    -- Data dump path
    --constant c_DUMPS_PATH : string := c_ROOT_PATH & "tb_results/"
    constant c_DUMPS_PATH : string := c_ROOT_PATH & "";

    ----------------------------------------------------------------------------
    -- AXI Constants
    ----------------------------------------------------------------------------
    constant C_S00_AXI_DATA_WIDTH : integer := 32;
    constant C_S00_AXI_ADDR_WIDTH : integer := 12;

    component csom_grid_axi_wrapper is
        generic (
            C_S00_AXI_DATA_WIDTH : integer;
            C_S00_AXI_ADDR_WIDTH : integer
        );
        port (
            s00_axi_aclk    : in std_logic;
            s00_axi_aresetn : in std_logic;
            s00_axi_awaddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
            s00_axi_awprot  : in std_logic_vector(2 downto 0);
            s00_axi_awvalid : in std_logic;
            s00_axi_awready : out std_logic;
            s00_axi_wdata   : in std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
            s00_axi_wstrb   : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8) - 1 downto 0);
            s00_axi_wvalid  : in std_logic;
            s00_axi_wready  : out std_logic;
            s00_axi_bresp   : out std_logic_vector(1 downto 0);
            s00_axi_bvalid  : out std_logic;
            s00_axi_bready  : in std_logic;
            s00_axi_araddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
            s00_axi_arprot  : in std_logic_vector(2 downto 0);
            s00_axi_arvalid : in std_logic;
            s00_axi_arready : out std_logic;
            s00_axi_rdata   : out std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
            s00_axi_rresp   : out std_logic_vector(1 downto 0);
            s00_axi_rvalid  : out std_logic;
            s00_axi_rready  : in std_logic
        );
    end component csom_grid_axi_wrapper;

    signal s00_axi_aclk    : std_logic := '1';
    signal s00_axi_aresetn : std_logic := '0';

    signal s00_axi_awaddr  : std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0) := (others => '0');
    signal s00_axi_awprot  : std_logic_vector(2 downto 0)                        := (others => '0');
    signal s00_axi_awvalid : std_logic                                           := '0';
    signal s00_axi_awready : std_logic;
    signal s00_axi_wdata   : std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0)     := (others => '0');
    signal s00_axi_wstrb   : std_logic_vector((C_S00_AXI_DATA_WIDTH/8) - 1 downto 0) := (others => '0');
    signal s00_axi_wvalid  : std_logic                                               := '0';
    signal s00_axi_wready  : std_logic;
    signal s00_axi_bresp   : std_logic_vector(1 downto 0);
    signal s00_axi_bvalid  : std_logic;
    signal s00_axi_bready  : std_logic                                           := '0';
    signal s00_axi_araddr  : std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0) := (others => '0');
    signal s00_axi_arprot  : std_logic_vector(2 downto 0)                        := (others => '0');
    signal s00_axi_arvalid : std_logic                                           := '0';
    signal s00_axi_arready : std_logic;
    signal s00_axi_rdata   : std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
    signal s00_axi_rresp   : std_logic_vector(1 downto 0);
    signal s00_axi_rvalid  : std_logic;
    signal s00_axi_rready  : std_logic := '0';

    constant s00_axi_aclk_period : time := 10 ns;

    -- Registers offset
    constant INFO_REG_OFFSET        : integer := 0;    -- register 0
    constant STATUS_REG_OFFSET      : integer := 4;    -- register 1
    constant CTRL_INFO_REG_OFFSET   : integer := 8;    -- register 2
    constant ALPHA_REG_OFFSET       : integer := 12;   -- register 3
    constant ETA_PRIME_REG_OFFSET   : integer := 16;   -- register 4
    constant ADDR_SELECT_REG_OFFSET : integer := 20;   -- register 5
    constant GRID_SIZE_REG_OFFSET   : integer := 24;   -- register 6
    constant VECTOR_REG_BASE        : integer := 2048; -- vector registers base

    -- Data read from registers
    signal rd_data : std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0) := (others => '0');

    ----------------------------------------------------------------------------

begin

    csom_grid_axi_wrapper_i : entity work.csom_grid_axi_wrapper
        generic map(
            C_S00_AXI_DATA_WIDTH => C_S00_AXI_DATA_WIDTH,
            C_S00_AXI_ADDR_WIDTH => C_S00_AXI_ADDR_WIDTH
        )
        port map(
            s00_axi_aclk    => s00_axi_aclk,
            s00_axi_aresetn => s00_axi_aresetn,
            s00_axi_awaddr  => s00_axi_awaddr,
            s00_axi_awprot  => s00_axi_awprot,
            s00_axi_awvalid => s00_axi_awvalid,
            s00_axi_awready => s00_axi_awready,
            s00_axi_wdata   => s00_axi_wdata,
            s00_axi_wstrb   => s00_axi_wstrb,
            s00_axi_wvalid  => s00_axi_wvalid,
            s00_axi_wready  => s00_axi_wready,
            s00_axi_bresp   => s00_axi_bresp,
            s00_axi_bvalid  => s00_axi_bvalid,
            s00_axi_bready  => s00_axi_bready,
            s00_axi_araddr  => s00_axi_araddr,
            s00_axi_arprot  => s00_axi_arprot,
            s00_axi_arvalid => s00_axi_arvalid,
            s00_axi_arready => s00_axi_arready,
            s00_axi_rdata   => s00_axi_rdata,
            s00_axi_rresp   => s00_axi_rresp,
            s00_axi_rvalid  => s00_axi_rvalid,
            s00_axi_rready  => s00_axi_rready
        );

    s00_axi_aclk <= not s00_axi_aclk after s00_axi_aclk_period / 2;

    waveform_proc : process
        variable bmu_addr        : t_grid_addr;
        variable input_count     : integer;
        file dump_file           : text;
        variable seed1           : integer := 1;
        variable seed2           : integer := 1;
        variable COMP_COUNT      : natural;
        variable real_value      : real;
        variable vec             : vec_t;
        variable real_vec        : real_vec_t;
        variable float32_value   : float32;
        variable last_grid_state : t_vec_dim3;
        file f_init_vector       : text;
        variable unit_addr       : t_grid_addr;
        variable row             : line;

        ---------------------------------------------------------------------------------
        -- Write a value in an AXI register
        ---------------------------------------------------------------------------------
        procedure write_reg(
            reg_offset : integer;
            wr_data    : std_logic_vector((C_S00_AXI_DATA_WIDTH - 1) downto 0)
        ) is
        begin

            s00_axi_awaddr  <= std_logic_vector(to_unsigned(reg_offset, C_S00_AXI_ADDR_WIDTH));
            s00_axi_awvalid <= '1';
            s00_axi_wdata   <= wr_data;
            s00_axi_wvalid  <= '1';
            s00_axi_wstrb   <= (others => '1');
            if s00_axi_awready = '0' then
                wait until s00_axi_awready = '1';
            end if;
            if s00_axi_wready = '0' then
                wait until s00_axi_wready = '1';
            end if;
            wait for s00_axi_aclk_period;
            s00_axi_wvalid  <= '0';
            s00_axi_awvalid <= '0';

            if s00_axi_bvalid = '0' then
                wait until s00_axi_bvalid = '1';
            end if;
            wait for s00_axi_aclk_period;
            s00_axi_bready <= '1';
            wait for s00_axi_aclk_period;
            s00_axi_bready <= '0';
            wait for s00_axi_aclk_period;

        end procedure write_reg;

        ---------------------------------------------------------------------------------
        -- Read a value from an AXI register
        ---------------------------------------------------------------------------------
        procedure read_reg(
            reg_offset     : in integer;
            signal rd_data : out std_logic_vector((C_S00_AXI_DATA_WIDTH - 1) downto 0)
        ) is
        begin

            s00_axi_araddr  <= std_logic_vector(to_unsigned(reg_offset, C_S00_AXI_ADDR_WIDTH));
            s00_axi_arvalid <= '1';
            if s00_axi_arready = '0' then
                wait until s00_axi_arready = '1';
            end if;
            if s00_axi_rvalid = '0' then
                wait until s00_axi_rvalid = '1';
            end if;
            wait for s00_axi_aclk_period;
            s00_axi_arvalid <= '0';
            s00_axi_rready  <= '1';
            rd_data         <= s00_axi_rdata;
            wait for s00_axi_aclk_period;
            s00_axi_rready <= '0';
            wait for s00_axi_aclk_period;

        end procedure read_reg;

        -----------------------------------------------------------------------------

        procedure wait_grid_ready is
        begin
            loop
                read_reg(STATUS_REG_OFFSET, rd_data);
                if rd_data(0) = '1' then
                    exit;
                end if;
            end loop;
        end procedure wait_grid_ready;

        procedure set_addr_select_reg(addr : t_grid_addr) is
            variable addr_s                    : std_logic_vector(31 downto 0);
        begin
            --logger.log_note("Write address select register: " & to_string(addr));
            addr_s := "00000000" & std_logic_vector(to_unsigned(addr.x, 8)) & std_logic_vector(to_unsigned(addr.y, 8)) & std_logic_vector(to_unsigned(addr.z, 8));
            write_reg(ADDR_SELECT_REG_OFFSET, addr_s);
        end procedure set_addr_select_reg;

        procedure get_bmu_addr(addr : out t_grid_addr)is
        begin
            --logger.log_note("Write address select register: " & to_string(addr));
            read_reg(ADDR_SELECT_REG_OFFSET, rd_data);
            addr.x := to_integer(unsigned(rd_data(23 downto 16)));
            addr.y := to_integer(unsigned(rd_data(15 downto 8)));
            addr.z := to_integer(unsigned(rd_data(7 downto 0)));
        end procedure get_bmu_addr;

        procedure print_vec_reg is
            variable l_vec : vec_t;
        begin
            for I in 0 to COMP_COUNT - 1 loop
                read_reg(VECTOR_REG_BASE + (I * 4), rd_data);
                float32_value := float32(rd_data);
                l_vec(I)      := to_scalar_t(float32_value);
            end loop;
            logger.log_note("Read component register: " & to_string(l_vec));
        end procedure print_vec_reg;

        -- Assumes that it is called right after a BMU election and 
        -- that address reg has already been updated by HDL with
        -- the correct BMU address
        procedure print_bmu_weight is
            variable l_vec : vec_t;
        begin
            for I in 0 to COMP_COUNT - 1 loop
                read_reg(VECTOR_REG_BASE + (I * 4), rd_data);
                float32_value := float32(rd_data);
                l_vec(I)      := to_scalar_t(float32_value);
            end loop;
            logger.log_note("BMU weight after update: " & to_string(l_vec));
        end procedure print_bmu_weight;

        procedure save_grid_weights is
            -- last_grid_state : t_vec_dim3
            variable l_addr : t_grid_addr;
            variable l_vec  : vec_t;
        begin
            for x_idx in 0 to (c_GRID_X_DIM - 1) loop
                for y_idx in 0 to (c_GRID_Y_DIM - 1) loop
                    for z_idx in 0 to (c_GRID_Z_DIM - 1) loop
                        l_addr := (x => x_idx, y => y_idx, z => z_idx);
                        set_addr_select_reg(l_addr);
                        for I in 0 to COMP_COUNT - 1 loop
                            read_reg(VECTOR_REG_BASE + (I * 4), rd_data);
                            float32_value                           := float32(rd_data);
                            last_grid_state(x_idx)(y_idx)(z_idx)(I) := to_scalar_t(float32_value);
                        end loop;
                    end loop;
                end loop;
            end loop;
        end procedure save_grid_weights;

        procedure print_grid_vec is
            variable l_addr : t_grid_addr;
            variable l_vec  : vec_t;
        begin
            for x_idx in 0 to (c_GRID_X_DIM - 1) loop
                for y_idx in 0 to (c_GRID_Y_DIM - 1) loop
                    for z_idx in 0 to (c_GRID_Z_DIM - 1) loop
                        l_addr := (x => x_idx, y => y_idx, z => z_idx);
                        set_addr_select_reg(l_addr);
                        for I in 0 to COMP_COUNT - 1 loop
                            read_reg(VECTOR_REG_BASE + (I * 4), rd_data);
                            float32_value := float32(rd_data);
                            l_vec(I)      := to_scalar_t(float32_value);
                        end loop;
                        logger.log_note("Read component register " & to_string(l_addr) & " : " & to_string(l_vec));
                    end loop;
                end loop;
            end loop;
        end procedure print_grid_vec;

        procedure set_grid_random_weight is
            variable l_addr : t_grid_addr;
            variable l_vec  : real_vec_t;
        begin
            for x_idx in 0 to (c_GRID_X_DIM - 1) loop
                for y_idx in 0 to (c_GRID_Y_DIM - 1) loop
                    for z_idx in 0 to (c_GRID_Z_DIM - 1) loop
                        l_addr := (x => x_idx, y => y_idx, z => z_idx);
                        set_addr_select_reg(l_addr);
                        randomize_vect(seed1, seed2, l_vec);
                        for I in 0 to COMP_COUNT - 1 loop
                            float32_value := to_float(l_vec(I));
                            write_reg(VECTOR_REG_BASE + (I * 4), std_logic_vector(float32_value));
                        end loop;
                        logger.log_note("Write vec register " & to_string(l_addr) & " : " & to_string(l_vec));
                        write_reg(CTRL_INFO_REG_OFFSET, X"00000008");
                    end loop;
                end loop;
            end loop;
        end procedure set_grid_random_weight;

        --* Dump an input value to a file
        procedure dump_input(
            file f     : text;
            new_in     : real_vec_t;
            iter_count : integer
        ) is
            variable tmp_line : line;
        begin
            write(tmp_line, string'(integer'image(iter_count)));
            write(tmp_line, string'(" : input : (0, 0, 0) : "));
            write(tmp_line, string'(to_string(new_in)));
            writeline(f, tmp_line);
        end;

        -- Generate next input with a 0.5 probability to be in 0.0-0.5 (on all axis),
        -- and 0.5 probability to be in 0.5-1.0 (on all axis).
        procedure get_new_input(
            variable seed1, seed2 : inout integer;
            variable vec          : out real_vec_t
        ) is
            variable rand : real;
            variable pop  : real;
        begin
            uniform(seed1, seed2, pop);
            for i in 0 to vec'length - 1 loop
                uniform(seed1, seed2, rand); -- generate random number
                if pop < 0.5 then
                    vec(i) := rand * 0.5;
                else
                    vec(i) := rand * 0.5 + 0.5;
                end if;
            end loop;
        end;

        procedure drive_input(
            variable input_count : in integer;
            new_in               : in real_vec_t
        ) is

        begin
            wait_grid_ready;
            for I in 0 to COMP_COUNT - 1 loop
                float32_value := to_float(new_in(I));
                write_reg(VECTOR_REG_BASE + (I * 4), std_logic_vector(float32_value));
            end loop;
            wait_grid_ready;
            logger.log_note("Driving new input : " & to_string(new_in));
            dump_input(dump_file, new_in, input_count);
            write_reg(CTRL_INFO_REG_OFFSET, X"00000010");
        end procedure drive_input;

        procedure drive_random_input(variable input_count : in integer) is
            variable new_in                                   : real_vec_t;
        begin
            wait_grid_ready;
            --randomize_vect(seed1, seed2, new_in);
            get_new_input(seed1, seed2, new_in);
            for I in 0 to COMP_COUNT - 1 loop
                float32_value := to_float(new_in(I));
                write_reg(VECTOR_REG_BASE + (I * 4), std_logic_vector(float32_value));
            end loop;
            wait_grid_ready;
            logger.log_note("Driving new input : " & to_string(new_in));
            dump_input(dump_file, new_in, input_count);
            write_reg(CTRL_INFO_REG_OFFSET, X"00000010");

        end procedure drive_random_input;

        ----------------------------------------------------------------------------

        --* Write dump header to a file
        procedure dump_header(
            file f : text
        ) is
            variable tmp_line : line;
            -- Work around for vivado bug: real'image doesn't produces
            -- correct output when used with constants
            variable alpha : real := c_ALPHA;
            variable eta   : real := c_ETA;
        begin
            write(tmp_line, string'("# grid size: (" & integer'image(c_GRID_X_DIM) & ", "));
            write(tmp_line, string'(integer'image(c_GRID_Y_DIM) & ", "));
            write(tmp_line, string'(integer'image(c_GRID_Z_DIM) & ")"));
            writeline(f, tmp_line);

            write(tmp_line, "# alpha: " & real'image(alpha));
            writeline(f, tmp_line);

            write(tmp_line, "# eta: " & real'image(eta));
            writeline(f, tmp_line);

            write(tmp_line, string'("# input count: " & integer'image(c_TEST_INPUT_COUNT)));
            writeline(f, tmp_line);

            write(tmp_line, string'("# source: tb_" & c_DUT_NAME & ".vhd"));
            writeline(f, tmp_line);

            write(tmp_line, string'("# distribution: vhdl uniform + 2 offsets"));
            writeline(f, tmp_line);

            write(tmp_line, string'("# init state: vhdl uniform"));
            writeline(f, tmp_line);
        end;

        --* Dump grid address and weights of a unit to a provided file
        procedure dump_unit_weights(
            file f     : text;
            unit_addr  : t_grid_addr;
            iter_count : integer
        ) is
            variable tmp_line : line;
            variable l_vec    : vec_t;
        begin
            set_addr_select_reg(unit_addr);
            for I in 0 to COMP_COUNT - 1 loop
                read_reg(VECTOR_REG_BASE + (I * 4), rd_data);
                float32_value := float32(rd_data);
                l_vec(I)      := to_scalar_t(float32_value);
            end loop;
            write(tmp_line, string'(integer'image(iter_count)));
            write(tmp_line, string'(" : unit  : "));
            write(tmp_line, string'(to_string(unit_addr)));
            write(tmp_line, string'(" : "));
            write(tmp_line, string'(to_string(l_vec)));
            writeline(f, tmp_line);
        end dump_unit_weights;

        --* Dump a BMU weight to a file
        procedure dump_bmu_weights(
            file f     : text;
            bmu_addr   : t_grid_addr;
            iter_count : integer
        ) is
            variable tmp_line : line;
        begin
            write(tmp_line, string'(integer'image(iter_count)));
            write(tmp_line, string'(" : bmu   : "));
            write(tmp_line, string'(to_string(bmu_addr)));
            write(tmp_line, string'(" : "));
            write(tmp_line, string'(to_string(last_grid_state(bmu_addr.x)(bmu_addr.y)(bmu_addr.z))));
            writeline(f, tmp_line);
        end;

        --* Dump the full grid weights to a file
        procedure dump_grid_weights(
            file f     : text;
            iter_count : integer
        ) is
            variable tmp_line : line;
        begin
            for x in 0 to (c_GRID_X_DIM - 1) loop
                for y in 0 to (c_GRID_Y_DIM - 1) loop
                    for z in 0 to (c_GRID_Z_DIM - 1) loop
                        dump_unit_weights(f, (x => x, y => y, z => z), iter_count);
                    end loop;
                end loop;
            end loop;
        end;

        -- Open a dump file
        procedure open_dump_file(
            file f   : text;
            filename : in string
        ) is
        begin
            file_open(f, filename, WRITE_MODE);
        end;

        -- Close a dump file
        procedure close_dump_file(
            file f : text
        ) is
        begin
            file_close(f);
        end;

        procedure init_grid_weight_with_addr is
        begin
            wait_grid_ready;
            logger.log_note("Initial grid weight:");
            print_grid_vec;
            logger.log_note("Weight initialization:");

            for x_idx in 0 to (c_GRID_X_DIM - 1) loop
                for y_idx in 0 to (c_GRID_Y_DIM - 1) loop
                    for z_idx in 0 to (c_GRID_Z_DIM - 1) loop
                        set_addr_select_reg((x => x_idx, y => y_idx, z => z_idx));
                        vec(0) := to_scalar_t(real(x_idx));
                        vec(1) := to_scalar_t(real(y_idx));
                        vec(2) := to_scalar_t(real(z_idx));
                        for I in 0 to COMP_COUNT - 1 loop
                            float32_value := to_float(to_real(vec(I))/10.0);
                            write_reg(VECTOR_REG_BASE + (I * 4), std_logic_vector(float32_value));
                        end loop;
                        logger.log_note("Write vec register: " & to_string(vec));
                        write_reg(CTRL_INFO_REG_OFFSET, X"00000008");
                    end loop;
                end loop;
            end loop;

            logger.log_note("Grid weight after initialization:");
            print_grid_vec;
        end;

        ----------------------------------------------------------------------------

    begin

        -- Open log file in the simulation folder
        logger.set_log_file("tb_report.log");

        -- Open HTML report in the simulation folder
        logger.set_html_report_file("tb_" & c_DUT_NAME & "_report.html",
        "Testbench report: " & "tb_" & c_DUT_NAME,
        "");

        logger.set_log_file_time(true);

        logger.log_note("Clock period: " & time'image(PCLK_PERIOD));
        logger.log_note("Scalar representation: wE: " & integer'image(wE) & ", wF: " & integer'image(wF));
        logger.log_note("Vector component count: C_VEC_COMP_COUNT: " & integer'image(C_VEC_COMP_COUNT));
        logger.log_note("Epsilon: " & to_string(to_scalar_t(c_EPSILON)));

        if (c_GRID_X_DIM /= 5) or (c_GRID_Y_DIM /= 5) or (c_GRID_Z_DIM /= 5) then
            logger.log_failure("This test is intended to be run on a 5x5x5 grid!");
            wait until rising_edge(s00_axi_aclk);
            stop(0);
            wait;
        end if;

        open_dump_file(dump_file, c_DUMPS_PATH & "csom_data.txt");
        dump_header(dump_file);

        ------------------------------------------------------------------------

        -- hold reset
        s00_axi_aresetn <= '0';
        wait for s00_axi_aclk_period * 10;
        s00_axi_aresetn <= '1';
        wait for s00_axi_aclk_period * 20;

        ------------------------------------------------------------------------

        read_reg(INFO_REG_OFFSET, rd_data);
        --logger.log_note("Read info register: " & to_hex_string(rd_data));
        COMP_COUNT := to_integer(unsigned(rd_data(15 downto 0)));
        wait for s00_axi_aclk_period * 40;

        read_reg(STATUS_REG_OFFSET, rd_data);
        --logger.log_note("Read status register: " & to_hex_string(rd_data));
        wait for s00_axi_aclk_period * 40;

        read_reg(CTRL_INFO_REG_OFFSET, rd_data);
        --logger.log_note("Read control/info register: " & to_hex_string(rd_data));
        wait for s00_axi_aclk_period * 40;

        read_reg(GRID_SIZE_REG_OFFSET, rd_data);
        --logger.log_note("Read grid size register: " & to_hex_string(rd_data));
        wait for s00_axi_aclk_period * 40;

        ------------------------------------------------------------------------
        if true then
            real_value    := c_ALPHA;
            float32_value := to_float(real_value);
            logger.log_note("Write alpha register: " & real'image(real_value));
            write_reg(ALPHA_REG_OFFSET, std_logic_vector(float32_value));
            wait for s00_axi_aclk_period * 40;

            real_value    := - 1.0 * (sqrt(real(c_VEC_COMP_COUNT))/c_ETA);
            float32_value := to_float(real_value);
            logger.log_note("Write eta prime register: " & real'image(real_value));
            write_reg(ETA_PRIME_REG_OFFSET, std_logic_vector(float32_value));
            wait for s00_axi_aclk_period * 40;

            logger.log_note("Write addr select register");
            write_reg(ADDR_SELECT_REG_OFFSET, X"00000102");
            wait for s00_axi_aclk_period * 40;

            read_reg(ALPHA_REG_OFFSET, rd_data);
            float32_value := float32(rd_data);
            real_value    := to_real(float32_value);
            logger.log_note("Read alpha register: " & real'image(real_value));
            wait for s00_axi_aclk_period * 40;

            read_reg(ETA_PRIME_REG_OFFSET, rd_data);
            float32_value := float32(rd_data);
            real_value    := to_real(float32_value);
            logger.log_note("Read eta register: " & real'image(real_value));
            wait for s00_axi_aclk_period * 40;

            read_reg(ADDR_SELECT_REG_OFFSET, rd_data);
            --logger.log_note("Read addr select register: " & to_hex_string(rd_data));
            wait for s00_axi_aclk_period * 40;

        end if;

        ------------------------------------------------------------------------

        if true then
            wait_grid_ready;
            logger.log_warning("Test of input distance computation / BMU election");
            input_count := 0;
            loop
                ----------------------------------------------------------------
                -- Get a new input
                logger.log_warning("Input n: " & integer'image(input_count));
                init_grid_weight_with_addr;
                -- Always dump initial weights for full grid
                dump_grid_weights(dump_file, input_count);
                save_grid_weights;
                --drive_random_input(input_count);
                drive_input(input_count, (0.24, 0.24, 0.24));
                wait_grid_ready;
                get_bmu_addr(bmu_addr);
                logger.log_note("BMU : " & to_string(bmu_addr));
                logger.log_note("BMU weight before update: " & to_string(last_grid_state(bmu_addr.x)(bmu_addr.y)(bmu_addr.z)));
                dump_bmu_weights(dump_file, bmu_addr, input_count);
                print_bmu_weight;
                save_grid_weights;
                --
                input_count := input_count + 1;
                if input_count = 6 then
                    exit;
                end if;
                -- If enabled, dump weight at each iteration
                dump_grid_weights(dump_file, input_count);
                logger.log_warning("Grid state");
                print_grid_vec;
                ----------------------------------------------------------------
            end loop;

            dump_grid_weights(dump_file, input_count);
        end if;

        ------------------------------------------------------------------------

        logger.log_note("Testbench ending, emptying pipeline...");
        for I in 1 to 500 loop
            wait until rising_edge(s00_axi_aclk);
        end loop;
        logger.final_report;
        logger.log_note("Testbench end");
        wait until rising_edge(s00_axi_aclk);
        stop(0);
        wait;

    end process waveform_proc;

end behavioral;