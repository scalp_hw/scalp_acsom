----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Quentin Berthet <quentin.berthet@hesge.ch>
--
-- Module Name: csom_grid_axi_wrapper - arch
-- Target Device: hepia-cores.ch:scalp_node:part0:0.1 xc7z015clg485-2
-- Tool version: 2020.2
-- Description: AXI4-Lite registers for csom_grid_axi_wrapper
--
-- Last update: 2021/11/16 13:30:06
--
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.util_pkg.all;
use work.csom_pkg.all;

entity csom_grid_axi_wrapper_S00_AXI is
    generic (
        -- Users to add parameters here

        -- User parameters ends

        -- Width of S_AXI data bus
        C_S_AXI_DATA_WIDTH : integer := 32;
        -- Width of S_AXI address bus
        C_S_AXI_ADDR_WIDTH : integer := 12
    );
    port (
        -- Users to add ports here

        grid_ready_i     : in std_logic;    -- Register Status
        x_valid_o        : out std_logic;   -- Register Control
        m_valid_o        : out std_logic;   -- Register Control
        init_weight_o    : out std_logic;   -- Register Control
        addr_select_o    : out t_grid_addr; -- Register Unit Grid Address select
        alpha_o          : out scalar_t;    -- Register Alpha
        eta_prime_o      : out scalar_t;    -- Register Eta prime
        vec_i            : in vec_t;        -- Registers Vector
        vec_o            : out vec_t;       -- Registers Vector
        bmu_addr_i       : in t_grid_addr;
        bmu_addr_valid_i : in std_logic;

        -- User ports ends

        -- Global Clock Signal
        S_AXI_ACLK : in std_logic;
        -- Global Reset Signal. This Signal is Active LOW
        S_AXI_ARESETN : in std_logic;
        -- Write address (issued by master, acceped by Slave)
        S_AXI_AWADDR : in std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
        -- Write channel Protection type. This signal indicates the
        -- privilege and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        S_AXI_AWPROT : in std_logic_vector(2 downto 0);
        -- Write address valid. This signal indicates that the master signaling
        -- valid write address and control information.
        S_AXI_AWVALID : in std_logic;
        -- Write address ready. This signal indicates that the slave is ready
        -- to accept an address and associated control signals.
        S_AXI_AWREADY : out std_logic;
        -- Write data (issued by master, acceped by Slave)
        S_AXI_WDATA : in std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
        -- Write strobes. This signal indicates which byte lanes hold
        -- valid data. There is one write strobe bit for each eight
        -- bits of the write data bus.
        S_AXI_WSTRB : in std_logic_vector((C_S_AXI_DATA_WIDTH/8) - 1 downto 0);
        -- Write valid. This signal indicates that valid write
        -- data and strobes are available.
        S_AXI_WVALID : in std_logic;
        -- Write ready. This signal indicates that the slave
        -- can accept the write data.
        S_AXI_WREADY : out std_logic;
        -- Write response. This signal indicates the status
        -- of the write transaction.
        S_AXI_BRESP : out std_logic_vector(1 downto 0);
        -- Write response valid. This signal indicates that the channel
        -- is signaling a valid write response.
        S_AXI_BVALID : out std_logic;
        -- Response ready. This signal indicates that the master
        -- can accept a write response.
        S_AXI_BREADY : in std_logic;
        -- Read address (issued by master, acceped by Slave)
        S_AXI_ARADDR : in std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether the
        -- transaction is a data access or an instruction access.
        S_AXI_ARPROT : in std_logic_vector(2 downto 0);
        -- Read address valid. This signal indicates that the channel
        -- is signaling valid read address and control information.
        S_AXI_ARVALID : in std_logic;
        -- Read address ready. This signal indicates that the slave is
        -- ready to accept an address and associated control signals.
        S_AXI_ARREADY : out std_logic;
        -- Read data (issued by slave)
        S_AXI_RDATA : out std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
        -- Read response. This signal indicates the status of the
        -- read transfer.
        S_AXI_RRESP : out std_logic_vector(1 downto 0);
        -- Read valid. This signal indicates that the channel is
        -- signaling the required read data.
        S_AXI_RVALID : out std_logic;
        -- Read ready. This signal indicates that the master can
        -- accept the read data and response information.
        S_AXI_RREADY : in std_logic
    );
end csom_grid_axi_wrapper_S00_AXI;

architecture arch of csom_grid_axi_wrapper_S00_AXI is

    component InputIEEE is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(31 downto 0);
            R   : out std_logic_vector(wE + wF + 2 downto 0)
        );
    end component;

    component OutputIEEE is
        port (
            clk : in std_logic;
            rst : in std_logic;
            X   : in std_logic_vector(wE + wF + 2 downto 0);
            R   : out std_logic_vector(31 downto 0)
        );
    end component;

    -- AXI4LITE signals
    signal axi_awaddr  : std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
    signal axi_awready : std_logic;
    signal axi_wready  : std_logic;
    signal axi_bresp   : std_logic_vector(1 downto 0);
    signal axi_bvalid  : std_logic;
    signal axi_araddr  : std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
    signal axi_arready : std_logic;
    signal axi_rdata   : std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    signal axi_rresp   : std_logic_vector(1 downto 0);
    signal axi_rvalid  : std_logic;

    -- Example-specific design signals
    -- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
    -- ADDR_LSB is used for addressing 32/64 bit registers/memories
    -- ADDR_LSB = 2 for 32 bits (n downto 2)
    -- ADDR_LSB = 3 for 64 bits (n downto 3)
    constant ADDR_LSB : integer := (C_S_AXI_DATA_WIDTH/32) + 1;

    -- We have 8 fixed register, then a variable count depending on vector size
    constant FIX_OPT_MEM_ADDR_BITS : integer := 3 - 1;
    constant OPT_MEM_ADDR_BITS     : integer := FIX_OPT_MEM_ADDR_BITS + bits_width(C_VEC_COMP_COUNT);

    ------------------------------------------------
    ---- Convertion FP <-> IEE754
    --------------------------------------------------

    -- From IEEE754 to FP:
    signal S_AXI_WDATA_as_FP : scalar_t;

    -- From FP to IEEE754:
    signal to_OutputIEEE   : scalar_t;
    signal from_OutputIEEE : std_logic_vector(32 - 1 downto 0);

    ------------------------------------------------
    ---- Signals for user logic register space example
    --------------------------------------------------
    ---- Number of Slave Registers:
    --signal slv_reg0 : std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    --signal slv_reg1 : std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    signal addr_select_reg : std_logic_vector(23 downto 0);
    signal bmu_addr_s      : std_logic_vector(23 downto 0);

    signal alpha_reg     : scalar_t;
    signal eta_prime_reg : scalar_t;
    signal vec_reg       : vec_t;

    signal slv_reg_rden : std_logic;
    signal slv_reg_wren : std_logic;

    signal reg_data_out : std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    signal byte_index   : integer;

begin

    ----------------------------------------------------------------

    b_fp_io    : block
        signal rst : std_logic;
        signal clk : std_logic;
    begin

        clk <= S_AXI_ACLK;
        rst <= not S_AXI_ARESETN;

        i_InputIEEE : InputIEEE
        port map(
            clk => clk,
            rst => rst,
            X   => S_AXI_WDATA,
            R   => S_AXI_WDATA_as_FP
        );

        i_OutputIEEE : OutputIEEE
        port map(
            clk => clk,
            rst => rst,
            X   => to_OutputIEEE,
            R   => from_OutputIEEE
        );

    end block b_fp_io;
    ----------------------------------------------------------------

    alpha_o     <= alpha_reg;
    eta_prime_o <= eta_prime_reg;
    vec_o       <= vec_reg;

    addr_select_o <= (
        x => to_integer(unsigned(addr_select_reg(23 downto 16))),
        y => to_integer(unsigned(addr_select_reg(15 downto 8))),
        z => to_integer(unsigned(addr_select_reg(7 downto 0)))
        );

    bmu_addr_s <=
        std_logic_vector(to_unsigned(bmu_addr_i.x, 8)) &
        std_logic_vector(to_unsigned(bmu_addr_i.y, 8)) &
        std_logic_vector(to_unsigned(bmu_addr_i.z, 8));

    ----------------------------------------------------------------

    -- I/O Connections assignments

    S_AXI_AWREADY <= axi_awready;
    S_AXI_WREADY  <= axi_wready;
    S_AXI_BRESP   <= axi_bresp;
    S_AXI_BVALID  <= axi_bvalid;
    S_AXI_ARREADY <= axi_arready;
    S_AXI_RDATA   <= axi_rdata;
    S_AXI_RRESP   <= axi_rresp;
    S_AXI_RVALID  <= axi_rvalid;
    -- Implement axi_awready generation
    -- axi_awready is asserted for one S_AXI_ACLK clock cycle when both
    -- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
    -- de-asserted when reset is low.

    process (S_AXI_ACLK)
    begin
        if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
                axi_awready <= '0';
            else
                if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1') then
                    -- slave is ready to accept write address when
                    -- there is a valid write address and write data
                    -- on the write address and data bus. This design
                    -- expects no outstanding transactions.
                    axi_awready <= '1';
                else
                    axi_awready <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Implement axi_awaddr latching
    -- This process is used to latch the address when both
    -- S_AXI_AWVALID and S_AXI_WVALID are valid.

    process (S_AXI_ACLK)
    begin
        if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
                axi_awaddr <= (others => '0');
            else
                if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1') then
                    -- Write Address latching
                    axi_awaddr <= S_AXI_AWADDR;
                end if;
            end if;
        end if;
    end process;

    -- Implement axi_wready generation
    -- axi_wready is asserted for one S_AXI_ACLK clock cycle when both
    -- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is
    -- de-asserted when reset is low.

    process (S_AXI_ACLK)
    begin
        if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
                axi_wready <= '0';
            else
                if (axi_wready = '0' and S_AXI_WVALID = '1' and S_AXI_AWVALID = '1') then
                    -- slave is ready to accept write data when
                    -- there is a valid write address and write data
                    -- on the write address and data bus. This design
                    -- expects no outstanding transactions.
                    axi_wready <= '1';
                else
                    axi_wready <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Implement memory mapped register select and write logic generation
    -- The write data is accepted and written to memory mapped registers when
    -- axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
    -- select byte enables of slave registers while writing.
    -- These registers are cleared when reset (active low) is applied.
    -- Slave register write enable is asserted when valid address and data are available
    -- and the slave is ready to accept the write address and write data.
    slv_reg_wren <= axi_wready and S_AXI_WVALID and axi_awready and S_AXI_AWVALID;

    ----------------------------------------------------------------
    -- Registers mapping
    ----------------------------------------------------------------
    -- slave reg. 0 : Info
    --  - bits [15:0] : C_VEC_COMP_COUNT
    --  - bits [23:16] : wF
    --  - bits [31:24] : wE
    ----------------------------------------------------------------
    -- slave reg. 1 : Status
    --  - bit  [0] : grid ready
    --  - bits [31:1] : reserved
    ----------------------------------------------------------------
    -- slave reg. 2 : Control
    --  - bit  [0] : randomly initialize weights
    --  - bit  [1:2] : reserved
    --  - bit  [3] : m valid
    --  - bit  [4] : x valid
    --  - bits [31:5] : reserved
    ----------------------------------------------------------------
    -- slave reg. 3 : Learning rate Alpha
    --  - bits [31:0] : IEEE754 float alpha
    ----------------------------------------------------------------
    -- slave reg. 4 : Learning rate Eta prime
    --  - bits [31:0] : IEEE754 float Eta prime
    ----------------------------------------------------------------
    -- slave reg. 5 : Unit address selec
    --  - bits [7:0]   : Unit z
    --  - bits [15:8]  : Unit y
    --  - bits [23:16] : Unit x
    --  - bits [31:24] : reserved
    ----------------------------------------------------------------
    -- slave reg. 6 : Grid size
    --  - bits [7:0]   : Grid z size
    --  - bits [15:8]  : Grid y size
    --  - bits [23:16] : Grid x size
    --  - bits [31:24] : reserved
    ----------------------------------------------------------------
    -- slave reg. 7 : reserved
    --  - bits [31:0] : reserved
    ----------------------------------------------------------------
    -- slave reg. 8 to 8+n-1 : Vector components
    --  - bits [31:0] : IEEE754 float component n
    ----------------------------------------------------------------

    -- Registers write access process
    process (S_AXI_ACLK)
        variable loc_addr  : std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
        variable comp_addr : integer range 0 to C_VEC_COMP_COUNT - 1;
    begin
        if rising_edge(S_AXI_ACLK) then
            addr_select_reg <= addr_select_reg;
            alpha_reg       <= alpha_reg;
            eta_prime_reg   <= eta_prime_reg;
            vec_reg         <= vec_reg;
            init_weight_o   <= '0';
            x_valid_o       <= '0';
            m_valid_o       <= '0';
            if S_AXI_ARESETN = '0' then
                alpha_reg       <= (others => '0');
                eta_prime_reg   <= (others => '0');
                addr_select_reg <= (others => '0');
                vec_reg         <= (others => (others => '0'));
                --slv_reg0 <= (others => '0');
                --slv_reg1 <= (others => '0');
            else
                -- Special BMU address case: if bmu_addr_valid_i is high, register the BMU address
                -- in addr_select_reg
                if bmu_addr_valid_i = '1' then
                    addr_select_reg <= bmu_addr_s;
                end if;

                if (slv_reg_wren = '1') then
                    -- Address decoding for writing registers
                    loc_addr := axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);

                    if axi_awaddr(axi_awaddr'left) = '0' then
                        case loc_addr(FIX_OPT_MEM_ADDR_BITS downto 0) is

                                --
                                -- Data 0 : Info : no write
                                --  - bits [15:0] : C_VEC_COMP_COUNT
                                --  - bits [23:16] : wF
                                --  - bits [31:24] : wE
                                --
                            when b"000" =>
                                null;

                                --
                                -- Data 1 : Status : no write
                                --  - bit  [0] : unit grid ready
                                --  - bits [31:1] : reserved
                                --
                            when b"001" =>
                                null;

                                --
                                -- Data 2 : Control
                                --  - bit  [0] : randomly initialize weights
                                --  - bit  [1] : reserved
                                --  - bit  [2] : reserved
                                --  - bit  [3] : m valid
                                --  - bit  [4] : x valid
                                --  - bits [31:5] : reserved
                                --
                            when b"010" =>
                                init_weight_o <= S_AXI_WDATA(0);
                                m_valid_o     <= S_AXI_WDATA(3);
                                x_valid_o     <= S_AXI_WDATA(4);

                                --
                                -- Data 3 : Learning rate Alpha
                                --  - bits [31:0] : IEEE754 float alpha
                                --
                            when b"011" =>
                                alpha_reg <= S_AXI_WDATA_as_FP;

                                --
                                -- Data 4 : Learning rate Eta prime
                                --  - bits [31:0] : IEEE754 float Eta prime
                                --
                            when b"100" =>
                                eta_prime_reg <= S_AXI_WDATA_as_FP;

                                --
                                -- Data 5 : Unit grid address
                                --  - bits [7:0]   : Unit z coordinate
                                --  - bits [15:8]  : Unit y coordinate
                                --  - bits [23:16] : Unit x coordinate
                                --  - bits [31:24] : reserved
                                --
                            when b"101" =>
                                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8 - 1) - 1 loop
                                    if (S_AXI_WSTRB(byte_index) = '1') then
                                        -- Respective byte enables are asserted as per write strobes
                                        -- slave register 1
                                        addr_select_reg(byte_index * 8 + 7 downto byte_index * 8) <= S_AXI_WDATA(byte_index * 8 + 7 downto byte_index * 8);
                                    end if;
                                end loop;

                                --
                                -- Data 6 : Grid size : no write
                                --
                            when b"110" =>
                                null;

                                --
                                -- Data 7 : Distance : no write
                                --  - bits [31:0] : IEEE754 float distance
                                --
                            when b"111" =>
                                null;

                                --
                            when others =>
                                -- Data 7 to 7+n-1 : Vector components , no write for now (FIXME)
                                --  - bits [31:0] : IEEE754 float component n

                        end case;
                    else
                        comp_addr := to_integer(unsigned(axi_awaddr(OPT_MEM_ADDR_BITS - 1 downto FIX_OPT_MEM_ADDR_BITS))) mod C_VEC_COMP_COUNT;
                        vec_reg(comp_addr) <= S_AXI_WDATA_as_FP;
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- Implement write response logic generation
    -- The write response and response valid signals are asserted by the slave
    -- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.
    -- This marks the acceptance of address and indicates the status of
    -- write transaction.

    process (S_AXI_ACLK)
    begin
        if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
                axi_bvalid <= '0';
                axi_bresp  <= "00"; --need to work more on the responses
            else
                if (axi_awready = '1' and S_AXI_AWVALID = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0') then
                    axi_bvalid <= '1';
                    axi_bresp  <= "00";
                elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then --check if bready is asserted while bvalid is high)
                    axi_bvalid <= '0';                                   -- (there is a possibility that bready is always asserted high)
                end if;
            end if;
        end if;
    end process;

    -- Implement axi_arready generation
    -- axi_arready is asserted for one S_AXI_ACLK clock cycle when
    -- S_AXI_ARVALID is asserted. axi_awready is
    -- de-asserted when reset (active low) is asserted.
    -- The read address is also latched when S_AXI_ARVALID is
    -- asserted. axi_araddr is reset to zero on reset assertion.

    process (S_AXI_ACLK)
    begin
        if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
                axi_arready <= '0';
                axi_araddr  <= (others => '1');
            else
                if (axi_arready = '0' and S_AXI_ARVALID = '1') then
                    -- indicates that the slave has acceped the valid read address
                    axi_arready <= '1';
                    -- Read Address latching
                    axi_araddr <= S_AXI_ARADDR;
                else
                    axi_arready <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Implement axi_arvalid generation
    -- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both
    -- S_AXI_ARVALID and axi_arready are asserted. The slave registers
    -- data are available on the axi_rdata bus at this instance. The
    -- assertion of axi_rvalid marks the validity of read data on the
    -- bus and axi_rresp indicates the status of read transaction.axi_rvalid
    -- is deasserted on reset (active low). axi_rresp and axi_rdata are
    -- cleared to zero on reset (active low).
    process (S_AXI_ACLK)
    begin
        if rising_edge(S_AXI_ACLK) then
            if S_AXI_ARESETN = '0' then
                axi_rvalid <= '0';
                axi_rresp  <= "00";
            else
                if (axi_arready = '1' and S_AXI_ARVALID = '1' and axi_rvalid = '0') then
                    -- Valid read data is available at the read data bus
                    axi_rvalid <= '1';
                    axi_rresp  <= "00"; -- 'OKAY' response
                elsif (axi_rvalid = '1' and S_AXI_RREADY = '1') then
                    -- Read data is accepted by the master
                    axi_rvalid <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Implement memory mapped register select and read logic generation
    -- Slave register read enable is asserted when valid address is available
    -- and the slave is ready to accept the read address.
    slv_reg_rden <= axi_arready and S_AXI_ARVALID and (not axi_rvalid);

    process (all)
        variable loc_addr  : std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
        variable comp_addr : integer range 0 to C_VEC_COMP_COUNT - 1;
    begin
        if S_AXI_ARESETN = '0' then
            to_OutputIEEE <= (others => '0');
            reg_data_out  <= (others => '1');
        else
            -- Address decoding for reading registers
            loc_addr := axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);

            if axi_araddr(axi_araddr'left) = '0' then

                case loc_addr(FIX_OPT_MEM_ADDR_BITS downto 0) is
                    when b"000" =>

                        -- Data 0 : Info
                        --  - bits [15:0] : C_VEC_COMP_COUNT
                        --  - bits [23:16] : wF
                        --  - bits [31:24] : wE
                        to_OutputIEEE <= (others => '0');
                        reg_data_out  <= std_logic_vector(to_unsigned(wE, 8) & to_unsigned(wF, 8) & to_unsigned(C_VEC_COMP_COUNT, 16));
                    when b"001" =>
                        -- 1 : Status
                        --  - bit  [0] : unit ready
                        --  - bits [31:1] : reserved
                        to_OutputIEEE <= (others => '0');
                        reg_data_out  <= (
                            0      => grid_ready_i,
                            others => '0'
                            );
                    when b"010" =>
                        -- Data 2 : Control: write only
                        to_OutputIEEE <= (others => '0');
                        reg_data_out  <= (others => '0');
                    when b"011"              =>
                        -- Data 3 : Learning rate Alpha
                        --  - bits [31:0] : IEEE754 float alpha
                        to_OutputIEEE <= alpha_reg;
                        reg_data_out  <= from_OutputIEEE;
                    when b"100" =>
                        -- Data 4 : Learning rate Eta prime
                        --  - bits [31:0] : IEEE754 float Eta prime
                        to_OutputIEEE <= eta_prime_reg;
                        reg_data_out  <= from_OutputIEEE;
                    when b"101" =>
                        -- Data 5 : Unit address select
                        --  - bits [7:0]   : Unit z coordinate
                        --  - bits [15:8]  : Unit y coordinate
                        --  - bits [23:16] : Unit x coordinate
                        --  - bits [31:24] : reserved
                        to_OutputIEEE <= (others => '0');
                        reg_data_out  <= "00000000" & addr_select_reg;
                    when b"110" =>
                        -- Data 6 : Grid size
                        --  - bits [7:0]   : Grid z size
                        --  - bits [15:8]  : Grid y size
                        --  - bits [23:16] : Grid x size
                        --  - bits [31:24] : reserved
                        to_OutputIEEE <= (others => '0');
                        reg_data_out  <= "00000000" & std_logic_vector(to_unsigned(c_GRID_X_DIM, 8) & to_unsigned(c_GRID_Y_DIM, 8) & to_unsigned(c_GRID_Z_DIM, 8));
                    when others =>
                        -- Data 7 : Reserved
                        to_OutputIEEE <= (others => '0');
                        reg_data_out  <= (others => '0');
                end case;
            else
                -- Data 7 to 7+n-1 : Vector components
                --  - bits [31:0] : IEEE754 float component n
                comp_addr := to_integer(unsigned(axi_araddr(OPT_MEM_ADDR_BITS - 1 downto FIX_OPT_MEM_ADDR_BITS))) mod C_VEC_COMP_COUNT;
                to_OutputIEEE <= vec_i(comp_addr);
                reg_data_out  <= from_OutputIEEE;
            end if;
        end if;
    end process;

    -- Output register or memory read data
    process (S_AXI_ACLK) is
    begin
        if (rising_edge (S_AXI_ACLK)) then
            if (S_AXI_ARESETN = '0') then
                axi_rdata <= (others => '0');
            else
                if (slv_reg_rden = '1') then
                    -- When there is a valid read address (S_AXI_ARVALID) with
                    -- acceptance of read address by the slave (axi_arready),
                    -- output the read dada
                    -- Read address mux
                    axi_rdata <= reg_data_out; -- register read data
                end if;
            end if;
        end if;
    end process;
    -- Add user logic here

    -- User logic ends

end arch;