
# ACSOM HDL implementation source code

## Description

The repository contains the source code of an implementation of the Asychronous Cellular Self-Organizing Maps (ACSOM) algorithm, presented in a paper under review for publication.

**Note:** HDL code have not been updated since moving from CSOM to ACSOM, but all the code contained in this repository is only about ACSOM.

## Folder structure

```
.
├── data
│   ├── run_input
│   │   ├── grid_4x4x2_3
│   │   ├── grid_4x4x3_3
│   │   ├── grid_4x4x4_3
│   │   ├── input_3.txt
│   │   └── valid_3.txt
│   └── run_output
├── dev
│   ├── fpga
│   │   ├── constrs
│   │   │   └── clock.xdc
│   │   ├── ips
│   │   │   ├── hw
│   │   │   │   ├── csom_grid
│   │   │   │   ├── csom_unit
│   │   │   │   ├── euclidean_distance
│   │   │   │   ├── flopoco
│   │   │   │   └── weight_update
│   │   │   └── vivado
│   │   ├── packages
│   │   │   ├── hw
│   │   │   │   ├── csom_pkg
│   │   │   │   ├── fixed_pkg
│   │   │   │   ├── float_pkg
│   │   │   │   ├── logger_pkg
│   │   │   │   ├── tlmvm_pkg
│   │   │   │   └── util_pkg
│   │   │   └── vivado
│   │   └── peripherals
│   │       ├── hw
│   │       │   ├── csom_grid_axi_wrapper
│   │       │   └── csom_unit_axi_wrapper
│   │       └── vivado
│   └── python
└── README.md
```

## Usage

The folder `dev/python` contains the helper scripts used to run the simulations of an ACSOM grid. They are intended to be run from this folder. Vivado must be in the path. Simfloat python package must be installed to use the input and initial position generation script.

* `python3 run_gen_init.py` can be used to generate random initial weight of units in `run_input` folder (the output file used for paper results is already added to this repository).

* `python3 run_gen_input.py` can be used to generate random input populations in `run_input` folder (the output file used for paper results is already added to this repository).

* `python3 run_sim_grid.py` can be used to launch simulation of ACSOM. Output of the test are created in `data/run_output`

* `python3 run_gen_valid.py` can be used to generate random validation dataset in `run_input` folder (the output file used for paper results is already added to this repository).

* `python3 run_valid.py` can be used to launch a python validation to compute the ASE of the run present in `data/run_output`.

Symlinks are used to change between the grid topologies and Flopoco representation. If another configuration needs to be tested, a new Flopoco and/or csom_pkg folder have to be created. A script with to set the corresponding symlink needs to be added as well.